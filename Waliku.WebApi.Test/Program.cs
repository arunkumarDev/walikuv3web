﻿using System;
using System.Threading.Tasks;
using Waliku.Domain.Models.WebModel.RequestModel;
using Waliku.Web.RestClient;

namespace Waliku.WebApi.Test
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.ReadKey();
            IAuthenticationRestService authenticationRestService = new AuthenticationRestService(string.Empty, string.Empty);
            var result = await authenticationRestService.Login(new LoginRequest
            {
                grant_type = "password",
                UserName = "Hendyani",
                Password = "b1234"
            });
            IReasonRestService reasonRestService = new ReasonRestService(result.Access_Token, "en");
            var reasonTypes = await reasonRestService.GetAllReasonType();
            Console.WriteLine(result);
            Console.Read();
        }
    }
}
