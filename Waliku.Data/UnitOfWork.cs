using System;
using System.Data;
using Waliku.Data.ErrorLog;
using Waliku.Data.Repositories;
using Waliku.Data.Repositories.Interface;

namespace Waliku.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        private IAcademicTermRepository _academicTermRepository;
        private IAttendanceRepository _attendanceRepository;
        private IClassRepository _classRepository;
        private IConfigurationRepository _configurationRepository;
        private IFeedbackRepository _feedbackRepository;
        private IFirstAidRepository _firstAidRepository;
        private IHolidayRepository _holidayRepository;
        private IMasterRepository _masterRepository;
        private IPermissionRepository _permissionRepository;
        private IReasonRepository _reasonRepository;
        private IReportRepository _reportRepository;
        private ISchoolRepository _schoolRepository;
        private IStudentRepository _studentRepository;
        private IUserRepository _userRepository;
        private IVisitorHomeRepository _visitorHomeRepository;
        private ICalendarRepository _calendarRepository;
        private IChildAssesmentRepository _childassesmentRepository;
        private ILogRepository _logRepository;

        private bool _disposed;

        public UnitOfWork(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.Connection;
            try
            {
                _connection.Open();
                _transaction = _connection.BeginTransaction();
            }
            catch (Exception ex)
            {
                //do nothing, but log
                ExceptionLogger.LogErrorToFile(ex);
                throw;
            }
        }

        public IAcademicTermRepository AcademicTermRepository => _academicTermRepository ?? (_academicTermRepository = new AcademicTermRepository(_transaction));
        public IAttendanceRepository AttendanceRepository => _attendanceRepository ?? (_attendanceRepository = new AttendanceRepository(_transaction));
        public IClassRepository ClassRepository => _classRepository ?? (_classRepository = new ClassRepository(_transaction));
        public IConfigurationRepository ConfigurationRepository => _configurationRepository ?? (_configurationRepository = new ConfigurationRepository(_transaction));
        public IFeedbackRepository FeedbackRepository => _feedbackRepository ?? (_feedbackRepository = new FeedbackRepository(_transaction));
        public IFirstAidRepository FirstAidRepository => _firstAidRepository ?? (_firstAidRepository = new FirstAidRepository(_transaction));
        public IHolidayRepository HolidayRepository => _holidayRepository ?? (_holidayRepository = new HolidayRepository(_transaction));
        public IMasterRepository MasterRepository => _masterRepository ?? (_masterRepository = new MasterRepository(_transaction));
        public IPermissionRepository PermissionRepository => _permissionRepository ?? (_permissionRepository = new PermissionRepository(_transaction));
        public IReasonRepository ReasonRepository => _reasonRepository ?? (_reasonRepository = new ReasonRepository(_transaction));
        public IReportRepository ReportRepository => _reportRepository ?? (_reportRepository = new ReportRepository(_transaction));
        public ISchoolRepository SchoolRepository => _schoolRepository ?? (_schoolRepository = new SchoolRepository(_transaction));
        public IStudentRepository StudentRepository => _studentRepository ?? (_studentRepository = new StudentRepository(_transaction));
        public IUserRepository UserRepository => _userRepository ?? (_userRepository = new UserRepository(_transaction));
        public IVisitorHomeRepository VisitorHomeRepository => _visitorHomeRepository ?? (_visitorHomeRepository = new VisitorHomeRepository(_transaction));
        public ICalendarRepository CalendarRepository => _calendarRepository ?? (_calendarRepository = new CalendarRepository(_transaction));
        public IChildAssesmentRepository childAssesmentRepository => _childassesmentRepository ?? (_childassesmentRepository = new ChildAssesmentRepository(_transaction));
        public ILogRepository LogRepository => _logRepository ?? (_logRepository = new LogRepository(_transaction));

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = _connection.BeginTransaction();
                resetRepositories();
            }
        }

        private void resetRepositories()
        {
            _attendanceRepository = null;
            _classRepository = null;
            _feedbackRepository = null;
            _firstAidRepository = null;
            _holidayRepository = null;
            _masterRepository = null;
            _reasonRepository = null;
            _schoolRepository = null;
            _studentRepository = null;
            _userRepository = null;
            _visitorHomeRepository = null;
            _calendarRepository = null;
            _childassesmentRepository = null;
            _logRepository = null;
        }

        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }

        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~UnitOfWork()
        {
            dispose(false);
        }
    }
}
