﻿using Autofac;
using System.Reflection;
using Module = Autofac.Module;

namespace Waliku.Data
{
    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ConnectionFactory>().As<IConnectionFactory>();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>();
        }
    }
}
