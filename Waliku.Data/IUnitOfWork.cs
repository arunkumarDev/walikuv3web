using System;
using Waliku.Data.Repositories.Interface;

namespace Waliku.Data
{
    public interface IUnitOfWork : IDisposable
    {
        IAcademicTermRepository AcademicTermRepository { get; }
        IAttendanceRepository AttendanceRepository { get; }
        IClassRepository ClassRepository { get; }
        IConfigurationRepository ConfigurationRepository { get; }
        IFeedbackRepository FeedbackRepository { get; }
        IFirstAidRepository FirstAidRepository { get; }
        IHolidayRepository HolidayRepository { get; }
        IMasterRepository MasterRepository { get; }
        IPermissionRepository PermissionRepository { get; }
        IReasonRepository ReasonRepository { get; }
        IReportRepository ReportRepository { get; }
        ISchoolRepository SchoolRepository { get; }
        IStudentRepository StudentRepository { get; }
        IUserRepository UserRepository { get; }
        IVisitorHomeRepository VisitorHomeRepository { get; }
        ICalendarRepository CalendarRepository { get; }
        IChildAssesmentRepository childAssesmentRepository { get; }
        ILogRepository LogRepository { get; }

        void Commit();
    }
}
