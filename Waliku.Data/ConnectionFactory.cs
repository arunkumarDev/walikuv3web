﻿using System.Configuration;
using System.Data;
using System.Data.Common;

namespace Waliku.Data
{
    public class ConnectionFactory : IConnectionFactory
    {
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["WalikuConnectionString"].ConnectionString;
        public IDbConnection Connection
        {
            get
            {
                var factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                var conn = factory.CreateConnection();
                conn.ConnectionString = connectionString;
                //conn.Open();
                return conn;
            }
        }
    }

    public interface IConnectionFactory
    {
        IDbConnection Connection { get; }
    }
}
