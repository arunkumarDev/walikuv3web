﻿using System;
using System.IO;
using System.Web.Hosting;

namespace Waliku.Data.ErrorLog
{
    /// <summary>  
    /// Summary description for ExceptionLogging  
    /// </summary>  
    public static class ExceptionLogger
    {
        private static String Errormsg, extype, exurl, ErrorLocation, StackTrace, InnerException;

        private static string _env = HostingEnvironment.ApplicationPhysicalPath;

        public static void LogErrorToFile(Exception ex, string apiurl = "", string hostIp = "")
        {
            var line = Environment.NewLine + Environment.NewLine;            

            //ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
            Errormsg = ex.GetType().Name.ToString();
            extype = ex.GetType().ToString();
            exurl = apiurl;
            ErrorLocation = ex.Message.ToString();
            StackTrace = ex.StackTrace;
            if (ex.InnerException != null)
                InnerException = ex.InnerException.Message;

            try
            {
                string filepath = _env + Path.DirectorySeparatorChar.ToString() + "ErrorLogs" + Path.DirectorySeparatorChar.ToString();  //Text File Path
                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }

                filepath = filepath + DateTime.Today.ToString("yy-MM-dd") + ".txt";   //Text File Name
                if (!File.Exists(filepath))
                {
                    File.Create(filepath).Dispose();
                }

                using (StreamWriter sw = File.AppendText(filepath))
                {
                    string error = "Log Written Date:" + " " + DateTime.Now.ToString()
                        + line + "Error Message:" + " " + Errormsg
                        + line + "Exception Type:" + " " + extype
                        + line + "Stack Trace:" + " " + StackTrace
                        + line + "Error Location:" + " " + ErrorLocation
                        + line + "Inner Exception:" + " " + InnerException
                        + line + "Page/API Url:" + " " + exurl
                        + line + "User Host IP:" + " " + hostIp;
                    sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now.ToString() + "-----------------");
                    sw.WriteLine(error);
                    sw.WriteLine("--------------------------------*End*------------------------------------------");
                    sw.WriteLine(line);
                    sw.Flush();
                    sw.Close();
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
        }
    }
}