﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models.ViewModel;
using Waliku.Domain.Models.WebModel;
using Waliku.Domain.Models.WebModel.ViewModel;

namespace Waliku.Data.Repositories
{
    internal class ReportRepository : RepositoryBase, IReportRepository
    {
        public ReportRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public List<ChildDashboardSummary> GetChildDashboardSummary(int ChildID, DateTime selectedDate)
        => Connection.Query<ChildDashboardSummary>(
               "MobileChildDashboardSummary",
               param: new { AttendanceDate = selectedDate, ChildID = ChildID },
               transaction: Transaction,
               commandType: CommandType.StoredProcedure
           ).ToList();


        public List<ReasonSummary> GetChildReasonSummary(int ChildID, DateTime selectedDate, string langId)
         => Connection.Query<ReasonSummary>(
               "MobileChildReasonSummary",
               param: new
               {
                   AttendanceDate = selectedDate,
                   ChildID = ChildID,
                   LangId = langId

               },
               transaction: Transaction,
               commandType: CommandType.StoredProcedure
           ).ToList();

        public IEnumerable<AbsentOrPresentMaster> GetClassPresentsOrAbsentsChildren(string classRoomId, DateTime selectedDate)
         => Connection.Query<AbsentOrPresentMaster>(
                @"SELECT ChildrenMaster.*, UTM.IsPresent as IsPresent 
                  FROM ChildrenMaster 
                    LEFT JOIN AttendanceMaster UTM on ChildrenMaster.ID = UTM.ChildID 
                  WHERE UTM.ClassRoomID = @ClassRoomId
                        AND YEAR(UTM.AttendanceDate) = @Year AND MONTH(UTM.AttendanceDate) = @Month
                        AND DAY(UTM.AttendanceDate) = @Day",
                param: new { ClassRoomId = classRoomId, selectedDate.Year, selectedDate.Month, selectedDate.Day },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReport(int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "TeacherDailyReport",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportForAdmin(int schoolId, int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "AdminDailyReport",
                param: new
                {
                    AttendanceDate = selectedDate,
                    SchoolID = schoolId,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportOnload(string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "TeacherInitialReport",
                param: new { AttendanceDate = selectedDate, Type = selectedType, TeacherId = teacherId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetHolidayList(int classId, string selectedDate, int selectedType)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "GetHolidayList",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummary(int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "StudentAttendaceSummary",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForAdmin(int schoolId, int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "StudentAttendaceSummaryForAdmin",
                param: new
                {
                    AttendanceDate = selectedDate,
                    SchoolID = schoolId,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "StudentAttendaceSummaryForSA",
                param: new
                {
                    AttendanceDate = selectedDate,
                    SchoolID = schoolId,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetMonthlyReasonCount(int classId, string selectedDate, int selectedType, int teacherId, string langId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "MonthlyAbsentReasonSummary",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId,
                    LangID = langId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetMonthlyReasonCount2(string selectedDate)
           => Connection.Query<ChartMaster.PresentAbsetReport>(
                "ReasonCountNonHealth",
                param: new { AttendanceDate = selectedDate },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IDictionary<string, IEnumerable<ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId)
        {
            IDictionary<string, IEnumerable<ChartMaster.PresentAbsetReport>> reportDictionary = new Dictionary<string, IEnumerable<ChartMaster.PresentAbsetReport>>();
            var resultSet = Connection.QueryMultiple(
                "MonthlyAbsentReasonSummaryForSA",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    SchoolId = schoolId,
                    Type = selectedType,
                    TeacherId = teacherId,
                    LangID = langId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
            var top3Reason = resultSet.Read<ChartMaster.PresentAbsetReport>();
            var pieChart = resultSet.Read<ChartMaster.PresentAbsetReport>();
            reportDictionary.Add("Top3Reason", top3Reason);
            reportDictionary.Add("PieChart", pieChart);
            return reportDictionary;
        }

        public IDictionary<string, IEnumerable<ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountNew(int classId, string selectedDate, int selectedType, int teacherId, string langId)
        {
            IDictionary<string, IEnumerable<ChartMaster.PresentAbsetReport>> reportDictionary = new Dictionary<string, IEnumerable<ChartMaster.PresentAbsetReport>>();
            var resultSet = Connection.QueryMultiple(
                "MonthlyAbsentReasonSummary",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId,
                    LangID = langId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
            var top3Reason = resultSet.Read<ChartMaster.PresentAbsetReport>();
            var pieChart = resultSet.Read<ChartMaster.PresentAbsetReport>();
            reportDictionary.Add("Top3Reason", top3Reason);
            reportDictionary.Add("PieChart", pieChart);
            return reportDictionary;
        }

        public IEnumerable<ChartMaster.PresentAbsetReport> GetMonthlyReasonReportRate(int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "MonthlyReasonReportRate",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsent(int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "StudentMonthlyAttendanceSummary",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsentForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<ChartMaster.PresentAbsetReport>(
                "StudentMonthlyAttendanceSummaryForSA",
                param: new
                {
                    AttendanceDate = selectedDate,
                    SchoolID = schoolId,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<VmChildrenReport> GetStudentWisePresentAbsentReport(int classId, string selectedDate, int selectedType, int teacherId)
            => Connection.Query<VmChildrenReport>(
                "StudentAttendaceReport",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public VmYearlyReport GetTeacherYearly(int classId, int teacherId)
            => Connection.Query<VmYearlyReport>(
                "Usp_Rpt_TeacherYearlyReport",
                param: new { ClassID = classId, TeacherID = teacherId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

        public IEnumerable<dynamic> GetYearlyHealthReason(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId)
            => Connection.Query(
                "GetYearlyHealthReason",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId,
                    LangID = langId,
                    SchoolId = schoolId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<dynamic> GetYearlyNonHealthReason(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId)
            => Connection.Query(
                "GetYearlyNonHealthReason",
                param: new
                {
                    AttendanceDate = selectedDate,
                    ClassRoomID = classId,
                    Type = selectedType,
                    TeacherId = teacherId,
                    LangID = langId,
                    SchoolId = schoolId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<YearlyAbsenteesSummary> GetYearlyAbsenteesChartReport(int TeacherID, DateTime AttendDate, int CRoomID)
          => Connection.Query<YearlyAbsenteesSummary>(
              "YearlyChartViewForMobile",
              param: new
              {
                  AttendanceDate = AttendDate,
                  ClassRoomID = CRoomID,
                  TeacherId = TeacherID
              },
              transaction: Transaction,
              commandType: CommandType.StoredProcedure
          );

        public IEnumerable<YearlyTopFiveAbsenteesSummary> GetYearlyTopFiveHealthChartReport(int TeacherID, DateTime AttendDate, int CRoomID, int RType, string LanguageID)
         => Connection.Query<YearlyTopFiveAbsenteesSummary>(
              "YearlyChartViewTop5AbsentForMobile",
              param: new
              {
                  AttendanceDate = AttendDate,
                  ClassRoomID = CRoomID,
                  TeacherId = TeacherID,
                  ReasonType = RType,
                  LangID = LanguageID
              },
              transaction: Transaction,
              commandType: CommandType.StoredProcedure
          );

        public IEnumerable<YearlyChronicAbsenteesSummary> GetYearlyChronicAbsentChartReport(int TeacherID, DateTime AttendDate, int CRoomID)
        => Connection.Query<YearlyChronicAbsenteesSummary>(
              "YearlyChartViewChronicAbsentForMobile",
              param: new
              {
                  AttendanceDate = AttendDate,
                  ClassRoomID = CRoomID,
                  TeacherId = TeacherID
              },
              transaction: Transaction,
              commandType: CommandType.StoredProcedure
          );

        public IEnumerable<ClassDashboardSummary> GetWeeklyChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
        => Connection.Query<ClassDashboardSummary>(
              "WeeklyChartViewForMobile",
              param: new
              {
                  AttendanceDate = AttendanceDate,
                  ClassRoomID = ClassRoomID,
                  Type = Type,
                  TeacherId = TeacherID,
                  LangId = LangID
              },
              transaction: Transaction,
              commandType: CommandType.StoredProcedure
          );

        public IEnumerable<ClassDashboardSummary> GetClassDashboardReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
        => Connection.Query<ClassDashboardSummary>(
              "AdminIndividualClassSummary_Mobile",
              param: new
              {
                  AttendanceDate = AttendanceDate,
                  ClassRoomID = ClassRoomID,
                  Type = Type,
                  TeacherId = TeacherID,
                  LangId = LangID
              },
              transaction: Transaction,
              commandType: CommandType.StoredProcedure
          );
        public IEnumerable<ClassTopReasonsForAbsence> GetClassTopReasonsForAbsence(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
       => Connection.Query<ClassTopReasonsForAbsence>(
             "ClassTopReasonsForAbsence",
             param: new
             {
                 AttendanceDate = AttendanceDate,
                 ClassRoomID = ClassRoomID,
                 Type = Type,
                 TeacherId = TeacherID,
                 LangId = LangID
             },
             transaction: Transaction,
             commandType: CommandType.StoredProcedure
         );

        public IEnumerable<ChildDashboardSummary> GetMonthlyListReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type)
            => Connection.Query<ChildDashboardSummary>(
             "MonthlyListView",
             param: new
             {
                 AttendanceDate = AttendanceDate,
                 ClassRoomID = ClassRoomID,
                 Type = Type,
                 TeacherId = TeacherID
             },
             transaction: Transaction,
             commandType: CommandType.StoredProcedure
         );

        public SummaryData GetMonthlySummaryReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
        {
            var response = Connection.Query<ClassChartReasonsForAbsence>(
                "MonthlySummaryViewForMobile",
                param: new
                {
                    AttendanceDate = AttendanceDate,
                    ClassRoomID = ClassRoomID,
                    Type = Type,
                    TeacherId = TeacherID,
                    LangID = LangID
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

            SummaryData resultData = new SummaryData();

            if (response != null && response.Count() > 0)
            {
                List<ClassChartReasonsForAbsence> lst = response.ToList();

                if (lst.Count > 0)
                {

                    int highriskindex = lst.FindIndex(a => a.Description == "HighRiskAbsent");
                    if (highriskindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[highriskindex]);
                        lst.RemoveAt(highriskindex);
                    }

                    int allindex = lst.FindIndex(a => a.Description == "Overall");
                    if (allindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[allindex]);
                        lst.RemoveAt(allindex);
                    }

                    int healthindex = lst.FindIndex(a => a.Description == "HealthPercent");
                    if (healthindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[healthindex]);
                        lst.RemoveAt(healthindex);
                    }

                    int nonhealthindex = lst.FindIndex(a => a.Description == "NonHealthPercent");
                    if (nonhealthindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[nonhealthindex]);
                        lst.RemoveAt(nonhealthindex);
                    }

                    int unknownindex = lst.FindIndex(a => a.Description == "UnKnownPercent");
                    if (unknownindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[unknownindex]);
                        lst.RemoveAt(unknownindex);
                    }

                    if (lst.Count > 0)
                    {
                        resultData.TopReasonData = lst.OrderByDescending(o => o.ReasonCount).ToList();
                    }
                }
            }
            return resultData;
        }


        public IEnumerable<VmChildrenReport> GetWeeklyListReport(int ClassRoomID, int TeacherID, DateTime AttendanceDate, int Type)
        {
            var response = Connection.Query(
                "WeeklyListReport",
                param: new
                {
                    AttendanceDate = AttendanceDate,
                    ClassRoomID = ClassRoomID,
                    Type = Type,
                    TeacherId = TeacherID
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

            List<VmChildrenReport> lst = new List<VmChildrenReport>();
            var result = new List<VmChildrenReport>();
            if (response != null & response.Count() > 0)
            {
                foreach (var item in response)
                {
                    VmChildrenReport obj = new VmChildrenReport();

                    obj.ID = item.ID;
                    obj.Name = item.Name;
                    obj.AttendanceData.Attendance = item.IsPresent?.ToString() ?? string.Empty;
                    obj.AttendanceData.WeekAttendanceDate = Convert.ToDateTime(item.Date).ToString("yyyy-MM-dd");
                    obj.AttendanceData.AttendanceType = item.AttendanceType?.ToString() ?? string.Empty;
                    obj.Gender = item.Gender.ToString();
                    obj.AttendanceData.ReasonType = item.ReasonType?.ToString() ?? string.Empty;
                    lst.Add(obj);
                }

                result = lst.GroupBy(l1 => l1.ID)
                                .Select(c => new VmChildrenReport
                                {
                                    ID = c.Key,
                                    Name = c.FirstOrDefault().Name,
                                    Gender = c.FirstOrDefault().Gender,
                                    VmAttendanceDataList = c.Select(cn => cn.AttendanceData).ToList()
                                }).ToList();


            }
            return result;
        }

        public SummaryData GetWeeklySummaryReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
        {
            var response = Connection.Query<ClassChartReasonsForAbsence>(
                "WeeklySummaryViewForMobile",
                param: new
                {
                    AttendanceDate = AttendanceDate,
                    ClassRoomID = ClassRoomID,
                    Type = Type,
                    TeacherId = TeacherID,
                    LangID = LangID
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

            SummaryData resultData = new SummaryData();

            if (response != null && response.Count() > 0)
            {
                List<ClassChartReasonsForAbsence> lst = response.ToList();

                if (lst.Count > 0)
                {
                    int boysindex = lst.FindIndex(a => a.Description == "BoysPercent");
                    if (boysindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[boysindex]);
                        lst.RemoveAt(boysindex);
                    }

                    int girlsindex = lst.FindIndex(a => a.Description == "GirlsPercent");
                    if (girlsindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[girlsindex]);
                        lst.RemoveAt(girlsindex);
                    }

                    int allindex = lst.FindIndex(a => a.Description == "OverAllPercent");
                    if (allindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[allindex]);
                        lst.RemoveAt(allindex);
                    }

                    int healthindex = lst.FindIndex(a => a.Description == "HealthPercent");
                    if (healthindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[healthindex]);
                        lst.RemoveAt(healthindex);
                    }

                    int nonhealthindex = lst.FindIndex(a => a.Description == "NonHealthPercent");
                    if (nonhealthindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[nonhealthindex]);
                        lst.RemoveAt(nonhealthindex);
                    }

                    int unknownindex = lst.FindIndex(a => a.Description == "UnKnownPercent");
                    if (unknownindex >= 0)
                    {
                        resultData.ReasonSummaryData.Add(lst[unknownindex]);
                        lst.RemoveAt(unknownindex);
                    }

                    if (lst.Count > 0)
                    {
                        resultData.TopReasonData = lst;
                    }
                }
            }
            return resultData;
        }

        public List<Offline_V2_ReasonSummary> GetChildReasonSummary_Offline(int ChildID, DateTime selectedDate)
       => Connection.Query<Offline_V2_ReasonSummary>(
              "MobileChildReasonSummary_Offline_V2",
              param: new { AttendanceDate = selectedDate, ChildId = ChildID },
              transaction: Transaction,
              commandType: CommandType.StoredProcedure
          ).ToList();
    }
}
