﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Data.Repositories
{
    internal class SchoolRepository : RepositoryBase, ISchoolRepository
    {
        public SchoolRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void DeleteSchoolById(int schoolId) => Connection.Execute(
                "UPDATE SchoolMaster SET IsActive = 0 WHERE ID = @SchoolId",
                param: new { SchoolId = schoolId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<SchoolMaster> GetAllSchool() => Connection.Query<SchoolMaster>(
                "SELECT * FROM SchoolMaster",
                transaction: Transaction
            );

        public IEnumerable<SchoolMaster> GetSchoolListForUser(int userId, string langId) => Connection.Query<SchoolMaster>(
                "GetSchoolListForUser",
                param: new { ID = userId, LangID = langId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public int ManageSchool(WebModel.SchoolMaster schoolMaster)
        {
            var parameters = new DynamicParameters();

            parameters.Add("ID", schoolMaster.ID);
            parameters.Add("SchoolName", schoolMaster.SchoolName);
            parameters.Add("Email", schoolMaster.Email);
            parameters.Add("Image", schoolMaster.Image);
            parameters.Add("City", schoolMaster.City);
            parameters.Add("Address", schoolMaster.Address);
            parameters.Add("Country", schoolMaster.Country);
            parameters.Add("Phone1", schoolMaster.Phone1);
            parameters.Add("Phone2", schoolMaster.Phone2);
            parameters.Add("Note", schoolMaster.Note);
            parameters.Add("GeoLocation", schoolMaster.GeoLocation);
            parameters.Add("IsActive", schoolMaster.IsActive);
            parameters.Add("CreatedBy", schoolMaster.CreatedBy);
            parameters.Add("CreatedOn", schoolMaster.CreatedOn);
            parameters.Add("Note_Indonesian", schoolMaster.Note_Indonesian);
            parameters.Add("@InsertedID", dbType: DbType.Int32, direction: ParameterDirection.Output);

            Connection.Execute(
                "ManageSchool",
                parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
            return parameters.Get<int>("InsertedID");
        }
        #region V3 - Req# 3
        public IEnumerable<SchoolMaster> GetDistrictSchools(string districtName) => Connection.Query<SchoolMaster>(
        "SELECT * FROM SchoolMaster WHERE [District] = @District AND [IsActive] = 1",
        param: new { District = districtName },
        transaction: Transaction
        );
        #endregion
    }
}
