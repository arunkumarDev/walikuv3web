﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models.ViewModel;
using Waliku.Domain.Models.WebModel;
using ClassMaster = Waliku.Domain.Models.ClassMaster;

namespace Waliku.Data.Repositories
{
    internal class ClassRepository : RepositoryBase, IClassRepository
    {
        public ClassRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void DeleteClassById(int classId) => Connection.Execute(
                "UPDATE ClassMaster SET IsActive = 0 WHERE ID = @ClassId",
                param: new { ClassId = classId },
                transaction: Transaction
            );

        public IEnumerable<ClassMaster> GetAllClassBySchool(int schoolId) => Connection.Query<ClassMaster>(
                @"SELECT * FROM ClassMaster WHERE SchoolId = @SchoolID",
                param: new { SchoolID = schoolId },
                transaction: Transaction
            );

        public IEnumerable<Domain.Models.WebModel.ClassMaster> GetAllClasses() => Connection.Query<Domain.Models.WebModel.ClassMaster>(
                @"SELECT cm.ID, cm.ClassName, cm.SchoolId, cm.CreatedBy, 
                    cm.CreatedOn, sm.SchoolName,cm.IsActive 
                  FROM ClassMaster cm 
                    LEFT JOIN  SchoolMaster sm ON cm.SchoolId = sm.ID 
                  WHERE cm.IsActive =1",
                transaction: Transaction
            );

        public IEnumerable<Domain.Models.WebModel.ClassMaster> GetAllClassesForMe(int schoolId, int userId) => Connection.Query<Domain.Models.WebModel.ClassMaster>(
                "Usp_GetClassListByUser",
                param: new {
                    UserID = userId,
                    schoolID = schoolId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<TeacherClassMapping> GetAllTeacherClasses(int userId, bool flag) => Connection.Query<TeacherClassMapping>(
                @"SELECT TeacherClassMapping.*, 
                    (UserMaster.Name +' '+ UserMaster.Surname) as Name,
                    ClassMaster.ClassName
                FROM ClassMaster
                LEFT JOIN TeacherClassMapping ON TeacherClassMapping.ClassRoomID = ClassMaster.ID
                LEFT JOIN UserMaster on UserMaster.ID = TeacherClassMapping.TeacherID
                WHERE ClassMaster.SchoolId IN (SELECT SchoolID FROM SchoolAdminMapping WHERE UserId = @UserId)",
                param: new { UserId = userId },
                transaction: Transaction
            );

        public IEnumerable<Domain.Models.WebModel.ClassMaster> GetAllTeacherClasses(int teacherId) => Connection.Query<Domain.Models.WebModel.ClassMaster>(
                @"SELECT ClassMaster.* 
                  FROM TeacherClassMapping 
                  LEFT JOIN  UserMaster on UserMaster.ID = TeacherClassMapping.TeacherID 
                  LEFT JOIN ClassMaster on ClassMaster.ID = TeacherClassMapping.ClassRoomID 
                  WHERE TeacherID = @TeacherID",
                param: new { TeacherID = teacherId },
                transaction: Transaction
            );

        public IEnumerable<ClassMaster> GetClassesByTeacherId(int teacherId) => Connection.Query<ClassMaster>(
                @"SELECT ClassMaster.ID, ClassMaster.ClassName 
                    FROM TeacherClassMapping
                    LEFT JOIN ClassMaster ON ClassMaster.ID = TeacherClassMapping.ClassRoomID
                    WHERE TeacherClassMapping.TeacherID = @TeacherID AND TeacherClassMapping.IsActive = 1",
                param: new { TeacherID = teacherId },
                transaction: Transaction
            );

        public void ManageClass(Domain.Models.WebModel.ClassMaster classMaster)
        {
            Connection.Execute(
                "ManageClass",
                param: new
                {
                    classMaster.ID,
                    classMaster.ClassName,
                    classMaster.SchoolId,
                    classMaster.CreatedBy,
                    classMaster.CreatedOn,
                    classMaster.IsActive
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
        }


     
        public IEnumerable<VmClassAttendanceSummary> GetMyClassAttendanceSummary(int teacherId, int ClassRoomId)
      => Connection.Query<VmClassAttendanceSummary>(
             "GetMyClassAttendanceSummary",
             param: new
             {
                 TeacherId = teacherId,
                 ClassRoomId = ClassRoomId
             },
             transaction: Transaction,
             commandType: CommandType.StoredProcedure
         );

    }
}
