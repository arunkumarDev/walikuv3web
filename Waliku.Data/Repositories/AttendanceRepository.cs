﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Data.Repositories
{
    internal class AttendanceRepository : RepositoryBase, IAttendanceRepository
    {
        public AttendanceRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public VmAbsenteeReason GetAbsenteeReasonDetails(int childId, int classroomId, DateTime createOn)
        {
            if (classroomId <= 0) throw new ArgumentOutOfRangeException(nameof(classroomId));
            return Connection.Query<VmAbsenteeReason>(
                "Usp_Get_AbsenteeReasonDetails",
                param: new {ChildID = childId, ClassroomID = classroomId, CreatedOn = createOn},
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();
        }

        public AttendanceMaster GetAttendanceMasterDetails(int id) => Connection.Query<AttendanceMaster>(
                "SELECT * FROM AttendanceMaster WHERE ID = @ID",
                param: new { ID = id },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

        public bool IsAttendanceExists(int childId, int classRoomId, DateTime attendanceDate)
        {
            int attendanceCount = Connection.ExecuteScalar<int>(
                    "Usp_GetAttendanceMasterCount",
                    param: new
                    {
                        ChildID = childId,
                        ClassRoomID = classRoomId,
                        AttendanceDate = attendanceDate
                    },
                    transaction: Transaction,
                    commandType: CommandType.StoredProcedure
                );
            return attendanceCount > 0;
        }

        public bool ManageStudentAbsence(VmAbsenteeReason entity)
        {
            Connection.Execute(
                    "ManageAbsentReasonDetails",
                    param: new
                    {
                        entity.ID,
                        entity.ChildID,
                        entity.TeacherID,
                        entity.IsHealthReason,
                        entity.InformationProvider,
                        entity.HasPermission,
                        entity.CreatedBy,
                        entity.CreatedOn,
                        entity.Notes,
                        entity.IsPresent,
                        entity.ReasonID,
                        entity.Notes_Indonesian
                    },
                    transaction: Transaction,
                    commandType: CommandType.StoredProcedure
                );
            return true;
        }

        public bool ManageStudentAttendance(List<AttendanceMaster> listAttendanceMaster)
        {
            foreach (var entity in listAttendanceMaster)
            {
                if (entity == null)
                    throw new ArgumentNullException(nameof(entity));

                Connection.Execute(
                    "ManageStudentAttendance",
                    param: new
                    {
                        entity.ClassRoomID,
                        entity.TeacherID,
                        entity.ChildID,
                        entity.AttendanceDate,
                        entity.IsPresent,
                        CreatedBy = entity.TeacherID,
                        CreatedOn = entity.AttendanceDate,
                        entity.IsSubmitted,
                    },
                    transaction: Transaction,
                    commandType: CommandType.StoredProcedure
                );
            }
            return true;
        }
    }
}
