﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Data.Repositories
{
    internal class FeedbackRepository : RepositoryBase, IFeedbackRepository
    {
        public FeedbackRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IEnumerable<VMFeedBack> GetFeedBackData(int schoolId, DateTime starDate, DateTime endDate) => Connection.Query<VMFeedBack>(
                "GetFeedBackList",
                param: new { SchoolID = schoolId, StarDate = starDate, EndDate = endDate },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<WebModel.VmFeedBack> GetFeedBackData(int schoolId, DateTime starDate, DateTime endDate, string langId)
         => Connection.Query<WebModel.VmFeedBack>(
                "GetFeedBackList",
                param: new { SchoolID = schoolId, StarDate = starDate, EndDate = endDate, LangID = langId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public void Manage(FeedBack entity) => Connection.Execute(
                "ManageFeedBack",
                param: new
                {
                    entity.Content,
                    entity.CreatedOn,
                    entity.CreatedBy,
                    entity.Content_Indonesian
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );


        public IEnumerable<FeedBackTypeMaster> GetFeedBackTypes(string langId)
        {
            var currentCulture = langId == "id" ? "Description_Indonesian" : "Description";
            var sql = "select ID, ISNULL(" + @currentCulture + ",'') as Description from FeedBackTypeMaster";
            var result = Connection.Query<FeedBackTypeMaster>(
                       sql,
                     param: new { currentCulture = currentCulture },
                     transaction: Transaction
                 );
            return result;
        }

        public IEnumerable<FeedBackTypeMaster_Offline> GetFeedBackTypes_Offline()
        {
            var result = Connection.Query<FeedBackTypeMaster_Offline>(
                       "select ID, Description, Description_Indonesian from FeedBackTypeMaster",
                     transaction: Transaction
                 );
            return result;
        }
    }
}
