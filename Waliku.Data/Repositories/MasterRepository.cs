﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Data.Repositories
{
    internal class MasterRepository : RepositoryBase, IMasterRepository
    {
        public MasterRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IEnumerable<ReasonType> GetAllActiveReasonTypes() => Connection.Query<ReasonType>(
                @"SELECT ID, ReasonType, ISNULL({0},'') AS Description FROM ReasonMaster 
                    WHERE IsActive = 1 OR description ='Others' OR Description_Indonesian='Lainnya'",
                transaction: Transaction
            );

        public IEnumerable<ReasonType> GetAllReasonType() => Connection.Query<ReasonType>(
                "SELECT ID, Code, ISNULL({0},'') AS Description FROM ReasonType",
                transaction: Transaction
            );

        public IEnumerable<SickChildMaster> GetAllSickChildReason() => Connection.Query<SickChildMaster>(
                "SELECT * FROM SickChildReasonMaster",
                transaction: Transaction
            );

        #region V3 - Req# 3
        public IEnumerable<vmRole> GetAllActiveRoles() => Connection.Query<vmRole>(
                 @"SELECT r.Id, r.Name, ut.UserType_Indonesian as NameIndonesian                   
                    FROM dbo.AspNetRoles r
                    JOIN dbo.UserTypeMaster ut ON r.Name = ut.UserType
                    WHERE ut.IsActive = 1",
                transaction: Transaction
            );

        public IEnumerable<vmRole> GetUnderprivilegedRoles(string roleName) => Connection.Query<vmRole>(
                "GetUserRolesUnderprivileged",
                param: new { RoleName = roleName },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<vmDistrict> GetAllActiveDistricts() => Connection.Query<vmDistrict>(
             @"SELECT DISTINCT(District) AS Name, '' AS Code FROM [dbo].[SchoolMaster] WHERE IsActive = 1 AND ISNULL([District],'') <> ''",
            transaction: Transaction
        );
        #endregion
    }
}
