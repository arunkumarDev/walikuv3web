﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models.WebModel.DataModels;
using WebModel = Waliku.Domain.Models.WebModel.DataModels;
namespace Waliku.Data.Repositories
{
    internal class AcademicTermRepository : RepositoryBase, IAcademicTermRepository
    {
        public AcademicTermRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public bool DeleteAcademicTermById(int id)
        {
            Connection.Execute(
                  "UPDATE AcademicTermDetails SET IsCurrentTerm = 0 WHERE ID = @Id",
                  param: new { Id = id },
                  transaction: Transaction
              );
            return true;
        }
        public IEnumerable<WebModel.AcademicTerm> GetAcademicTermDetails(int userId, int userType) => Connection.Query<WebModel.AcademicTerm>(
                "Usp_GetAcademicTermDetails",
                param: new
                {
                    UserID = userId,
                    UserType = userType
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public bool IsCurrentTermExist(int schoolId)
        {
            var count = Connection.ExecuteScalar<int>(
                "SELECT COUNT(1) FROM AcademicTermDetails WHERE SchoolId = @SchoolId AND IsCurrentTerm = 1",
                param: new { SchoolId = schoolId },
                transaction: Transaction
            );
            return count > 0;
        }

        public void ManageAcademicTerm(AcademicTerm academicTerm) => Connection.Execute(
                "Usp_ManageAcademicTermDetails",
                param: new
                {
                    academicTerm.ID,
                    StartDate = academicTerm.Start_Date,
                    EndDate = academicTerm.End_Date,
                    SchoolID = academicTerm.SchoolId,
                    academicTerm.IsCurrentTerm,
                    academicTerm.CreatedBy,
                    academicTerm.CreatedOn
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
    }
}
