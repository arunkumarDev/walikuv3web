﻿using Dapper;
using System.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;

namespace Waliku.Data.Repositories
{
    internal class LogRepository : RepositoryBase, ILogRepository
    {
        public LogRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }
        public long AddErrorLog(Error error)
        {
            var parameters = new DynamicParameters();

            parameters.Add("LogTime", System.DateTime.Now);
            parameters.Add("Url", error.Url);
            parameters.Add("Request", error.Request);
            parameters.Add("Message", error.Message);
            parameters.Add("Exception", error.Exception);
            parameters.Add("@InsertedID", dbType: DbType.Int64, direction: ParameterDirection.Output);

            Connection.Execute(
                "[log].[AddErrorLog]",
                parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
            return parameters.Get<long>("InsertedID");
        }
        public long AddActionLog(Action action)
        {
            var parameters = new DynamicParameters();

            parameters.Add("AccessedTime", System.DateTime.Now);
            parameters.Add("UserId", action.UserId);
            parameters.Add("Ip", action.Ip);
            parameters.Add("Api", action.Api);
            parameters.Add("Service", action.Service);
            parameters.Add("@InsertedID", dbType: DbType.Int64, direction: ParameterDirection.Output);

            Connection.Execute(
                "[log].[AddActionLog]",
                parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
            return parameters.Get<long>("InsertedID");
        }
        public long AddAuditLog(Audit audit)
        {
            var parameters = new DynamicParameters();

            parameters.Add("LogTime", System.DateTime.Now);
            parameters.Add("UserId", audit.UserId);
            parameters.Add("DeviceInfo", audit.DeviceInfo);
            parameters.Add("Action", audit.Action);
            parameters.Add("Request", audit.Request);
            parameters.Add("Response", audit.Response);
            parameters.Add("Remarks", audit.Remarks);
            parameters.Add("@InsertedID", dbType: DbType.Int64, direction: ParameterDirection.Output);

            Connection.Execute(
                "[log].[AddAuditLog]",
                parameters,
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
            return parameters.Get<long>("InsertedID");
        }
    }
}
