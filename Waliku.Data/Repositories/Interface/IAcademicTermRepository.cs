﻿using System.Collections.Generic;
using WebModel = Waliku.Domain.Models.WebModel.DataModels;

namespace Waliku.Data.Repositories.Interface
{
    public interface IAcademicTermRepository
    {
        IEnumerable<WebModel.AcademicTerm> GetAcademicTermDetails(int userId, int userType);
        bool DeleteAcademicTermById(int id);
        void ManageAcademicTerm(WebModel.AcademicTerm academicTerm);
        bool IsCurrentTermExist(int schoolId);
    }
}