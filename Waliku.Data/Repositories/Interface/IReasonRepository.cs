﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Data.Repositories.Interface
{
    public interface IReasonRepository
    {
        IEnumerable<ReasonType> GetAllReasonType(string langId);
        IEnumerable<ReasonMaster> GetAllReasonMasterByReasonType(int reasonType, string langId);
        IEnumerable<AbsenceInformerMaster> GetAllAbsenceInformerMaster(string langId);
        IEnumerable<WebModel.ReasonMaster> GetAllReasonMaster(string langId);
        void ManageReasonType(WebModel.ReasonMaster reason);
        void ManageReason(WebModel.FirstAidMaster reason);
        void DeleteReasonTypeById(int reasonTypeId);
        void DeleteReasonById(int reasonId);

        #region "V2 Offline"
        IEnumerable<Offline_V2_ReasonType> GetAllReasonType_Offline();
        Offline_V2_ReasonListResponse GetReasonList_Offline();
        IEnumerable<Offline_V2_AbsenceInformerMaster> GetAllAbsenceInformerMaster_Offline();
        IEnumerable<Offline_V2_AbsenceRecordMode> GetAllAbsenceRecordModes_Offline();
        #endregion
    }
}