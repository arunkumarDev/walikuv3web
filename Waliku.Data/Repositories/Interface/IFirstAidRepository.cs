﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;

namespace Waliku.Data.Repositories.Interface
{
    public interface IFirstAidRepository
    {
        bool ManageFirstAidAnalytics(FirstAidAnlytics entity);
        IEnumerable<WebViewModel.VmFirstAidAnalytics> GetFirstAidCounterList();
        IEnumerable<WebModel.FirstAidMaster> GetAllFirstAidList();
    }
}