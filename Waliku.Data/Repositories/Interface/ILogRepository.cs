﻿using Waliku.Domain.Models;

namespace Waliku.Data.Repositories.Interface
{
    public interface ILogRepository
    {
        long AddErrorLog(Error error);
        long AddActionLog(Action action);
        long AddAuditLog(Audit audit);
    }
}
