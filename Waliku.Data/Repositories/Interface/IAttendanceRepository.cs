﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Data.Repositories.Interface
{
    public interface IAttendanceRepository
    {
        bool ManageStudentAttendance(List<AttendanceMaster> listAttendanceMaster);
        bool ManageStudentAbsence(VmAbsenteeReason absenteeReasonDetails);
        VmAbsenteeReason GetAbsenteeReasonDetails(int childId, int classroomId, DateTime createOn);
        AttendanceMaster GetAttendanceMasterDetails(int id);
        bool IsAttendanceExists(int childId, int classRoomId, DateTime attendanceDate);
    }
}