﻿using System.Collections.Generic;
using Waliku.Domain.Models;
namespace Waliku.Data.Repositories.Interface
{
    public interface IPermissionRepository
    {
        RoleMatrix GetRoleMatrix(int[] roleIds);
        RoleMatrix GetUserRoleMatrix(string userName);
        RoleMatrix GetAllRoleMatrix();
        IEnumerable<Component> GetUserComponents(string userName);
        IEnumerable<Component> GetUserRoleComponents(string userName, string roleName);
        int SavePermission(Permission permission);
        void ClearPermissions();
        IEnumerable<vmRole> GetUserRoles(string userName);
    }
}