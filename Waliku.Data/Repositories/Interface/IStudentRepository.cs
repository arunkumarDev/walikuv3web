﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Data.Repositories.Interface
{
    public interface IStudentRepository
    {
        IEnumerable<VmChildren> GetAbsentChildrenByClassId(int classId, DateTime startDate, DateTime endDate);
        IEnumerable<VmChildren> GetAbsentChildrenReport(int classId, DateTime startDate, DateTime endDate);
        IEnumerable<ChildrenMaster> GetAllChildren();
        IEnumerable<ChildrenMaster> GetAllChildrenForMe(int userId, int userType, string langId);
        IEnumerable<ChildrenMaster> GetAllChildrenForMeByFilter(int userId, int userType, int schoolId, int classRoomId, string langId);
        ChildrenMaster GetChildDetailById(int childId);
        string GetChildImageById(int childId);
        IEnumerable<VmChildren> GetChildrenByClassId(int classId, DateTime attendanceDate);
        IEnumerable<ChildrenMaster> GetChildrenByTeacherId(int teacherId);
        IEnumerable<AttendanceMaster> GetChildrenByTeacherIdAndClassIdAndDateWise(int teacherId, int classRoomId, DateTime attendanceDate);
        VmCount GetTeacherClassCountAndAbsenceCount(int teacherId, DateTime attendanceDate);
        void ManageChildren(ChildrenMaster entity);
        void DeleteChildById(int childId);
        VmChildren GetChildDetailsById(int childId);
       List<VMChildAbsenceDetails> GetStudentDashboardAbsenceDetails(int childId);

        #region "V2 Offline"
        IEnumerable<VmChildren> GetAbsentChildrenByClassID_Offline(int classId, DateTime startDate, DateTime endDate);
        bool IsAssessmentCompletedToday(int ChildId, DateTime AttendanceDate);
        #endregion
    }
}