﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Data.Repositories.Interface
{
   public interface IChildAssesmentRepository
    {
        bool CheckIfChildIsAbsentForThreedays(int ChildID, DateTime selectedDate);

        List<ChildAssesmentDetails> GetChildAssesmentDetails(int TeacherID, int AssesmentType, string LangId);
        List<ExplanatoryNotes> GetAllExplanatoryNotes(int TeacherID, int AssesmentType, string LangId);
        List<Recommendations> GetAllRecommendations(int TeacherID, int AssesmentType, string LangId);
        List<SubComplaintsMaster> GetAllSubComplaints(int TeacherID, int AssesmentType, string LangId);
        List<SubComplaintAdvice> GetAllMappedAdvices(int TeacherID, int AssesmentType, string LangId);

        void UpdateChildAssesmentDetails(VmChildAssesmentSummary summary);

        #region "V2 offline"
        List<Offline_V2_ChildAssesmentDetails> GetChildAssesmentDetails_Offline(int TeacherId, int AssesmentType);
        List<Offline_V2_ExplanatoryNotes> GetAllExplanatoryNotes_Offline(int TeacherId, int AssesmentType);
        List<Offline_V2_Recommendations> GetAllRecommendations_Offline(int TeacherId, int AssesmentType);
        List<Offline_V2_SubComplaintsMaster> GetAllSubComplaints_Offline(int TeacherId, int AssesmentType);
        List<Offline_V2_SubComplaintAdvice> GetAllMappedAdvices_Offline(int TeacherId, int AssesmentType);
        #endregion


    }
}
