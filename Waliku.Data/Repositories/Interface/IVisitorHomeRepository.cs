﻿using System.Collections.Generic;
using Waliku.Domain.Models;

namespace Waliku.Data.Repositories.Interface
{
    public interface IVisitorHomeRepository
    {
        void ManageVisitHome(VisitHomeMaster visitHomeMaster);
        IEnumerable<VisitHomeMaster> GetVisitHomeDetailByTeacherId(int teacherId);
    }
}