﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;

namespace Waliku.Data.Repositories.Interface
{
    public interface IReportRepository
    {
        WebModel.ViewModel.VmYearlyReport GetTeacherYearly(int classId, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportOnload(string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.AbsentOrPresentMaster> GetClassPresentsOrAbsentsChildren(string classRoomId, DateTime selectedDate);
        IEnumerable<VmChildrenReport> GetStudentWisePresentAbsentReport(int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReport(int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportForAdmin(int schoolId, int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummary(int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForAdmin(int schoolId, int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonReportRate(int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsent(int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsentForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetHolidayList(int classId, string selectedDate, int selectedType);
        IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountNew(int classId, string selectedDate, int selectedType, int teacherId, string langId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonCount(int classId, string selectedDate, int selectedType, int teacherId, string langId);
        IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId);
        IEnumerable<dynamic> GetYearlyHealthReason(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId);
        IEnumerable<dynamic> GetYearlyNonHealthReason(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId);
        IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonCount2(string selectedDate);

        #region "Yearly Absenteese Chart Report"
        IEnumerable<YearlyAbsenteesSummary> GetYearlyAbsenteesChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID);
        IEnumerable<YearlyTopFiveAbsenteesSummary> GetYearlyTopFiveHealthChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int ReasonType, string LangID);
        IEnumerable<YearlyChronicAbsenteesSummary> GetYearlyChronicAbsentChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID);
        #endregion
        IEnumerable<ChildDashboardSummary> GetMonthlyListReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type);
        SummaryData GetMonthlySummaryReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID);
        IEnumerable<VmChildrenReport> GetWeeklyListReport(int ClassRoomID, int TeacherID, DateTime AttendanceDate, int Type);
        SummaryData GetWeeklySummaryReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID);

        List<ChildDashboardSummary> GetChildDashboardSummary(int ChildID, DateTime selectedDate);
        List<ReasonSummary> GetChildReasonSummary(int ChildID, DateTime selectedDate, string langId);
        IEnumerable<ClassDashboardSummary> GetWeeklyChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID);
        IEnumerable<ClassDashboardSummary> GetClassDashboardReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID);
        IEnumerable<ClassTopReasonsForAbsence> GetClassTopReasonsForAbsence(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID);

        List<Offline_V2_ReasonSummary> GetChildReasonSummary_Offline(int ChildID, DateTime AttendanceDate);
    }
}