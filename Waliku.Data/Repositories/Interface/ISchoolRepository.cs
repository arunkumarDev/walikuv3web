﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Data.Repositories.Interface
{
    public interface ISchoolRepository
    {
        IEnumerable<SchoolMaster> GetAllSchool();
        IEnumerable<SchoolMaster> GetSchoolListForUser(int userId, string langId);
        int ManageSchool(WebModel.SchoolMaster schoolMaster);
        void DeleteSchoolById(int schoolId);
        IEnumerable<SchoolMaster> GetDistrictSchools(string districtName);
    }
}