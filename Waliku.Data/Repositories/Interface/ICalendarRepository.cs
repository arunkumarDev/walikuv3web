﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel.ViewModel;

namespace Waliku.Data.Repositories.Interface
{
    public interface ICalendarRepository
    {
        IEnumerable<CalendarEvents> GetAllCalendarEventsBySchool(string LanguageID, int SchoolID, DateTime calendarDate);
    }
}
