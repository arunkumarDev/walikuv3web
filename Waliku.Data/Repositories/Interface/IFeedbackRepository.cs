﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Data.Repositories.Interface
{
    public interface IFeedbackRepository
    {
        void Manage(FeedBack entity);
        IEnumerable<VMFeedBack> GetFeedBackData(int schoolId, DateTime starDate, DateTime endDate);
        IEnumerable<WebModel.VmFeedBack> GetFeedBackData(int schoolId, DateTime starDate, DateTime endDate, string langId);

        IEnumerable <FeedBackTypeMaster> GetFeedBackTypes(string langId);
        IEnumerable<FeedBackTypeMaster_Offline> GetFeedBackTypes_Offline();
    }
}