﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Data.Repositories.Interface
{
    public interface IConfigurationRepository
    {
        IEnumerable<ConfigurationGroup> GetAllConfigurationGroup();
        ConfigurationGroup GetConfigurationGroupById(int id);
        IEnumerable<Configuration> GetAllConfigurations();
        IEnumerable<Configuration> GetConfigurationByGroupId(int configurationGroupId);
        IEnumerable<Configuration> GetConfigurationByGroupName(string configurationGroupName);
        Configuration GetConfigurationByKey(string key);
    }
}