﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Data.Repositories.Interface
{
    public interface IClassRepository
    {
        IEnumerable<ClassMaster> GetClassesByTeacherId(int teacherId);
        IEnumerable<ClassMaster> GetAllClassBySchool(int schoolId);
        IEnumerable<WebModel.ClassMaster> GetAllClasses();
        IEnumerable<WebModel.TeacherClassMapping> GetAllTeacherClasses(int userId, bool flag);
        IEnumerable<WebModel.ClassMaster> GetAllTeacherClasses(int teacherId);
        IEnumerable<WebModel.ClassMaster> GetAllClassesForMe(int schoolId, int userId);
        void ManageClass(WebModel.ClassMaster classMaster);
        void DeleteClassById(int classId);

       IEnumerable<VmClassAttendanceSummary> GetMyClassAttendanceSummary(int teacherId, int ClassRoomId);


    }
}