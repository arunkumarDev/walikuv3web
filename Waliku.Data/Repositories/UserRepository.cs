﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models.WebModel;
using UserMaster = Waliku.Domain.Models.UserMaster;

namespace Waliku.Data.Repositories
{
    internal class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public dynamic GetUserById(int userId) => Connection.Query(
                @"SELECT * from UserMaster 
                  WHERE ID = @UserId",
                param: new { UserId = userId },
                transaction: Transaction
            ).FirstOrDefault();

        public IEnumerable<UserMaster> GetAllCommunityWorker() => Connection.Query<UserMaster>(
                "SELECT *,'' UserTypeName FROM UserMaster WHERE UserType=3",
                transaction: Transaction
            );

        public IEnumerable<UserMaster> GetAllActiveUsers() => Connection.Query<UserMaster>(
                "SELECT UserMaster.*, UserTypeMaster.UserType UserTypeName FROM UserMaster JOIN UserTypeMaster ON UserMaster.UserType = UserTypeMaster.Id WHERE UserMaster.IsActive=1",
                transaction: Transaction
            );

        public IEnumerable<UserMaster> GetUsersByType(int userType) => Connection.Query<UserMaster>(
                "GetUsersByType",
                param: new { UserType = userType },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public int Ping() => Connection.Query(
                "SELECT 1 as Result",
                transaction: Transaction
            ).FirstOrDefault()?.Result;

        public UserMaster ValidateUser(string userName, string password) => Connection.Query<UserMaster>(
                "SELECT * FROM UserMaster WHERE UserName = @UserName AND Password = @Password",
                param: new { UserName = userName, Password = password },
                transaction: Transaction
            ).FirstOrDefault();

        public IEnumerable<UserMaster> GetAllUsers() => Connection.Query<UserMaster>(
                @"SELECT UserMaster.ID, UserName, Password, ISNULL(Name,'') as Name, 
                    ISNULL(Surname,'') as Surname, ISNULL(Image,'') Image, ISNULL(UserMaster.UserType,'') UserType,
                    ISNULL(Address,'') Address, ISNULL(Phone,'') Phone, ISNULL(Note,'') Note, UTM.UserType as UserTypeName,
                    UserMaster.GeoLocation, UserMaster.Gender, UserMaster.Email
                  FROM UserMaster Left Join UserTypeMaster UTM on UTM.ID = UserMaster.UserType 
                  WHERE UserMaster.IsActive=1",
                transaction: Transaction
            );

        public IEnumerable<SchoolAdminMappingMaster> GetAllSchoolAdmin() => Connection.Query<SchoolAdminMappingMaster>(
                @"SELECT SchoolAdminMapping.SchoolId, SchoolAdminMapping.UserId,
                    SchoolAdminMapping.Id as ID,
                    SchoolMaster.SchoolName as SchoolName, (UserMaster.Name+' '+UserMaster.Surname) as UserName 
                  FROM SchoolAdminMapping 
                    LEFT JOIN  UserMaster on UserMaster.ID = SchoolAdminMapping.UserId 
                    LEFT JOIN SchoolMaster on SchoolMaster.ID = SchoolAdminMapping.SchoolId
                  WHERE SchoolAdminMapping.IsActive = 1 ORDER BY SchoolMaster.SchoolName",
                transaction: Transaction
            );

        public IEnumerable<UserTypeMaster> GetAllUserTypes()
        {
            string query = @"SELECT ID, UserType, UserType_Indonesian, Rank, WebAccess, AppAccess
                            FROM UserTypeMaster WHERE IsActive=1";
            
            return Connection.Query<UserTypeMaster>(query, transaction: Transaction);
        }

        public IEnumerable<UserTypeMaster> GetAllUserTypes(int userType)
        {
            string query = string.Empty;
            switch (userType)
            {
                case 0:
                    query = @"SELECT ID, UserType, UserType_Indonesian 
                            FROM UserTypeMaster";
                    break;
                case 4:
                    query = @"SELECT ID, UserType, UserType_Indonesian 
                            FROM UserTypeMaster 
                            WHERE ID in (1,2,4)";
                    break;
                case 1:
                    query = @"SELECT ID, UserType, UserType_Indonesian 
                            FROM UserTypeMaster 
                            WHERE ID IN (1,2)";
                    break;
            }
            return Connection.Query<UserTypeMaster>(query, transaction: Transaction);
        }

        public void ManageUser(Domain.Models.WebModel.WebUserMaster userMaster)
        {
            if (userMaster.ID > 0)
            {
                if (string.IsNullOrEmpty(userMaster.Password))
                {
                    var user = GetUserById(userMaster.ID);
                    if (user != null)
                    {
                        userMaster.Password = user.Password.ToString();
                    }
                }
            }

            Connection.Execute(
                "ManageUser",
                param: new
                {
                    userMaster.ID,
                    userMaster.UserName,
                    userMaster.Password,
                    userMaster.Name,
                    userMaster.Surname,
                    userMaster.Image,
                    userMaster.UserType,
                    userMaster.Address,
                    userMaster.Phone,
                    userMaster.Note,
                    userMaster.GeoLocation,
                    userMaster.Gender,
                    userMaster.CreatedBy,
                    userMaster.CreatedOn,
                    userMaster.IsActive
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
        }

        public void DeleteUserById(int userId) => Connection.Execute(
                "UPDATE UserMaster SET IsActive=0 WHERE ID = @UserId",
                param: new { UserId = userId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public void ManageAdmin(SchoolAdminMappingMaster schoolAdmin) => Connection.Execute(
                "ManageAdminMapping",
                param: new
                {
                    schoolAdmin.IsActive,
                    schoolAdmin.SchoolId,
                    schoolAdmin.UserId,
                    schoolAdmin.CreatedBy,
                    schoolAdmin.CreatedOn
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
        //duplicate
        public void ManageAdminMapping(SchoolAdminMapping schoolAdminMapping) => Connection.Execute(
                "ManageAdminMapping",
                param: new
                {
                    schoolAdminMapping.SchoolId,
                    schoolAdminMapping.UserId,
                    schoolAdminMapping.IsActive,
                    schoolAdminMapping.CreatedBy,
                    schoolAdminMapping.CreatedOn
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public void ManageClassTeacher(TeacherClassMapping teacherClassMapping) => Connection.Execute(
                "ManageClassTeacher",
                param: new
                {
                    teacherClassMapping.ID,
                    teacherClassMapping.ClassRoomID,
                    teacherClassMapping.TeacherID,
                    teacherClassMapping.CreatedBy,
                    teacherClassMapping.CreatedOn,
                    teacherClassMapping.IsActive
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public void DeleteSchoolAdminById(int id) => Connection.Execute(
                "UPDATE SchoolAdminMapping SET IsActive = 0 WHERE ID = @Id",
                param: new { Id = id },
                transaction: Transaction
            );

        public IEnumerable<Domain.Models.WebModel.WebUserMaster> GetAllTeachers() => Connection.Query<Domain.Models.WebModel.WebUserMaster>(
                @"SELECT UserMaster.ID, UserName, Password, ISNULL(Name,'') as Name, 
                    ISNULL(Surname,'') as Surname, ISNULL(Image,'') Image, 
                    ISNULL(UserMaster.UserType,'') UserType,ISNULL(Address,'') Address,
                    ISNULL(Phone,'') Phone, ISNULL(Note,'') Note, UTM.UserType as UserTypeName,
                    UserMaster.GeoLocation,UserMaster.Gender,UserMaster.Email
                  FROM UserMaster 
                    LEFT JOIN UserTypeMaster UTM ON UTM.ID = UserMaster.UserType 
                  WHERE UserMaster.IsActive=1 AND UserMaster.UserType=2",
                transaction: Transaction
            );

        public void DeleteClassTeacherById(int id) => Connection.Execute(
                "UPDATE TeacherClassMapping SET IsActive = 0 WHERE ID = @Id",
                param: new { Id = id },
                transaction: Transaction
            );

        public IEnumerable<SchoolAdminMapping> GetSchoolAdminMapping(int schoolId) => Connection.Query<SchoolAdminMapping>(
                "SELECT * FROM SchoolAdminMapping WHERE SchoolId = @SchoolId",
                param: new { SchoolId = schoolId },
                transaction: Transaction
            );

        public IEnumerable<Domain.Models.WebModel.WebUserMaster> GetTeacherClassMappingDetails(int classRoomId) => Connection.Query<Domain.Models.WebModel.WebUserMaster>(
                "Usp_GetTeacherClassMappingDetails",
                param: new { ClassRoomID = classRoomId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<Domain.Models.WebModel.WebUserMaster> GetMyUserList(int userId, string langId) => Connection.Query<Domain.Models.WebModel.WebUserMaster>(
                "Usp_Get_MyUserList",
                param: new { UserID = userId, LangID = langId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
    }
}
