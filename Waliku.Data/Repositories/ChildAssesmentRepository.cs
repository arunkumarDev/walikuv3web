﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Data.Repositories
{

    internal class ChildAssesmentRepository : RepositoryBase, IChildAssesmentRepository
    {
        public ChildAssesmentRepository(IDbTransaction transaction)
           : base(transaction)
        {

        }

        public bool CheckIfChildIsAbsentForThreedays(int ChildID, DateTime AttendanceDate)
        {
            int absenceDays = Connection.ExecuteScalar<int>(
                    "Usp_CheckIfChildIsAbsentForThreedays",
                    param: new
                    {
                        ChildID = ChildID,
                        AttendanceDate = AttendanceDate
                    },
                    transaction: Transaction,
                    commandType: CommandType.StoredProcedure
                );
            return absenceDays > 0 ? true : false;
        }

        public List<ChildAssesmentDetails> GetChildAssesmentDetails(int TeacherID, int AssesmentType, string LangId)
        {
            var answerDescription = (LangId == "id" ? "A.Description_Indonesian" : "A.Description");
            var questionDescription = (LangId == "id" ? "Q.Description_Indonesian" : "Q.Description");

            var result = Connection.Query<ChildAssesmentDetails>(

                       "SELECT A.ID as AnswerID,Q.ID as QuestionID, ISNULL( " + @answerDescription + ",'') as AnswerDescription, ISNULL( " + @questionDescription + ",'')  as QuestionDescription, " +
                                                                    "Q.AssementID, AM.Description as AssesmentName , " +
                                                                    "(SELECT TOP 1 ID FROM SubComplaintsMaster WHERE AssementAId= A.ID) SubComplaintID " +
                                                                    " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID " +
                                                                    " INNER JOIN ChildAssesmentMaster AM ON " +
                                                                    "AM.ID = Q.AssementID AND AM.AssesmentType = @AssesmentType",
                     param: new { answerDescription = answerDescription, questionDescription = questionDescription, TeacherId = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }

        public List<ExplanatoryNotes> GetAllExplanatoryNotes(int TeacherID, int AssesmentType, string LangId)
        {
            string description = (LangId == "id" ? "EN.Description_Indonesian" : "EN.Description");
            string title = (LangId == "id" ? "EN.Title_Indonesian" : "EN.Title");

            var result = Connection.Query<ExplanatoryNotes>(

                       "SELECT S.ID SubComplaintID, ISNULL(" + @title + ",'') AS Title, ISNULL(" + @description + ",'') AS Description " +
                                        " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID " +
                                        " INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID " +
                                        " INNER JOIN SubComplaintsMaster S ON S.AssementAID = A.ID " +
                                        " INNER JOIN ExplanatoryNotes EN ON EN.SubComplaintID = S.ID WHERE " +

                                        " AM.AssesmentType = " + @AssesmentType + " ",
                     param: new { title = title, description = description, TeacherId = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }
        public List<Recommendations> GetAllRecommendations(int TeacherID, int AssesmentType, string LangId)
        {
            string description = (LangId == "id" ? "RC.Description_Indonesian" : "RC.Description");

            var result = Connection.Query<Recommendations>(
                       "SELECT S.ID SubComplaintID, ISNULL(" + @description + ", '') AS Description " +
                                        " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID" +
                                        " INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID" +
                                        " INNER JOIN SubComplaintsMaster S ON S.AssementAID = A.ID" +
                                        " INNER JOIN Recommendations RC ON RC.SubComplaintID = S.ID WHERE " +
                                        " AM.AssesmentType= " + @AssesmentType + " ",
                     param: new { description = description, TeacherId = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction

                 ).ToList();
            return result;
        }
        public List<SubComplaintsMaster> GetAllSubComplaints(int TeacherID, int AssesmentType, string LangId)
        {
            string description = (LangId == "id" ? "S.Description_Indonesian" : "S.Description");


            var result = Connection.Query<SubComplaintsMaster>(
                       "SELECT A.ID as ComplaintID,S.ID ,ISNULL(" + @description + ", '') as Description,S.ComplaintType " +
                                                                " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID " +
                                                                " INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID " +
                                                                " INNER JOIN SubComplaintsMaster S ON S.AssementAID = A.ID WHERE " +

                                                                " AM.AssesmentType= " + @AssesmentType + " ",
                     param: new { description = description, TeacherId = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }
        public List<SubComplaintAdvice> GetAllMappedAdvices(int TeacherID, int AssesmentType, string LangId)
        {
            string description = (LangId == "id" ? "AD.Description_Indonesian" : "AD.Description");

            var result = Connection.Query<SubComplaintAdvice>(
                       "SELECT S.ID SubComplaintID, ISNULL(" + @description + ", '') AS Description " +
                                        " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID" +
                                        " INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID" +
                                        " INNER JOIN SubComplaintsMaster S ON S.AssementAID = A.ID" +
                                        " INNER JOIN SubComplaintAdvice AD ON AD.SubComplaintID = S.ID" +
                                        " WHERE AM.TeacherID = " + @TeacherID + " AND AM.AssesmentType= " + @AssesmentType + " ",
                     param: new { description = description, TeacherId = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }


        public void UpdateChildAssesmentDetails(VmChildAssesmentSummary summary) => Connection.Execute(
              "UpdateChildAssesmentDetails",
              param: new
              {
                  ComplaintID = summary.ComplaintID,
                  ChildID = summary.ChildID,
                  LangID = summary.LangID,
                  SubComplaintID = summary.SubComplaintID,
                  AttendanceDate = summary.AttendanceDate,
                  TeacherID = summary.TeacherID,
                  FreeText = summary.FreeText,
                  TeacherSubmissionDate = summary.TeacherSubmissionDate
              },
              transaction: Transaction,
              commandType: CommandType.StoredProcedure
          );

        public List<Offline_V2_ChildAssesmentDetails> GetChildAssesmentDetails_Offline(int TeacherID, int AssesmentType)
        {
            var result = Connection.Query<Offline_V2_ChildAssesmentDetails>(
                                                                    "SELECT A.ID as AnswerID,Q.ID as QuestionID, ISNULL(A.Description,'') as AnswerDescription, ISNULL(A.Description_Indonesian,'') as AnswerDescription_Indonesian, " +
                                                                    " ISNULL(Q.Description,'')  as QuestionDescription,ISNULL(Q.Description_Indonesian,'')  as QuestionDescription_Indonesian," +
                                                                    " Q.AssementID, AM.Description as AssesmentName ," +
                                                                    "(SELECT TOP 1 ID FROM SubComplaintsMaster WHERE AssementAId= A.ID) SubComplaintID " +
                                                                    " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID AND AM.AssesmentType = " + @AssesmentType + "",
                     param: new { TeacherID = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }

        public List<Offline_V2_ExplanatoryNotes> GetAllExplanatoryNotes_Offline(int TeacherID, int AssesmentType)
        {
            var result = Connection.Query<Offline_V2_ExplanatoryNotes>(
                             "SELECT S.ID SubComplaintID, ISNULL(EN.Title,'') AS Title,ISNULL(EN.Title_Indonesian,'') AS Title_Indonesian, " +
                             " ISNULL(EN.Description,'') AS Description ,ISNULL(EN.Description_Indonesian,'') AS Description_Indonesian " +
                             " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID " +
                             " INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID " +
                             " INNER JOIN SubComplaintsMaster S ON S.AssementAID = A.ID " +
                             " INNER JOIN ExplanatoryNotes EN ON EN.SubComplaintID = S.ID WHERE " +
                             " AM.AssesmentType = " + @AssesmentType + "",
        param: new { TeacherID = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }

        public List<Offline_V2_Recommendations> GetAllRecommendations_Offline(int TeacherID, int AssesmentType)
        {
            var result = Connection.Query<Offline_V2_Recommendations>(
                            "SELECT S.ID SubComplaintID, ISNULL(RC.Description,'') AS Description, ISNULL(RC.Description_Indonesian,'') AS Description_Indonesian " +
                                       " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID" +
                                       " INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID" +
                                       " INNER JOIN SubComplaintsMaster S ON S.AssementAID = A.ID" +
                                       " INNER JOIN Recommendations RC ON RC.SubComplaintID = S.ID WHERE " +
                                       " AM.AssesmentType = " + @AssesmentType + "",
        param: new { TeacherID = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }

        public List<Offline_V2_SubComplaintsMaster> GetAllSubComplaints_Offline(int TeacherID, int AssesmentType)
        {
            var result = Connection.Query<Offline_V2_SubComplaintsMaster>(
                           "SELECT A.ID as ComplaintID,S.ID ,ISNULL(S.Description,'') as Description ,ISNULL(S.Description_Indonesian,'') as Description_Indonesian,S.ComplaintType " +
                                                              " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID " +
                                                              " INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID " +
                                                              " INNER JOIN SubComplaintsMaster S ON S.AssementAID = A.ID WHERE " +
                                                              " AM.AssesmentType =" + @AssesmentType + "",
        param: new { TeacherID = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }

        public List<Offline_V2_SubComplaintAdvice> GetAllMappedAdvices_Offline(int TeacherID, int AssesmentType)
        {
            var result = Connection.Query<Offline_V2_SubComplaintAdvice>(
                          "SELECT S.ID SubComplaintID, ISNULL(AD.Description,'') AS Description, ISNULL(AD.Description_Indonesian,'') AS Description_Indonesian " +
                                       " FROM AssesmentAnswerOptions A INNER JOIN AssesmentQuestions Q ON Q.ID = A.AssementQID" +
                                       " INNER JOIN ChildAssesmentMaster AM ON AM.ID = Q.AssementID" +
                                       " INNER JOIN SubComplaintsMaster S ON S.AssementAID = A.ID" +
                                       " INNER JOIN SubComplaintAdvice AD ON AD.SubComplaintID = S.ID" +
                                       " WHERE AM.TeacherID =" + @TeacherID + " AND AM.AssesmentType =" + @AssesmentType + "",
        param: new { TeacherID = TeacherID, AssesmentType = AssesmentType },
                     transaction: Transaction
                 ).ToList();
            return result;
        }

    }
}
