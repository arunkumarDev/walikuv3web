﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.WebModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Data.Repositories
{
    internal class FirstAidRepository : RepositoryBase, IFirstAidRepository
    {
        public FirstAidRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IEnumerable<FirstAidMaster> GetAllFirstAidList() => Connection.Query<FirstAidMaster>(
                "SELECT ID, Title, Content, ContentIndian FROM FirstAidMaster",
                transaction: Transaction
            );

        public IEnumerable<WebViewModel.VmFirstAidAnalytics> GetFirstAidCounterList() => Connection.Query<WebViewModel.VmFirstAidAnalytics>(
                "GetFirstAidCounterList",
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public bool ManageFirstAidAnalytics(FirstAidAnlytics entity)
        {
            Connection.Execute(
                    "Usp_ManageFirstAidAnalytics",
                    param: new
                    {
                        entity.VisitorId, entity.VisitedOn
                    },
                    transaction: Transaction,
                    commandType: CommandType.StoredProcedure
                );
            return true;
        }
    }
}
