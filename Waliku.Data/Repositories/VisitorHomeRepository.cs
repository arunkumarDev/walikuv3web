﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;

namespace Waliku.Data.Repositories
{
    internal class VisitorHomeRepository : RepositoryBase, IVisitorHomeRepository
    {
        public VisitorHomeRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void ManageVisitHome(VisitHomeMaster entity) => Connection.Execute(
                "ManageVisitHome",
                param: new
                {
                    entity.ChildID,
                    entity.TeacherID,
                    entity.IsHealthReason,
                    entity.ReasonID
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<VisitHomeMaster> GetVisitHomeDetailByTeacherId(int teacherId)
        {
            return Connection.Query<VisitHomeMaster>(
                @"SELECT ChildrenMaster.ID AS ChildID, 
                   @TeacherID AS TeacherID, 
                   ChildrenMaster.Name, 
                   Count(AttendanceMaster.ChildID) AS AbsentCount, 
                   Max(AttendanceMaster.AttendanceDate) AS VisitHomeDate, 
                   ChildrenMaster.Image, 
                   Cast(0 AS BIT) AS IsHealthReason, 
                   0 AS ReasonID 
            FROM   AttendanceMaster 
                   LEFT JOIN ChildrenMaster 
                          ON ChildrenMaster.ID = AttendanceMaster.ChildID 
            WHERE  attendancemaster.IsPresent = 0 
                   AND ChildrenMaster.ClassroomID IN (SELECT ClassRoomID 
                                                      FROM   teacherclassmaster 
                                                      WHERE  TeacherID = @TeacherID) 
                   AND (SELECT Isnull(Max(Cast(VisitHomeDate AS DATE)), Cast( 
                               Getdate() AS DATE)) 
                        FROM   VisitHomeMaster 
                        WHERE  ChildID = ChildrenMaster.ID) < Isnull(Cast( 
                       AttendanceMaster.AttendanceDate AS DATE), Cast( 
                       Getdate() AS DATE)) 
            GROUP  BY ChildrenMaster.ID, 
                      ChildrenMaster.Name, 
                      ChildrenMaster.Image 
            UNION ALL 
            SELECT ChildrenMaster.ID AS ChildID, @TeacherID AS TeacherID, 
                   ChildrenMaster.Name, Count(AttendanceMaster.ChildID) AS AbsentCount, 
                   Max(AttendanceMaster.AttendanceDate) AS VisitHomeDate, 
                   ChildrenMaster.Image, Cast(0 AS BIT) AS IsHealthReason, 
                   0 AS ReasonID 
            FROM   AttendanceMaster 
                   LEFT JOIN ChildrenMaster 
                          ON ChildrenMaster.ID = AttendanceMaster.ChildID 
            WHERE  AttendanceMaster.IsPresent = 0 
                   AND ChildrenMaster.ClassroomID IN (SELECT ClassRoomID 
                                                      FROM   TeacherClassMaster 
                                                      WHERE  TeacherID = @TeacherID) 
                   AND (SELECT Max(VisitHomeDate) 
                        FROM   VisitHomeMaster 
                        WHERE  ChildID = ChildrenMaster.ID) IS NULL 
            GROUP  BY ChildrenMaster.ID, 
                      ChildrenMaster.Name, 
                      ChildrenMaster.Image",
                param: new { TeacherID = teacherId },
                transaction: Transaction
            );
        }
    }
}
