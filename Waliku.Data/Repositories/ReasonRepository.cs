﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using Waliku.Domain.Models.WebModel;
using ReasonMaster = Waliku.Domain.Models.WebModel.ReasonMaster;
using ReasonType = Waliku.Domain.Models.ReasonType;

namespace Waliku.Data.Repositories
{
    internal class ReasonRepository : RepositoryBase, IReasonRepository
    {
        public ReasonRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void DeleteReasonById(int reasonId) => Connection.Execute(
                "UPDATE FROM FirstAidMaster SET IsActive = 0 WHERE ID = ReasonId",
                param: new { ReasonId = reasonId },
                transaction: Transaction
            );

        public void DeleteReasonTypeById(int reasonTypeId) => Connection.Execute(
                "UPDATE ReasonMaster SET IsActive = 0 WHERE ID = @ReasonTypeId",
                param: new { ReasonTypeId = reasonTypeId },
                transaction: Transaction
            );

        public IEnumerable<AbsenceInformerMaster> GetAllAbsenceInformerMaster(string langId) => Connection.Query<AbsenceInformerMaster>(
                "SELECT ID, ISNULL(@LangID,'') AS Description FROM AbsenceInformerMaster",
                param: new { LangID = langId == "id" ? "Description_Indonesian" : "Description" },
                transaction: Transaction
            );

        public IEnumerable<ReasonMaster> GetAllReasonMaster(string langId) => Connection.Query<ReasonMaster>(
                "Usp_Get_ReasonMaster",
                param: new { LangID = langId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<Domain.Models.ReasonMaster> GetAllReasonMasterByReasonType(int reasonType, string langId) => Connection.Query<Domain.Models.ReasonMaster>(
                reasonType > 0 ? @"SELECT ID,ReasonType, ISNULL(@LangID,'') AS Description FROM ReasonMaster WHERE ReasonType=@ReasonType"
                : @"SELECT ID, ReasonType, ISNULL(@LangID,'') AS Description FROM ReasonMaster 
                    WHERE IsActive = 1 OR description ='Others' OR Description_Indonesian='Lainnya'",
                param: new { ReasonType = reasonType, LangID = langId == "id" ? "Description_Indonesian" : "Description" },
                transaction: Transaction
            );

        public IEnumerable<ReasonType> GetAllReasonType(string langId) => Connection.Query<ReasonType>(
                "SELECT ID, Code, ISNULL(@LangID,'') AS Description FROM ReasonType",
                param: new { LangID = langId == "id" ? "Description_Indonesian" : "Description" },
                transaction: Transaction
            );

        public void ManageReason(FirstAidMaster reason) => Connection.Execute(
                "ManageReason",
                param: new
                {
                    reason.ID,
                    reason.Title,
                    reason.Content,
                    reason.ContentIndian
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public void ManageReasonType(ReasonMaster reason) => Connection.Execute(
                "ManageClassReasonType",
                param: new
                {
                    reason.ID,
                    reason.ReasonType,
                    reason.Description,
                    DescriptionIndian = reason.Description_Indonesian
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<Offline_V2_ReasonType> GetAllReasonType_Offline() => Connection.Query<Offline_V2_ReasonType>(
               "SELECT Id, Code, Description, Description_Indonesian, IsActive FROM ReasonType",
               transaction: Transaction
           );

        public Offline_V2_ReasonListResponse GetReasonList_Offline()
        {
            var reasonList = new Offline_V2_ReasonListResponse();
            var reasonMasterList = Connection.Query(
                "Usp_GetReasonList",
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

            reasonList.HealthReason = reasonMasterList.Where(x => x.ReasonType == 1).Select(i => new Offline_V2_ReasonMaster
            {
                ID = i.ID,
                ReasonType = i.ReasonType ?? 0,
                Description = i.Description,
                Description_Indonesian = i.Description_Indonesian,
                ReasonSubType = i.ReasonSubType ?? 0
            }).OrderBy(o => o.Description).ToList();

            reasonList.SocialReason = reasonMasterList.Where(x => x.ReasonType == 2).Select(i => new Offline_V2_ReasonMaster
            {
                ID = i.ID,
                ReasonType = i.ReasonType ?? 0,
                Description = i.Description,
                Description_Indonesian = i.Description_Indonesian,
                ReasonSubType = i.ReasonSubType ?? 0
            }).OrderBy(o => o.Description).ToList().ToList();

            return reasonList;
        }

        public IEnumerable<Offline_V2_AbsenceInformerMaster> GetAllAbsenceInformerMaster_Offline() => Connection.Query<Offline_V2_AbsenceInformerMaster>(
                "SELECT ID, Description,Description_Indonesian,SequenceNo FROM AbsenceInformerMaster ORDER BY SequenceNo ",
                transaction: Transaction
            );

        public IEnumerable<Offline_V2_AbsenceRecordMode> GetAllAbsenceRecordModes_Offline() => Connection.Query<Offline_V2_AbsenceRecordMode>(
                "SELECT ID, Description, Description_Indonesian FROM AbsenceRecordMode ",
                transaction: Transaction
            );
    }
}
