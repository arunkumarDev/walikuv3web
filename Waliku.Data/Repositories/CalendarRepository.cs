﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel.ViewModel;

namespace Waliku.Data.Repositories
{
    internal class CalendarRepository : RepositoryBase, ICalendarRepository
    {
        public CalendarRepository(IDbTransaction transaction)
           : base(transaction)
        {
        }
        public IEnumerable<CalendarEvents> GetAllCalendarEventsBySchool(string LanguageID, int SchoolId, DateTime calendarDate) => Connection.Query<CalendarEvents>(
               "GetCalendarEventForMonthBySchool",
               param: new { FilterMonth = calendarDate.Month, FilterYear = calendarDate.Year, SchoolID = SchoolId, LanguageID },
               transaction: Transaction,
               commandType: CommandType.StoredProcedure
           );
    }
}
