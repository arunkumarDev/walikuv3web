﻿using Dapper;
using System.Collections.Generic;
using System.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;

namespace Waliku.Data.Repositories
{
    internal class ConfigurationRepository : RepositoryBase, IConfigurationRepository
    {
        public ConfigurationRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public IEnumerable<ConfigurationGroup> GetAllConfigurationGroup() => Connection.Query<ConfigurationGroup>(
            @"SELECT * FROM ConfigurationGroup WHERE IsActive = 1",
            transaction: Transaction
        );

        public ConfigurationGroup GetConfigurationGroupById(int id) => Connection.QueryFirstOrDefault<ConfigurationGroup>(
            @"SELECT * FROM ConfigurationGroup WHERE Id = @ConfigurationGroupId AND IsActive = 1 ",
            param: new { ConfigurationGroupId = id },
            transaction: Transaction
        );

        public IEnumerable<Configuration> GetAllConfigurations() => Connection.Query<Configuration>(
            @"SELECT * FROM Configuration WHERE IsActive = 1",
            transaction: Transaction
        );

        public IEnumerable<Configuration> GetConfigurationByGroupId(int configurationGroupId) => Connection.Query<Configuration>(
            @"SELECT * FROM Configuration WHERE ConfigurationGroupId = @ConfigurationGroupId AND IsActive = 1 ",
            param: new { ConfigurationGroupId = configurationGroupId },
            transaction: Transaction
        );

        public IEnumerable<Configuration> GetConfigurationByGroupName(string configurationGroupName) =>
            Connection.Query<Configuration>(
                @"SELECT c.* FROM Configuration c
                    INNER JOIN ConfigurationGroup cg ON cg.ConfigurationGroupId = c.Id
                    WHERE cg.Name = @ConfigurationGroupName AND cg.IsActive = 1",
                param: new {ConfigurationGroupName = configurationGroupName},
                transaction: Transaction
            );

        public Configuration GetConfigurationByKey(string key) =>
            Connection.QueryFirstOrDefault<Configuration>(
                @"SELECT c.* FROM Configuration c WHERE c.Key = @Key AND c.IsActive = 1",
                param: new { Key = key },
                transaction: Transaction
            );
    }
}
