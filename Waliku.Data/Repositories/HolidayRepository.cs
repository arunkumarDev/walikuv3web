﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.WebModel;

namespace Waliku.Data.Repositories
{
    internal class HolidayRepository : RepositoryBase, IHolidayRepository
    {
        public HolidayRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public void DeleteHolidayById(int id) => Connection.Execute(
                "UPDATE YearlyHolidayMaster SET IsActive = 0 WHERE ID = @Id",
                param: new { Id = id },
                transaction: Transaction
            );

        public IEnumerable<AcademicCalenderMaster> GetAllHoliday(int schoolId, string langId) => Connection.Query<AcademicCalenderMaster>(
                @"SELECT ID, HolidayDate, AcademicYear, CreatedBy, CreatedOn, SchoolId, 
                    IsActive, ISNULL(@LangId,'') AS Reason 
                 FROM YearlyHolidayMaster 
                 WHERE SchoolId=@SchoolId AND IsActive=1 
                 ORDER BY HolidayDate",
                param: new { SchoolId = schoolId, LangId = langId == "id" ? "Reason_Indonesian" : "Reason" },
                transaction: Transaction
            );

        public IEnumerable<YearlyHolidayMaster> GetHolidayListByMonthForTeacher(DateTime holidayDate, int userId, int schoolId, string currentCulture)
        {
            return Connection.Query(
                "Usp_Get_HolidayListByMonth",
                param: new { HolidayDate = holidayDate, TeacherID = userId, SchoolID = schoolId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).Select(item => new YearlyHolidayMaster
            {
                HolidayDate = (DateTime)item.HolidayDate,
                ID = (int)item.HolidyId,
                SchoolId = (int)item.SchoolId,
                Reason = currentCulture == "id" ? item.Reason_Indonesian.ToString() : item.Reason.ToString()
            });
        }

        public void ManageHoliday(AcademicCalenderMaster academicCalender)
        {
            Connection.Execute(
                "AcademicCalender",
                param: new
                {
                    academicCalender.ID,
                    academicCalender.Reason,
                    SchoolId = academicCalender.SchoolID,
                    academicCalender.HolidayDate,
                    AcademicYear = academicCalender.HolidayDate.Year,
                    academicCalender.IsActive,
                    academicCalender.CreatedBy,
                    academicCalender.CreatedOn,
                    academicCalender.Reason_Indonesian
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
        }
    }
}
