﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;

namespace Waliku.Data.Repositories
{
    internal class PermissionRepository : RepositoryBase, IPermissionRepository
    {
        public PermissionRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        

        public RoleMatrix GetRoleMatrix(int[] roleIds)
        {
            var roleMatrix = new RoleMatrix();
            roleMatrix.Roles = Connection.Query<Role>(@"SELECT r.* FROM dbo.AspNetRoles r 
                    JOIN dbo.UserTypeMaster ut ON r.Name = ut.UserType AND ut.WebAccess = 1
                    WHERE r.Id IN @RoleIds",
                param: new { RoleIds = roleIds },
                transaction: Transaction
            );

            roleMatrix.Components = Connection.Query<Component>(@"SELECT * FROM dbo.Component c WHERE c.IsActive = 1",
                transaction: Transaction
            );

            roleMatrix.Permissions = Connection.Query<Permission>(@"SELECT * FROM dbo.[Permission] p WHERE p.RoleId IN @RoleIds",
                param: new { RoleIds = roleIds },
                transaction: Transaction
            );

            return roleMatrix;
        }

        public RoleMatrix GetUserRoleMatrix(string userName)
        {
            var roleMatrix = new RoleMatrix();
            roleMatrix.Roles = Connection.Query<Role>(
                @"SELECT r.* FROM dbo.AspNetUsers u
                    JOIN dbo.AspNetUserRoles ur ON u.Id = ur.UserId
                    JOIN dbo.AspNetRoles r ON ur.RoleId = r.Id
                WHERE u.UserName = @UserName",
                param: new { UserName = userName },
                transaction: Transaction
            );

            roleMatrix.Components = Connection.Query<Component>(@"SELECT * FROM dbo.Component c WHERE c.IsActive = 1",
                transaction: Transaction
            );

            roleMatrix.Permissions = Connection.Query<Permission>(@"SELECT * FROM dbo.[Permission] p WHERE p.RoleId IN @RoleIds",
                param: new { RoleIds = roleMatrix.Roles.Select(p => p.Id).ToArray() },
                transaction: Transaction
            );

            return roleMatrix;
        }

        public RoleMatrix GetAllRoleMatrix()
        {
            var roleMatrix = new RoleMatrix();
            roleMatrix.Roles = Connection.Query<Role>(@"SELECT r.* FROM dbo.AspNetRoles r 
                    JOIN dbo.UserTypeMaster ut ON r.Name = ut.UserType " ,
// AND ut.WebAccess = 1",
                transaction: Transaction
            );

            roleMatrix.Components = Connection.Query<Component>(@"SELECT c.* FROM dbo.Component c WHERE c.IsActive = 1",
                transaction: Transaction
            );

            roleMatrix.Permissions = Connection.Query<Permission>(@"SELECT p.* FROM dbo.[Permission] p",
                transaction: Transaction
            );

            return roleMatrix;
        }

        public IEnumerable<Component> GetUserComponents(string userName) => Connection.Query<Component>(
                @"SELECT c.* FROM dbo.AspNetUsers u
                    JOIN dbo.AspNetUserRoles ur ON u.Id = ur.UserId
                    JOIN dbo.AspNetRoles r ON ur.RoleId = r.Id
                    JOIN [dbo].[Permission] p ON r.Id = p.RoleId
                    JOIN dbo.Component c ON p.ComponentId = c.Id
                WHERE u.UserName = @UserName AND p.Allow = 1",
                param: new { UserName = userName },
                transaction: Transaction
            );
        
        public IEnumerable<Component> GetUserRoleComponents(string userName, string roleName) => Connection.Query<Component>(
        @"SELECT c.* FROM dbo.AspNetUsers u
                    JOIN dbo.AspNetUserRoles ur ON u.Id = ur.UserId
                    JOIN dbo.AspNetRoles r ON ur.RoleId = r.Id
                    JOIN [dbo].[Permission] p ON r.Id = p.RoleId
                    JOIN dbo.Component c ON p.ComponentId = c.Id
                WHERE u.UserName = @UserName AND r.Name = @RoleName AND p.Allow = 1",
        param: new { UserName = userName, RoleName = roleName },
        transaction: Transaction
        );

        public int SavePermission(Permission permission)
        {
            var sqlStatement = @"
                INSERT INTO [dbo].[Permission]
                       ([ComponentId]
                       ,[RoleId]
                       ,[Allow]
                       ,[Readonly])
                 VALUES(
                    @ComponentId,
                    @RoleId,
                    @Allow,
                    @Readonly);

                SELECT CAST(SCOPE_IDENTITY() as INT)";
            return Connection.ExecuteScalar<int>(sqlStatement,
                param: permission,
                transaction: Transaction);
        }

        public void ClearPermissions() =>
            Connection.Execute(@"DELETE Permission", transaction: Transaction);

        public IEnumerable<vmRole> GetUserRoles(string userName)
        {
           var result =  Connection.Query<vmRole>(
               @"SELECT r.Id, r.Name, ut.UserType_Indonesian as NameIndonesian FROM dbo.AspNetUsers u
                    JOIN dbo.AspNetUserRoles ur ON u.Id = ur.UserId
                    JOIN dbo.AspNetRoles r ON ur.RoleId = r.Id
                    JOIN dbo.UserTypeMaster ut ON r.Name = ut.UserType
                WHERE u.UserName = @UserName",
               param: new { UserName = userName },
               transaction: Transaction
           );
            return result;
        }

    }
}
