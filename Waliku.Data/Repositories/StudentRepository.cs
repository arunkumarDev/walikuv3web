﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Data.Repositories
{
    internal class StudentRepository : RepositoryBase, IStudentRepository
    {
        public StudentRepository(IDbTransaction transaction)
            : base(transaction)
        {
        }

        public ChildrenMaster GetChildDetailById(int childId) => Connection.Query<ChildrenMaster>(
                "SELECT * FROM ChildrenMaster WHERE Id=@Id",
                param: new { Id = childId },
                transaction: Transaction
            ).FirstOrDefault();


        public IEnumerable<AttendanceMaster> GetChildrenByTeacherIdAndClassIdAndDateWise(int teacherId, int classRoomId, DateTime attendanceDate)
        {
            return Connection.Query<AttendanceMaster>(
                @" SELECT AttendanceMaster.ID, ChildrenMaster.ClassRoomID, ChildrenMaster.CommunityWorkerID as TeacherID, ChildrenMaster.ID as ChildID,
                   ChildrenMaster.Name, IsNull(AttendanceMaster.AttendanceDate, @AttendanceDate) AS AttendanceDate, AttendanceMaster.IsPresent  
                   FROM ChildrenMaster LEFT JOIN AttendanceMaster ON AttendanceMaster.TeacherID = ChildrenMaster.CommunityWorkerID
                   AND AttendanceMaster.ClassRoomID = ChildrenMaster.ClassRoomID AND AttendanceMaster.ChildID=ChildrenMaster.ID 
                   AND  CAST(AttendanceMaster.AttendanceDate as Date) = CAST( @AttendanceDate as Date) 
                   WHERE  ChildrenMaster.ClassroomID = @ClassRoomID",
                param: new { TeacherID = teacherId, ClassRoomID = classRoomId, AttendanceDate = attendanceDate },
                transaction: Transaction
            );
        }

        public IEnumerable<ChildrenMaster> GetChildrenByTeacherId(int teacherId) => Connection.Query<ChildrenMaster>(
                @"SELECT * from ChildrenMaster WHERE CommunityWorkerID = @TeacherID",
                param: new { TeacherID = teacherId },
                transaction: Transaction
            );

        public IEnumerable<ChildrenMaster> GetAllChildren() => Connection.Query<ChildrenMaster>(
                @"SELECT CM.ID, CM.Name, ISNULL(CM.Middlename, '') as Middlename, CM.Surname, 
                    CM.ParentName, CM.CommunityWorkerID, CM.Email, CM.SchoolID, CM.ClassroomID, CM.Address, CM.Image,
                    CM.Phone, CM.Note , '' as ImageBase, CM.GeoLocation,CM.Gender, 
                    UM.Name as CommunityWorkerName, SM.SchoolName,ClassMaster.ClassName as ClassroomName 
                 FROM  ChildrenMaster CM 
                    LEFT JOIN UserMaster UM on UM.ID = CM.CommunityWorkerID 
                    LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID 
                    LEFT JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID
                 WHERE CM.IsActive = 1  
                 ORDER BY CM.ID",
                transaction: Transaction
            );

        public IEnumerable<ChildrenMaster> GetAllChildrenForMe(int userId, int userType, string langId)
        {
            var query = @"SELECT CM.ID, CM.Name, ISNULL(CM.Middlename, '') as Middlename, CM.Surname, 
                    CM.ParentName, CM.CommunityWorkerID, CM.Email, CM.SchoolID, CM.ClassroomID, CM.Address, CM.Image,
                    CM.Phone, CM.Note , '' as ImageBase, CM.GeoLocation,CM.Gender, 
                    UM.Name as CommunityWorkerName, SM.SchoolName,ClassMaster.ClassName as ClassroomName 
                 FROM  ChildrenMaster CM 
                    LEFT JOIN UserMaster UM on UM.ID = CM.CommunityWorkerID 
                    LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID 
                    LEFT JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID
                 WHERE CM.IsActive = 1 {0}
                 ORDER BY CM.ID";
            query = userType == 1 ? string.Format(query, string.Empty) : string.Format(query, "AND CM.CommunityWorkerID = @UserId");
            return Connection.Query<ChildrenMaster>(query, param: new { UserId = userId }, transaction: Transaction);
        }

        public void ManageChildren(ChildrenMaster childrenMaster)
        {
            if (childrenMaster == null)
                throw new ArgumentNullException(nameof(childrenMaster));

            Connection.Execute(
                "ManageChildren",
                param: new
                {
                    childrenMaster.ID,
                    childrenMaster.Name,
                    childrenMaster.Surname,
                    childrenMaster.ParentName,
                    childrenMaster.CommunityWorkerID,
                    childrenMaster.Email,
                    childrenMaster.Image,
                    childrenMaster.SchoolID,
                    childrenMaster.ClassroomID,
                    childrenMaster.Address,
                    childrenMaster.Phone,
                    childrenMaster.Note,
                    GeoLocation = childrenMaster.GeoLocation == null ? string.Empty : childrenMaster.GeoLocation,
                    childrenMaster.Gender,
                    childrenMaster.IsActive,
                    childrenMaster.CreatedBy,
                    childrenMaster.CreatedOn,
                    childrenMaster.Note_Indonesian
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );
        }

        public IEnumerable<VmChildren> GetAbsentChildrenByClassId(int classId, DateTime startDate, DateTime endDate) => Connection.Query<VmChildren>(
                "Usp_GetAllAbsenceChildren",
                param: new { ClassRoomID = classId, FromDate = startDate, ToDate = endDate },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<VmChildren> GetAbsentChildrenReport(int classId, DateTime startDate, DateTime endDate) => Connection.Query<VmChildren>(
                "Usp_Rpt_GetAbsenceChildrenList",
                param: new { ClassRoomID = classId, FromDate = startDate, ToDate = endDate },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<VmChildren> GetChildrenByClassId(int classId, DateTime attendanceDate) => Connection.Query<VmChildren>(
                "Usp_Get_AllChildrenByClass",
                param: new { ClassRoomID = classId, AttendanceDate = attendanceDate },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public VmCount GetTeacherClassCountAndAbsenceCount(int teacherId, DateTime attendanceDate) => Connection.Query<VmCount>(
                "Usp_Get_ClassCountAbsenceCount",
                param: new { TeacherID = teacherId, AttendanceDate = attendanceDate },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();

        public string GetChildImageById(int childId) => Connection.Query(
                "SELECT Image FROM ChildrenMaster WHERE ID = @Id",
                param: new { Id = childId },
                transaction: Transaction
            ).FirstOrDefault()?.Image;

        public void DeleteChildById(int childId) => Connection.Execute(
                "UPDATE ChildrenMaster SET IsActive = 0 WHERE ID = @ChildId",
                param: new { ChildId = childId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public IEnumerable<ChildrenMaster> GetAllChildrenForMeByFilter(int userId, int userType, int schoolId, int classRoomId, string langId)
             => Connection.Query<ChildrenMaster>(
                "GetAllChildrenByFilter",
                param: new
                {
                    UserType = userType,
                    SchoolID = schoolId,
                    ClassRoomID = classRoomId,
                    UserID = userId,
                    LangID = langId
                },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            );

        public VmChildren GetChildDetailsById(int childId) => Connection.Query<VmChildren>(
                "Usp_Get_ChildDetailsByID",
                param: new { ChildID = childId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).FirstOrDefault();


        public List<VMChildAbsenceDetails> GetStudentDashboardAbsenceDetails(int childId) => Connection.Query<VMChildAbsenceDetails>(
                "Usp_GetStudentDashboardAbsenceDetails",
                param: new { ChildID = childId },
                transaction: Transaction,
                commandType: CommandType.StoredProcedure
            ).ToList();

        #region "V2 offline"
        public IEnumerable<VmChildren> GetAbsentChildrenByClassID_Offline(int classId, DateTime startDate, DateTime endDate) => Connection.Query<VmChildren>(
              "Usp_GetAllAbsenceChildren_Offline_V2",
              param: new { ClassRoomID = classId, FromDate = startDate, ToDate = endDate },
              transaction: Transaction,
              commandType: CommandType.StoredProcedure
          );

        public bool IsAssessmentCompletedToday(int ChildID, DateTime Attendancedate)
        {
            var response = Connection.Query<bool>(
                @" Select case when count(*)>0 Then 1 else 0 end IsAbsenteeReasonExists 
                        from AbsenteeReasonDetails 
                        where ChildID = @ChildId and cast(CreatedOn as date) = cast(@AttendanceDate as date)",
                param: new { ChildId = ChildID, AttendanceDate = Attendancedate },
                transaction: Transaction
            ).FirstOrDefault();

            return Convert.ToBoolean(response);
        }
        #endregion
    }
}
