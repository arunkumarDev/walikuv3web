﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using WalikuWebSite.Model;
using WalikuWebSite.DataAccess;
using System.Data.SqlClient;
using WalikuWebSite.App_Start;
using System.Linq;

namespace WalikuWebSite
{
    public partial class AcademicYears : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<AcademicTerm> GetAllAcademicYearsList(int SchoolID)
        {
            var myUser = SqlHelper.GetUserByID(Common.CommonFunction.GetUserIdFromCookieByID());

            int uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());
            var uid = Common.CommonFunction.GetUserIdFromCookieByID();
            return AcademicTermDataAccess.GetAcademicTermDetails(uid, uType,SchoolID);

        }

        [WebMethod]
        public static VmResponseData DeleteAcademicYear(int ID)
        {
            AcademicTermDataAccess.DeleteAcademicTermByID(ID);
            return new VmResponseData() { Message = "Success" };
        }

        [WebMethod]
        public static VmResponseData ManageAcademicYear(int ID, string Name, string StartDate, string EndDate, bool IsCurrentTerm, int SchoolID)
        {
            var uid = Common.CommonFunction.GetUserIdFromCookieByID();

            //int SchoolID = SqlHelper.GetSchoolListForUser(uid,"").FirstOrDefault().ID;

            VmResponseData response = new VmResponseData();
            try
            {
                if (IsCurrentTerm == true)
                {
                    if (AcademicTermDataAccess.IsCurrentTermExist1(SchoolID,ID))
                    {
                        response.Message = "Academic Term Already Set For this school";
                        return response;
                    }
                }
                else
                {

                }

                if( ID == 0)
                {


                    AcademicTermDataAccess.ManageAcademicTerm(new AcademicTerm()
                    {

                        ID = 0,
                        SchoolId = SchoolID,
                        Start_Date = Convert.ToDateTime(StartDate),
                        End_Date = Convert.ToDateTime(EndDate),
                        IsCurrentTerm = IsCurrentTerm,
                        CreatedOn = DateTime.Now,
                        CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                        AcademicYearName = Name


                    });
                }
                else
                {
                    AcademicTermDataAccess.UpdateAcademicTerm(new AcademicTerm()
                    {
                        ID = ID,
                        SchoolId = SchoolID,
                        Start_Date = Convert.ToDateTime(StartDate),
                        End_Date = Convert.ToDateTime(EndDate),
                        IsCurrentTerm = IsCurrentTerm,
                        CreatedOn = DateTime.Now,
                        CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                        AcademicYearName = Name


                    });
                }
                
                response.Message = "Success";
            }
            catch (SqlException SqlEx)
            {
                if (SqlEx.Number == 2601)
                    response.Message = "Term Details Already Set For this school";
            }
            catch (Exception ex)
            {
                response.Message = "Failed";

            }
            return response;
        }
    }
}