﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminReportChildSummary.aspx.cs" Inherits="WalikuWebSite.AdminReportChildSummary" %>


 <script>
     var LocalResources = {
           "Jan":'<%=Resources.Resource.Lbl_Jan %>' 

          }
        
        

    </script>

<div class="clearfix">
       
      
      <div class="row">
     <form class="custom-material">
          <div class="col-md-3 childsummary_profilephoto">
                            <img src="images/user.png" id="studentPhoto" class="img-responsive img-border" alt="preview">
                    </div>
          <div class="col-md-9">
             <strong><span class="childsummary_profilename"></span></strong>
         </div>
         <div class="childsummary_layout">
         <div class="clearfix"></div>
        <div class="col-md-9 childsummary_content">
            <div class="col-md-3"></div>
            <div class="col-md-3">
             <span class="childsummary_age"></span> <br />
            <span class="childsummary_nisn"></span>
            </div>
             <div class="col-md-6" id="showSmileyIfValues">
             <%-- <div class="childsummary_smiley"></div>--%>
                 <img class="smiley-img" id="showGreen" src="../images/icons/happy.png" />
                 <img class="smiley-img" id="showYellow" src="../images/icons/normal.png" />
                 <img class="smiley-img" id="showRed" src="../images/icons/sad.png" />

                 <strong id="attendance-percent"></strong>
                 <br />
                 <span id="TotalDays_Att_child" class="totalschool"> </span>
             </div>
            <div class="col-md-12" style="margin-top: 60px;">
                <span class="childsummary_bars_green"><span></span></span>
                <span class="childsummary_bars_yellow"><span></span></span>
                <span class="childsummary_bars_red"><span></span></span>
            </div>

            <div class="row">
                <div class="col-md-4 nodayspresent">
                    <%--Present Days--%>
                    <%=Resources.Resource.Lbl_PrDays %>
                </div>
                 <div class="col-md-4 nodaysabsent">
                   
                     <%=Resources.Resource.Lbl_knwAbsDays %>
                </div>
                 <div class="col-md-4 nodaysunknown">
                     <%=Resources.Resource.Lbl_unknwAbsDays %>
                 
                </div>
            </div>
        </div>
              <div class="col-md-3 childsummary_contentabox">
            <div class="stats col-md-12" style="border: 1px solid #00ca9d;">
            <div class="i_absencecount"></div>
                <span id="MonthlyAbsence"></span>
              <strong  ><%=Resources.Resource.Lbl_DaysAbsent.ToUpper()%></strong>
            </div>
          <div class="stats col-sm-12 mt-20" style="border: 1px solid #00ca9d; max-height: 172px; min-height: 162px; overflow-y: auto;">
               <strong class=""><%=Resources.Resource.Lbl_TopReason.ToUpper()%></strong>
              <div class="col-md-12" style="width: 100%;">
               <div class="i_healthreasoncount" style="width:50px"></div>
                   <ul class="studentlist" id="hide_reason" style="text-align:initial; font-size:14px; line-height:26px !important">
                  <li id="Reason1"> </li>
                  <li id="Reason2"> </li>
                  <li id="Reason3"> </li>
                <%--  <li id="Health4"> </li>
                  <li id="Health5"> </li>--%>
              </ul> 
            <%--  <strong id="Reason1"></strong>
              <strong id="Reason2"></strong>
              <strong id="Reason3"></strong>--%>
              <%--<span id="AbsentDays1"></span>--%>
              </div>
          <%--    <div class="col-md-12">
              <strong id="Reason2"></strong>
              <span id="AbsentDays2"></span>
                  </div>
                  <div class="col-md-12">
              <strong id="Reason3"></strong>
              <span id="AbsentDays3"></span>
                      </div>--%>
             <%-- <div class="clearfix"></div> --%> 
            </div>
          </div> 
                  </div>
        <div class="col-md-12 mt-20" id="MonthlyNewsFeed">

            <div class="pull-right mb-10 bmd-form-group is-filled">
                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Adm_chdsum_month%></label>
                <select id="selmonth" class="form-control">
                    <option value="01"><%=Resources.Resource.Lbl_Jan%></option>
                    <option value="02"><%=Resources.Resource.Lbl_Feb%></option>
                    <option value="03"><%=Resources.Resource.Lbl_Mar%></option>
                    <option value="04"><%=Resources.Resource.Lbl_Apr%></option>
                    <option value="05"><%=Resources.Resource.Lbl_May%></option>
                    <option value="06"><%=Resources.Resource.Lbl_June%></option>
                    <option value="07"><%=Resources.Resource.Lbl_July%></option>
                    <option value="08"><%=Resources.Resource.Lbl_Aug%></option>
                    <option value="09"><%=Resources.Resource.Lbl_Sep%></option>
                    <option value="10"><%=Resources.Resource.Lbl_Oct%></option>
                    <option value="11"><%=Resources.Resource.Lbl_Nov%></option>
                    <option value="12"><%=Resources.Resource.Lbl_Dec%></option>
                </select>
                <i class="fa fa-angle-down fa-icons text-18"></i>
            </div>
             <div class="data-table-wrapper mt-20">
                    <table id="dataTable" class="table table-hover mb-0 student-table" style="width:100%">
                        <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th><span><%=Resources.Resource.Lbl_Adm_chdsum_Date%></span></th>
                                <th><span><%=Resources.Resource.Lbl_Adm_chdsum_res%></span></th>
                                <th><%=Resources.Resource.Lbl_Adm_chdsum_notes%> </th>                               
                            </tr>
                        </thead>
                       <tbody></tbody>
                    </table>
                </div>
        </div>
 </form> 
          </div>
</div>

<script type="text/javascript" src="../Scripts/Reports/AdminReportChildSummary.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script>
           var Lbl_AddStudent = '<%=Resources.Resource.Lbl_AddStudent %>'
          var Lbl_EditStudent = '<%=Resources.Resource.Lbl_EditStudent %>'
    </script>