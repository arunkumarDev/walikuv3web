﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeacherReportMonthly.aspx.cs" Inherits="WalikuWebSite.TeacherReportMonthly" %>

      <div class="clearfix"></div>
      <h4 id="MonthlyLeftSummary"><%=Resources.Resource.Title_MonthlyAttendanceSummary %> </h4>
      
      <div class="row report-leftsec">
    
        <div class="col-sm-3" id="leftsumarry">          
 
            <ol class="list-group mt-20">
                <li class="list-group-item" id="Monthlyleftsec"><span><%=Resources.Resource.Lbl_MonthlyAttendanceRate %></span><span class="badge badge-success" id="MonthlyAttendance">%</span></li>
              </ol>  
          <p class="mt-15" id="MonthlySummary" ><%=Resources.Resource.Lbl_MonthlyAbsenteeismRate %> </p>      
          <div class="stats" style="border-top:0;" id ="MonthlySummary1">
            <div>
              <span><img src="images/icons/Class1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_Class.ToUpper() %></span> </strong><span id="AbsenteesimRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/boy1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_Boys.ToUpper() %></span> </strong><span id="BoysAbsenteeRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/girl1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_Girls.ToUpper() %></span></strong><span id="GirlsAbsenteeRate">%</span>
            </div>
          </div>
          <div class="stats" id ="MonthlySummary2">
            <div>
              <span><img src="images/icons/health.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_Health.ToUpper() %></span></strong><span id="HealthAbsenteeRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/absent.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_NonHealth.ToUpper() %></span></strong><span id="NonHealthAbsenteeRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/unknown.png" alt="image"></span>
              <strong class="mb-5 mt-5" ><span><%=Resources.Resource.Lbl_Unknown.ToUpper() %></span></strong><span id="UnKnownAbsenteeRate">%</span>
            </div>
          </div>
          <ol class="list-group mt-20" id ="MonthlySummary3">
            <!-- <li class="list-group-item">Monthly Attendance Rate <span class="badge badge-success">14</span></li> -->
            <li class="list-group-item"><span> <%=Resources.Resource.Lbl_StudentAbsent %> &gt;= 3 <%=Resources.Resource.Lbl_Days.ToUpper() %></span> <span class="badge  badge-error" id="StudentCount1"></span></li>
            <li class="list-group-item"> <span> <%=Resources.Resource.Lbl_StudentAbsent %> &lt; 3 <%=Resources.Resource.Lbl_Days.ToUpper() %> </span><span class="badge badge-warning"  id="StudentCount2"></span></li>
          </ol>
          <p id ="MonthlySummary4"> <%=Resources.Resource.Lbl_NumberOfStudentsAbsent %> </p>
          <div class="Health-Reasons" id ="MonthlySummary5">
            <div class="panel panel-default">
              <div class="panel-heading">
                 <%=Resources.Resource.Lbl_CommonHealthReasons %>
              </div>
              <div class="panel-body" id="dvHealthReasonCount">
                
              </div>
              <div class="panel-heading">
                <%=Resources.Resource.Lbl_CommonNonHealthReasons %>
              </div>
                <div class="panel-body" id="dvNonHealthReasonCount">
              
              </div>
            </div>
          </div>
      
   
        </div>
       
        <div class="col-sm-9">
          <div class="page-content">
            <div class="portlet">
              <div class="portlet-title" id="grid">  
                  <div class="caption caption-red">
                   <p id="monthName"> </p>
               </div>
                <ul class="nav nav-tabs">
                  <li>
                    <a href="#portlet_tab2" data-toggle="tab">
                      <i class="fa fa-list-ul" aria-hidden="true"></i> </a>
                  </li>
                  <li class="active">
                    <a href="#portlet_tab1" data-toggle="tab">
                      <i class="fa fa-th" aria-hidden="true"></i> </a>
                  </li>
                </ul>
              </div>
              <div class="portlet-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="portlet_tab1">
                      <p id="MonthlyGraphHeading"><%=Resources.Resource.Lbl_Attendance %></p>
                      <p id="YearlyGraphHeading"> <%=Resources.Resource.Title_ChronicAndSevereChronic %></p>
                    <div class="filter-box mt-0">
                      <div id="attendance-chart" class="custom-charts" style="min-width: 310px;height:300px; margin: 0 auto"></div>
                         <div id="attendance-chart2" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                        <div id="attendance-chart3" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                        <div id="attendance-chart4" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                    </div>
                  </div>
                  <div class="tab-pane" id="portlet_tab2">
                    <div class="custom-scroll" style="padding-right: 5px;">
                      <div class="table-responsive gridview-tbl">
                        <table id="dataTableAttendance" class="table table-striped table-bordered data-table mb-0" role="grid">
                          <thead id="tblhead">
                 
                          </thead>
                             <tbody id="tblBody">

                             </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <ul class="list-inline mt-20 gridview-tbl-info">
                      <li><span class="legend"><%=Resources.Resource.Holiday %></span>- <%=Resources.Resource.Lbl_Holiday %></li>
                      <li><span class="legend"><%=Resources.Resource.Present %></span>- <%=Resources.Resource.Lbl_Present %></li>
                      <li><span class="legend"><%=Resources.Resource.Health %></span>- <%=Resources.Resource.Lbl_Health %> </li>
                      <li><span class="legend"><%=Resources.Resource.NonHealth %></span>- <%=Resources.Resource.Lbl_NonHealth %></li>
                      <li><span class="legend"><%=Resources.Resource.UnknownLeave %></span>- <%=Resources.Resource.Lbl_UnknownLeave %></li>
                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>
            <br/><br/>
          <div class="page-content" id="Monthlygraph">
          <div class="col-md-6">
              <div id="burden-healthabsence" class="custom-charts" style="min-width: 310px;height: 250px; max-width: 600px; margin: 0 auto"></div>
          </div> 
          <div class="col-md-6">
              <div id="burden-non-healthabsence" class="custom-charts" style="min-width: 310px;height: 250px; max-width: 600px; margin: 0 auto"></div>
          </div> 
          </div>

        </div>
      </div>


    <script src="../Scripts/Reports/TeacherReportMonthly.js?9"></script>

