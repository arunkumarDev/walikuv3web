﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.DataAccess;

namespace WalikuWebSite
{
    public partial class StudentList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<ChildrenMaster> GetAllChildren()
        {
            var result = SqlHelper.GetAllChildren();
            return result;
        }

        //[WebMethod]
        //public static List<ChildrenMaster> GetAllChildrenForMe()
        //{
        //    var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        //    if (uid == 0)
        //        return new List<ChildrenMaster>();
        //    var udata = SqlHelper.GetUserByID(uid);
        //    return SqlHelper.GetAllChildrenForMe(uid, int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()));
        //}

        [WebMethod]
        public static List<ChildrenMaster> GetAllChildrenForMe(int SchoolID, int ClassRoomID)
        {
            var uid = Common.CommonFunction.GetUserIdFromCookieByID();
            if (uid == 0)
                return new List<ChildrenMaster>();
            var udata = SqlHelper.GetUserByID(uid);
            if (SchoolID == 0 && ClassRoomID == 0)
                return SqlHelper.GetAllChildrenForMe(uid, 1, Common.CommonFunction.GetCurrentCulture());
            else
                return SqlHelper.GetAllChildrenForMeByFilter(uid, int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()), SchoolID, ClassRoomID, Common.CommonFunction.GetCurrentCulture());
        }

        [WebMethod]
        public static string GetChildImageByID(int ChildID)
        {
            var s = SqlHelper.GetChildImageByID(ChildID);
            byte[] bytes = Convert.FromBase64String(s);
            string filePath = "~/Photos/" + ChildID.ToString() + "_Child.jpg";
            File.WriteAllBytes(HttpContext.Current.Server.MapPath(filePath), bytes);
            return filePath.Replace("~", "..");

        }

        [WebMethod]
        public static List<UserMaster> GetAllCommunityWorker(int PageIndex)
        {
            return SqlHelper.GetAllUsers();


        }

        [WebMethod]
        public static List<UserMaster> GetTeacherClassMapping(int ClassRoomID)
        {
            return SqlHelper.GetTeacherClassMappingDetails(ClassRoomID);
        }



        [WebMethod]
        public static List<SchoolMaster> GetAllSchool(int PageIndex)
        {
            return SqlHelper.GetAllSchools();
        }

        //[WebMethod]
        //public static List<ClassMaster> GetAllClassRoom(int PageIndex)
        //{
        //    return SqlHelper.GetAllClasses();

        //   // HttpFileCollectionBase files = Request.Files;
        //}

        [WebMethod]
        public static List<ClassMaster> GetAllClassRoomForMe(int SchoolID)
        {
            var uid = Common.CommonFunction.GetUserIdFromCookieByID();
            return SqlHelper.GetAllClassesForMe(SchoolID, uid);
        }

        protected void UploadBtn_Click(object sender, EventArgs e)
        {
            //if (this.FileUploadControl.HasFile)
            //{

            //    this.FileUploadControl.SaveAs(@"C:\temp\" + FileUploadControl.FileName);

            //}
            //else
            //{

            //}
        }

        [WebMethod]
        //
        public static void ManageChild(int WalikuID, string Name, string Surname, string ParentName, int CommunityWorkerID, string Email, 
            string Image, int SchoolID, int ClassroomID, string Address, string Phone, string Note,string Note_Indonesian, string Gender, string Nisn, int GradeID, int DeleteReasonID)
        {

            SqlHelper.ManageChildren(new ChildrenMaster()
            {
                ID = WalikuID,
                Name = Name == null ? "" :Name.Trim(),
                Surname = Surname == null? "": Surname.Trim(),
                ParentName = ParentName == null ? "" :ParentName.Trim(),
                Email = Email == null ? "" :  Email.Trim(),
                Image = string.Empty,
                SchoolID = SchoolID,
                ClassroomID = ClassroomID,
                Address = Address== null? "" : Address.Trim(),
                Phone = Phone == null ? "" : Phone.Trim(),
                Note = Note == null ? "" : Note.Trim(),
                Note_Indonesian = Note_Indonesian == null ? "" : Note_Indonesian.Trim(),
                CommunityWorkerID = CommunityWorkerID,
                Gender = Gender == null ? "" : Gender,
                NISN = Nisn == null ? "" : Nisn,
                GradeID = GradeID,
                DeleteReasonID = DeleteReasonID,
                CreatedOn = DateTime.Now,
                CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                IsActive = 1
            });
        }
        [WebMethod]
        public static void DeleteChildByID(int ID)
        {
            SqlHelper.DeleteChildByID(ID);
        }

        [WebMethod]
        public static VmChildren GetChildDetailsByID(int ID)
        {
            return ChildrenDataAccess.GetChildDetailsByID(ID);
        }

        [WebMethod]
        public static List<GradeMaster> GetAllGrade()
        {
            return SqlHelper.GetAllGrade();
        }
        [WebMethod]
        public static List<DelteReasonMaster> GetAllReasonForDelete()
        {
            return SqlHelper.GetAllReasonForDelete(Common.CommonFunction.GetCurrentCulture());
        }
    }
}