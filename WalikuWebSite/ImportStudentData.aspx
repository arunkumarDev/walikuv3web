﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ImportStudentData.aspx.cs" Inherits="WalikuWebSite.ImportStudentData" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <main class="l-main custom-material">
    <script src="Scripts/ImportData/ImportStudentData.js">
     
    </script>
         <div id="alertAction" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="btnclosePopup2" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Title_ImportSuccess%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="row">
                        <div class="">
                             <h4 class="modal-title"><%=Resources.Resource.Alrt_ImportSuccess%></h4>
                            <h4 id="ImportErrorsList" style="display:none;">However some of them errored out. To See Errored Data, Please download the errored file from <a href="ImportExport/Errors/ImportFailed.xlsx">Here</a></h4>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
         </div>
         <div id="comp-Fields" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="btnclosePopup" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Title_CompFields%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="row">
                        <div class="">
                             <h4 class="modal-title"><%=Resources.Resource.Lbl_CompFields%></h4>
                         </div>
                    </div>
                    </div>
                </div>
            </div>
         </div>

          <div id="Nisn-Existing" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="btnclosePopup1" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Title_CompFields%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="row">
                        <div class="">
                             <h4 class="modal-title"><%=Resources.Resource.Lbl_IsNisnLength%></h4>
                         </div>
                    </div>
                    </div>
                </div>
            </div>
         </div>

         <div id="Data-Existing" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="btnclosePopup3" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Title_CompFields%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="row">
                        <div class="">
                             <h4 class="modal-title"><%=Resources.Resource.Lbl_DataExist%></h4>
                         </div>
                    </div>
                    </div>
                </div>
            </div>
         </div>

         <div id="File-Error" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="btnclosePopup4" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Title_CompFields%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="row">
                        <div class="">
                             <h4 class="modal-title"><%=Resources.Resource.Lbl_fileerror%></h4>
                         </div>
                    </div>
                    </div>
                </div>
            </div>
         </div>


         <div id="class-Match" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" id="btnclosePopup5" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Title_CompFields%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="row">
                        <div class="">
                             <h4 class="modal-title"><%=Resources.Resource.Lbl_classerror%></h4>
                         </div>
                    </div>
                    </div>
                </div>
            </div>
         </div>

<div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="col-sm-6">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Select %></label>
                       <select id="ddlImportType" class="form-control">
                           <option value="0" title="<%=Resources.Resource.Lbl_Student %>"><%=Resources.Resource.Lbl_Student %></option>
                           <option value="1" title="Teacher" style="display:none">Teacher</option>
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
            <div class="col-sm-6" id="imp-school">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                       <select id="ddlSchoolSearch" class="form-control">
                       </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>

             <div class ="col-sm-8" id="EngFile">
                 <label for="txtImportFile" class="btn btn-default">Choose File</label>
                  <span id="Filename"> </span>
                     <input type="file" id="txtImportFile" name="txtImage" style="visibility:hidden;" title="<%=Resources.Resource.Lbl_AddFile %>">
                 <%--  <label for="files" class="btn btn-default">browse</label>
                <input id="files" type="file" class="btn btn-default"  style="visibility:hidden;"/>

                 <button style="display:block;width:120px; height:30px;" onclick="document.getElementById('getFile').click()">Your text here</button>


<input type='file' id="getFile" style="display:none"> --%>
                 </div>


             <div class ="col-sm-8" id="IdFile">
                 <label for="txtImportFile" class="btn btn-default">Pilih dokumen</label>
                 <span id="Filename1"> </span>
                     <input type="file" id="txtImportFile" name="txtImage" style="visibility:hidden;"  title="<%=Resources.Resource.Lbl_AddFile %>">
                
                 </div>

            <div class ="col-sm-3">
                 <input type="button" class="btn btn-success" value="<%=Resources.Resource.Lbl_Submit %>" id="btnImportData">
                        </div>
            
              
            
            <div class="col-md-6">
                <ul>
                    <li id="eng" runat="server">
                        <p>Please Download the Student Import Template (ENG) from <a href="ImportExport/Template/ChildrenImportEnglish.xlsx">Here</a></p>
                    </li>
                    <li id="ind" runat="server">
                        <p>Silakan Unduh Template Impor Siswa (IND) dari
                            <a href="ImportExport/Template/ChildrenImportIndonesian.xlsx">sini</a></p>

                        <%--<p>Please Download the Student Import Template (IND) from <a href="ImportExport/Template/ChildrenImportIndonesian.xlsx">Here</a></p>--%>
                    </li>
                    <li style="display:none">
                        <p>Please Download the Teacher Import Template from <a href="ImportExport/Template/TeacherImport.xlsx">Here</a></p>
                    </li>
                </ul>
                
            </div>
        </div>


         <div class="row" id="Engtext">
             <div class="col-md-12">
                 <span> PLEASE NOTE:</span>
             <ul> 
                   <li>
                        1. Use Import Data only for new students/NISN. You cannot import students/ NISN already in Waliku database. </li>
                   <li> 2. Ensure to Add/Edit Class for students before Import Data. </li>
                   <li> 3. Ensure to Add/ Edit User and Assign Mobile Login for Classes before Import Data. </li>
                   <li> 4. Ensure All mandatory fields in template are entered – No, Name, Gender, NISN, Date of birth, Current Class.
                     </li>
                 <li>
                     5. If NISN is not available for a student, use add/edit student to enter the child and remove him/her from the import list.
                 </li>
             </ul>
           </div>
         </div>

          <div class="row" id="Idtext">
             <div class="col-md-12">
                 <span> PERHATIKAN:</span>
             <ul> 
                   <li>
                        1. Hanya gunakan impor data untuk murid baru/NISN. Anda tidak bisa menambahkan murid/NISN yang sudah dimasukkan sebelumnya. </li>
                   <li> 2. Pastikan anda sudah menambahkan/edit Kelas sebelum import data murid. </li>
                   <li> 3. Pastikan anda sudah Menambahkan/Edit pengguna dan Membuat Akun Guru di web sebelum impor data. </li>
                   <li> 4. Pastikan semua bagian yang wajib diisi SUDAH DIISI – No, Nama, Jenis Kelamin, NISN, Tanggal Lahir, Rombel Saat Ini.
                     </li>
                 <li>5. Harus isi NISN dalam import data. Jika NISN belum ada, gunakan fitur Tambah/Edit siswa untuk entry siswa dan hapus dari daftar import.</li>
             </ul>
           </div>
         </div>

</main>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
</asp:Content>