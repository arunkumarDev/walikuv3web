﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="AcademicTermDetails.aspx.cs" Inherits="WalikuWebSite.AcademicTermDetails" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
                <div class="row">
                        <div class="col-sm-2">
                            <div class="bmd-form-group">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_StartDate %></label>
                                <input type="text" class="form-control date-icon hasDatepicker" id="dtStartDate">
    
                                <i class="fa fa-calendar fa-icons"></i>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="bmd-form-group">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_EndDate %></label>
                                <input type="text" class="form-control date-icon hasDatepicker" id="dtEndDate">
    
                                <i class="fa fa-calendar fa-icons"></i>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="bmd-form-group is-filled">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                                <select class="form-control" id="ddlSchool">
                                    
                                </select>
                                <i class="fa fa-angle-down fa-icons text-18"></i>
                            </div>
                        </div>
                       <%-- <div class="col-sm-2">
                            <div class="bmd-form-group is-filled">
                                <label class="bmd-label-floating">Select</label>
                                <select class="form-control" id="ddlIsCurrentTerm">
                                    <option value="0">Active</option>
                                    <option value="1">In-Active</option>
                                </select>
                                <i class="fa fa-angle-down fa-icons text-18"></i>
                            </div>
                        </div>--%>
                        <div class="col-sm-2 mt-15">
                            <button type="button" class="btn btn-primary" title="" id="btnAdd"><%=Resources.Resource.Lbl_Add %></button>
                        </div>
                    </div>
            
            <div class="page-content">
                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-striped table-hover" style="width:100%">
                                        <thead>
                                                <tr>
                                                    <th><%=Resources.Resource.Lbl_SerialNo %></th>
                                                    <th><%=Resources.Resource.Lbl_StartDate %></th>
                                                    <th><%=Resources.Resource.Lbl_EndDate %></th>
                                                    <th><%=Resources.Resource.Lbl_SchoolName %></th>
                                                    <th><%=Resources.Resource.Lbl_Status %></th>
                                                    <th><%=Resources.Resource.Lbl_Action %></th>
                                                </tr>
                                            </thead>
                                            <tbody>

            
                                            </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

     <script src="Scripts/bootstrap-datepicker.js"></script>
     <script>document.write("<script type='text/javascript' src='Scripts/AcademicTermDetails.js?v=" + JsVerion + "'><\/script>");</script>

    <script>
        $('#dtStartDate').datepicker({
            autoclose: true,
             format: 'dd/mm/yyyy'
        });
        var date = new Date();
        $('#dtStartDate').datepicker('setDate', date);

         $('#dtEndDate').datepicker({
             autoclose: true,
             format: 'dd/mm/yyyy'
        });
        date = new Date();
        $('#dtEndDate').datepicker('setDate', date);

       var Lbl_Active = '<%=Resources.Resource.Lbl_Active %>'

    </script>

</asp:Content>
