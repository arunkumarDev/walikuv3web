﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalikuWebSite
{
    public partial class AssignAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Common.CommonFunction.IsUserLoggedIn())
                {
                    Response.Redirect("login");
                }
            }

        }

        [WebMethod]
        public static List<SchoolMaster> GetSchoolListForUser()
        {
            var uid = Common.CommonFunction.GetUserIdFromCookieByID();
            return SqlHelper.GetSchoolListForUser(uid, Common.CommonFunction.GetCurrentCulture());
        }

        [WebMethod]
        public static List<UserMaster> GetAllUser()
        {
            var uid = 1;
            return SqlHelper.GetUsersByType(uid);
        }
        [WebMethod]
        public static List<SchoolAdminMappingMaster> GetAllSchoolAdmin(int PageIndex)
        {
            return SqlHelper.GetAllSchoolAdmin();

        }
        [WebMethod]
        public static void ManageAdmin(int ID, int SchoolId, int UserId)
        {

            SqlHelper.ManageAdmin(new SchoolAdminMappingMaster()
            {
                ID = ID,
                SchoolId = SchoolId,
                UserId = UserId,
                IsActive = true,
                CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                CreatedOn = DateTime.Now
            });
        }

        [WebMethod]
        public static void DeleteSchoolAdminByID(int ID)
        {
            SqlHelper.DeleteSchoolAdminByID(ID);
        }

    }
}