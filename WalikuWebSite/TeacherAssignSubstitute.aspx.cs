﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.DataAccess;

public partial class TeacherAssignSubstitute : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }
    }

    [WebMethod]
    public static List<AssignSubstitute> GetTeacherAssignSubstituteList()
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        return TeacherAssignSubstituteDataAccess.GetTeacherAssignSubstituteList(uid);
    }

    [WebMethod]
    public static void DeleteTeacherAssignSubstituteByID(int ID)
    {
        SqlHelper.DeleteTeacherAssignSubstituteByID(ID);
    }
}