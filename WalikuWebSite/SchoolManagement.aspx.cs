﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SchoolManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }

    }
    [WebMethod]
    public static List<SchoolMaster> GetAllSchools(int PageIndex)
    {
        return SqlHelper.GetAllSchools();

    }

    //[WebMethod]
    //public static void ManageSchool(int ID, string SchoolName, string Email, string Image,
    //    string City, string Address, string Country, string Phone1, string Phone2, string Note, string GeoLocation, int AdminId)
    //{

    //    int InsertedID = SqlHelper.ManageSchool(new SchoolMaster()
    //    {
    //        ID = ID,
    //        SchoolName = SchoolName.Trim(),
    //        Email = Email.Trim(),
    //        Image = Image.Trim(),
    //        City = City.Trim(),
    //        Address = Address.Trim(),
    //        Country = Country.Trim(),
    //        Phone1 = Phone1.Trim(),
    //        Phone2 = Phone2.Trim(),
    //        Note = Note.Trim(),
    //        GeoLocation = GeoLocation,
    //        CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
    //        CreatedOn = DateTime.Now,
    //        IsActive = true
    //    });

    //    if (AdminId > 0)
    //    {
    //        SqlHelper.ManageAdminMapping(new SchoolAdminMapping()
    //        {
    //            SchoolId = (ID == 0) ? InsertedID : ID,
    //            UserId = AdminId,
    //            CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
    //            CreatedOn = DateTime.Now,
    //            IsActive = true
    //        });
    //    }
    //}
    [WebMethod]
    public static void DeleteSchoolByID(int ID)
    {
        SqlHelper.DeleteSchoolByID(ID);
    }

    [WebMethod]
    public static List<SchoolMaster> GetMySchools()
    {
        return SqlHelper.GetSchoolListForUser(Common.CommonFunction.GetUserIdFromCookieByID(), Common.CommonFunction.GetCurrentCulture());
    }

    [WebMethod]
    public static List<SchoolAdminMapping> GetSchoolAdminDetails(int SchoolId)
    {
        return SqlHelper.GetSchoolAdminMapping(SchoolId);
    }

    [WebMethod]
    public static List<SchoolAdminMappingDetails> GetUserByType(int usertype)
    {
        return SqlHelper.GetUserByType(usertype);
    }
    

}