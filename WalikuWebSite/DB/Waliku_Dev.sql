USE [master]
GO
/****** Object:  Database [DB_A39427_WalikuDev]    Script Date: 19/02/2019 19:49:51 ******/
CREATE DATABASE [DB_A39427_WalikuDev]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DB_A39427_WalikuDev_Data', FILENAME = N'H:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\DB_A39427_WalikuDev_DATA.mdf' , SIZE = 9792KB , MAXSIZE = 2048000KB , FILEGROWTH = 10%)
 LOG ON 
( NAME = N'DB_A39427_WalikuDev_Log', FILENAME = N'H:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\DB_A39427_WalikuDev_Log.LDF' , SIZE = 3072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DB_A39427_WalikuDev].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET ARITHABORT OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET  MULTI_USER 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DB_A39427_WalikuDev]
GO
/****** Object:  Schema [HangFire]    Script Date: 19/02/2019 19:49:54 ******/
CREATE SCHEMA [HangFire]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_diagramobjects]    Script Date: 19/02/2019 19:49:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	CREATE FUNCTION [dbo].[fn_diagramobjects]() 
	RETURNS int
	WITH EXECUTE AS N'dbo'
	AS
	BEGIN
		declare @id_upgraddiagrams		int
		declare @id_sysdiagrams			int
		declare @id_helpdiagrams		int
		declare @id_helpdiagramdefinition	int
		declare @id_creatediagram	int
		declare @id_renamediagram	int
		declare @id_alterdiagram 	int 
		declare @id_dropdiagram		int
		declare @InstalledObjects	int

		select @InstalledObjects = 0

		select 	@id_upgraddiagrams = object_id(N'dbo.sp_upgraddiagrams'),
			@id_sysdiagrams = object_id(N'dbo.sysdiagrams'),
			@id_helpdiagrams = object_id(N'dbo.sp_helpdiagrams'),
			@id_helpdiagramdefinition = object_id(N'dbo.sp_helpdiagramdefinition'),
			@id_creatediagram = object_id(N'dbo.sp_creatediagram'),
			@id_renamediagram = object_id(N'dbo.sp_renamediagram'),
			@id_alterdiagram = object_id(N'dbo.sp_alterdiagram'), 
			@id_dropdiagram = object_id(N'dbo.sp_dropdiagram')

		if @id_upgraddiagrams is not null
			select @InstalledObjects = @InstalledObjects + 1
		if @id_sysdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 2
		if @id_helpdiagrams is not null
			select @InstalledObjects = @InstalledObjects + 4
		if @id_helpdiagramdefinition is not null
			select @InstalledObjects = @InstalledObjects + 8
		if @id_creatediagram is not null
			select @InstalledObjects = @InstalledObjects + 16
		if @id_renamediagram is not null
			select @InstalledObjects = @InstalledObjects + 32
		if @id_alterdiagram  is not null
			select @InstalledObjects = @InstalledObjects + 64
		if @id_dropdiagram is not null
			select @InstalledObjects = @InstalledObjects + 128
		
		return @InstalledObjects 
	END
	

GO
/****** Object:  UserDefinedFunction [dbo].[Fn_GetNumberOfWorkingDays]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_GetNumberOfWorkingDays](
	@SchoolID int,
	@StartDate DATE,
	@EndDate DATE
)
RETURNS int
AS BEGIN
    DECLARE @WorkingDays int
	

    SET @WorkingDays = 0;

--SELECT @StartDate = Start_Date,@EndDate = End_Date FROM AcademicTermDetails WHERE SchoolId = @SchoolID ;
    
WITH CTE(WorkingDay) AS (SELECT WorkingDay = convert(DATETIME,@StartDate)

UNION ALL SELECT WorkingDay = DATEADD(DAY,1,WorkingDay) FROM CTE WHERE WorkingDay < @EndDate)

SELECT @WorkingDays =  COUNT(*) FROM (SELECT WorkingDay FROM CTE WHERE (DATENAME(WEEKDAY, WorkingDay)) NOT IN ('Sunday')  


						EXCEPT 

			--	SELECT HolidayDate FROM YearlyHolidayMaster WHERE IsActive = 1 AND SchoolId = @SchoolId AND AcademicYear= YEAR(@StartDate)) AS A

				SELECT HolidayDate FROM YearlyHolidayMaster WHERE IsActive = 1 AND SchoolId = @SchoolId AND CAST(HolidayDate AS DATE) BETWEEN @StartDate AND @EndDate) AS A
				OPTION ( MAXRECURSION 0);

    RETURN @WorkingDays
END





GO
/****** Object:  UserDefinedFunction [dbo].[Fn_GetYearly_Report_DateRange]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[Fn_GetYearly_Report_DateRange]
(
	@SchoolID as int
)
RETURNS @TempMonth TABLE
(
	MonthNumber INT,
	FirstDayOfMonth DATE,
	LastDayOfMonth DATE,
	WorkingDays INT NULL DEFAULT 0
)

BEGIN

DECLARE @start DATE 
DECLARE @end DATE 

DECLARE @TermStartDate DATE 
DECLARE @TermEndDate DATE 



SELECT @TermStartDate = Start_Date,@TermEndDate = End_Date FROM AcademicTermDetails WHERE SchoolId = @SchoolID  AND IsCurrentTerm = 1



SET @start = @TermStartDate
SET @end = GETDATE()

	IF GETDATE() > @TermEndDate
		BEGIN
			SET @end = @TermEndDate
		END

	

;with months (date)
AS
(
    SELECT @start
    UNION ALL
    SELECT DATEADD(month,1,date)
    from months
    where DATEADD(month,1,date) < @end
)
INSERT INTO @TempMonth(MonthNumber,FirstDayOfMonth,LastDayOfMonth) SELECT  			 
			
			  [MonthNumber]  = DATEPART(mm ,Date) 
		     ,[FirstDayOfMonth] = CONVERT(DATE, DATEADD(MONTH, DATEDIFF(MONTH, 0, EOMONTH(Date)), 0)) 
		     ,[LastDayOfMonth]  = EOMONTH(Date)
FROM months


UPDATE @TempMonth SET FirstDayOfMonth = @start WHERE MonthNumber =  MONTH(@start) 
UPDATE @TempMonth SET LastDayOfMonth = @end WHERE MonthNumber =  MONTH(@end) 


UPDATE @TempMonth SET WorkingDays =  dbo.Fn_GetNumberOfWorkingDays(@SChoolID,FirstDayOfMonth,LastDayOfMonth)



RETURN

END







GO
/****** Object:  UserDefinedFunction [dbo].[GetClassList]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetClassList]
(
@TeacherId as int
)
RETURNS @ClassList TABLE
(
ID int,
ClassName varchar(50)
)

BEGIN

declare @UserTypeSelected varchar(50)

set @UserTypeSelected = (select UTM.UserType from UserMaster UM 
join UserTypeMaster UTM on UM.UserType = UTM.Id
where UM.Id = @TeacherId)

if(@UserTypeSelected = 'Teacher')
begin

insert into @ClassList
	select CM.ID,CM.ClassName from TeacherClassMapping TCM 
	join ClassMaster CM on TCM.ClassRoomID = CM.Id
	and  TCM.TeacherId = @TeacherId and isnull(TCM.IsActive,0) = 1 and isnull(CM.IsActive,0) = 1

end

else if(@UserTypeSelected = 'Admin')
begin

insert into @ClassList
	select CM.ID,CM.ClassName from SchoolAdminMapping SAM 
	join  ClassMaster CM on SAM.SchoolId = CM.SchoolId
	and SAM.UserId = @TeacherId and isnull(SAM.IsActive,0) = 1 and isnull(CM.IsActive,0) = 1

end

else if(@UserTypeSelected = 'Super Admin')
begin

insert into @ClassList
	select CM.ID,CM.ClassName from ClassMaster CM  
	WHERE isnull(CM.IsActive,0) = 1

end

RETURN

END



GO
/****** Object:  UserDefinedFunction [dbo].[GetClassListForSchool]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetClassListForSchool]
(
@SchoolId as int
)
RETURNS @ClassList TABLE
(
ID int,
ClassName varchar(50)
)

BEGIN


insert into @ClassList
	select ID,ClassName from ClassMaster where SchoolId = @SchoolId and isnull(IsActive,0) = 1


RETURN

END
GO
/****** Object:  Table [dbo].[AbsenceInformerMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AbsenceInformerMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Description] [varchar](100) NULL,
	[Description_Indonesian] [nvarchar](100) NULL,
 CONSTRAINT [PK_AbsenceInformerMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AbsenteeReasonDetails]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AbsenteeReasonDetails](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChildID] [int] NULL,
	[TeacherID] [int] NULL,
	[VisitHomeDate] [datetime] NULL,
	[IsHealthReason] [bit] NULL,
	[ReasonID] [int] NULL,
	[InformationProvider] [int] NULL,
	[HasPermission] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[Notes] [varchar](100) NULL,
	[Notes_Indonesian] [nvarchar](500) NULL,
 CONSTRAINT [PK_VisitHomeMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AbsenteeReasonDetails_BAK]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AbsenteeReasonDetails_BAK](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ChildID] [int] NULL,
	[TeacherID] [int] NULL,
	[VisitHomeDate] [datetime] NULL,
	[IsHealthReason] [bit] NULL,
	[ReasonID] [int] NULL,
	[InformationProvider] [int] NULL,
	[HasPermission] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[Notes] [varchar](100) NULL,
	[Notes_Indonesian] [nvarchar](500) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AcademicTermDetails]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AcademicTermDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Start_Date] [datetime] NOT NULL,
	[End_Date] [datetime] NOT NULL,
	[No_Of_Days] [int] NOT NULL,
	[SchoolId] [int] NOT NULL,
	[IsCurrentTerm] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [pk_AcademicTermDetailsId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AttendanceMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttendanceMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClassRoomID] [int] NULL,
	[TeacherID] [int] NULL,
	[ChildID] [int] NULL,
	[AttendanceDate] [datetime] NULL,
	[IsPresent] [bit] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[IsSubmitted] [bit] NULL DEFAULT ((0)),
 CONSTRAINT [PK_AttendanceMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[AttendanceMasterArchive]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AttendanceMasterArchive](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NULL,
	[AttendanceDate] [datetime] NULL,
	[IsPresent] [bit] NULL,
	[IsSubmitted] [bit] NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [pk_attendancemasterarchive_id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChildrenMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ChildrenMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[ParentName] [varchar](50) NULL,
	[CommunityWorkerID] [int] NULL,
	[Email] [varchar](50) NULL,
	[Image] [varchar](max) NULL,
	[SchoolID] [int] NULL,
	[ClassroomID] [int] NULL,
	[Address] [varchar](50) NULL,
	[Phone] [varchar](50) NULL,
	[Note] [varchar](50) NULL,
	[GeoLocation] [varchar](100) NULL,
	[Gender] [char](1) NULL,
	[ParentPhone] [varchar](50) NULL,
	[Middlename] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[IsActive] [bit] NULL DEFAULT ((1)),
	[Note_Indonesian] [nvarchar](max) NULL,
 CONSTRAINT [PK_ChildrenMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClassMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClassMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClassName] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[SchoolId] [int] NULL,
	[IsActive] [bit] NULL DEFAULT ((1)),
 CONSTRAINT [PK_ClassMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FeedBack]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FeedBack](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Content] [varchar](500) NOT NULL,
	[CreatedOn] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[IsActive] [bit] NULL DEFAULT ((1)),
	[Content_Indonesian] [nvarchar](max) NULL,
 CONSTRAINT [pk_Feedback] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[FirstAidAnalytics]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FirstAidAnalytics](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VisitorId] [int] NOT NULL,
	[VisitedOn] [datetime] NOT NULL,
 CONSTRAINT [pk_FirstAidAnalyticsId] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FirstAidMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FirstAidMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](max) NULL,
	[Content] [varchar](max) NULL,
	[ContentIndian] [varchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[IsActive] [bit] NULL DEFAULT ((1)),
 CONSTRAINT [PK_SickChildReasonMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReasonMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReasonMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReasonType] [int] NULL,
	[Description] [varchar](200) NULL,
	[DescriptionIndian] [varchar](200) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL CONSTRAINT [DF__ReasonMas__Creat__123EB7A3]  DEFAULT (NULL),
	[IsActive] [bit] NULL CONSTRAINT [DF__ReasonMas__IsAct__540C7B00]  DEFAULT ((1)),
	[Description_Indonesian] [varchar](200) NULL,
 CONSTRAINT [PK_ResonMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReasonType]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ReasonType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Description] [varchar](50) NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[IsActive] [bit] NULL CONSTRAINT [DF_ReasonType_IsActive]  DEFAULT ((1)),
	[Description_Indonesian] [varchar](200) NULL,
 CONSTRAINT [pk_ReasonType] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Schedular_Log]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Schedular_Log](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ErrorNumber] [int] NULL,
	[Description] [varchar](max) NULL,
	[ErrorMessage] [varchar](max) NULL,
	[CreatedOn] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SchoolAdminMapping]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SchoolAdminMapping](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SchoolId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[IsActive] [bit] NULL DEFAULT ((1)),
 CONSTRAINT [pk_SchoolAdminMapping] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SchoolMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SchoolMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[SchoolName] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Image] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Address] [varchar](max) NULL,
	[Country] [varchar](50) NULL,
	[Phone1] [varchar](50) NULL,
	[Phone2] [varchar](50) NULL,
	[Note] [varchar](max) NULL,
	[GeoLocation] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[IsActive] [bit] NULL DEFAULT ((1)),
	[Note_Indonesian] [nvarchar](max) NULL,
 CONSTRAINT [PK_SchoolMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TeacherClassMapping]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TeacherClassMapping](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TeacherID] [int] NULL,
	[ClassRoomID] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[IsActive] [bit] NULL DEFAULT ((1)),
 CONSTRAINT [PK_TeacherClassMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_TeacherClassMapping] UNIQUE NONCLUSTERED 
(
	[ClassRoomID] ASC,
	[IsActive] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[Name] [varchar](50) NULL,
	[Surname] [varchar](50) NULL,
	[Image] [varchar](50) NULL,
	[UserType] [int] NULL,
	[Address] [nvarchar](max) NULL,
	[Phone] [varchar](50) NULL,
	[Note] [nvarchar](max) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[IsActive] [bit] NULL DEFAULT ((1)),
	[GeoLocation] [varchar](100) NULL,
	[Gender] [char](1) NULL,
	[Email] [varchar](100) NULL,
	[Note_Indonesian] [nvarchar](max) NULL,
 CONSTRAINT [PK_UserMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_UserMasterUserName] UNIQUE NONCLUSTERED 
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserTypeMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserTypeMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserType] [varchar](50) NULL,
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL DEFAULT (NULL),
	[IsActive] [bit] NULL DEFAULT ((1)),
	[UserType_Indonesian] [varchar](100) NULL,
 CONSTRAINT [PK_UserTypeMaster] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[YearlyHolidayMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[YearlyHolidayMaster](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[HolidayDate] [datetime] NOT NULL,
	[AcademicYear] [int] NOT NULL,
	[IsActive] [bit] NOT NULL DEFAULT ((1)),
	[CreatedBy] [int] NULL,
	[CreatedOn] [datetime] NULL,
	[SchoolId] [int] NULL,
	[Reason] [varchar](100) NULL,
	[Reason_Indonesian] [nvarchar](200) NULL,
 CONSTRAINT [pk_YearlyHolidayMasterId] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [HangFire].[AggregatedCounter]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[AggregatedCounter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [bigint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_CounterAggregated] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Counter]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Counter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [smallint] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Counter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Hash]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Hash](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Field] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime2](7) NULL,
 CONSTRAINT [PK_HangFire_Hash] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Job]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Job](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StateId] [int] NULL,
	[StateName] [nvarchar](20) NULL,
	[InvocationData] [nvarchar](max) NOT NULL,
	[Arguments] [nvarchar](max) NOT NULL,
	[CreatedAt] [datetime] NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Job] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[JobParameter]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobParameter](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Name] [nvarchar](40) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_JobParameter] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[JobQueue]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[JobQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Queue] [nvarchar](50) NOT NULL,
	[FetchedAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_JobQueue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[List]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[List](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Value] [nvarchar](max) NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_List] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Schema]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Schema](
	[Version] [int] NOT NULL,
 CONSTRAINT [PK_HangFire_Schema] PRIMARY KEY CLUSTERED 
(
	[Version] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Server]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Server](
	[Id] [nvarchar](100) NOT NULL,
	[Data] [nvarchar](max) NULL,
	[LastHeartbeat] [datetime] NOT NULL,
 CONSTRAINT [PK_HangFire_Server] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[Set]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[Set](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](100) NOT NULL,
	[Score] [float] NOT NULL,
	[Value] [nvarchar](256) NOT NULL,
	[ExpireAt] [datetime] NULL,
 CONSTRAINT [PK_HangFire_Set] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [HangFire].[State]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [HangFire].[State](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[JobId] [int] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Reason] [nvarchar](100) NULL,
	[CreatedAt] [datetime] NOT NULL,
	[Data] [nvarchar](max) NULL,
 CONSTRAINT [PK_HangFire_State] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UX_HangFire_CounterAggregated_Key]    Script Date: 19/02/2019 19:49:55 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_CounterAggregated_Key] ON [HangFire].[AggregatedCounter]
(
	[Key] ASC
)
INCLUDE ( 	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Counter_Key]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Counter_Key] ON [HangFire].[Counter]
(
	[Key] ASC
)
INCLUDE ( 	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Hash_ExpireAt]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_ExpireAt] ON [HangFire].[Hash]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Hash_Key]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Hash_Key] ON [HangFire].[Hash]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UX_HangFire_Hash_Key_Field]    Script Date: 19/02/2019 19:49:55 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_Hash_Key_Field] ON [HangFire].[Hash]
(
	[Key] ASC,
	[Field] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Job_ExpireAt]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_ExpireAt] ON [HangFire].[Job]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Job_StateName]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Job_StateName] ON [HangFire].[Job]
(
	[StateName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_JobParameter_JobIdAndName]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_JobParameter_JobIdAndName] ON [HangFire].[JobParameter]
(
	[JobId] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_JobQueue_QueueAndFetchedAt]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_JobQueue_QueueAndFetchedAt] ON [HangFire].[JobQueue]
(
	[Queue] ASC,
	[FetchedAt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_List_ExpireAt]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_ExpireAt] ON [HangFire].[List]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_List_Key]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_List_Key] ON [HangFire].[List]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt],
	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_Set_ExpireAt]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_ExpireAt] ON [HangFire].[Set]
(
	[ExpireAt] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_HangFire_Set_Key]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_Set_Key] ON [HangFire].[Set]
(
	[Key] ASC
)
INCLUDE ( 	[ExpireAt],
	[Value]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [UX_HangFire_Set_KeyAndValue]    Script Date: 19/02/2019 19:49:55 ******/
CREATE UNIQUE NONCLUSTERED INDEX [UX_HangFire_Set_KeyAndValue] ON [HangFire].[Set]
(
	[Key] ASC,
	[Value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_HangFire_State_JobId]    Script Date: 19/02/2019 19:49:55 ******/
CREATE NONCLUSTERED INDEX [IX_HangFire_State_JobId] ON [HangFire].[State]
(
	[JobId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AbsenteeReasonDetails_BAK] ADD  DEFAULT (NULL) FOR [CreatedOn]
GO
ALTER TABLE [dbo].[AttendanceMasterArchive] ADD  DEFAULT ((0)) FOR [IsSubmitted]
GO
ALTER TABLE [dbo].[AbsenteeReasonDetails]  WITH CHECK ADD  CONSTRAINT [fk_AbsenteeReasonDetailsChildId] FOREIGN KEY([ChildID])
REFERENCES [dbo].[ChildrenMaster] ([ID])
GO
ALTER TABLE [dbo].[AbsenteeReasonDetails] CHECK CONSTRAINT [fk_AbsenteeReasonDetailsChildId]
GO
ALTER TABLE [dbo].[AbsenteeReasonDetails]  WITH CHECK ADD  CONSTRAINT [fk_AbsenteeReasonDetailsCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[AbsenteeReasonDetails] CHECK CONSTRAINT [fk_AbsenteeReasonDetailsCreatedBy]
GO
ALTER TABLE [dbo].[AbsenteeReasonDetails]  WITH CHECK ADD  CONSTRAINT [fk_AbsenteeReasonDetailsTeacherId] FOREIGN KEY([TeacherID])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[AbsenteeReasonDetails] CHECK CONSTRAINT [fk_AbsenteeReasonDetailsTeacherId]
GO
ALTER TABLE [dbo].[AcademicTermDetails]  WITH CHECK ADD  CONSTRAINT [fk_AcademicTermDetails_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[AcademicTermDetails] CHECK CONSTRAINT [fk_AcademicTermDetails_CreatedBy]
GO
ALTER TABLE [dbo].[AcademicTermDetails]  WITH CHECK ADD  CONSTRAINT [fk_AcademicTermDetails_SchoolId] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([ID])
GO
ALTER TABLE [dbo].[AcademicTermDetails] CHECK CONSTRAINT [fk_AcademicTermDetails_SchoolId]
GO
ALTER TABLE [dbo].[AttendanceMaster]  WITH NOCHECK ADD  CONSTRAINT [fk_AttendanceMasterChildId] FOREIGN KEY([ChildID])
REFERENCES [dbo].[ChildrenMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMaster] CHECK CONSTRAINT [fk_AttendanceMasterChildId]
GO
ALTER TABLE [dbo].[AttendanceMaster]  WITH NOCHECK ADD  CONSTRAINT [fk_AttendanceMasterClassRoomId] FOREIGN KEY([ClassRoomID])
REFERENCES [dbo].[ClassMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMaster] CHECK CONSTRAINT [fk_AttendanceMasterClassRoomId]
GO
ALTER TABLE [dbo].[AttendanceMaster]  WITH NOCHECK ADD  CONSTRAINT [fk_AttendanceMasterCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMaster] CHECK CONSTRAINT [fk_AttendanceMasterCreatedBy]
GO
ALTER TABLE [dbo].[AttendanceMaster]  WITH NOCHECK ADD  CONSTRAINT [fk_AttendanceMasterTeacherId] FOREIGN KEY([TeacherID])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMaster] CHECK CONSTRAINT [fk_AttendanceMasterTeacherId]
GO
ALTER TABLE [dbo].[AttendanceMasterArchive]  WITH CHECK ADD  CONSTRAINT [fk_AttendanceMasterArchiveChildId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[ChildrenMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMasterArchive] CHECK CONSTRAINT [fk_AttendanceMasterArchiveChildId]
GO
ALTER TABLE [dbo].[AttendanceMasterArchive]  WITH CHECK ADD  CONSTRAINT [fk_AttendanceMasterArchiveCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMasterArchive] CHECK CONSTRAINT [fk_AttendanceMasterArchiveCreatedBy]
GO
ALTER TABLE [dbo].[AttendanceMasterArchive]  WITH CHECK ADD  CONSTRAINT [fk_AttendanceMasterArchiverCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMasterArchive] CHECK CONSTRAINT [fk_AttendanceMasterArchiverCreatedBy]
GO
ALTER TABLE [dbo].[AttendanceMasterArchive]  WITH CHECK ADD  CONSTRAINT [fk_AttenMstArchiveCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMasterArchive] CHECK CONSTRAINT [fk_AttenMstArchiveCreatedBy]
GO
ALTER TABLE [dbo].[AttendanceMasterArchive]  WITH CHECK ADD  CONSTRAINT [fk_AttenMstArchiveStudendId] FOREIGN KEY([StudentId])
REFERENCES [dbo].[ChildrenMaster] ([ID])
GO
ALTER TABLE [dbo].[AttendanceMasterArchive] CHECK CONSTRAINT [fk_AttenMstArchiveStudendId]
GO
ALTER TABLE [dbo].[ChildrenMaster]  WITH CHECK ADD  CONSTRAINT [fk_ChildrenMasterClassRoomId] FOREIGN KEY([ClassroomID])
REFERENCES [dbo].[ClassMaster] ([ID])
GO
ALTER TABLE [dbo].[ChildrenMaster] CHECK CONSTRAINT [fk_ChildrenMasterClassRoomId]
GO
ALTER TABLE [dbo].[ChildrenMaster]  WITH CHECK ADD  CONSTRAINT [fk_ChildrenMasterCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[ChildrenMaster] CHECK CONSTRAINT [fk_ChildrenMasterCreatedBy]
GO
ALTER TABLE [dbo].[ChildrenMaster]  WITH CHECK ADD  CONSTRAINT [fk_ChildrenMasterSchoolId] FOREIGN KEY([SchoolID])
REFERENCES [dbo].[SchoolMaster] ([ID])
GO
ALTER TABLE [dbo].[ChildrenMaster] CHECK CONSTRAINT [fk_ChildrenMasterSchoolId]
GO
ALTER TABLE [dbo].[ClassMaster]  WITH CHECK ADD  CONSTRAINT [fk_ClassMasterCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[ClassMaster] CHECK CONSTRAINT [fk_ClassMasterCreatedBy]
GO
ALTER TABLE [dbo].[ClassMaster]  WITH CHECK ADD  CONSTRAINT [fk_ClassMasterSchoolId] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([ID])
GO
ALTER TABLE [dbo].[ClassMaster] CHECK CONSTRAINT [fk_ClassMasterSchoolId]
GO
ALTER TABLE [dbo].[FeedBack]  WITH CHECK ADD  CONSTRAINT [fk_FeedBackCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[FeedBack] CHECK CONSTRAINT [fk_FeedBackCreatedBy]
GO
ALTER TABLE [dbo].[FirstAidAnalytics]  WITH CHECK ADD  CONSTRAINT [fk_FirstAidAnalytics_VisitorId] FOREIGN KEY([VisitorId])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[FirstAidAnalytics] CHECK CONSTRAINT [fk_FirstAidAnalytics_VisitorId]
GO
ALTER TABLE [dbo].[FirstAidMaster]  WITH CHECK ADD  CONSTRAINT [fk_FirstAidMasterCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[FirstAidMaster] CHECK CONSTRAINT [fk_FirstAidMasterCreatedBy]
GO
ALTER TABLE [dbo].[ReasonMaster]  WITH CHECK ADD  CONSTRAINT [fk_ReasonMasterCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[ReasonMaster] CHECK CONSTRAINT [fk_ReasonMasterCreatedBy]
GO
ALTER TABLE [dbo].[ReasonMaster]  WITH CHECK ADD  CONSTRAINT [fk_ReasonMasterReasonType] FOREIGN KEY([ReasonType])
REFERENCES [dbo].[ReasonType] ([Id])
GO
ALTER TABLE [dbo].[ReasonMaster] CHECK CONSTRAINT [fk_ReasonMasterReasonType]
GO
ALTER TABLE [dbo].[ReasonType]  WITH CHECK ADD  CONSTRAINT [fk_ReasonTypeCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[ReasonType] CHECK CONSTRAINT [fk_ReasonTypeCreatedBy]
GO
ALTER TABLE [dbo].[SchoolAdminMapping]  WITH CHECK ADD  CONSTRAINT [fk_SchoolAdminMappingCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[SchoolAdminMapping] CHECK CONSTRAINT [fk_SchoolAdminMappingCreatedBy]
GO
ALTER TABLE [dbo].[SchoolAdminMapping]  WITH CHECK ADD  CONSTRAINT [fk_SchoolAdminMappingShoolId] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([ID])
GO
ALTER TABLE [dbo].[SchoolAdminMapping] CHECK CONSTRAINT [fk_SchoolAdminMappingShoolId]
GO
ALTER TABLE [dbo].[SchoolAdminMapping]  WITH CHECK ADD  CONSTRAINT [fk_SchoolAdminMappingUserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[SchoolAdminMapping] CHECK CONSTRAINT [fk_SchoolAdminMappingUserId]
GO
ALTER TABLE [dbo].[SchoolMaster]  WITH CHECK ADD  CONSTRAINT [fk_SchoolMasterCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[SchoolMaster] CHECK CONSTRAINT [fk_SchoolMasterCreatedBy]
GO
ALTER TABLE [dbo].[TeacherClassMapping]  WITH CHECK ADD  CONSTRAINT [fk_TeacherClassMappingCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[TeacherClassMapping] CHECK CONSTRAINT [fk_TeacherClassMappingCreatedBy]
GO
ALTER TABLE [dbo].[UserMaster]  WITH CHECK ADD  CONSTRAINT [fk_UserMasterCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[UserMaster] CHECK CONSTRAINT [fk_UserMasterCreatedBy]
GO
ALTER TABLE [dbo].[UserMaster]  WITH CHECK ADD  CONSTRAINT [fk_UserMasterUserType] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[UserMaster] CHECK CONSTRAINT [fk_UserMasterUserType]
GO
ALTER TABLE [dbo].[UserTypeMaster]  WITH CHECK ADD  CONSTRAINT [fk_UserTypeMasterCreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[UserTypeMaster] CHECK CONSTRAINT [fk_UserTypeMasterCreatedBy]
GO
ALTER TABLE [dbo].[YearlyHolidayMaster]  WITH CHECK ADD  CONSTRAINT [fk_YearlyHolidayMaster_CreatedBy] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[UserMaster] ([ID])
GO
ALTER TABLE [dbo].[YearlyHolidayMaster] CHECK CONSTRAINT [fk_YearlyHolidayMaster_CreatedBy]
GO
ALTER TABLE [dbo].[YearlyHolidayMaster]  WITH CHECK ADD  CONSTRAINT [fk_YearlyHolidayMaster_SchoolId] FOREIGN KEY([SchoolId])
REFERENCES [dbo].[SchoolMaster] ([ID])
GO
ALTER TABLE [dbo].[YearlyHolidayMaster] CHECK CONSTRAINT [fk_YearlyHolidayMaster_SchoolId]
GO
ALTER TABLE [HangFire].[JobParameter]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_JobParameter_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[JobParameter] CHECK CONSTRAINT [FK_HangFire_JobParameter_Job]
GO
ALTER TABLE [HangFire].[State]  WITH CHECK ADD  CONSTRAINT [FK_HangFire_State_Job] FOREIGN KEY([JobId])
REFERENCES [HangFire].[Job] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [HangFire].[State] CHECK CONSTRAINT [FK_HangFire_State_Job]
GO
/****** Object:  StoredProcedure [dbo].[AcademicCalender]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[AcademicCalender]
	@ID int,
	@Reason varchar(50),
	@SchoolId int,
	@HolidayDate DATETIME,
	@AcademicYear int,
	@IsActive bit,
	@CreatedBy int,
	@CreatedOn DATETIME,
	@Reason_Indonesian NVARCHAR(200)
	
AS
BEGIN
	
	SELECT @ID = ID FROM YearlyHolidayMaster WHERE CAST(HolidayDate AS DATE) = @HolidayDate;

	IF @ID>0
		BEGIN
			UPDATE YearlyHolidayMaster SET Reason=@Reason, SchoolId = @SchoolId,HolidayDate=@HolidayDate,AcademicYear=@AcademicYear, CreatedBy = @CreatedBy,CreatedOn=@CreatedOn
			, IsActive = 1,Reason_Indonesian = @Reason_Indonesian WHERE ID = @ID
		END
	ELSE
		BEGIN
			INSERT INTO YearlyHolidayMaster
			(Reason,SchoolId,HolidayDate,AcademicYear,CreatedBy,CreatedOn,IsActive,Reason_Indonesian)
			Values
			(@Reason,@SchoolId,@HolidayDate,@AcademicYear, @CreatedBy,@CreatedOn,@IsActive,@Reason_Indonesian)
		END
END






GO
/****** Object:  StoredProcedure [dbo].[AdminDailyReport]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[AdminDailyReport]
	
	@AttendanceDate DATE,
	@SchoolID int,
	@ClassRoomID int,
	@Type TINYINT,
	@TeacherId INT

	AS
	BEGIN
		Declare @MonthStartDate date, 
				@MonthEndDate date, 
				@YearStartDate Date, 
				@YearEndDate Date,
				@userType int
				
		select @userType = (select UserType from UserMaster where ID = @TeacherId AND IsActive = 1)
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)


		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

IF @userType = 1
BEGIN

	IF @Type = 0
		BEGIN
			select AM.ClassRoomId, CL.ClassName,
			count(case when AM.IsPresent = 1 then 1 end ) as Present,
			count(case when AM.IsPresent = 0 then 0 end ) as 'Absent'
			from
			AttendanceMaster AM
			inner join dbo.GetClassList(@TeacherId) CL on AM.ClassRoomId = CL.ID
			where convert(date,AM.AttendanceDate) = @AttendanceDate
			group by AM.ClassRoomId,CL.ClassName
		END

	ELSE IF @Type = 1
		BEGIN 
		select A.ClassRoomId, A.ClassName,
		 CONVERT(decimal(5,2),(sum(A.Present)/CONVERT(decimal(16,3), nullif(sum(A.TotalDays),0)) * 100)) as PresentPercent,
		 CONVERT(decimal(5,2),(sum(A.Absent)/CONVERT(decimal(16,3), nullif(sum(A.TotalDays),0)) * 100)) as 'AbsentPercent'
			from
			(
			select AM.ClassRoomId, CL.ClassName, AM.ChildID, count(AM.ChildID) TotalDays,
						count(case when AM.IsPresent = 1 then 1 end ) as Present,
						count(case when AM.IsPresent = 0 then 0 end ) as 'Absent'
						from
						AttendanceMaster AM
						inner join dbo.GetClassList(@TeacherId) CL on AM.ClassRoomId = CL.ID
						where convert(date,AM.AttendanceDate) between @MonthStartDate and @MonthEndDate
						group by AM.ClassRoomId,CL.ClassName,AM.ChildID
						)A
				group by A.ClassRoomId,A.ClassName
		END

	ELSE IF @Type = 2
		BEGIN 		 

		 select B.AttendanceMon, B.AttendanceMonth AttendanceMonthName, B.AttendanceYear,
			sum(B.TotalDays) TotalDays, sum(B.Present) Present, sum(B.Absent)'Absent',
			CONVERT(decimal(5,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalDays),0)) * 100)) as PresentPercent,
			CONVERT(decimal(5,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalDays),0)) * 100)) as AbsentPercent
			from
			(
				select A.ClassRoomID,A.ClassName, A.ID ChildId, A.Name,	A.AttendanceMon, A.AttendanceMonth,	
				count(A.AttendanceMon) as TotalDays, A.AttendanceYear,
				count(case when A.IsPresent = 1 then 1 end) as Present,
				count(case when A.IsPresent = 0 then 0 end) as 'Absent'
				from
				(
					select AM.ClassRoomID,CM.ClassName, CHM.ID, (CHM.Name + ' ' + CHM.Surname) Name, DATEPART(m, AM.AttendanceDate)										AttendanceMon, AM.IsPresent, Convert(char(3), AM.AttendanceDate, 0) AttendanceMonth, year(AM.AttendanceDate) AttendanceYear
					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
					inner join ChildrenMaster CHM on AM.ChildID = CHM.ID		
					inner join dbo.GetClassList(@TeacherId) CL on AM.ClassRoomId = CL.ID			
					WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate AND CHM.IsActive = 1
					)A
					Group By A.ClassRoomID,A.ClassName , A.ID, A.Name, A.AttendanceMon, A.AttendanceMonth, A.AttendanceYear
			)B
		group by B.AttendanceMon, B.AttendanceMonth, B.AttendanceYear
		order by B.AttendanceYear,B.AttendanceMon	 


		END
END

ELSE IF @userType = 4
BEGIN

	IF @Type = 0
		BEGIN
			select AM.ClassRoomId, CL.ClassName,
			count(case when AM.IsPresent = 1 then 1 end ) as Present,
			count(case when AM.IsPresent = 0 then 0 end ) as 'Absent'
			from
			AttendanceMaster AM
			inner join dbo.GetClassListForSchool(@SchoolID) CL on AM.ClassRoomId = CL.ID
			where convert(date,AM.AttendanceDate) = @AttendanceDate
			group by AM.ClassRoomId,CL.ClassName
			order by CL.ClassName
		END

	ELSE IF @Type = 1
		BEGIN 
		select A.ClassRoomId, A.ClassName,
		 CONVERT(decimal(5,2),(sum(A.Present)/CONVERT(decimal(16,3), nullif(sum(A.TotalDays),0)) * 100)) as PresentPercent,
		 CONVERT(decimal(5,2),(sum(A.Absent)/CONVERT(decimal(16,3), nullif(sum(A.TotalDays),0)) * 100)) as 'AbsentPercent'
			from
			(
			select AM.ClassRoomId, CL.ClassName, AM.ChildID, count(AM.ChildID) TotalDays,
						count(case when AM.IsPresent = 1 then 1 end ) as Present,
						count(case when AM.IsPresent = 0 then 0 end ) as 'Absent'
						from
						AttendanceMaster AM
						inner join dbo.GetClassListForSchool(@SchoolID) CL on AM.ClassRoomId = CL.ID
						where convert(date,AM.AttendanceDate) between @MonthStartDate and @MonthEndDate
						group by AM.ClassRoomId,CL.ClassName,AM.ChildID
						)A
				group by A.ClassRoomId,A.ClassName
				order by A.ClassName
		END

	ELSE IF @Type = 2
		BEGIN 		 

		 select B.AttendanceMon, B.AttendanceMonth AttendanceMonthName, B.AttendanceYear,
			sum(B.TotalDays) TotalDays, sum(B.Present) Present, sum(B.Absent)'Absent',
			CONVERT(decimal(5,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalDays),0)) * 100)) as PresentPercent,
			CONVERT(decimal(5,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalDays),0)) * 100)) as AbsentPercent
			from
			(
				select A.ClassRoomID,A.ClassName, A.ID ChildId, A.Name,	A.AttendanceMon, A.AttendanceMonth,	
				count(A.AttendanceMon) as TotalDays, A.AttendanceYear,
				count(case when A.IsPresent = 1 then 1 end) as Present,
				count(case when A.IsPresent = 0 then 0 end) as 'Absent'
				from
				(
					select AM.ClassRoomID,CM.ClassName, CHM.ID, (CHM.Name + ' ' + CHM.Surname) Name, DATEPART(m, AM.AttendanceDate)										AttendanceMon, AM.IsPresent, Convert(char(3), AM.AttendanceDate, 0) AttendanceMonth, year(AM.AttendanceDate) AttendanceYear
					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
					inner join ChildrenMaster CHM on AM.ChildID = CHM.ID		
					inner join dbo.GetClassListForSchool(@SchoolID) CL on AM.ClassRoomId = CL.ID			
					WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate AND CHM.IsActive = 1
					)A
					Group By A.ClassRoomID,A.ClassName , A.ID, A.Name, A.AttendanceMon, A.AttendanceMonth, A.AttendanceYear
			)B
		group by B.AttendanceMon, B.AttendanceMonth, B.AttendanceYear
		order by B.AttendanceYear,B.AttendanceMon	 


		END
END


END


	/*
	
	EXEC AdminDailyReport @AttendanceDate ='2018-11-14',
	@SchoolID=11,
	@ClassRoomID = 48,
	@Type = 2,
	@TeacherId = 115

	*/
GO
/****** Object:  StoredProcedure [dbo].[GetAdminMappingDetailsForSchool]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetAdminMappingDetailsForSchool]	
	@SchoolId varchar(50)	

AS
BEGIN
	SELECT UM.ID,UM.Name,UM.Surname from  SchoolAdminMapping SAM
	INNER JOIN UserMaster UM on UM.ID = SAM.UserId
	where schoolId = @SchoolId 
END



GO
/****** Object:  StoredProcedure [dbo].[GetAllChildrenByFilter]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAllChildrenByFilter]
	@UserType int,
	@SchoolID int,
	@ClassRoomID int,
	@UserID int,
	@LangID CHAR(3)
AS
BEGIN

		DECLARE @TermStartDate DATE
		DECLARE @TermEndDate DATE
		DECLARE @WorkingDays INT

		SELECT @TermStartDate = Start_Date,@TermEndDate = End_Date FROM AcademicTermDetails WHERE SchoolId = @SchoolID  AND IsCurrentTerm = 1

SELECT @WorkingDays = dbo.Fn_GetNumberOfWorkingDays(@SChoolID,ISNULL(@TermStartDate,GETDATE()),GETDATE()) 


	IF (@UserType =  1) --admin
		BEGIN			
			
					    SELECT CM.ID, CM.Name, ISNULL(CM.Middlename,'') as					
						Middlename,CM.Surname,CM.ParentName,CM.CommunityWorkerID, CM.Email,CM.SchoolID,CM.ClassroomID,CM.Address,						CM.Image, CM.Phone, '' as ImageBase, CM.GeoLocation,CM.Gender, UM.Name as					
						CommunityWorkerName,SM.SchoolName,ClassMaster.ClassName as ClassroomName ,
						AttendanceSummary.PresentCount,						
						ISNULL((CAST(AttendanceSummary.PresentCount AS float) / CAST(@WorkingDays AS float) * 100),0) As AttendancePercent	
						,ISNULL(CASE WHEN @LangID = 'id' THEN CM.Note_Indonesian ELSE CM.Note END
		,'') AS Note					
						FROM  ChildrenMaster CM 

						LEFT JOIN (
							 SELECT ChildID,count(IsPresent)  as PresentCount
							 FROM AttendanceMaster 
							 WHERE AttendanceDate BETWEEN @TermStartDate AND @TermEndDate 
								AND  IsPresent = 1
							 GROUP BY ChildID
						) AS AttendanceSummary on AttendanceSummary.ChildID = CM.ID

                        Left join UserMaster UM on UM.ID = CM.CommunityWorkerID
                        LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID 
                        LEFT JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID
                        WHERE CM.SchoolID =  @SchoolID AND CM.ClassRoomID = @ClassRoomID AND CM.IsActive = 1 ORDER BY CM.Name
				
		END

	ELSE IF (@UserType =  2) --Teacher
		BEGIN
			
					SELECT CM.ID, CM.Name, ISNULL(CM.Middlename,'') as Middlename,CM.Surname,CM.ParentName,CM.CommunityWorkerID, 
					CM.Email,CM.SchoolID,CM.ClassroomID,CM.Address, CM.Image, 
					CM.Phone, '' as ImageBase, CM.GeoLocation,CM.Gender, UM.Name as	
					CommunityWorkerName,SM.SchoolName,ClassMaster.ClassName as ClassroomName ,

					AttendanceSummary.PresentCount,						
							ISNULL((CAST(AttendanceSummary.PresentCount AS float) / CAST(@WorkingDays AS float) * 100),0) As AttendancePercent
							,ISNULL(CASE WHEN @LangID = 'id' THEN CM.Note_Indonesian ELSE CM.Note END
		,'') AS Note								
						FROM  ChildrenMaster CM 

						LEFT JOIN (
							 SELECT ChildID,count(IsPresent)  as PresentCount
							 FROM AttendanceMaster 
							 WHERE AttendanceDate BETWEEN @TermStartDate AND @TermEndDate 
								AND  IsPresent = 1
							 GROUP BY ChildID
						) AS AttendanceSummary on AttendanceSummary.ChildID = CM.ID

					LEFT JOIN UserMaster UM on UM.ID = @UserID
					LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID 
					LEFT JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID 
					INNER JOIN TeacherClassMapping tcm on CM.ClassroomID = tcm.ClassroomID AND tcm.TeacherID = @UserID 
					WHERE 
					 CM.SchoolID =  @SchoolID AND CM.ClassRoomID = @ClassRoomID AND CM.IsActive = 1 ORDER BY CM.Name					
			
			
		END

		ELSE IF (@UserType =  4) --SuperAdmin
		BEGIN
				SELECT CM.ID, CM.Name, ISNULL(CM.Middlename,'') as Middlename,CM.Surname,CM.ParentName,CM.CommunityWorkerID, 
					CM.Email,CM.SchoolID,CM.ClassroomID,CM.Address, CM.Image, 
					CM.Phone, '' as ImageBase, CM.GeoLocation,CM.Gender, UM.Name as	
					CommunityWorkerName,SM.SchoolName,'' as ClassroomName ,

					AttendanceSummary.PresentCount,						
							ISNULL((CAST(AttendanceSummary.PresentCount AS float) / CAST(@WorkingDays AS float) * 100),0) As AttendancePercent
							,ISNULL(CASE WHEN @LangID = 'id' THEN CM.Note_Indonesian ELSE CM.Note END
		,'') AS Note							
						FROM  ChildrenMaster CM 

						LEFT JOIN (
							 SELECT ChildID,count(IsPresent)  as PresentCount
							 FROM AttendanceMaster 
							 WHERE AttendanceDate BETWEEN @TermStartDate AND @TermEndDate 
								AND  IsPresent = 1
							 GROUP BY ChildID
						) AS AttendanceSummary on AttendanceSummary.ChildID = CM.ID
					LEFT JOIN UserMaster UM on UM.ID = @UserID
					LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID 				
					WHERE CM.SchoolID =  @SchoolID AND CM.ClassRoomID = @ClassRoomID AND CM.IsActive = 1 ORDER BY CM.Name
		END
END


-- EXEC GetAllChildrenByFilter @UserType = 4,@SchoolID = 23 ,@ClassRoomID = 40, @UserID = 58




GO
/****** Object:  StoredProcedure [dbo].[GetAttendancereport]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAttendancereport] @STARTDATE DATETIME, 
                                     @ENDDATE   DATETIME ,@ClassRoomID INT
AS 
  BEGIN 
	
	
      WITH daterange 
           AS (SELECT DT =Dateadd(dd, 0, @STARTDATE) 
               WHERE  Dateadd(dd, 1, @STARTDATE) <= @ENDDATE 
               UNION ALL 
               SELECT Dateadd(dd, 1, dt) 
               FROM   daterange 
               WHERE  Dateadd(dd, 1, dt) <= @ENDDATE) 
      SELECT * 
      INTO   #tmp_dates 
      FROM   daterange 

	  	CREATE TABLE #TMP  
(  
   ChildID INT,  
   Name varchar(100),
   DATE DATETIME ,  
   PRESENT_STATUS VARCHAR(10)  
)  
		
		INSERT INTO #TMP (ChildID,Name, DATE, PRESENT_STATUS)
		select ChildID,
		(ChildrenMaster.Name+' '+ChildrenMaster.Surname) as Name,
		AttendanceDate,Case when IsPresent = 1 then 'P' else 'A' end
		from AttendanceMaster 
		LEFT JOIN ChildrenMaster on ChildrenMaster.ID = AttendanceMaster.ChildID
		where AttendanceDate between @STARTDATE and @ENDDATE
		and AttendanceMaster.ClassroomID =@ClassRoomID
		

      DECLARE @COLUMN VARCHAR(max) 

      SELECT @COLUMN = Isnull(@COLUMN+',', '') + '[' 
                       + Cast(CONVERT(DATE, T.dt) AS VARCHAR) + ']' 
      FROM   #tmp_dates T 

      DECLARE @Columns2 VARCHAR(max) 

      SET @Columns2 = Substring((SELECT DISTINCT ',ISNULL([' 
                                                 + Cast(CONVERT(DATE, dt) AS VARCHAR ) 
                                                 + '],''N/A'') AS [' 
                                                 + Cast(CONVERT(DATE, dt) AS 
                                                 VARCHAR ) 
                                                 + ']' 
                                 FROM   #tmp_dates 
                                 GROUP  BY dt 
                                 FOR xml path('')), 2, 8000) 

      DECLARE @QUERY VARCHAR(max) 

      SET @QUERY = 'SELECT ChildID,Name, ' + @Columns2 +' FROM   
(  
SELECT A.ChildID ,A.Name, B.DT AS DATE, A.PRESENT_STATUS FROM #TMP A LEFT  JOIN #TMP_DATES B ON A.DATE=B.DT   
) X  
PIVOT   
(  
MIN([PRESENT_STATUS])  
FOR [DATE] IN (' + @COLUMN + ')  
) P   WHERE ISNULL(NAME,'''')<>''''  
'  

      EXEC (@QUERY) 
	  
      DROP TABLE #tmp_dates 
	  drop table #TMP
  END 


GO
/****** Object:  StoredProcedure [dbo].[GetFeedBackList]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetFeedBackList]
	@SchoolID int,
	@StarDate DATETIME,
	@EndDate DATETIME,
	@LangID CHAR(3)
	
AS
BEGIN

	SET @StarDate = CAST(CAST(@StarDate AS DATE) AS DATETIME)
	SET @EndDate  = DATEADD(day, DATEDIFF(day, 0, @EndDate), 1)
	

	IF @SchoolID > 0
		BEGIN

			SELECT DISTINCT fb.id,fb.CreatedOn, fb.CreatedBy, (um.Name + ' '+ um.Surname) as Name
			,CASE WHEN @LangID = 'id' THEN fb.Content_Indonesian ELSE fb.Content END AS Content
			FROM FeedBack fb 
			LEFT JOIN UserMaster um on um.ID = fb.CreatedBY
			LEFT JOIN TeacherClassMapping tcm on tcm.TeacherID = fb.CreatedBY
			LEFT JOIN ClassMaster cm on cm.ID = tcm.ClassRoomID AND cm.SchoolId = @SchoolID
			WHERE fb.CreatedOn >= @StarDate AND fb.CreatedOn < @EndDate ORDER BY fb.CreatedOn DESC
			--WHERE Convert(date,fb.CreatedOn) >= @StarDate AND Convert(date,fb.CreatedOn) <= @EndDate 
			
		END

ELSE
		BEGIN
			SELECT DISTINCT fb.id,fb.CreatedOn, fb.CreatedBy, (um.Name + ' '+ um.Surname) as Name
			,CASE WHEN @LangID = 'id' THEN fb.Content_Indonesian ELSE fb.Content END AS Content
			FROM FeedBack fb 
			LEFT JOIN UserMaster um on um.ID = fb.CreatedBY
			LEFT JOIN TeacherClassMapping tcm on tcm.TeacherID = fb.CreatedBY			
			WHERE fb.CreatedOn >= @StarDate AND fb.CreatedOn < @EndDate ORDER BY fb.CreatedOn DESC
			--WHERE Convert(date,fb.CreatedOn) >= @StarDate AND Convert(date,fb.CreatedOn) <= @EndDate 
			
		END


END


--	Exec GetFeedBackList @SchoolID = 23, @StarDate='2019-01-31', @EndDate='2019-01-31',@LangID ='en'



GO
/****** Object:  StoredProcedure [dbo].[GetFirstAidCounterList]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetFirstAidCounterList]
	
AS
BEGIN

/*
  SELECT FM.ID,FM.CreatedBy,SM.SchoolName,FM.CreatedOn
  
  FROM  FirstAidMaster FM 
 
  LEFT JOIN TeacherClassMapping TM on FM.CreatedBy = TM.teacherid 
 
  LEFT JOIN ClassMaster CM on CM.ID = TM.ClassroomID  
 
  LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolId 
 
  WHERE FM.IsActive = 1 
	
	*/
	
	 SELECT FA.ID,FA.VisitorId,SM.SchoolName,FA.VisitedOn, ISNULL(UM.Name,'') As Name ,ISNULL(Um.SurName,'') As Surname
  
	 FROM  FirstAidAnalytics FA 
 
	 INNER JOIN TeacherClassMapping TM on FA.VisitorId = TM.teacherid 
 
	 INNER JOIN ClassMaster CM on CM.ID = TM.ClassroomID  
 
	 INNER JOIN SchoolMaster SM on SM.ID = CM.SchoolId 

	INNER JOIN UserMaster UM on UM.ID = FA.VisitorId 
					
	ORDER BY FA.VisitedOn DESC, UM.Name, SM.SchoolName ASC
END



-- EXEC GetFirstAidCounterList

GO
/****** Object:  StoredProcedure [dbo].[GetHolidayList]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetHolidayList]
	
	@AttendanceDate DATE,
	@ClassRoomID int,
	@Type TINYINT

AS
	BEGIN

	Declare @MonthStartDate date, 
				@MonthEndDate date, 
				@WeekStartDate Date, 
				@WeekEndDate Date, 
				@YearStartDate Date, 
				@YearEndDate Date
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
		select @WeekStartDate = DATEADD(DAY, 2 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE)) 
		select @WeekEndDate = DATEADD(DAY, 8 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE))
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		IF @Type = 1
		BEGIN

		select YHM.Id HolidyId, convert(date,HolidayDate) HolidayDate,YHM.Reason from 
		ClassMaster CM 
		join YearlyHolidayMaster YHM on CM.SchoolId = YHM.SchoolId and isnull(YHM.IsActive,0) = 1 
		and convert(date,YHM.HolidayDate) between @MonthStartDate and @MonthEndDate and CM.Id = @ClassRoomID	

		END

		ELSE IF @Type = 2
		BEGIN

		select YHM.Id HolidyId, convert(date,HolidayDate) HolidayDate,YHM.Reason from 
		ClassMaster CM 
		join YearlyHolidayMaster YHM on CM.SchoolId = YHM.SchoolId and isnull(YHM.IsActive,0) = 1 
		and convert(date,YHM.HolidayDate) between @WeekStartDate and @WeekEndDate and CM.Id = @ClassRoomID		

		END

		ELSE IF @Type = 3
		BEGIN

		select YHM.Id HolidyId, convert(date,HolidayDate) HolidayDate,YHM.Reason from 
		ClassMaster CM 
		join YearlyHolidayMaster YHM on CM.SchoolId = YHM.SchoolId and isnull(YHM.IsActive,0) = 1 
		and convert(date,YHM.HolidayDate) between @YearStartDate and @YearEndDate and CM.Id = @ClassRoomID	

		END

	END




GO
/****** Object:  StoredProcedure [dbo].[GetSchoolListForUser]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSchoolListForUser]
	@ID int,
	@LangID CHAR(3)
	
AS
BEGIN
	 DECLARE @UserType int
	 SELECT @UserType = UserType FROM UserMaster WHERE ID = @ID

	  IF(@UserType = 1) -- ADMIN
	  BEGIN

			SELECT sm.ID, sm.SchoolName,sm.Email,sm.Image,sm.City,sm.Address,sm.Country,sm.Phone1,sm.Phone2,sm.GeoLocation,sm.CreatedBy, sm.CreatedOn,sm.IsActive 
			,ISNULL(CASE WHEN @LangID = 'id' THEN sm.Note_Indonesian ELSE sm.Note END
		,'') AS Note
			from  SchoolAdminMapping AdminMap
			INNER JOIN  SchoolMaster sm  ON AdminMap.SchoolId = sm.ID AND sm.IsActive = 1
			WHERE AdminMap.UserId = @ID ORDER BY sm.SchoolName
				
	  END

    IF(@UserType = 2) -- Teacher
	BEGIN
	/*
		SELECT * FROM TeacherClassMapping tcm 
		INNER JOIN ClassMaster cm on cm.ID = tcm.ClassRoomID 
		INNER JOIN SchoolMaster sm on sm.ID = cm.SchoolId
		WHERE tcm.TeacherID = @ID
		*/
		
				SELECT DISTINCT sm.ID, 					
			sm.SchoolName,sm.Email,sm.Image,sm.City,sm.Address,sm.Country,sm.Phone1,sm.Phone2,sm.GeoLocation,sm.CreatedBy, sm.CreatedOn,sm.IsActive
			,ISNULL(CASE WHEN @LangID = 'id' THEN sm.Note_Indonesian ELSE sm.Note END
		,'') AS Note
			  FROM TeacherClassMapping tcm 
				INNER JOIN ClassMaster cm on cm.ID = tcm.ClassRoomID 
				INNER JOIN SchoolMaster sm on sm.ID = cm.SchoolId
				WHERE tcm.TeacherID = @ID ORDER BY sm.SchoolName

	END


	ELSE IF(@UserType = 4)  --Super Admin
	 BEGIN
		SELECT sm.ID, sm.SchoolName,sm.Email,sm.Image,sm.City,sm.Address,sm.Country,sm.Phone1,sm.Phone2,sm.GeoLocation,sm.CreatedBy, sm.CreatedOn,sm.IsActive
		,ISNULL(CASE WHEN @LangID = 'id' THEN sm.Note_Indonesian ELSE sm.Note END
		,'') AS Note
		 FROM SchoolMaster sm WHERE sm.IsActive = 1 ORDER BY sm.SchoolName
	 END

	  
	
END





-- EXEC GetSchoolListForUser 5, 'id'

GO
/****** Object:  StoredProcedure [dbo].[GetUsersByType]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetUsersByType]
	@UserType int
	
AS
BEGIN
	SELECT ID, Name, SurName,UserName FROM UserMaster WHERE UserType = @UserType AND IsActive=1 ORDER BY Name
	
END


--	exec GetUsersByType 1

GO
/****** Object:  StoredProcedure [dbo].[GetYearlyHealthReason]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetYearlyHealthReason]
@AttendanceDate DATE,
	@ClassRoomID int,
	@TeacherId INT,
	@Type TINYINT,
	@SchoolId INT,
	@LangID CHAR(3)

	AS
	BEGIN

	Declare @YearStartDate Date,
			@YearEndDate Date,
			@cols AS NVARCHAR(MAX),
			@query  AS NVARCHAR(MAX),
			@userType INT

			select @userType = (select UserType from UserMaster where id = @TeacherId and IsActive = 1)

			select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1 and SM.Id = @SchoolId										where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

			select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1 and SM.Id = @SchoolId										where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))


		SELECT @cols = STUFF((SELECT  ',' + QUOTENAME(CASE WHEN @LangID = 'id' THEN isnull(Description_Indonesian,'DI') ELSE isnull(Description,'D') END) 
                    FROM ReasonMaster where ReasonType=1 group by CASE WHEN @LangID = 'id' THEN isnull(Description_Indonesian,'DI') ELSE isnull							(Description,'D') END
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')


IF @userType = 1

BEGIN
		SELECT @query = 
		'select * from
		(select A.AttendanceMon, A.AttendanceMonthName,
				A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount				
					from
					(
					select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate, CHM.ID ChildID,
								CASE WHEN ''' + @LangID + ''' = ''id'' THEN isnull(RM.Description_Indonesian,''DI'') ELSE isnull(RM.Description,''D'') END Description, RM.ReasonType, RT.Code, DATEPART(m, AM.AttendanceDate) AttendanceMon, 
								Convert(char(3), AM.AttendanceDate, 0) AttendanceMonthName
									from AttendanceMaster AM 
									inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
									inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
									inner join dbo.GetClassList(''' + CAST(@TeacherId AS VARCHAR) + ''') CL on CL.Id = AM.ClassRoomID
									left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
									left join ReasonMaster RM on RM.ID = ARD. ReasonID
									left join ReasonType RT on RT.Id = RM.ReasonType 
									WHERE Convert(date,AM.AttendanceDate) between ''' + CAST(@YearStartDate AS VARCHAR) + ''' AND ''' + CAST(@YearEndDate AS VARCHAR) + '''
									and AM.IsPresent = 0 and RT.Code = ''Health'' AND CHM.IsActive = 1
					)A
					group by A.AttendanceMon, A.AttendanceMonthName,A.Description, A.ReasonType, A.Code

			
					) x

		PIVOT 
		(
			AVG(ReasonCount)			
			for Description in (' + @cols + ')
		) P
		order by AttendanceMon'

END

ELSE IF @userType = 4

BEGIN
		SELECT @query = 
		'select * from
		(select A.AttendanceMon, A.AttendanceMonthName,
				A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount				
					from
					(
					select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate, CHM.ID ChildID,
								CASE WHEN ''' + @LangID + ''' = ''id'' THEN isnull(RM.Description_Indonesian,''DI'') ELSE isnull(RM.Description,''D'') END Description, RM.ReasonType, RT.Code, DATEPART(m, AM.AttendanceDate) AttendanceMon, 
								Convert(char(3), AM.AttendanceDate, 0) AttendanceMonthName
									from AttendanceMaster AM 
									inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
									inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
									inner join dbo.GetClassListForSchool(''' + CAST(@SchoolId AS VARCHAR) + ''') CL on CL.Id = AM.ClassRoomID
									left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
									left join ReasonMaster RM on RM.ID = ARD. ReasonID
									left join ReasonType RT on RT.Id = RM.ReasonType 
									WHERE Convert(date,AM.AttendanceDate) between ''' + CAST(@YearStartDate AS VARCHAR) + ''' AND ''' + CAST(@YearEndDate AS VARCHAR) + '''
									and AM.IsPresent = 0 and RT.Code = ''Health'' AND CHM.IsActive = 1
					)A
					group by A.AttendanceMon, A.AttendanceMonthName,A.Description, A.ReasonType, A.Code

			
					) x

		PIVOT 
		(
			AVG(ReasonCount)			
			for Description in (' + @cols + ')
		) P
		order by AttendanceMon'

END


EXEC SP_EXECUTESQL @query

END



/*

EXEC GetYearlyHealthReason
	@AttendanceDate = '2019-02-06',
		@ClassRoomID = 67 ,
		@TeacherId = 116,
		@Type = 2,
		@SchoolId = 14,
		@LangID = 'en'

	*/

--	select * from absenteereasondetails order by createdon desc
GO
/****** Object:  StoredProcedure [dbo].[GetYearlyNonHealthReason]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetYearlyNonHealthReason]
@AttendanceDate DATE,
	@ClassRoomID int,
	@TeacherId INT,
	@Type TINYINT,
	@SchoolId INT,
	@LangID CHAR(3)

	AS
	BEGIN

	Declare @YearStartDate Date,
			@YearEndDate Date,
			@cols AS NVARCHAR(MAX),
			@query  AS NVARCHAR(MAX),
			@userType INT

			select @userType = (select UserType from UserMaster where id = @TeacherId and IsActive = 1)


			select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1 and SM.Id = @SchoolId										where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

			select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1 and SM.Id = @SchoolId										where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))


		SELECT @cols = STUFF((SELECT  ',' + QUOTENAME(CASE WHEN @LangID = 'id' THEN isnull(Description_Indonesian,'DI') ELSE isnull(Description,'D') END) 
                    FROM ReasonMaster where ReasonType=2 group by CASE WHEN @LangID = 'id' THEN isnull(Description_Indonesian,'DI') ELSE isnull							(Description,'D') END
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')


IF @userType = 1

BEGIN
		SELECT @query = 
		'select * from
		(select A.AttendanceMon, A.AttendanceMonthName,
				A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount				
					from
					(
					select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate, CHM.ID ChildID,
								CASE WHEN ''' + @LangID + ''' = ''id'' THEN isnull(RM.Description_Indonesian,''DI'') ELSE isnull(RM.Description,''D'') END Description, RM.ReasonType, RT.Code, DATEPART(m, AM.AttendanceDate) AttendanceMon, 
								Convert(char(3), AM.AttendanceDate, 0) AttendanceMonthName
									from AttendanceMaster AM 
									inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
									inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
									inner join dbo.GetClassList(''' + CAST(@TeacherId AS VARCHAR) + ''') CL on CL.Id = AM.ClassRoomID
									left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
									left join ReasonMaster RM on RM.ID = ARD. ReasonID
									left join ReasonType RT on RT.Id = RM.ReasonType 
									WHERE Convert(date,AM.AttendanceDate) between ''' + CAST(@YearStartDate AS VARCHAR) + ''' AND ''' + CAST(@YearEndDate AS VARCHAR) + '''
									and AM.IsPresent = 0 and RT.Code = ''Non-Health'' AND CHM.IsActive = 1
					)A
					group by A.AttendanceMon, A.AttendanceMonthName,A.Description, A.ReasonType, A.Code

			
					) x

		PIVOT 
		(
			AVG(ReasonCount)			
			for Description in (' + @cols + ')
		) P
		order by AttendanceMon'
END

ELSE IF @userType = 4

BEGIN

		SELECT @query = 
		'select * from
		(select A.AttendanceMon, A.AttendanceMonthName,
				A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount				
					from
					(
					select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate, CHM.ID ChildID,
								CASE WHEN ''' + @LangID + ''' = ''id'' THEN isnull(RM.Description_Indonesian,''DI'') ELSE isnull(RM.Description,''D'') END Description, RM.ReasonType, RT.Code, DATEPART(m, AM.AttendanceDate) AttendanceMon, 
								Convert(char(3), AM.AttendanceDate, 0) AttendanceMonthName
									from AttendanceMaster AM 
									inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
									inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
									inner join dbo.GetClassListForSchool(''' + CAST(@SchoolId AS VARCHAR) + ''') CL on CL.Id = AM.ClassRoomID
									left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
									left join ReasonMaster RM on RM.ID = ARD. ReasonID
									left join ReasonType RT on RT.Id = RM.ReasonType 
									WHERE Convert(date,AM.AttendanceDate) between ''' + CAST(@YearStartDate AS VARCHAR) + ''' AND ''' + CAST(@YearEndDate AS VARCHAR) + '''
									and AM.IsPresent = 0 and RT.Code = ''Non-Health'' AND CHM.IsActive = 1
					)A
					group by A.AttendanceMon, A.AttendanceMonthName,A.Description, A.ReasonType, A.Code

			
					) x

		PIVOT 
		(
			AVG(ReasonCount)			
			for Description in (' + @cols + ')
		) P
		order by AttendanceMon'
END

EXEC SP_EXECUTESQL @query

END




GO
/****** Object:  StoredProcedure [dbo].[ManageAbsentReasonDetails]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageAbsentReasonDetails]
	@ID int,
	@ChildID INT,
	@TeacherID INT,
	@IsHealthReason BIT,
	@InformationProvider INT,
	@HasPermission BIT,
	@CreatedBy INT,
	@CreatedOn DATETIME,
	@Notes VARCHAR(100),
	@IsPresent BIT,
	@ReasonID INT,
	@Notes_Indonesian NVARCHAR(500)

	
AS
BEGIN
	IF @ID>0
		BEGIN
			UPDATE AbsenteeReasonDetails SET 
				IsHealthReason=@IsHealthReason,
				InformationProvider=@InformationProvider,
				HasPermission=@HasPermission,
				Notes=@Notes,
				ReasonID =@ReasonID ,
				Notes_Indonesian = @Notes_Indonesian
		
			 WHERE ChildID = @ChildID  
			 -- AND TeacherID = @TeacherID
	       AND CAST(CreatedOn AS DATE) = @CreatedOn
			
		END
	ELSE
		BEGIN
			INSERT INTO AbsenteeReasonDetails
			(ChildID,TeacherID,IsHealthReason,InformationProvider,HasPermission,CreatedBy,CreatedOn,Notes,ReasonID,Notes_Indonesian )
			Values
			(@ChildID,@TeacherID,@IsHealthReason,@InformationProvider,@HasPermission,@CreatedBy,@CreatedOn,@Notes,@ReasonID,@Notes_Indonesian)
		END
END

/*
exec ManageAbsentReasonDetails 
@ID =2,
	@ChildID = 8,
	@TeacherID =1,
	@IsHealthReason = 1,
	@InformationProvider =2,
	@HasPermission =0,
	@CreatedBy =3,
	@CreatedOn ='2019-01-03 06:14:59.577',
	@Notes ='notes goes here',
	@AttendanceDate ='2019-01-03'

*/


GO
/****** Object:  StoredProcedure [dbo].[ManageAdminMapping]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageAdminMapping]	
	@SchoolId varchar(50),
	@UserId varchar(50),
	@IsActive varchar(50),	
	@CreatedBy int,
	@CreatedOn DATETIME
	

AS
BEGIN
	 DECLARE @Cnt int
	 SELECT @Cnt = count(1) FROM SchoolAdminMapping WHERE SchoolId = @SchoolId

	IF @Cnt>0
		BEGIN
			UPDATE SchoolAdminMapping SET UserId=@UserId									
			WHERE SchoolId=@SchoolId
		END
	ELSE
		BEGIN
			INSERT INTO SchoolAdminMapping
			(SchoolId,UserId,CreatedBy,CreatedOn,IsActive)
			Values
			(@SchoolId,@UserId,@CreatedBy,@CreatedOn ,@IsActive)
		END
END



GO
/****** Object:  StoredProcedure [dbo].[ManageChildren]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageChildren]
	@ID int,
	@Name varchar(50),
	@Surname varchar(50),
	@ParentName varchar(50),
	@CommunityWorkerID INT,
	@Email varchar(50),
	@Image varchar(MAX),
	@SchoolID INT,
	@ClassroomID INT,
	@Address varchar(50),
	@Phone varchar(50),
	@Note varchar(50),
	@GeoLocation varchar(100),
	@Gender CHAR(1) NULL,
	@ParentPhone varchar(50) = NULL,
	@CreatedBy int,
	@CreatedOn DATETIME,
	@IsActive int,
	@Note_Indonesian NVARCHAR(MAX)

AS
BEGIN
	IF @ID>0
		BEGIN
			UPDATE ChildrenMaster SET Name=@Name,
									Surname=@Surname,
									ParentName=@ParentName,
									CommunityWorkerID=@CommunityWorkerID,
									Email=@Email,
									Image=@Image,
									SchoolID=@SchoolID,
									ClassroomID=@ClassroomID,
									Address=@Address,
									Phone=@Phone,
									Note=@Note,
									GeoLocation=@GeoLocation,
									Gender=@Gender,
									ParentPhone =@ParentPhone,
									Note_Indonesian = @Note_Indonesian

			WHERE ID=@ID
		END
	ELSE
		BEGIN
			INSERT INTO ChildrenMaster
			(Name,Surname,ParentName,CommunityWorkerID,Email,Image,SchoolID,ClassroomID,Address,Phone,Note,GeoLocation,Gender,ParentPhone,CreatedBy,CreatedOn,IsActive,Note_Indonesian)
			Values
			(@Name,@Surname,@ParentName,@CommunityWorkerID,@Email,@Image,@SchoolID,@ClassroomID,@Address,@Phone,@Note,@GeoLocation,@Gender,@ParentPhone,@CreatedBy,@CreatedOn,@IsActive,@Note_Indonesian)
		END
END



GO
/****** Object:  StoredProcedure [dbo].[ManageClass]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageClass]
	@ID int,
	@ClassName varchar(50),
	@SchoolId int,
	@CreatedBy int,
	@CreatedOn DATETIME,
	@IsActive bit
	
AS
BEGIN
	IF @ID>0
		BEGIN
			UPDATE ClassMaster SET ClassName=@ClassName, SchoolId = @SchoolId, CreatedBy = @CreatedBy,CreatedOn=@CreatedOn
									
			WHERE ID=@ID
		END
	ELSE
		BEGIN
			INSERT INTO ClassMaster
			(ClassName,SchoolId,CreatedBy,CreatedOn,IsActive)
			Values
			(@ClassName,@SchoolId, @CreatedBy,@CreatedOn,@IsActive)
		END
END

update classmaster set IsActive=2

select * from usermaster

GO
/****** Object:  StoredProcedure [dbo].[ManageClassReasonType]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ManageClassReasonType]
	@ID int,
	@ReasonType int,
	@Description varchar(200),
	@DescriptionIndian varchar(200)	
AS
BEGIN
	IF @ID>0
		BEGIN
			UPDATE ReasonMaster SET ReasonType=@ReasonType, Description =@Description, Description_Indonesian=@DescriptionIndian
										
			WHERE ID=@ID
		END
	ELSE
		BEGIN
			INSERT INTO ReasonMaster
			(ReasonType,Description,Description_Indonesian)
			Values
			(@ReasonType,@Description,@DescriptionIndian)
		END
END




GO
/****** Object:  StoredProcedure [dbo].[ManageClassTeacher]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageClassTeacher]
	@ID int,
	@ClassRoomID INT,
	@TeacherID INT,
	@CreatedBy INT,
	@CreatedOn DATETIME,
	@IsActive bit
AS
BEGIN
	IF @ID>0
		BEGIN
			UPDATE TeacherClassMapping set TeacherID=@TeacherID,ClassRoomID=@ClassRoomID									
			WHERE ID=@ID
		END
	ELSE
		BEGIN
			INSERT INTO TeacherClassMapping
			(TeacherID,ClassRoomID,CreatedBy,CreatedOn,IsActive)
			Values
			(@TeacherID,@ClassRoomID,@CreatedBy,@CreatedOn,@IsActive)
		END
END



GO
/****** Object:  StoredProcedure [dbo].[ManageFeedBack]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageFeedBack]
	@Content VARCHAR(500),
	@CreatedOn DATETIME,
	@CreatedBy INT,
	@Content_Indonesian NVARCHAR(MAX)
	
AS
BEGIN
	INSERT INTO FeedBack (Content,CreatedOn,CreatedBy,Content_Indonesian)	VALUES (@Content,@CreatedOn,@CreatedBy,@Content_Indonesian)
END



GO
/****** Object:  StoredProcedure [dbo].[ManageReason]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageReason]
	@ID int,
	@Title varchar(max),
	@Content varchar(max),
	@ContentIndian varchar(max)
	
AS
BEGIN
	IF @ID>0
		BEGIN
			UPDATE SickChildReasonMaster SET Title=@Title,Content=@Content,ContentIndian=@ContentIndian
									
			WHERE ID=@ID
		END
	ELSE
		BEGIN
			INSERT INTO SickChildReasonMaster
			(Title,Content,ContentIndian)
			Values
			(@Title,@Content,@ContentIndian)
		END
END



GO
/****** Object:  StoredProcedure [dbo].[ManageSchool]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageSchool]
	@ID int,
	@SchoolName varchar(50),
	@Email varchar(50),
	@Image varchar(50),
	@City varchar(50),
	@Address varchar(max),
	@Country varchar(50),
	@Phone1 varchar(50),
	@Phone2 varchar(50),
	@Note nvarchar(max),
	@GeoLocation varchar(50),
	@CreatedBy int,
	@CreatedOn DATETIME,
	@IsActive int,
	@Note_Indonesian nvarchar(max),
	@InsertedID VARCHAR(30) OUTPUT

	

AS
BEGIN
	IF @ID>0
		BEGIN
			UPDATE SchoolMaster SET SchoolName=@SchoolName,
									Email=@Email,
									Image=@Image,
									City=@City,
									Address=@Address,
									Country=@Country,
									Phone1=@Phone1,
									Phone2=@Phone2,
									Note=@Note,
									GeoLocation=@GeoLocation,
									Note_Indonesian = @Note_Indonesian
			WHERE ID=@ID
			SELECT @InsertedID = 0
		END
	ELSE
		BEGIN
			INSERT INTO SchoolMaster
			(SchoolName,Email,Image,City,Address,Country,Phone1,Phone2,Note,GeoLocation,CreatedBy,CreatedOn,IsActive,Note_Indonesian)
			Values
			(@SchoolName,@Email,@Image,@City,@Address,@Country,@Phone1,@Phone2,@Note,@GeoLocation,@CreatedBy,@CreatedOn,@IsActive,@Note_Indonesian)

			SELECT @InsertedID = SCOPE_IDENTITY()
		END
END



GO
/****** Object:  StoredProcedure [dbo].[ManageStudentAttendance]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageStudentAttendance]	
	@ClassRoomID INT,
	@TeacherID INT,
	@ChildID INT,
	@AttendanceDate DATETIME,
	@IsPresent BIT,
	@CreatedBy INT,
	@CreatedOn DATETIME,
	@IsSubmitted BIT
AS
BEGIN

	DECLARE @ID INT = 0

	SELECT @ID =  ID FROM AttendanceMaster WHERE ChildID = @ChildID  AND ClassRoomID = @ClassRoomID 
	AND CAST(AttendanceDate AS DATE) = CAST(@AttendanceDate	AS DATE)

	IF @ID>0
		BEGIN
			UPDATE AttendanceMaster SET IsPresent=@IsPresent	
			,IsSubmitted = @IsSubmitted
			WHERE ID = @ID  
		END
	ELSE
		BEGIN
			INSERT INTO AttendanceMaster
			(ClassRoomID,TeacherID,ChildID,AttendanceDate,IsPresent,CreatedBy,CreatedOn,IsSubmitted)
			Values
			(@ClassRoomID,@TeacherID,@ChildID,@AttendanceDate,@IsPresent,@CreatedBy,@CreatedOn,@IsSubmitted)
		END
	
END



GO
/****** Object:  StoredProcedure [dbo].[ManageUser]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageUser]
	@ID int,
	@UserName varchar(50),
	@Password varchar(50),
	@Name varchar(50),
	@Surname varchar(50),
	@Image varchar(50),
	@UserType int,
	@Address nvarchar(max),
	@Phone varchar(50),
	@Note nvarchar(max),
	@GeoLocation varchar(100),
	@Gender char(1),
	@CreatedBy int,
	@CreatedOn DATETIME,
	@IsActive bit,
	@Email varchar(100),
	@Note_Indonesian nvarchar(max)


AS
BEGIN
	IF @ID>0
		BEGIN
			UPDATE UserMaster SET UserName=@UserName,
									Password=@Password,
									Name=@Name,
									Surname=@Surname,
									Image=ISNULL(@Image,Image),
									UserType=@UserType,
									Address=@Address,
									Phone=@Phone,
									Note=@Note,
									GeoLocation= @GeoLocation,
									Gender = @Gender,
									Email =  @Email,
									Note_Indonesian = @Note_Indonesian

			WHERE ID=@ID
		END
	ELSE
		BEGIN
			INSERT INTO UserMaster
			(UserName,Password,Name,Surname,Image,	UserType,Address,Phone,Note,GeoLocation,Gender,CreatedBy,CreatedOn,IsActive,Email,Note_Indonesian)
			Values			(@UserName,@Password,@Name,@Surname,@Image,@UserType,@Address,@Phone,@Note,@GeoLocation,@Gender,@CreatedBy,@CreatedOn,@IsActive,@Email,@Note_Indonesian)
		END
END



GO
/****** Object:  StoredProcedure [dbo].[ManageVisitHome]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ManageVisitHome]

	@ChildID int,
	@TeacherID int,
	@IsHealthReason bit,
	@ReasonID int,
	@InformationProvider int,
	@HasPermission bit
AS
BEGIN
	
			INSERT INTO VisitHomeMaster
			(ChildID,TeacherID,VisitHomeDate,IsHealthReason,ReasonID,InformationProvider,HasPermission)
			Values
			(@ChildID,@TeacherID,GETDATE(),@IsHealthReason,@ReasonID,@InformationProvider,@HasPermission)
	
END


GO
/****** Object:  StoredProcedure [dbo].[MonthlyAbsentReasonSummary]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[MonthlyAbsentReasonSummary]

 @AttendanceDate DATE,
	@ClassRoomID int,
	@TeacherId INT,
	@Type TINYINT,
	@LangID CHAR(3)

	AS
	BEGIN

	Declare @MonthStartDate date, 
				@MonthEndDate date, 				
				@YearStartDate Date,
				@YearEndDate Date,
				@userType int

		select @userType = (select UserType from UserMaster where id = @TeacherId)
				
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)		
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

IF @userType = 2
BEGIN

		IF @Type = 1
		BEGIN

			select A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount
			from
			(
			select AM.ClassRoomID,CM.ClassName,
						--DAY(AM.AttendanceDate) AttendanceDate, 
						 CHM.ID ChildID,
						 RM.ReasonType, RT.Code
						,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE RM.Description END
,'') AS Description    

							from AttendanceMaster AM 
							inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
							inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
							left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
							left join ReasonMaster RM on RM.ID = ARD. ReasonID
							left join ReasonType RT on RT.Id = RM.ReasonType 
							WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate and AM.ClassRoomID = @ClassRoomID
							and AM.IsPresent = 0 and RT.Code != 'UnKnown' AND CHM.IsActive = 1

						group by AM.ClassRoomID,CM.ClassName,CHM.ID, RM.ReasonType, RT.Code,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE											RM.Description END,'')
			)A
			group by A.Description, A.ReasonType, A.Code

			order by ReasonType, ReasonCount desc

		END

		ELSE IF @Type = 3
		BEGIN

			select A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount
			from
			(
			select AM.ClassRoomID,CM.ClassName,
						 --DAY(AM.AttendanceDate) AttendanceDate, 
						 CHM.ID ChildID,
						 RM.ReasonType, RT.Code
						,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE RM.Description END
,'') AS Description    
							from AttendanceMaster AM 
							inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
							inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
							left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
							left join ReasonMaster RM on RM.ID = ARD. ReasonID
							left join ReasonType RT on RT.Id = RM.ReasonType 
							WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate and AM.ClassRoomID = @ClassRoomID
							and AM.IsPresent = 0 and RT.Code != 'UnKnown' AND CHM.IsActive = 1

						group by AM.ClassRoomID,CM.ClassName,CHM.ID, RM.ReasonType, RT.Code,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE											RM.Description END,'')
			)A
			group by A.Description, A.ReasonType, A.Code
			
			order by ReasonType, ReasonCount desc

		END

END

ELSE IF @userType = 1 OR @userType = 4
BEGIN
	IF @Type = 1

		BEGIN
		select A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount
			from
			(
			select AM.ClassRoomID,CM.ClassName,
						 --DAY(AM.AttendanceDate) AttendanceDate, 
						 CHM.ID ChildID,
						 RM.ReasonType, RT.Code
						,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE RM.Description END
,'') AS Description    
							from AttendanceMaster AM 
							inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
							inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
							inner join dbo.GetClassList(@TeacherId) CL on CL.Id = AM.ClassRoomID
							left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
							left join ReasonMaster RM on RM.ID = ARD. ReasonID
							left join ReasonType RT on RT.Id = RM.ReasonType 
							WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate
							and AM.IsPresent = 0 and RT.Code != 'UnKnown' AND CHM.IsActive = 1

						group by AM.ClassRoomID,CM.ClassName,CHM.ID, RM.ReasonType, RT.Code,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE											RM.Description END,'')
			)A
			group by A.Description, A.ReasonType, A.Code

			order by ReasonType, ReasonCount desc

		END

		ELSE IF @Type = 2

		BEGIN
		select A.AttendanceMon, A.AttendanceMonthName,
		A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount				
			from
			(
			select AM.ClassRoomID,CM.ClassName,
						DAY(AM.AttendanceDate) AttendanceDate, 
						CHM.ID ChildID,
						RM.ReasonType, RT.Code, DATEPART(m, AM.AttendanceDate)																				AttendanceMon,Convert(char(3), AM.AttendanceDate, 0) AttendanceMonthName
						,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE RM.Description END
,'') AS Description    
							from AttendanceMaster AM 
							inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
							inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
							inner join dbo.GetClassList(@TeacherId) CL on CL.Id = AM.ClassRoomID
							left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
							left join ReasonMaster RM on RM.ID = ARD. ReasonID
							left join ReasonType RT on RT.Id = RM.ReasonType 
							WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate
							and AM.IsPresent = 0 and RT.Code != 'UnKnown' AND CHM.IsActive = 1
			)A
			group by A.AttendanceMon, A.AttendanceMonthName,A.Description, A.ReasonType, A.Code

			order by AttendanceMon,ReasonType, ReasonCount desc

		END

END

END




GO
/****** Object:  StoredProcedure [dbo].[MonthlyAbsentReasonSummaryForSA]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MonthlyAbsentReasonSummaryForSA]

 @AttendanceDate DATE,
	@ClassRoomID int,
	@TeacherId INT,
	@Type TINYINT,
	@LangID CHAR(3),
	@SchoolId INT

	AS
	BEGIN

	Declare @MonthStartDate date, 
				@MonthEndDate date, 				
				@YearStartDate Date,
				@YearEndDate Date
				
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)		
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))


	IF @Type = 1

		BEGIN
		select A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount
			from
			(
			select AM.ClassRoomID,CM.ClassName,
						 --DAY(AM.AttendanceDate) AttendanceDate, 
						 CHM.ID ChildID,
						 RM.ReasonType, RT.Code
						,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE RM.Description END
,'') AS Description    
							from AttendanceMaster AM 
							inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
							inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
							inner join dbo.GetClassListForSchool(@SchoolId) CL on CL.Id = AM.ClassRoomID
							left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
							left join ReasonMaster RM on RM.ID = ARD. ReasonID
							left join ReasonType RT on RT.Id = RM.ReasonType 
							WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate
							and AM.IsPresent = 0 and RT.Code != 'UnKnown' AND CHM.IsActive = 1

						group by AM.ClassRoomID,CM.ClassName,CHM.ID, RM.ReasonType, RT.Code,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE											RM.Description END,'')
			)A
			group by A.Description, A.ReasonType, A.Code

			order by ReasonType, ReasonCount desc

		END

		ELSE IF @Type = 2

		BEGIN
		select A.AttendanceMon, A.AttendanceMonthName,
		A.Description, A.ReasonType, A.Code ReasonCode, count(A.Description) ReasonCount				
			from
			(
			select AM.ClassRoomID,CM.ClassName,
						DAY(AM.AttendanceDate) AttendanceDate, 
						CHM.ID ChildID,
						RM.ReasonType, RT.Code, DATEPART(m, AM.AttendanceDate)																				AttendanceMon,Convert(char(3), AM.AttendanceDate, 0) AttendanceMonthName
						,ISNULL(CASE WHEN @LangID = 'id' THEN RM.Description_Indonesian ELSE RM.Description END
,'') AS Description    
							from AttendanceMaster AM 
							inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
							inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
							inner join dbo.GetClassListForSchool(@SchoolId) CL on CL.Id = AM.ClassRoomID
							left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
							left join ReasonMaster RM on RM.ID = ARD. ReasonID
							left join ReasonType RT on RT.Id = RM.ReasonType 
							WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate
							and AM.IsPresent = 0 and RT.Code != 'UnKnown' AND CHM.IsActive = 1
			)A
			group by A.AttendanceMon, A.AttendanceMonthName,A.Description, A.ReasonType, A.Code

			order by AttendanceMon,ReasonType, ReasonCount desc

		END


END


GO
/****** Object:  StoredProcedure [dbo].[MonthlyReasonReportRate]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	
	
	CREATE PROCEDURE [dbo].[MonthlyReasonReportRate]
	
	@AttendanceDate DATE,
	@ClassRoomID int,
	@Type TINYINT,	
	@TeacherId INT

	
	--select @AttendanceDate='2019-01-12',@TeacherId=51,@ClassRoomID=35
	AS

	BEGIN
		Declare @MonthStartDate date, 
				@MonthEndDate date, 
				@WeekStartDate Date, 
				@WeekEndDate Date
				
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
		select @WeekStartDate = DATEADD(DAY, 2 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE)) 
		select @WeekEndDate = DATEADD(DAY, 8 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE))
		
		IF @Type = 1
		BEGIN	
		--select * 
		select (sum(B.sickness_count)/CAST((count(B.ChildID) * count(B.AttendanceDate)) AS float))Abstsmratesickness,(sum(B.nonhealth_count)/CAST((count(B.ChildID) * count(B.AttendanceDate)) AS float))Abstsmratenonhealth,(sum(B.UnKnown_count)/CAST((count(B.ChildID) * count(B.AttendanceDate)) AS float))AbstsmrateUnknown
		from 
		(select A.Id ID, A.Name, A.Gender,A.Attendance,Day(A.AttendanceDate)AttendanceDate,A.ReasonID,A.Code,A.ChildID,
		case when (A.Attendance='A' and A.Code = 'Health') then 'H'
			 when (A.Attendance='A' and A.Code = 'Non-Health') then 'NH'
			 when (A.Attendance='A' and isnull(A.Code,'UnKnown') = 'UnKnown') then 'A'
			 when (A.Attendance='P') then 'P' end AttendanceType,
			 count(case when A.Attendance='P'  then 1 end) as present_count,
			 count(case when (A.Attendance='A' and A.Code = 'Health') then 1 end) as sickness_count,
			 count(case when (A.Attendance='A' and A.Code = 'Non-Health') then 1 end) as nonhealth_count,
			 count(case when (A.Attendance='A' and A.Code = 'UnKnown') then 1 end) as UnKnown_count

			 from
				(select CHM.Id,(CHM.Name + ' ' + CHM.Surname) Name,AM.AttendanceDate,AM.ChildID,
				case when (isnull(CHM.Gender,'M') = 'M') then 'Male' else 'Female' end Gender,
				case when isnull(AM.IsPresent,0)=1 then 'P' else 'A' end Attendance,
				RT.Code,
				RM.ID ReasonID

				from ChildrenMaster CHM join dbo.GetClassList(@TeacherId) CM on CHM.ClassroomID = CM.Id and isnull(CHM.IsActive,0) = 1
				left join AttendanceMaster AM on AM.ChildId = CHM.Id and convert(date,AttendanceDate) BETWEEN @MonthStartDate AND @MonthEndDate and AM.TeacherId =					 @TeacherId
				left join AbsenteeReasonDetails ARD on ARD.ChildID = CHM.Id and ARD.TeacherId = @TeacherId and convert(date,ARD.CreatedOn) =				 @AttendanceDate
				left join ReasonType RT on RT.Id = isnull(ARD.ReasonID,0)
				left join Reasonmaster RM on RM.ReasonType = RT.ID
				--WHERE CHM.ID = @ClassRoomID
				WHERE CHM.IsActive = 1
				)A
				Group by  A.Id, A.Name, A.Gender,A.Attendance,A.AttendanceDate,A.ReasonID,A.Code,A.ChildID)B
		END

		IF @Type = 2
		BEGIN	
		--select * 
		select (sum(B.sickness_count)/CAST((nullif(count(B.ChildID),0) * nullif(count(B.AttendanceDate),0)) AS float))Abstsmratesickness,(sum(B.nonhealth_count)/CAST((nullif(count(B.ChildID),0) * nullif(count(B.AttendanceDate),0)) AS float))Abstsmratenonhealth,(sum(B.UnKnown_count)/CAST((nullif(count(B.ChildID),0) * nullif(count(B.AttendanceDate),0)) AS float))AbstsmrateUnknown
		from 
		(select A.Id ID, A.Name, A.Gender,A.Attendance,Day(A.AttendanceDate)AttendanceDate,A.ReasonID,A.Code,A.ChildID,
		case when (A.Attendance='A' and A.Code = 'Health') then 'H'
			 when (A.Attendance='A' and A.Code = 'Non-Health') then 'NH'
			 when (A.Attendance='A' and isnull(A.Code,'UnKnown') = 'UnKnown') then 'A'
			 when (A.Attendance='P') then 'P' end AttendanceType,
			 count(case when A.Attendance='P'  then 1 end) as present_count,
			 count(case when (A.Attendance='A' and A.Code = 'Health') then 1 end) as sickness_count,
			 count(case when (A.Attendance='A' and A.Code = 'Non-Health') then 1 end) as nonhealth_count,
			 count(case when (A.Attendance='A' and A.Code = 'UnKnown') then 1 end) as UnKnown_count

			 from
				(select CHM.Id,(CHM.Name + ' ' + CHM.Surname) Name,AM.AttendanceDate,AM.ChildID,
				case when (isnull(CHM.Gender,'M') = 'M') then 'Male' else 'Female' end Gender,
				case when isnull(AM.IsPresent,0)=1 then 'P' else 'A' end Attendance,
				RT.Code,
				RM.ID ReasonID

				from ChildrenMaster CHM join dbo.GetClassList(@TeacherId) CM on CHM.ClassroomID = CM.Id and isnull(CHM.IsActive,0) = 1
				left join AttendanceMaster AM on AM.ChildId = CHM.Id and convert(date,AttendanceDate) BETWEEN @WeekStartDate AND @WeekEndDate and AM.TeacherId =					 @TeacherId
				left join AbsenteeReasonDetails ARD on ARD.ChildID = CHM.Id and ARD.TeacherId = @TeacherId and convert(date,ARD.CreatedOn) =				 @AttendanceDate
				left join ReasonType RT on RT.Id = isnull(ARD.ReasonID,0)
				left join Reasonmaster RM on RM.ReasonType = RT.ID
				--WHERE CHM.ID = @ClassRoomID
				WHERE CHM.IsActive = 1
				)A
				Group by  A.Id, A.Name, A.Gender,A.Attendance,A.AttendanceDate,A.ReasonID,A.Code,A.ChildID)B
		END

	
	
	END


GO
/****** Object:  StoredProcedure [dbo].[MonthlyStudentCount]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MonthlyStudentCount]
	
	@AttendanceDate DATE,
	@ClassRoomID int,
	@Type TINYINT,	
	@TeacherId INT

	AS
	BEGIN
	
		Declare @MonthStartDate date, 
				@MonthEndDate date, 
				@WeekStartDate Date, 
				@WeekEndDate Date
				
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
		select @WeekStartDate = DATEADD(DAY, 2 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE)) 
		select @WeekEndDate = DATEADD(DAY, 8 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE))
		
		IF @Type = 1
		select count(A.ChildID)StudentCount1 from 
		(select childid from attendancemaster AM
		where IsPresent = 0 and ClassRoomID=@ClassRoomID   and convert(date,AttendanceDate) BETWEEN @MonthStartDate AND @MonthEndDate		and	AM.TeacherId =	@TeacherId
		group by childid having count(IsPresent) >= 2)A

		select count(A.ChildID)StudentCount2 from 
		(select childid from attendancemaster AM
		where IsPresent = 0 and ClassRoomID=@ClassRoomID   and convert(date,AttendanceDate) BETWEEN @MonthStartDate AND @MonthEndDate		and	AM.TeacherId =	@TeacherId
		group by childid having count(IsPresent) < =2)A
	END


GO
/****** Object:  StoredProcedure [dbo].[ReasonCount]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ReasonCount]
@AttendanceDate DATE
AS

BEGIN
	Declare @MonthStartDate date, 
				@MonthEndDate date
	
	select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)

	select top 3 count(ARD.ReasonId)Reasoncount,RM.Description,RT.Code from AbsenteeReasonDetails ARD
	join ReasonMaster RM on ARD.ReasonId = RM.ID
	join ReasonType RT on RT.Id = RM.ReasonType
	--where Convert(date,) BETWEEN @MonthStartDate AND @MonthEndDate
	where RT.Id =1
	--where RT.Id =2
	--where RT.Id =3
	group by ARD.ReasonId,RM.Description,RT.Code
	
	select top 3 count(ARD.ReasonId)Reasoncount,RM.Description,RT.Code from AbsenteeReasonDetails ARD
	join ReasonMaster RM on ARD.ReasonId = RM.ID
	join ReasonType RT on RT.Id = RM.ReasonType
	--where Convert(date,) BETWEEN @MonthStartDate AND @MonthEndDate
	--where RT.Id =1
	where RT.Id =2
	--where RT.Id =3
	group by ARD.ReasonId,RM.Description,RT.Code

END

GO
/****** Object:  StoredProcedure [dbo].[ReasonCountHealth]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ReasonCountHealth]
@AttendanceDate DATE
AS

BEGIN
	Declare @MonthStartDate date, 
				@MonthEndDate date
	
	select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)

	select top 3 count(ARD.ReasonId)Reasoncount,RM.Description,RT.Code from AbsenteeReasonDetails ARD
	join ReasonMaster RM on ARD.ReasonId = RM.ID
	join ReasonType RT on RT.Id = RM.ReasonType
	--where Convert(date,) BETWEEN @MonthStartDate AND @MonthEndDate
	where RT.Id =1
	--where RT.Id =2
	--where RT.Id =3
	group by ARD.ReasonId,RM.Description,RT.Code

END

GO
/****** Object:  StoredProcedure [dbo].[ReasonCountNonHealth]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ReasonCountNonHealth]
@AttendanceDate DATE
AS

BEGIN
	Declare @MonthStartDate date, 
				@MonthEndDate date
	
	select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
	
	select top 3 count(ARD.ReasonId)Reasoncount,RM.Description,RT.Code from AbsenteeReasonDetails ARD
	join ReasonMaster RM on ARD.ReasonId = RM.ID
	join ReasonType RT on RT.Id = RM.ReasonType
	--where Convert(date,) BETWEEN @MonthStartDate AND @MonthEndDate
	--where RT.Id =1
	where RT.Id =2
	--where RT.Id =3
	group by ARD.ReasonId,RM.Description,RT.Code

END

GO
/****** Object:  StoredProcedure [dbo].[StudentAttendaceReport]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO








CREATE PROCEDURE [dbo].[StudentAttendaceReport]
	
	@AttendanceDate DATE,
	@ClassRoomID int,
	@TeacherId INT,
	@Type TINYINT

	AS
	BEGIN
		Declare @MonthStartDate date, 
				@MonthEndDate date, 
				@WeekStartDate Date, 
				@WeekEndDate Date,
				@YearStartDate Date,
				@YearEndDate Date,
				@userType int 

		select @userType = (select UserType from UserMaster where ID = @TeacherId and IsActive = 1)
				
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
		select @WeekStartDate = DATEADD(DAY, 2 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE)) 
		select @WeekEndDate = DATEADD(DAY, 8 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE))
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

	IF @userType = 2
		BEGIN	
			IF @Type = 0
			BEGIN
			select A.Id ID, A.Name, A.Gender,A.Attendance,Day(A.AttendanceDate)AttendanceDate,A.ReasonID,A.Code,
			case when (A.Attendance='A' and A.Code = 'Health') then 'H'
				 when (A.Attendance='A' and A.Code = 'Non-Health') then 'NH'
				 when (A.Attendance='A' and isnull(A.Code,'UnKnown') = 'UnKnown') then 'UL'
				 when (A.Attendance='P') then 'P' end AttendanceType
				 from
					(select CHM.Id,(CHM.Name + ' ' + CHM.Surname) Name,AM.AttendanceDate,
					case when (isnull(CHM.Gender,'M') = 'M') then 'Male' else 'Female' end Gender,
					case when isnull(AM.IsPresent,0)=1 then 'P' else 'A' end Attendance,
					RT.Code,
					RM.ID ReasonID

					from ChildrenMaster CHM join dbo.GetClassList(@TeacherId) CM on CHM.ClassroomID = CM.Id and isnull(CHM.IsActive,0) = 1
					left join AttendanceMaster AM on AM.ChildId = CHM.Id and convert(date,AttendanceDate)=@AttendanceDate  and AM.TeacherId =					 @TeacherId
					left join AbsenteeReasonDetails ARD on ARD.ChildID = CHM.Id and ARD.TeacherId = @TeacherId and convert(date,AM.AttendanceDate) = convert(date,ARD.CreatedOn) and convert(date,ARD.CreatedOn) = @AttendanceDate
					left join Reasonmaster RM on RM.ID = isnull(ARD.ReasonID,0)
					left join ReasonType RT on RM.ReasonType = RT.ID
					WHERE CHM.ClassroomID = @ClassRoomID AND CHM.IsActive = 1
					)A
			END

			ELSE IF @Type = 1
			BEGIN
			select A.Id ID, A.Name, A.Gender,A.Attendance,Day(A.AttendanceDate)AttendanceDate,A.ReasonID,A.Code,
			case when (A.Attendance='A' and A.Code = 'Health') then 'H'
				 when (A.Attendance='A' and A.Code = 'Non-Health') then 'NH'
				 when (A.Attendance='A' and isnull(A.Code,'UnKnown') = 'UnKnown') then 'UL'
				 when (A.Attendance='P') then 'P' end AttendanceType
				 from
					(select CHM.Id,(CHM.Name + ' ' + CHM.Surname) Name,AM.AttendanceDate,
					case when (isnull(CHM.Gender,'M') = 'M') then 'Male' else 'Female' end Gender,
					case when isnull(AM.IsPresent,0)=1 then 'P' else 'A' end Attendance,
					RT.Code,
					RM.ID ReasonID

					from ChildrenMaster CHM join dbo.GetClassList(@TeacherId) CM on CHM.ClassroomID = CM.Id and isnull(CHM.IsActive,0) = 1
					left join AttendanceMaster AM on AM.ChildId = CHM.Id and convert(date,AttendanceDate)BETWEEN @MonthStartDate AND		@MonthEndDate and AM.TeacherId =	@TeacherId
					left join AbsenteeReasonDetails ARD on ARD.ChildID = CHM.Id and ARD.TeacherId = @TeacherId and convert(date,AM.AttendanceDate) = convert(date,ARD.CreatedOn) and convert(date,ARD.CreatedOn) BETWEEN @MonthStartDate AND @MonthEndDate
					left join Reasonmaster RM on RM.ID = isnull(ARD.ReasonID,0)
					left join ReasonType RT on RM.ReasonType = RT.ID
					WHERE CHM.ClassroomID = @ClassRoomID AND CHM.IsActive = 1
					)A
			END

			ELSE IF @Type = 2
			BEGIN
			select A.Id ID, A.Name, A.Gender,A.Attendance,Day(A.AttendanceDate)AttendanceDate,A.ReasonID,A.Code,
			case when (A.Attendance='A' and A.Code = 'Health') then 'H'
				 when (A.Attendance='A' and A.Code = 'Non-Health') then 'NH'
				 when (A.Attendance='A' and isnull(A.Code,'UnKnown') = 'UnKnown') then 'UL'
				 when (A.Attendance='P') then 'P' end AttendanceType
				 from
					(select CHM.Id,(CHM.Name + ' ' + CHM.Surname) Name,AM.AttendanceDate,
					case when (isnull(CHM.Gender,'M') = 'M') then 'Male' else 'Female' end Gender,
					case when isnull(AM.IsPresent,0)=1 then 'P' else 'A' end Attendance,
					RT.Code,
					RM.ID ReasonID

					from ChildrenMaster CHM join dbo.GetClassList(@TeacherId) CM on CHM.ClassroomID = CM.Id and isnull(CHM.IsActive,0) = 1
					left join AttendanceMaster AM on AM.ChildId = CHM.Id and convert(date,AttendanceDate) BETWEEN @WeekStartDate AND		@WeekEndDate and AM.TeacherId =	@TeacherId
					left join AbsenteeReasonDetails ARD on ARD.ChildID = CHM.Id and ARD.TeacherId = @TeacherId and convert(date,AM.AttendanceDate) = convert(date,ARD.CreatedOn) and convert(date,ARD.CreatedOn) BETWEEN @WeekStartDate AND		@WeekEndDate
					left join Reasonmaster RM on RM.ID = isnull(ARD.ReasonID,0)
					left join ReasonType RT on RM.ReasonType = RT.ID
					WHERE CHM.ClassroomID = @ClassRoomID AND CHM.IsActive = 1
					)A
			END

			ELSE IF @Type = 3
			BEGIN
			select B.ChildId AS ID, B.Name, B.Absent AbsentDays, 
		case 
		when B.PresentPercent < 80 then 'Severely Chronically Absent'
		when B.PresentPercent between 80 and 90 then 'Chronically Absent' 
			
			when B.PresentPercent >= 90 then 'None' 
			end ReasonType
			from
			(
			select A.ClassRoomID,A.ClassName, A.ID ChildId, A.Name,		
				count(A.AttendanceDate) as TotalDays,
				count(case when A.IsPresent = 1 then 1 end) as Present,
				count(case when A.IsPresent = 0 then 0 end) as 'Absent',
				(count(case when A.IsPresent = 1 then 1 end) / convert(decimal(6,3), nullif(count(A.AttendanceDate),100)) * 100) as PresentPercent			
				from
					(
					select AM.ClassRoomID,CM.ClassName, CHM.ID, (CHM.Name + ' ' + CHM.Surname) Name, DAY(AM.AttendanceDate) AttendanceDate,
					AM.IsPresent

						from AttendanceMaster AM 
						inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
						inner join ChildrenMaster CHM on AM.ChildID = CHM.ID					
						WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate and AM.ClassRoomID = @ClassRoomID AND CHM.IsActive = 1
						) A
					Group By A.ClassRoomID,A.ClassName , A.ID, A.Name
					)B
					
			END
		
		END	
		
		
		ELSE IF @userType = 1 OR  @userType = 4	/*ELSE IF @userType = 1	*/	
		BEGIN
			IF @Type = 0
			BEGIN
				
				select A.Id ID, A.Name, A.Gender,A.Attendance,Day(A.AttendanceDate)AttendanceDate,A.ReasonID,A.Code,
				case when (A.Attendance='A' and A.Code = 'Health') then 'H'
				 when (A.Attendance='A' and A.Code = 'Non-Health') then 'NH'
				 when (A.Attendance='A' and isnull(A.Code,'UnKnown') = 'UnKnown') then 'UL'
				 when (A.Attendance='P') then 'P' end AttendanceType
				  ,'' AS ReasonType
				 from
				 (
				select CHM.Id,(CHM.Name + ' ' + CHM.Surname) Name,AM.AttendanceDate,
					case when (isnull(CHM.Gender,'M') = 'M') then 'Male' else 'Female' end Gender,
					case when isnull(AM.IsPresent,0)=1 then 'P' else 'A' end Attendance,
					RT.Code,
					RM.ID ReasonID 
				from AttendanceMaster AM 
				inner join dbo.GetClassList(@TeacherId) CL on AM.ClassRoomId = CL.ID
				inner join ChildrenMaster CHM on CHM.Id = AM.ChildID
				left join AbsenteeReasonDetails ARD on ARD.ChildID = CHM.Id and convert(date,AM.AttendanceDate) = convert(date,ARD.CreatedOn) and convert(date,ARD.CreatedOn) = @AttendanceDate
					left join Reasonmaster RM on RM.ID = isnull(ARD.ReasonID,0)
					left join ReasonType RT on RM.ReasonType = RT.ID
				
				where convert(date,AM.AttendanceDate) = @AttendanceDate and  AM.ClassRoomID = isnull(nullif(@ClassRoomID,0),AM.ClassRoomID) AND CHM.IsActive = 1
				)A
			END

			ELSE IF @Type = 1
			BEGIN
				
				select A.Id ID, A.Name, A.Gender,A.Attendance,Day(A.AttendanceDate)AttendanceDate,A.ReasonID,A.Code,
				case when (A.Attendance='A' and A.Code = 'Health') then 'H'
				 when (A.Attendance='A' and A.Code = 'Non-Health') then 'NH'
				 when (A.Attendance='A' and isnull(A.Code,'UnKnown') = 'UnKnown') then 'UL'
				 when (A.Attendance='P') then 'P' end AttendanceType
				 from
				 (
				select CHM.Id,(CHM.Name + ' ' + CHM.Surname) Name,AM.AttendanceDate,
					case when (isnull(CHM.Gender,'M') = 'M') then 'Male' else 'Female' end Gender,
					case when isnull(AM.IsPresent,0)=1 then 'P' else 'A' end Attendance,
					RT.Code,
					RM.ID ReasonID 
				from AttendanceMaster AM 
				inner join dbo.GetClassList(@TeacherId) CL on AM.ClassRoomId = CL.ID
				inner join ChildrenMaster CHM on CHM.Id = AM.ChildID
				left join AbsenteeReasonDetails ARD on ARD.ChildID = CHM.Id and convert(date,AM.AttendanceDate) = convert(date,ARD.CreatedOn) and convert(date,ARD.CreatedOn) between @MonthStartDate and @MonthEndDate
					left join Reasonmaster RM on RM.ID = isnull(ARD.ReasonID,0)
					left join ReasonType RT on RM.ReasonType = RT.ID
				
				where convert(date,AM.AttendanceDate) between @MonthStartDate and @MonthEndDate and  AM.ClassRoomID = isnull(nullif(@ClassRoomID,0),AM.ClassRoomID) AND CHM.IsActive = 1
				)A
			END

			ELSE IF @Type = 2
			BEGIN

				select B.ChildId AS ID, B.Name, B.Absent AbsentDays, 0 AS AttendanceDate, 0 AS ReasonID, '' AS Code,
					case when B.PresentPercent between 80 and 90 then 'Chronically Absent' 
				when B.PresentPercent < 80 then 'Severely Chronically Absent'
				when B.PresentPercent >= 90 then 'None' end ReasonType
				from
				(
				select A.ClassRoomID,A.ClassName, A.ID ChildId, A.Name,		
					count(A.AttendanceDate) as TotalDays,
					count(case when A.IsPresent = 1 then 1 end) as Present,
					count(case when A.IsPresent = 0 then 0 end) as 'Absent',
					(count(case when A.IsPresent = 1 then 1 end) / convert(decimal(16,3), nullif(count(A.AttendanceDate),100)) * 100) as PresentPercent			
					from
						(
						select AM.ClassRoomID,CM.ClassName, CHM.ID, (CHM.Name + ' ' + CHM.Surname) Name, DAY(AM.AttendanceDate) AttendanceDate,
						AM.IsPresent

							from AttendanceMaster AM 
							inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
							inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
							inner join dbo.getClassList(@TeacherId)	CL on AM.ClassRoomID = CL.ID			
							WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate and AM.ClassRoomID = isnull(nullif(@ClassRoomID,0),AM.ClassRoomID) AND CHM.IsActive = 1
							) A
						Group By A.ClassRoomID,A.ClassName , A.ID, A.Name
						)B
					

			END
		
		END
	
	END



	-- EXEC  StudentAttendaceReport @AttendanceDate = '2019-02-13',@ClassRoomID= 12,@TeacherId =111, @Type=1


		--  EXEC  StudentAttendaceReport @AttendanceDate = '2019-02-03',@ClassRoomID= 66,@TeacherId =116, @Type=1


		-- EXEC  StudentAttendaceReport @AttendanceDate = '2018-11-14',@ClassRoomID= 24,@TeacherId =115, @Type=3




GO
/****** Object:  StoredProcedure [dbo].[StudentAttendaceSummary]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[StudentAttendaceSummary]
	
	@AttendanceDate DATE,
	@ClassRoomID int,
	@Type TINYINT,
	@TeacherId INT

	AS
	BEGIN
	Declare		@MonthStartDate date, 
				@MonthEndDate date, 
				@WeekStartDate Date, 
				@WeekEndDate Date, 
				@YearStartDate Date, 
				@YearEndDate Date,
				@userType int

		select @userType = (select UserType from UserMaster where id = @TeacherId)
	
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
		select @WeekStartDate = DATEADD(DAY, 2 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE)) 
		select @WeekEndDate = DATEADD(DAY, 8 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE))
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

IF @userType = 2
BEGIN
	
	IF @Type = 1

		BEGIN
		select CONVERT(decimal(15,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
			CONVERT(decimal(15,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
			CONVERT(decimal(15,2),(sum(B.AbsentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalBoys),0)) * 100)) as BoysAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.AbsentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalGirls),0)) * 100)) as GirlsAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.Health) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.NonHealth) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.UnKnown) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
		from
		(
		select A.ClassRoomID,A.ClassName,A.AttendanceDate,
			count(A.ClassRoomID) as TotalStudents,
			count(case when A.IsPresent = 1 then 1 end) as Present,
			count(case when A.IsPresent = 0 then 0 end) as 'Absent',
			count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
			count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
			count(case when A.Gender = 'M' then  'Male' end) as TotalBoys,
			count(case when A.Gender = 'F' then  'Female' end) as TotalGirls,
			count(case when A.Code = 'Health' Then 'H' end) as Health,
			count(case when A.Code = 'Non-Health' Then 'NH' end) as NonHealth,
			count(case when A.Code = 'UnKnown' Then 'U' end) as UnKnown
			from
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate,
				AM.IsPresent,isnull(CHM.Gender,'M') Gender,case when IsPresent = 0 then isnull(RT.Code,'UnKnown') else RT.Code end Code

					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
					inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
					left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
					left join ReasonMaster RM on RM.ID = ARD. ReasonID
					left join ReasonType RT on RT.Id = RM.ReasonType 
					WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate and AM.ClassRoomID = @ClassRoomID AND CHM.IsActive = 1
					) A
				Group By A.ClassRoomID,A.ClassName,A.AttendanceDate
				) B

		END


		ELSE IF @Type = 2

		BEGIN
			select CONVERT(decimal(15,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
				CONVERT(decimal(15,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
				CONVERT(decimal(15,2),(sum(B.AbsentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalBoys),0)) * 100)) as BoysAbsenteeRate,
				CONVERT(decimal(15,2),(sum(B.AbsentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalGirls),0)) * 100)) as GirlsAbsenteeRate,
				CONVERT(decimal(15,2),(sum(B.Health) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
				CONVERT(decimal(15,2),(sum(B.NonHealth) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
				CONVERT(decimal(15,2),(sum(B.UnKnown) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
		from
		(
		select A.ClassRoomID,A.ClassName,A.AttendanceDate,
			count(A.ClassRoomID) as TotalStudents,
			count(case when A.IsPresent = 1 then 1 end) as Present,
			count(case when A.IsPresent = 0 then 0 end) as 'Absent',
			count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
			count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
			count(case when A.Gender = 'M' then  'Male' end) as TotalBoys,
			count(case when A.Gender = 'F' then  'Female' end) as TotalGirls,
			count(case when A.Code = 'Health' Then 'H' end) as Health,
			count(case when A.Code = 'Non-Health' Then 'NH' end) as NonHealth,
			count(case when A.Code = 'UnKnown' Then 'U' end) as UnKnown
			from
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate,
				AM.IsPresent,isnull(CHM.Gender,'M') Gender,case when IsPresent = 0 then isnull(RT.Code,'UnKnown') else RT.Code end Code

					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
					inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
					left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
					left join ReasonMaster RM on RM.ID = ARD. ReasonID
					left join ReasonType RT on RT.Id = RM.ReasonType 
					WHERE Convert(date,AM.AttendanceDate) between @WeekStartDate AND @WeekEndDate and AM.ClassRoomID = @ClassRoomID AND CHM.IsActive = 1
					) A
				Group By A.ClassRoomID,A.ClassName,A.AttendanceDate
				) B

				END

		ELSE IF @Type = 3

		BEGIN
		select CONVERT(decimal(15,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
			CONVERT(decimal(15,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
			CONVERT(decimal(15,2),(sum(B.AbsentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalBoys),0)) * 100)) as BoysAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.AbsentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalGirls),0)) * 100)) as GirlsAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.Health) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.NonHealth) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.UnKnown) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
		from
		(
		select A.ClassRoomID,A.ClassName,A.AttendanceDate,
			count(A.ClassRoomID) as TotalStudents,
			count(case when A.IsPresent = 1 then 1 end) as Present,
			count(case when A.IsPresent = 0 then 0 end) as 'Absent',
			count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
			count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
			count(case when A.Gender = 'M' then  'Male' end) as TotalBoys,
			count(case when A.Gender = 'F' then  'Female' end) as TotalGirls,
			count(case when A.Code = 'Health' Then 'H' end) as Health,
			count(case when A.Code = 'Non-Health' Then 'NH' end) as NonHealth,
			count(case when A.Code = 'UnKnown' Then 'U' end) as UnKnown
			from
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate,
				AM.IsPresent,isnull(CHM.Gender,'M') Gender,case when IsPresent = 0 then isnull(RT.Code,'UnKnown') else RT.Code end Code

					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
					inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
					left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
					left join ReasonMaster RM on RM.ID = ARD. ReasonID
					left join ReasonType RT on RT.Id = RM.ReasonType 
					WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate and AM.ClassRoomID = @ClassRoomID AND CHM.IsActive = 1
					) A
				Group By A.ClassRoomID,A.ClassName,A.AttendanceDate
				) B

		END

END

ELSE IF @userType = 1 OR @userType = 4	--// Admin , SuperAdmin
BEGIN
	IF @Type = 1

		BEGIN
		/*
		select CONVERT(decimal(5,2),(sum(B.Present)/CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
			CONVERT(decimal(5,2),(sum(B.Absent) / CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
			CONVERT(decimal(5,2),(sum(B.AbsentBoys) / CONVERT(decimal(6,3), nullif(sum(B.TotalBoys),0)) * 100)) as BoysAbsenteeRate,
			CONVERT(decimal(5,2),(sum(B.AbsentGirls) / CONVERT(decimal(6,3), nullif(sum(B.TotalGirls),0)) * 100)) as GirlsAbsenteeRate,
			CONVERT(decimal(5,2),(sum(B.Health) / CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
			CONVERT(decimal(5,2),(sum(B.NonHealth) / CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
			CONVERT(decimal(5,2),(sum(B.UnKnown) / CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
			*/
			SELECT	
			CONVERT(decimal(15,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
			CONVERT(decimal(15,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
			CONVERT(decimal(15,2),(sum(B.AbsentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalBoys),0)) * 100)) as BoysAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.AbsentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalGirls),0)) * 100)) as GirlsAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.Health) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.NonHealth) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.UnKnown) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
		from
		(
		select A.ClassRoomID,A.ClassName,A.AttendanceDate,
			count(A.ClassRoomID) as TotalStudents,
			count(case when A.IsPresent = 1 then 1 end) as Present,
			count(case when A.IsPresent = 0 then 0 end) as 'Absent',
			count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
			count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
			count(case when A.Gender = 'M' then  'Male' end) as TotalBoys,
			count(case when A.Gender = 'F' then  'Female' end) as TotalGirls,
			count(case when A.Code = 'Health' Then 'H' end) as Health,
			count(case when A.Code = 'Non-Health' Then 'NH' end) as NonHealth,
			count(case when A.Code = 'UnKnown' Then 'U' end) as UnKnown
			from
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate,
				AM.IsPresent,isnull(CHM.Gender,'M') Gender,case when IsPresent = 0 then isnull(RT.Code,'UnKnown') else RT.Code end Code

					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
					inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
					inner join dbo.GetClassList(@TeacherId) CL on CL.Id = AM.ClassRoomID
					left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
					left join ReasonMaster RM on RM.ID = ARD. ReasonID
					left join ReasonType RT on RT.Id = RM.ReasonType 
					WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate AND CHM.IsActive = 1
					) A
				Group By A.ClassRoomID,A.ClassName,A.AttendanceDate
				) B

		END


		ELSE IF @Type = 2

		BEGIN
		select B.AttendanceMon, B.AttendanceMonthName, B.AttendanceYear,
			CONVERT(decimal(15,2),(sum(B.Health) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.NonHealth) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.UnKnown) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
		from
		(
		select A.ClassRoomID,A.ClassName,A.AttendanceDate, A.AttendanceMon, A.AttendanceMonthName, A.AttendanceYear,
			count(A.ClassRoomID) as TotalStudents,						
			count(case when A.Code = 'Health' Then 'H' end) as Health,
			count(case when A.Code = 'Non-Health' Then 'NH' end) as NonHealth,
			count(case when A.Code = 'UnKnown' Then 'U' end) as UnKnown
			from
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate,DATEPART(m, AM.AttendanceDate)										AttendanceMon,Convert(char(3), AM.AttendanceDate, 0) AttendanceMonthName,
				AM.IsPresent,case when AM.IsPresent = 0 then isnull(RT.Code,'UnKnown') else RT.Code end Code, year(AM.AttendanceDate) AttendanceYear
					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID					
					inner join dbo.GetClassList(@TeacherId) CL on CL.Id = AM.ClassRoomID
					left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
					left join ReasonMaster RM on RM.ID = ARD. ReasonID
					left join ReasonType RT on RT.Id = RM.ReasonType 
					WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate
					) A
				Group By A.ClassRoomID,A.ClassName,A.AttendanceDate,A.AttendanceMon, A.AttendanceMonthName, A.AttendanceYear
				) B
			group by B.AttendanceMon, B.AttendanceMonthName, B.AttendanceYear
			order by B.AttendanceYear, B.AttendanceMon

		END


END

END



/*

exec StudentAttendaceSummary
	
	@AttendanceDate ='2018-11-14',
	@ClassRoomID = 48,
	@Type = 1,
	@TeacherId = 115

	*/
GO
/****** Object:  StoredProcedure [dbo].[StudentAttendaceSummaryForAdmin]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[StudentAttendaceSummaryForAdmin]
	
	@AttendanceDate DATE,
	@SchoolID int,
	@ClassRoomID int,
	@Type TINYINT,
	@TeacherId INT

	AS
	BEGIN

	Declare @userType int
				
	select @userType = (select UserType from UserMaster where ID = @TeacherId AND IsActive = 1)

IF @userType = 1
BEGIN
	
	IF @Type = 0
		BEGIN
			
		select CONVERT(decimal(5,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
				CONVERT(decimal(5,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
				CONVERT(decimal(5,2),(sum(B.AbsentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as BoysAbsenteeRate,
				CONVERT(decimal(5,2),(sum(B.AbsentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as GirlsAbsenteeRate,
				CONVERT(decimal(5,2),(sum(B.PresentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as BoysAttendanceRate,
				CONVERT(decimal(5,2),(sum(B.PresentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as GirlsAttendanceRate
				from
				(
					select count(case when A.IsPresent = 1 then 1 end) as Present,
					count(case when A.IsPresent = 0 then 0 end) as 'Absent',
					count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
					count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
					count(case when A.Gender = 'M' and A.IsPresent = 1 then  'M' end) as PresentBoys,
					count(case when A.Gender = 'F' and A.IsPresent = 1 then  'F' end) as PresentGirls,
					count(A.ChildId) TotalStudents
					from
						(
						select AM.ChildID, AM.IsPresent, CHM.Gender
						from AttendanceMaster AM 
							inner join dbo.GetClassList(@TeacherId) CL on CL.Id = AM.ClassRoomId
							inner join ChildrenMaster CHM on CHM.ID = AM.ChildID
								WHERE Convert(date,AM.AttendanceDate)= @AttendanceDate AND CHM.IsActive = 1
						)A
					)B
				END

END

ELSE IF @userType = 4
BEGIN
	
	IF @Type = 0
		BEGIN
			
		select CONVERT(decimal(5,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
				CONVERT(decimal(5,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
				CONVERT(decimal(5,2),(sum(B.AbsentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as BoysAbsenteeRate,
				CONVERT(decimal(5,2),(sum(B.AbsentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as GirlsAbsenteeRate,
				CONVERT(decimal(5,2),(sum(B.PresentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as BoysAttendanceRate,
				CONVERT(decimal(5,2),(sum(B.PresentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as GirlsAttendanceRate
				from
				(
					select count(case when A.IsPresent = 1 then 1 end) as Present,
					count(case when A.IsPresent = 0 then 0 end) as 'Absent',
					count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
					count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
					count(case when A.Gender = 'M' and A.IsPresent = 1 then  'M' end) as PresentBoys,
					count(case when A.Gender = 'F' and A.IsPresent = 1 then  'F' end) as PresentGirls,
					count(A.ChildId) TotalStudents
					from
						(
						select AM.ChildID, AM.IsPresent, CHM.Gender
						from AttendanceMaster AM 
							inner join dbo.GetClassListForSchool(@SchoolID) CL on CL.Id = AM.ClassRoomId
							inner join ChildrenMaster CHM on CHM.ID = AM.ChildID
								WHERE Convert(date,AM.AttendanceDate)= @AttendanceDate AND CHM.IsActive = 1
						)A
					)B
				END

END

END



	/*
	EXEC  StudentAttendaceSummaryForAdmin 
		@AttendanceDate = '2018-11-14',
		@SchoolID = 11,
		@ClassRoomID = 54,
		@Type  = 0,
		@TeacherId = 115

	*/


	/*select * from ChildrenMaster where ClassroomID = 54*/
GO
/****** Object:  StoredProcedure [dbo].[StudentAttendaceSummaryForSA]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StudentAttendaceSummaryForSA]
	
	@AttendanceDate DATE,
	@ClassRoomID int,
	@Type TINYINT,
	@TeacherId INT,
	@SchoolId INT

	AS
	BEGIN
	Declare		@MonthStartDate date, 
				@MonthEndDate date, 
				@WeekStartDate Date, 
				@WeekEndDate Date, 
				@YearStartDate Date, 
				@YearEndDate Date,
				@userType int
		
	
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
		select @WeekStartDate = DATEADD(DAY, 2 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE)) 
		select @WeekEndDate = DATEADD(DAY, 8 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE))
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))



	IF @Type = 1

		BEGIN
		/*
		select CONVERT(decimal(5,2),(sum(B.Present)/CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
			CONVERT(decimal(5,2),(sum(B.Absent) / CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
			CONVERT(decimal(5,2),(sum(B.AbsentBoys) / CONVERT(decimal(6,3), nullif(sum(B.TotalBoys),0)) * 100)) as BoysAbsenteeRate,
			CONVERT(decimal(5,2),(sum(B.AbsentGirls) / CONVERT(decimal(6,3), nullif(sum(B.TotalGirls),0)) * 100)) as GirlsAbsenteeRate,
			CONVERT(decimal(5,2),(sum(B.Health) / CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
			CONVERT(decimal(5,2),(sum(B.NonHealth) / CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
			CONVERT(decimal(5,2),(sum(B.UnKnown) / CONVERT(decimal(6,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
			*/
			SELECT	
			CONVERT(decimal(15,2),(sum(B.Present)/CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AttendanceRate,
			CONVERT(decimal(15,2),(sum(B.Absent) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as AbsenteesimRate,
			CONVERT(decimal(15,2),(sum(B.AbsentBoys) / CONVERT(decimal(16,3), nullif(sum(B.TotalBoys),0)) * 100)) as BoysAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.AbsentGirls) / CONVERT(decimal(16,3), nullif(sum(B.TotalGirls),0)) * 100)) as GirlsAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.Health) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.NonHealth) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.UnKnown) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
		from
		(
		select A.ClassRoomID,A.ClassName,A.AttendanceDate,
			count(A.ClassRoomID) as TotalStudents,
			count(case when A.IsPresent = 1 then 1 end) as Present,
			count(case when A.IsPresent = 0 then 0 end) as 'Absent',
			count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
			count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
			count(case when A.Gender = 'M' then  'Male' end) as TotalBoys,
			count(case when A.Gender = 'F' then  'Female' end) as TotalGirls,
			count(case when A.Code = 'Health' Then 'H' end) as Health,
			count(case when A.Code = 'Non-Health' Then 'NH' end) as NonHealth,
			count(case when A.Code = 'UnKnown' Then 'U' end) as UnKnown
			from
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate,
				AM.IsPresent,isnull(CHM.Gender,'M') Gender,isnull(RT.Code,'UnKnown') Code

					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
					inner join ChildrenMaster CHM on AM.ChildID = CHM.ID	
					inner join dbo.GetClassListForSchool(@SchoolId) CL on CL.Id = AM.ClassRoomID
					left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
					left join ReasonMaster RM on RM.ID = ARD. ReasonID
					left join ReasonType RT on RT.Id = RM.ReasonType 
					WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate AND CHM.IsActive = 1
					) A
				Group By A.ClassRoomID,A.ClassName,A.AttendanceDate
				) B

		END


		ELSE IF @Type = 2

		BEGIN
		select B.AttendanceMon, B.AttendanceMonthName, B.AttendanceYear,
			CONVERT(decimal(15,2),(sum(B.Health) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as HealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.NonHealth) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as NonHealthAbsenteeRate,
			CONVERT(decimal(15,2),(sum(B.UnKnown) / CONVERT(decimal(16,3), nullif(sum(B.TotalStudents),0)) * 100)) as UnKnownAbsenteeRate
		from
		(
		select A.ClassRoomID,A.ClassName,A.AttendanceDate, A.AttendanceMon, A.AttendanceMonthName, A.AttendanceYear,
			count(A.ClassRoomID) as TotalStudents,						
			count(case when A.Code = 'Health' Then 'H' end) as Health,
			count(case when A.Code = 'Non-Health' Then 'NH' end) as NonHealth,
			count(case when A.Code = 'UnKnown' Then 'U' end) as UnKnown
			from
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate,DATEPART(m, AM.AttendanceDate)										AttendanceMon,Convert(char(3), AM.AttendanceDate, 0) AttendanceMonthName,
				AM.IsPresent,isnull(RT.Code,'UnKnown') Code, year(AM.AttendanceDate) AttendanceYear
					from AttendanceMaster AM 
					inner join ClassMaster CM on AM.ClassRoomID = CM.ID					
					inner join dbo.GetClassListForSchool(@SchoolId) CL on CL.Id = AM.ClassRoomID
					left join AbsenteeReasonDetails ARD on ARD.ChildID = AM.ChildID and ARD.TeacherID = AM.TeacherID and convert(date,						ARD.CreatedOn) = convert(date,AM.AttendanceDate)
					left join ReasonMaster RM on RM.ID = ARD. ReasonID
					left join ReasonType RT on RT.Id = RM.ReasonType 
					WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate
					) A
				Group By A.ClassRoomID,A.ClassName,A.AttendanceDate,A.AttendanceMon, A.AttendanceMonthName, A.AttendanceYear
				) B
			group by B.AttendanceMon, B.AttendanceMonthName, B.AttendanceYear
			order by B.AttendanceYear, B.AttendanceMon

		END


END



/*

exec StudentAttendaceSummaryForSA
	
	@AttendanceDate ='2018-11-14',
	@ClassRoomID = 48,
	@Type = 1,
	@TeacherId = 115,
	@SchoolId = 19

	*/
GO
/****** Object:  StoredProcedure [dbo].[StudentMonthlyAttendanceSummary]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[StudentMonthlyAttendanceSummary]

 @AttendanceDate DATE,
	@ClassRoomID int,
	@TeacherId INT,
	@Type TINYINT

	AS
	BEGIN

	Declare @MonthStartDate date, 
				@MonthEndDate date, 				
				@YearStartDate Date,
				@YearEndDate Date,
				@userType int

		select @userType = (select UserType from UserMaster where id = @TeacherId)
				
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)		
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

	IF @userType = 2
	BEGIN

		IF @Type = 1
		BEGIN

				select count(case when B.Absent >= 3 then 1 end) as StudentCount1,
						count(case when B.Absent < 3 and B.Absent > 0 then 0 end) as StudentCount2
				from
				(
				select A.ClassRoomID,A.ClassName,A.ChildID,						
						count(case when A.IsPresent = 0 then 0 end) as 'Absent'			
				from 
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate, CHM.ID ChildID,
							AM.IsPresent

								from AttendanceMaster AM 
								inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
								inner join ChildrenMaster CHM on AM.ChildID = CHM.ID
								WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate and AM.ClassRoomID = @ClassRoomID
								and AM.IsPresent = 0 AND CHM.IsActive = 1
				)A
				Group By A.ClassRoomID,A.ClassName,A.ChildID
			)B

		END

		ELSE IF @Type = 3
		BEGIN

				select 	count(case when B.PresentPercent between 80 and 89 then 'Chronically Absent' end ) StudentCount1,
			count(case when B.PresentPercent < 80 then 'Severely Chronically Absent' end) StudentCount2
			from
			(
			select A.ClassRoomID,A.ClassName, A.ChildId, 		
				count(A.AttendanceDate) as TotalDays,
				count(case when A.IsPresent = 1 then 1 end) as Present,
				count(case when A.IsPresent = 0 then 0 end) as 'Absent',
				(count(case when A.IsPresent = 1 then 1 end) / convert(decimal(6,3), nullif(count(A.AttendanceDate),0)) * 100) as PresentPercent			
				from
					(
					select AM.ClassRoomID,CM.ClassName, AM.ChildID ChildID, DAY(AM.AttendanceDate) AttendanceDate,
					AM.IsPresent

						from AttendanceMaster AM 
						inner join ClassMaster CM on AM.ClassRoomID = CM.ID	
						WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate and AM.ClassRoomID = @ClassRoomID
						) A
					Group By A.ClassRoomID,A.ClassName , A.ChildId
					)B 
					where B.PresentPercent < 90

		END

	END

	ELSE IF @userType = 1 OR  @userType = 4
	BEGIN
		IF @Type = 1
		BEGIN

		select count(case when B.Absent >= 3 then 1 end) as StudentCount1,
						count(case when B.Absent < 3 and B.Absent > 0 then 0 end) as StudentCount2
				from
				(
				select A.ClassRoomID,A.ClassName,A.ChildID,						
						count(case when A.IsPresent = 0 then 0 end) as 'Absent'			
				from 
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate, CHM.ID ChildID,
							AM.IsPresent

								from AttendanceMaster AM 
								inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
								inner join ChildrenMaster CHM on AM.ChildID = CHM.ID
								inner join dbo.GetClassList(@TeacherId) CL on CL.Id = AM.ClassRoomID
								WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate AND CHM.IsActive = 1
								and AM.IsPresent = 0
				)A
				Group By A.ClassRoomID,A.ClassName,A.ChildID
			)B

		END


	END


	END




GO
/****** Object:  StoredProcedure [dbo].[StudentMonthlyAttendanceSummaryForSA]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[StudentMonthlyAttendanceSummaryForSA]

 @AttendanceDate DATE,
	@ClassRoomID int,
	@TeacherId INT,
	@Type TINYINT,
	@SchoolId INT

	AS
	BEGIN

	Declare @MonthStartDate date, 
				@MonthEndDate date, 				
				@YearStartDate Date,
				@YearEndDate Date
				
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)		
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

	
	
		IF @Type = 1
		BEGIN

		select count(case when B.Absent >= 3 then 1 end) as StudentCount1,
						count(case when B.Absent < 3 and B.Absent > 0 then 0 end) as StudentCount2
				from
				(
				select A.ClassRoomID,A.ClassName,A.ChildID,						
						count(case when A.IsPresent = 0 then 0 end) as 'Absent'			
				from 
				(
				select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate, CHM.ID ChildID,
							AM.IsPresent

								from AttendanceMaster AM 
								inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
								inner join ChildrenMaster CHM on AM.ChildID = CHM.ID
								inner join dbo.GetClassListForSchool(@SchoolId) CL on CL.Id = AM.ClassRoomID
								WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate AND CHM.IsActive = 1
								and AM.IsPresent = 0
				)A
				Group By A.ClassRoomID,A.ClassName,A.ChildID
			)B

		END	


	END
GO
/****** Object:  StoredProcedure [dbo].[TeacherDailyReport]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[TeacherDailyReport]
	
	@AttendanceDate DATE,
	@ClassRoomID int,
	@Type TINYINT,
	@TeacherId INT

	AS
	BEGIN
		Declare @MonthStartDate date, 
				@MonthEndDate date, 
				@WeekStartDate Date, 
				@WeekEndDate Date, 
				@YearStartDate Date, 
				@YearEndDate Date
		
		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@AttendanceDate)-1),@AttendanceDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@AttendanceDate))),
						  DATEADD(mm,1,@AttendanceDate)),120)
		select @WeekStartDate = DATEADD(DAY, 2 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE)) 
		select @WeekEndDate = DATEADD(DAY, 8 - DATEPART(WEEKDAY, @AttendanceDate), CAST(@AttendanceDate AS DATE))
		--select @YearStartDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate), 0)
		--select @YearEndDate = DATEADD(yy, DATEDIFF(yy, 0, @AttendanceDate) + 1, -1)

		select @YearStartDate = (select convert(date,ATD.Start_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

		select @YearEndDate = (select convert(date,ATD.End_Date) from 
							AcademicTermDetails ATD join SchoolMaster SM on ATD.SchoolId = SM.ID and SM.IsActive= 1
							join ClassMaster CM on CM.SchoolId = SM.ID and CM.IsActive= 1 and CM.Id = @ClassRoomID
							where convert(date,@AttendanceDate) between convert(date,ATD.Start_Date) and convert(date,ATD.End_Date))

	IF @Type = 0
		BEGIN
		select AM.ClassRoomID,CM.ClassName,
		(select count(id) from AttendanceMaster where IsPresent=0 and TeacherID = @TeacherId and ClassRoomId = @ClassRoomID and Convert(date,AttendanceDate) = @AttendanceDate group by ClassRoomID) as 'Absent',
		(select count(id) from AttendanceMaster where IsPresent=1 and TeacherID = @TeacherId and ClassRoomId = @ClassRoomID and Convert(date,AttendanceDate) = @AttendanceDate group by ClassRoomID) as 'Present',
		(select count(isnull(CHM.Gender,'M')) from ChildrenMaster CHM inner join AttendanceMaster AM on AM.ChildID = CHM.ID AND CHM.IsActive = 1 and isnull(CHM.Gender,'M')='M' and AM.ClassRoomID=@ClassRoomID and  Convert(date,AM.AttendanceDate) = @AttendanceDate and AM.IsPresent=0 Group By AM.ClassRoomID) as 'AbsentBoys',
		(select count(isnull(CHM.Gender,'')) from ChildrenMaster CHM inner join AttendanceMaster AM on AM.ChildID = CHM.ID AND CHM.IsActive = 1 and isnull(CHM.Gender,'')='F' and AM.ClassRoomID=@ClassRoomID and  Convert(date,AM.AttendanceDate) = @AttendanceDate and AM.IsPresent=0 Group By AM.ClassRoomID) as 'AbsentGirls',
		(select count(isnull(CHM.Gender,'M')) from ChildrenMaster CHM inner join AttendanceMaster AM on AM.ChildID = CHM.ID  and isnull(CHM.Gender,'M')='M' and AM.ClassRoomID=@ClassRoomID and  Convert(date,AM.AttendanceDate) = @AttendanceDate Group By AM.ClassRoomID) as 'TotalBoys',
		(select count(isnull(CHM.Gender,'')) from ChildrenMaster CHM inner join AttendanceMaster AM on AM.ChildID = CHM.ID  and isnull(CHM.Gender,'')='F' and AM.ClassRoomID=@ClassRoomID and  Convert(date,AM.AttendanceDate) = @AttendanceDate Group By AM.ClassRoomID) as 'TotalGirls'

		from AttendanceMaster AM 
			Inner join TeacherClassMapping TM on Am.TeacherID = @TeacherId and Tm.IsActive = 1 
			INNER Join ClassMaster CM on AM.ClassRoomID = CM.ID			
			WHERE Convert(date,AM.AttendanceDate) = @AttendanceDate and AM.ClassRoomID = @ClassRoomID
		Group By AM.ClassRoomID,CM.ClassName
		END

	ELSE IF @Type = 1
		BEGIN 
		select A.ClassRoomID,A.ClassName,A.AttendanceDate,
	count(case when A.IsPresent = 1 then 1 end) as Present,
	count(case when A.IsPresent = 0 then 0 end) as Absent,
	count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
	count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
	count(case when A.Gender = 'M' then  'Male' end) as TotalBoys,
	count(case when A.Gender = 'F' then  'Female' end) as TotalGirls
	from
		(
		select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate,
		AM.IsPresent,isnull(CHM.Gender,'M') Gender

			from AttendanceMaster AM 
			inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
			inner join ChildrenMaster CHM on AM.ChildID = CHM.ID		
			WHERE Convert(date,AM.AttendanceDate) between @MonthStartDate AND @MonthEndDate and AM.ClassRoomID = @ClassRoomID AND CHM.IsActive = 1
			) A
		Group By A.ClassRoomID,A.ClassName,A.AttendanceDate
		order by A.AttendanceDate
		END

	ELSE IF @Type = 2
		BEGIN 
		select A.ClassRoomID,A.ClassName,A.AttendanceDate, A.AttendanceYear, A.AttendanceMonth,
	count(case when A.IsPresent = 1 then 1 end) as Present,
	count(case when A.IsPresent = 0 then 0 end) as Absent,
	count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
	count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
	count(case when A.Gender = 'M' then  'Male' end) as TotalBoys,
	count(case when A.Gender = 'F' then  'Female' end) as TotalGirls
	from
		(
		select AM.ClassRoomID,CM.ClassName,DAY(AM.AttendanceDate) AttendanceDate, year(AM.AttendanceDate) AttendanceYear, DATEPART(m, AM.AttendanceDate) AttendanceMonth,
		AM.IsPresent,isnull(CHM.Gender,'M') Gender

			from AttendanceMaster AM 
			inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
			inner join ChildrenMaster CHM on AM.ChildID = CHM.ID		
			WHERE Convert(date,AM.AttendanceDate) between @WeekStartDate AND @WeekEndDate and AM.ClassRoomID = @ClassRoomID AND CHM.IsActive = 1
			) A
		Group By A.ClassRoomID,A.ClassName,A.AttendanceDate, A.AttendanceYear, A.AttendanceMonth
		order by A.AttendanceYear, A.AttendanceMonth, A.AttendanceDate
		END

	ELSE IF @Type = 3
		BEGIN 
		
			select C.AttendanceDate, C.AttendanceMonth AttendanceMonthName, C.AttendanceYear, 
			count(C.ChronicallyAbsent) ChronicallyAbsent, 
			count(SeverelyChronicallyAbsent) SeverelyChronicallyAbsent
			from
			(
			select B.ChildId, B.Name, B.Absent AbsentDays, B.AttendanceDate, B.AttendanceMonth,B.AttendanceYear,
			case when B.PresentPercent between 80 and 89 then 1 end 'ChronicallyAbsent',
			case when B.PresentPercent < 80 then 1 end 'SeverelyChronicallyAbsent'
				from
				(
				select A.ClassRoomID,A.ClassName, A.ID ChildId, A.Name,	A.AttendanceDate, A.AttendanceMonth,	
					count(A.AttendanceDate) as TotalDays,A.AttendanceYear,
					count(case when A.IsPresent = 1 then 1 end) as Present,
					count(case when A.IsPresent = 0 then 0 end) as 'Absent',
					(count(case when A.IsPresent = 1 then 1 end) / convert(decimal(6,3), nullif(count(A.AttendanceDate),100)) * 100) as PresentPercent			
					from
						(
						select AM.ClassRoomID,CM.ClassName, CHM.ID, (CHM.Name + ' ' + CHM.Surname) Name, DATEPART(m, AM.AttendanceDate)										AttendanceDate, AM.IsPresent, Convert(char(3), AM.AttendanceDate, 0) AttendanceMonth, year(AM.AttendanceDate) AttendanceYear
							from AttendanceMaster AM 
							inner join ClassMaster CM on AM.ClassRoomID = CM.ID			
							inner join ChildrenMaster CHM on AM.ChildID = CHM.ID
							WHERE Convert(date,AM.AttendanceDate) between @YearStartDate AND @YearEndDate and AM.ClassRoomID = @ClassRoomID AND CHM.IsActive = 1
							)A
							Group By A.ClassRoomID,A.ClassName , A.ID, A.Name, A.AttendanceDate, A.AttendanceMonth, A.AttendanceYear
						)B
						where B.PresentPercent < 90
					)C
				Group by C.AttendanceDate, C.AttendanceMonth, C.AttendanceYear
				order by C.AttendanceYear, C.AttendanceDate

		END
	END


/*
exec TeacherDailyReport 
	@AttendanceDate ='2018-11-02',
	@ClassRoomID =51,
	@Type =3,
	@TeacherId =97

	*/
GO
/****** Object:  StoredProcedure [dbo].[TeacherInitialReport]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[TeacherInitialReport]
	
	@AttendanceDate DATE,	
	@Type TINYINT,
	@TeacherId INT

	AS
	BEGIN		


	IF @Type = 0
		BEGIN
		select A.ClassRoomID,A.ClassName,
	count(case when A.IsPresent = 1 then 1 end) as Present,
	count(case when A.IsPresent = 0 then 0 end) as Absent,
	count(case when A.Gender = 'M' and A.IsPresent = 0 then  'M' end) as AbsentBoys,
	count(case when A.Gender = 'F' and A.IsPresent = 0 then  'F' end) as AbsentGirls,
	count(case when A.Gender = 'M' then  'Male' end) as TotalBoys,
	count(case when A.Gender = 'F' then  'Female' end) as TotalGirls
	from
		(
		select AM.ClassRoomID,CM.ClassName,
		AM.IsPresent,isnull(CHM.Gender,'M') Gender

			from AttendanceMaster AM 			
			INNER Join dbo.GetClassList(@TeacherId) CM on AM.ClassRoomID = CM.ID
			inner join ChildrenMaster CHM on AM.ChildID = CHM.ID		
			WHERE Convert(date,AM.AttendanceDate) = @AttendanceDate AND CHM.IsActive = 1
			) A
		Group By A.ClassRoomID,A.ClassName
		END

	END



GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_AbsenteeReasonDetails]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_Get_AbsenteeReasonDetails]
	@ChildID INT,
	@ClassroomID int,
	@CreatedOn DATE
--	@AttendanceMasterID INT
	
AS
BEGIN
	--DECLARE @CreatedOn DATE
	--DECLARE @TeacherID INT

	--SELECT @TeacherID =  TeacherID FROM TeacherClassMapping TCM WHERE TCM.ClassroomID = @ClassroomID AND TCM.IsActive=1

	--SELECT @CreatedOn = CAST(AttendanceDate AS DATE)  FROM AttendanceMaster WHERE ID = @AttendanceMasterID AND ChildID= @ChildID



	SELECT ReasonDetail.ID,
			ReasonDetail.ReasonID,
			ReasonDetail.ChildID,
			ReasonDetail.TeacherID,
			ReasonDetail.IsHealthReason,
			ReasonDetail.InformationProvider,
			ReasonDetail.HasPermission,
			ReasonDetail.CreatedBy,
			ReasonDetail.CreatedOn,
			ISNULL(ReasonDetail.Notes,'') AS Notes,
			ISNULL(ReasonDetail.Notes_Indonesian,'') AS Notes_Indonesian	
			,ReasonMaster.ReasonType FROM AbsenteeReasonDetails ReasonDetail
			LEFT JOIN ReasonMaster ReasonMaster ON ReasonDetail.ReasonID = ReasonMaster.ID
			 WHERE ChildID = @ChildID 
			AND CAST(ReasonDetail.CreatedOn AS DATE) = @CreatedOn
END

	
	-- EXEC Usp_Get_AbsenteeReasonDetails 3392, 66, '2019-02-01'

	-- select * from AbsenteeReasonDetails where childid=3392

GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_AllChildrenByClass]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_Get_AllChildrenByClass]
	@ClassRoomID INT,
	@AttendanceDate DATE
	
AS
BEGIN

	DECLARE @SChoolID int
	DECLARE @TermStartDate DATE
	DECLARE @TermEndDate DATE
	DECLARE @WorkingDays INT

SELECT @SChoolID =  SchoolId FROM ClassMaster WHERE ID =  @ClassRoomID

	SELECT @TermStartDate = Start_Date,@TermEndDate = End_Date FROM AcademicTermDetails WHERE SchoolId = @SchoolID  AND IsCurrentTerm = 1
	

SELECT @WorkingDays = dbo.Fn_GetNumberOfWorkingDays(@SChoolID,ISNULL(@TermStartDate,GETDATE()),GETDATE())


	SELECT CM.ID, CM.Name,		
	CM.Surname,CM.Image,CM.ClassroomID,
	CM.Phone,CM.Gender
	,CM.ParentName,CM.Address,CM.GeoLocation
	,ClassMaster.ClassName
	,AttendanceMaster.IsPresent
	,AttendanceMaster.IsSubmitted	
	,ISNULL(AttendanceDate,@AttendanceDate) AS AttendanceDate
	,CAST(ISNULL((CAST(AttendanceSummary.PresentCount AS float) / CAST(@WorkingDays AS float) * 100),0) as decimal(10,2)) As AttendancePercent	
	, (CASE WHEN AttendanceMaster.IsSubmitted IS NULL THEN 2  ELSE AttendanceMaster.IsSubmitted END ) AttendanceStatus
	,AttendanceMaster.ID AS AttendanceMasterID

	FROM ChildrenMaster CM
	LEFT JOIN AttendanceMaster ON AttendanceMaster.ChildID = CM.ID AND CAST(AttendanceDate AS DATE)  = @AttendanceDate 
	LEFT JOIN ClassMaster ON CM.ClassroomID = ClassMaster.ID
	INNER JOIN TeacherClassMapping TCM ON CM.ClassroomID = TCM.ClassRoomID

		LEFT JOIN (
							 SELECT ChildID,count(IsPresent)  as PresentCount
							 FROM AttendanceMaster 
							 WHERE AttendanceDate BETWEEN @TermStartDate AND @TermEndDate 
								AND  IsPresent = 1
							 GROUP BY ChildID
						) AS AttendanceSummary on AttendanceSummary.ChildID = CM.ID

    WHERE CM.ClassroomID =  @ClassRoomID AND CM.IsActive = 1

END


--	EXEC Usp_Get_AllChildrenByClass @ClassRoomID = 66,@AttendanceDate = '2019-02-08' 





GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_ChildDetailsByID]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_Get_ChildDetailsByID]
	@ChildID INT
AS
BEGIN

		SELECT CM.ID, CM.Name, ISNULL(CM.Middlename,'') as Middlename,CM.Surname,CM.ParentName,CM.CommunityWorkerID, 
					CM.Email,CM.SchoolID,CM.ClassroomID,CM.Address, CM.Image, 
					CM.Phone, CM.Note , '' as ImageBase, CM.GeoLocation,
					
					CASE WHEN(CM.Gender = 'F') THEN 'Boy' 
						WHEN(CM.Gender = 'M') THEN 'Girl' 
					END AS Gender,					
					(UM.Name + ' '+UM.Surname)  as TeacherName,
					SM.SchoolName,ClassMaster.ClassName as ClassroomName 				
					FROM  ChildrenMaster CM 				
					LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID 
					LEFT JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID 
					LEFT JOIN TeacherClassMapping tcm on CM.ClassroomID = tcm.ClassroomID AND tcm.IsActive = 1
					LEFT JOIN UserMaster UM on UM.ID = tcm.TeacherID
					WHERE 
					 CM.ID = @ChildID 
END


-- EXEC Usp_Get_ChildDetailsByID @ChildID = 40



GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_ClassCountAbsenceCount]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_Get_ClassCountAbsenceCount]
	@TeacherID INT,
	@AttendanceDate DATE
	
AS

	DECLARE @ClassCount INT = 0
	DECLARE @AbsenceCount INT = 0

	BEGIN
	
		SELECT @ClassCount = (SELECT COUNT(A.ClassRoomID) AS ClassCount FROM (SELECT TCM.ClassRoomID, 
					( 
						SELECT COUNT(1)  FROM AttendanceMaster WHERE TeacherID = TCM.TeacherID AND ClassRoomID = TCM.ClassRoomID AND IsSubmitted = 0 AND CAST(AttendanceDate AS DATE) = @AttendanceDate) AS NotSubmittedCount,

						(SELECT COUNT(1)  FROM AttendanceMaster WHERE TeacherID = TCM.TeacherID AND ClassRoomID = TCM.ClassRoomID AND CAST(AttendanceDate AS DATE) = @AttendanceDate) AS ClassAttendanceCount
						 FROM TeacherClassMapping TCM
						WHERE TCM.TeacherID =  @TeacherID 
					) A

			WHERE A.ClassAttendanceCount = 0 OR NotSubmittedCount > 0)
			

/*
		SELECT @AbsenceCount =  (
						SELECT COUNT(AM.ChildID) AS AbsenceCount FROM AttendanceMaster AM WHERE
						TeacherID = @TeacherID AND AM.IsPresent = 0 
						AND CAST(AttendanceDate AS DATE) = @AttendanceDate 
						AND NOT EXISTS 
							(SELECT ChildID FROM AbsenteeReasonDetails WHERE ChildID = AM.ChildID AND TeacherID = @TeacherID AND CAST(CreatedOn AS DATE) =  @AttendanceDate AND ISNULL(ReasonID,0) = 0) 
						)

		*/
		
		SELECT @AbsenceCount = (
		SELECT COUNT(ChildID) FROM (
			SELECT AM.*,ARD.ReasonID FROM AttendanceMaster AM
			LEFT JOIN AbsenteeReasonDetails ARD ON ARD.ChildID = AM.ChildID AND  ARD.TeacherID = @TeacherID  AND CAST(ARD.CreatedOn AS DATE) =  @AttendanceDate 

			WHERE AM.TeacherID = @TeacherID  AND CAST(AM.AttendanceDate AS DATE) =  @AttendanceDate AND AM.IsPresent = 0 

			) A

			WHERE ISNULL(A.ReasonID,0) = 0 
		)
					

  SELECT @ClassCount AS ClassCount, @AbsenceCount  AS AbsenceCount

	END 



	--  EXEC Usp_Get_ClassCountAbsenceCount 58, '2019-02-01'



GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_HolidayListByMonth]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Usp_Get_HolidayListByMonth]	
	@HolidayDate DATE,
	@TeacherID int,
	@SchoolID INT

AS
	BEGIN

		Declare @MonthStartDate date
		DECLARE @MonthEndDate date
				

		select @MonthStartDate =  CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@HolidayDate)-1),@HolidayDate),120) 
		select @MonthEndDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(DATEADD(mm,1,@HolidayDate))),
						  DATEADD(mm,1,@HolidayDate)),120)
		
		IF (@SchoolID = 0)
			BEGIN
				SELECT @SchoolID = ( SELECT DISTINCT sm.ID
				  FROM TeacherClassMapping tcm 
					INNER JOIN ClassMaster cm on cm.ID = tcm.ClassRoomID 
					INNER JOIN SchoolMaster sm on sm.ID = cm.SchoolId
					WHERE tcm.TeacherID = @TeacherID 
					)
						 
			END

		select YHM.Id HolidyId, convert(date,HolidayDate) HolidayDate,ISNULL(YHM.Reason,'') AS Reason
		,ISNULL(YHM.Reason_Indonesian,'') AS Reason_Indonesian,YHM.SchoolId from 
		YearlyHolidayMaster YHM WHERE SchooliD = @SchoolID and isnull(YHM.IsActive,0) = 1 
		and convert(date,YHM.HolidayDate) between @MonthStartDate and @MonthEndDate 
	END



	-- EXEC Usp_Get_HolidayListByMonth  '2019-01-01 ',74,0

GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_MappedClassList]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_Get_MappedClassList]
	@UserID int
	
AS
BEGIN

	DECLARE @UserType INT
	DECLARE @SchoolID  INT

	SELECT @UserType = UserType FROM UserMaster WHERE ID = @UserID
	
	IF(@UserType = 4)  --SuperAdmin
		BEGIN
			SELECT CM.ID, CM.ClassName,CM.SchoolId,CM.CreatedBy,CM.CreatedOn, CM.IsActive ,SM.SchoolName
			From ClassMaster CM 
			LEFT JOIN  SchoolMaster SM ON CM.SchoolId = SM.ID			
			WHERE CM.IsActive=1
			
			
		END
	ELSE IF(@UserType = 1)
		BEGIN
			SELECT @SchoolID = SchoolId FROM SchoolAdminMapping WHERE UserID = @UserID
			
			SELECT  CM.ID, CM.ClassName,CM.SchoolId,CM.CreatedBy,CM.CreatedOn,CM.IsActive,SM.SchoolName
			FROM  ClassMaster CM 	
			LEFT JOIN  SchoolMaster SM ON CM.SchoolId = SM.ID			
			WHERE CM.ID =  @SchoolID AND CM.IsActive = 1

		END
	
END


--	exec Usp_Get_MappedClassList 5



GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_MyUserList]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_Get_MyUserList]
	@UserID int,
	@LangID CHAR(3)
	
AS
BEGIN

	DECLARE @UserType INT
	DECLARE @SchoolID  INT

	SELECT @UserType = UserType FROM UserMaster WHERE ID = @UserID
	
	IF(@UserType = 4)  --SuperAdmin
		BEGIN		

			SELECT UserMaster.ID, UserName, Password, ISNULL(Name,'') as Name, ISNULL(Surname,'') as Surname,ISNULL(Image,'') Image, ISNULL(UserMaster.UserType,'') UserType,ISNULL(Address,'') Address,ISNULL(Phone,'') Phone, UTM.UserType as UserTypeName,UserMaster.GeoLocation,UserMaster.Gender,UserMaster.Email 
			,ISNULL(CASE WHEN @LangID = 'id' THEN UserMaster.Note_Indonesian ELSE UserMaster.Note END
		,'') AS Note
			 FROM UserMaster Left Join UserTypeMaster UTM on UTM.ID = UserMaster.UserType WHERE UserMaster.IsActive=1 ORDER BY Name


		END
	ELSE IF(@UserType = 1)
		BEGIN
			SELECT @SchoolID = SchoolId FROM SchoolAdminMapping WHERE UserID = @UserID
			
			/*			
			SELECT UserMaster.ID, UserName, Password, ISNULL(Name,'') as Name, ISNULL(Surname,'') as Surname,ISNULL(Image,'') Image, ISNULL(UserMaster.UserType,'') UserType,ISNULL(Address,'') Address,ISNULL(Phone,'') Phone,ISNULL(Note,'') Note, UTM.UserType as UserTypeName,UserMaster.GeoLocation,UserMaster.Gender,UserMaster.Email  FROM TeacherClassMapping TCM 
				INNER JOIN ClassMaster CM on CM.ID = TCM.ClassRoomID AND CM.SchoolId =  @SchoolID
				INNER JOIN UserMaster UserMaster on TCM.TeacherID = UserMaster.ID
			--	Left Join UserTypeMaster UTM on UTM.ID = @UserType
			Left Join UserTypeMaster UTM on UTM.ID =UserMaster.UserType
				WHERE UserMaster.IsActive = 1 ORDER BY Name

				*/

				SELECT UserMaster.ID, UserName, Password, ISNULL(Name,'') as Name, ISNULL(Surname,'') as Surname,ISNULL(Image,'') Image, ISNULL(UserMaster.UserType,'') UserType,ISNULL(Address,'') Address,ISNULL(Phone,'') Phone, UTM.UserType as UserTypeName,UserMaster.GeoLocation,UserMaster.Gender,UserMaster.Email 
				,ISNULL(CASE WHEN @LangID = 'id' THEN UserMaster.Note_Indonesian ELSE UserMaster.Note END
		,'') AS Note
				 FROM UserMaster  

				INNER JOIN 
				(
					SELECT DISTINCT TeacherID as UserID FROM TeacherClassMapping  TeacherMapping
					INNER JOIN ClassMaster on ClassMaster.ID = TeacherMapping.ClassRoomID AND ClassMaster.SchoolId = @SchoolID
					UNION  
					SELECT UserId as UserID from SchoolAdminMapping where schoolId = @SchoolID AND IsActive = 1

					UNION  
					SELECT ID as UserID from UserMaster where CreatedBy = @UserID AND IsActive = 1

				)  AS TempUser ON  UserMaster.ID = TempUser.UserID

				Left Join UserTypeMaster UTM on UTM.ID = UserMaster.UserType		
				
				ORDER BY Name


		END
	
END


--	exec Usp_Get_MyUserList 5,'id'





GO
/****** Object:  StoredProcedure [dbo].[Usp_Get_ReasonMaster]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_Get_ReasonMaster]
	@LangID char(3)
	
AS
BEGIN
	SELECT ReasonMaster.ID, ReasonMaster.ReasonType
	,CASE WHEN @LangID = 'id' THEN ReasonMaster.Description_Indonesian ELSE ReasonMaster.Description END
		AS Description
,CASE WHEN @LangID = 'id' THEN ReasonType.Description_Indonesian ELSE ReasonType.Description END
		AS ReasonTypeName
	
	FROM ReasonMaster
INNER JOIN ReasonType ON ReasonType.ID = ReasonMaster.ReasonType 
 WHERE ReasonMaster.IsActive=1 ORDER BY ReasonMaster.ReasonType, ReasonMaster.Description
END

 -- EXEC Usp_Get_ReasonMaster 'id'

GO
/****** Object:  StoredProcedure [dbo].[Usp_GetAcademicTermDetails]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Usp_GetAcademicTermDetails]
	@UserID INT,
	@UserType INT	

AS
BEGIN

IF(@UserType = 4)

	BEGIN
		SELECT AcademicTerm.ID,AcademicTerm.Start_Date,AcademicTerm.End_Date,AcademicTerm.No_Of_Days,
		AcademicTerm.SchoolId,AcademicTerm.IsCurrentTerm,AcademicTerm.CreatedBy,AcademicTerm.CreatedOn,
		SchoolMaster.SchoolName
		FROM AcademicTermDetails AcademicTerm
		INNER JOIN SchoolMaster ON AcademicTerm.SchoolId = SchoolMaster.ID
		WHERE IsCurrentTerm = 1
	END

ELSE IF (@UserType = 1)	
	BEGIN
			SELECT AcademicTerm.ID,AcademicTerm.Start_Date,AcademicTerm.End_Date,AcademicTerm.No_Of_Days,
		AcademicTerm.SchoolId,AcademicTerm.IsCurrentTerm,AcademicTerm.CreatedBy,AcademicTerm.CreatedOn,
		SchoolMaster.SchoolName
		FROM AcademicTermDetails AcademicTerm
			INNER JOIN SchoolMaster ON AcademicTerm.SchoolId = SchoolMaster.ID
			WHERE SchoolId IN (SELECT schoolId FROM SchoolAdminMapping WHERE UserId = @UserID AND IsActive =1)
			AND IsCurrentTerm = 1
	END
END






GO
/****** Object:  StoredProcedure [dbo].[Usp_GetAllAbsenceChildren]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_GetAllAbsenceChildren]
	@ClassRoomID INT,
--	@AttendanceDate DATE,
	@FromDate DATE,
	@ToDate DATE
	
AS
BEGIN


IF OBJECT_ID('dbo.#Temp', 'U') IS NOT NULL
  DROP TABLE #Temp; 

CREATE TABLE #Temp
(
	ID INT,
    ChildID INT, 
    TeacherID INT,
	ClassRoomID INT,
    AttendanceDate DATE,
	LastPresentDate DATE NULL,
	AbsentDays INT NULL DEFAULT 0
)



	DECLARE @SChoolID int
	DECLARE @TermStartDate DATE
	DECLARE @TermEndDate DATE
	DECLARE @WorkingDays INT

	--DECLARE @FromDate DATE
	--DECLARE @TODate DATE


	SELECT @SChoolID =  SchoolId FROM ClassMaster WHERE ID =  @ClassRoomID

	SELECT @TermStartDate = Start_Date,@TermEndDate = End_Date FROM AcademicTermDetails WHERE SchoolId = @SchoolID  AND IsCurrentTerm = 1

	
	
	SET @FromDate =  DATEADD(day,-6, @ToDate) 

	--SET @ToDate =  @AttendanceDate


	SELECT @WorkingDays = dbo.Fn_GetNumberOfWorkingDays(@SChoolID,ISNULL(@TermStartDate,GETDATE()),GETDATE()) ;


		INSERT INTO #Temp(ChildID) SELECT  AM.ChildID FROM AttendanceMaster AM WHERE AM.ClassRoomID = @ClassRoomID AND AM.IsPresent = 0  AND CAST(AM.AttendanceDate AS DATE) =  @ToDate
	

	UPdate #Temp SET ClassRoomID =  @ClassRoomID

	

	UPDATE t1
		SET t1.AttendanceDate = t2.AttendanceDate
		FROM #Temp t1
			CROSS APPLY (SELECT MAX(AM.AttendanceDate) AS AttendanceDate FROM AttendanceMaster AM WHERE					AM.ClassRoomID = @ClassRoomID AND AM.IsPresent = 0  AND AM.AttendanceDate BETWEEN @FromDate AND @ToDate AND AM.ChildID = t1.ChildID) AS t2



	UPDATE #Temp SET LastPresentDate = ( SELECT A.LastPresentDate FROM ( 	
									SELECT TOP 1 AM.AttendanceDate LastPresentDate FROM AttendanceMaster AM   
									WHERE AM.ClassRoomID = @ClassRoomID  AND AM.IsPresent = 1
									--AND CAST(AM.AttendanceDate AS date) BETWEEN @FromDate AND @FromDate 
									AND CAST(AM.AttendanceDate AS date) <= @ToDate 
									AND #Temp.ChildID = AM.ChildID
									ORDER BY AttendanceDate DESC
	
									) AS A
							)

-- SELECT * FROM #Temp

SELECT * FROM 
(

SELECT CM.Name, CM.Surname,CM.Image,CM.ClassroomID,Class.ClassName,CM.SchoolId,#Temp.AttendanceDate, 
--ISNULL(#Temp.LastPresentDate,#Temp.AttendanceDate) AS LastPresentDate,
SM.SchoolName,#Temp.ChildID AS ID, #Temp.ID as AttendanceMasterID
	,CM.ParentName,CM.Phone,CM.Address, 
	#Temp.LastPresentDate,
	/*
	dbo.Fn_GetNumberOfWorkingDays(@SChoolID, DATEADD(DAY,1,ISNULL(#Temp.LastPresentDate,#Temp.AttendanceDate)), CASE WHEN @ToDate > GETDATE() THEN GETDATE() ELSE @ToDate END) As AbsentDays
	*/
	
	AbsentReason.ReasonID, AbsentReason.InformationProvider,
	ReasonMaster.ReasonType,reasontype.Code AS AbsenceReason,AbsenceInformerMaster.Description As AbsenteeInformer,
	--DATEDIFF(HOUR,GETDATE(),#Temp.AttendanceDate) AS ElapsedTime ,
		DATEDIFF(HOUR,ISNULL(#Temp.LastPresentDate, @ToDate),CAST(GETDATE()AS DATE)) AS ElapsedTime ,
	AbsentReason.Notes,ReasonMaster.Description AS ReasonDescription
	,CAST(ISNULL((CAST(AttendanceSummary.PresentCount AS float) / CAST(@WorkingDays AS float) * 100),0) as decimal(10,2)) As AttendancePercent		


		,(SELECT COUNT(1)  FROM AttendanceMaster WHERE ChildID = #Temp.ChildID AND AttendanceDate 
			--	BETWEEN ISNULL(#Temp.LastPresentDate,CAST(GETDATE() AS DATE)) AND @ToDate AND IsPresent = 0
		  	BETWEEN ISNULL(#Temp.LastPresentDate, @ToDate) AND @ToDate AND IsPresent = 0
	) as AbsentDays
	
	, CM.GeoLocation
	
	 FROM #Temp

		INNER JOIN ChildrenMaster CM ON CM.ID = #Temp.ChildID

		INNER JOIN SchoolMaster SM ON SM.ID = CM.SchoolId
		INNER JOIN ClassMaster Class ON Class.ID = #Temp.ClassRoomID
		LEFT JOIN AbsenteeReasonDetails AbsentReason ON AbsentReason.ChildID = #Temp.ChildID AND CAST(AbsentReason.CreatedOn AS DATE)= #Temp.AttendanceDate
		LEFT JOIN ReasonMaster ON ReasonMaster.ID = AbsentReason.ReasonID
		LEFT JOIN reasontype ON reasontype.ID = ReasonMaster.ReasonType
		LEFT JOIN AbsenceInformerMaster ON AbsenceInformerMaster.ID = AbsentReason.InformationProvider
		--AND CAST(AbsentReason.CreatedOn AS date) = #Temp.AttendanceDate

		LEFT JOIN (
							 SELECT ChildID,count(IsPresent)  as PresentCount
							 FROM AttendanceMaster 
							 WHERE AttendanceDate BETWEEN @TermStartDate AND @TermEndDate 
								AND  IsPresent = 1
							 GROUP BY ChildID
						) AS AttendanceSummary on AttendanceSummary.ChildID = #Temp.ChildID

		) A
		WHERE ISNULL(A.ReasonID,0) = 0
		ORDER BY 24 DESC

IF OBJECT_ID('dbo.#Temp', 'U') IS NOT NULL
  DROP TABLE #Temp; 

END


--	 EXEC Usp_GetAllAbsenceChildren @ClassRoomID = 66,@FromDate = '2019-02-05',@ToDate = '2019-02-05'



GO
/****** Object:  StoredProcedure [dbo].[Usp_GetAllAbsenceChildren_Test]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_GetAllAbsenceChildren_Test]
	@ClassRoomID INT,
--	@AttendanceDate DATE,
	@FromDate DATE,
	@ToDate DATE
	
AS
BEGIN


IF OBJECT_ID('dbo.#Temp', 'U') IS NOT NULL
  DROP TABLE #Temp; 

CREATE TABLE #Temp
(
	ID INT,
    ChildID INT, 
    TeacherID INT,
	ClassRoomID INT,
    AttendanceDate DATE,
	LastPresentDate DATE NULL,
	AbsentDays INT NULL DEFAULT 0
)



	DECLARE @SChoolID int
	DECLARE @TermStartDate DATE
	DECLARE @TermEndDate DATE
	DECLARE @WorkingDays INT

	--DECLARE @FromDate DATE
	--DECLARE @TODate DATE


	SELECT @SChoolID =  SchoolId FROM ClassMaster WHERE ID =  @ClassRoomID

	SELECT @TermStartDate = Start_Date,@TermEndDate = End_Date FROM AcademicTermDetails WHERE SchoolId = @SchoolID  AND IsCurrentTerm = 1

	
	
	SET @FromDate =  DATEADD(day,-6, @ToDate) 

	--SET @ToDate =  @AttendanceDate


	SELECT @WorkingDays = dbo.Fn_GetNumberOfWorkingDays(@SChoolID,ISNULL(@TermStartDate,GETDATE()),GETDATE()) ;


		INSERT INTO #Temp(ChildID) SELECT  AM.ChildID FROM AttendanceMaster AM WHERE AM.ClassRoomID = @ClassRoomID AND AM.IsPresent = 0  AND CAST(AM.AttendanceDate AS DATE) =  @ToDate
	

	UPdate #Temp SET ClassRoomID =  @ClassRoomID

	

	UPDATE t1
		SET t1.AttendanceDate = t2.AttendanceDate
		FROM #Temp t1
			CROSS APPLY (SELECT MAX(AM.AttendanceDate) AS AttendanceDate FROM AttendanceMaster AM WHERE					AM.ClassRoomID = @ClassRoomID AND AM.IsPresent = 0  AND AM.AttendanceDate BETWEEN @FromDate AND @ToDate AND AM.ChildID = t1.ChildID) AS t2

SELECT @ToDate 


	UPDATE #Temp SET LastPresentDate = ( SELECT A.LastPresentDate FROM ( 	
									SELECT TOP 1 AM.AttendanceDate LastPresentDate FROM AttendanceMaster AM   
									WHERE AM.ClassRoomID = @ClassRoomID  AND AM.IsPresent = 1
									--AND CAST(AM.AttendanceDate AS date) BETWEEN @FromDate AND @FromDate 
									AND CAST(AM.AttendanceDate AS date) <= @ToDate 
									AND #Temp.ChildID = AM.ChildID
									ORDER BY AttendanceDate DESC
	
									) AS A
							)

 SELECT * FROM #Temp

SELECT * FROM 
(

SELECT CM.Name, CM.Surname,CM.Image,CM.ClassroomID,Class.ClassName,CM.SchoolId,#Temp.AttendanceDate, 
--ISNULL(#Temp.LastPresentDate,#Temp.AttendanceDate) AS LastPresentDate,
SM.SchoolName,#Temp.ChildID AS ID, #Temp.ID as AttendanceMasterID
	,CM.ParentName,CM.Phone,CM.Address, 
	#Temp.LastPresentDate,
	/*
	dbo.Fn_GetNumberOfWorkingDays(@SChoolID, DATEADD(DAY,1,ISNULL(#Temp.LastPresentDate,#Temp.AttendanceDate)), CASE WHEN @ToDate > GETDATE() THEN GETDATE() ELSE @ToDate END) As AbsentDays
	*/
	
	AbsentReason.ReasonID, AbsentReason.InformationProvider,
	ReasonMaster.ReasonType,reasontype.Code AS AbsenceReason,AbsenceInformerMaster.Description As AbsenteeInformer,
	--DATEDIFF(HOUR,GETDATE(),#Temp.AttendanceDate) AS ElapsedTime ,
	DATEDIFF(HOUR,ISNULL(#Temp.LastPresentDate, @ToDate),CAST(GETDATE()AS DATE)) AS ElapsedTime ,
	AbsentReason.Notes,ReasonMaster.Description AS ReasonDescription
	,CAST(ISNULL((CAST(AttendanceSummary.PresentCount AS float) / CAST(@WorkingDays AS float) * 100),0) as decimal(10,2)) As AttendancePercent		


		,(SELECT COUNT(1)  FROM AttendanceMaster WHERE ChildID = #Temp.ChildID AND AttendanceDate 
			--	BETWEEN ISNULL(#Temp.LastPresentDate,CAST(GETDATE() AS DATE)) AND @ToDate AND IsPresent = 0
			BETWEEN ISNULL(#Temp.LastPresentDate, @ToDate) AND @ToDate AND IsPresent = 0
	) as AbsentDays
	
	, CM.GeoLocation
	
	 FROM #Temp

		INNER JOIN ChildrenMaster CM ON CM.ID = #Temp.ChildID

		INNER JOIN SchoolMaster SM ON SM.ID = CM.SchoolId
		INNER JOIN ClassMaster Class ON Class.ID = #Temp.ClassRoomID
		LEFT JOIN AbsenteeReasonDetails AbsentReason ON AbsentReason.ChildID = #Temp.ChildID AND CAST(AbsentReason.CreatedOn AS DATE)= #Temp.AttendanceDate
		LEFT JOIN ReasonMaster ON ReasonMaster.ID = AbsentReason.ReasonID
		LEFT JOIN reasontype ON reasontype.ID = ReasonMaster.ReasonType
		LEFT JOIN AbsenceInformerMaster ON AbsenceInformerMaster.ID = AbsentReason.InformationProvider
		--AND CAST(AbsentReason.CreatedOn AS date) = #Temp.AttendanceDate

		LEFT JOIN (
							 SELECT ChildID,count(IsPresent)  as PresentCount
							 FROM AttendanceMaster 
							 WHERE AttendanceDate BETWEEN @TermStartDate AND @TermEndDate 
								AND  IsPresent = 1
							 GROUP BY ChildID
						) AS AttendanceSummary on AttendanceSummary.ChildID = #Temp.ChildID

		) A
		WHERE ISNULL(A.ReasonID,0) = 0
		ORDER BY 24 DESC

IF OBJECT_ID('dbo.#Temp', 'U') IS NOT NULL
  DROP TABLE #Temp; 

END


--	 EXEC Usp_GetAllAbsenceChildren_Test @ClassRoomID = 66,@FromDate = '2019-02-06',@ToDate = '2019-02-06'


-- select * from AttendanceMaster where ChildID = 3392 order by attendancedate

/*

SELECT COUNT(1)  FROM AttendanceMaster WHERE ChildID = 3392 AND AttendanceDate 
				BETWEEN ISNULL('2019-02-03',CAST(GETDATE() AS DATE)) AND '2019-02-05' AND IsPresent = 0
				
				select * from absenteereasondetails where childid = 3392

				select * from absenteereasondetails where id = 44

*/

GO
/****** Object:  StoredProcedure [dbo].[Usp_GetAttendanceMasterCount]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_GetAttendanceMasterCount]
	@ChildID INT,
	@ClassRoomID INT,
	@AttendanceDate DATE
	
AS
BEGIN
	SELECT COUNT(1) AS cnt FROM AttendanceMaster
	WHERE ChildID = @ChildID  AND ClassRoomID = @ClassRoomID 
	AND CAST(AttendanceDate AS DATE) = @AttendanceDate
		
END


-- Exec Usp_GetAttendanceMasterCount @ChildID=54, @ClassRoomID=40,@Attendanceate='2019-01-07'



GO
/****** Object:  StoredProcedure [dbo].[Usp_GetClassListByUser]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Usp_GetClassListByUser]
	@UserID int,
	@schoolID INT
	
AS
BEGIN
	 DECLARE @UserType int
	 DECLARE @TempSchoolID INT

	 SELECT @UserType = UserType FROM UserMaster WHERE ID = @UserID

	 IF(@schoolID > 0)
			 BEGIN
			  IF(@UserType = 1 OR @UserType = 4) -- ADMIN
			  BEGIN

					SELECT  CM.ID, CM.ClassName,CM.SchoolId,CM.CreatedBy,CM.CreatedOn,CM.IsActive ,SM.SchoolName
					From ClassMaster CM 
					LEFT JOIN  SchoolMaster SM ON CM.SchoolId = SM.ID		
					WHERE CM.SchoolID = @schoolID AND CM.IsActive = 1            
				
			  END

			IF(@UserType = 2) -- Teacher
			BEGIN
			
						SELECT  CM.ID, CM.ClassName,CM.SchoolId,CM.CreatedBy,CM.CreatedOn,CM.IsActive ,SM.SchoolName
						FROM TeacherClassMapping TCM
						INNER JOIN ClassMaster CM ON TCM.ClassRoomID = CM.ID AND  CM.SchoolID = @schoolID
						LEFT JOIN  SchoolMaster SM ON CM.SchoolId = SM.ID	
						WHERE TCM.TeacherID = @UserID AND TCM.IsActive = 1
			END
	END

	ELSE
		BEGIN
			IF(@UserType = 1) -- ADMIN
			  BEGIN
			  		SELECT @TempSchoolID = SchoolId FROM SchoolAdminMapping WHERE UserID = @UserID

					

					SELECT  CM.ID, CM.ClassName,CM.SchoolId,CM.CreatedBy,CM.CreatedOn,CM.IsActive ,SM.SchoolName
					From ClassMaster CM 
					LEFT JOIN  SchoolMaster SM ON CM.SchoolId = SM.ID		
					WHERE CM.SchoolID = @TempSchoolID AND CM.IsActive = 1            
				
			  END

			IF(@UserType = 2) -- Teacher
			BEGIN
			
						SELECT  CM.ID, CM.ClassName,CM.SchoolId,CM.CreatedBy,CM.CreatedOn,CM.IsActive ,SM.SchoolName
						FROM TeacherClassMapping TCM
						INNER JOIN ClassMaster CM ON TCM.ClassRoomID = CM.ID 
						LEFT JOIN  SchoolMaster SM ON CM.SchoolId = SM.ID	
						WHERE TCM.TeacherID = @UserID AND TCM.IsActive = 1
			END
		END

		
END


-- Exec Usp_GetClassListByUser @UserID=58, @schoolID= 0



GO
/****** Object:  StoredProcedure [dbo].[Usp_GetTeacherClassMappingDetails]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_GetTeacherClassMappingDetails]	
	@ClassRoomID varchar(50)	

AS
BEGIN
	SELECT  UM.ID, UserName, ISNULL(Name,'') as Name, ISNULL(Surname,'') as Surname from  TeacherClassMapping TCM
	INNER JOIN UserMaster UM on UM.ID = TCM.TeacherID
	where TCM.ClassRoomID = @ClassRoomID AND TCM.IsActive=1
END








GO
/****** Object:  StoredProcedure [dbo].[Usp_ManageAcademicTermDetails]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Usp_ManageAcademicTermDetails]
	@ID int,
	@StartDate DATETIME,
	@EndDate DATETIME,
	@SchoolID INT,
	@IsCurrentTerm BIT,
	@CreatedBy int,
	@CreatedOn DATETIME
	
AS
BEGIN

	INSERT INTO AcademicTermDetails
			(Start_Date,End_Date,No_Of_Days,SchoolId,IsCurrentTerm,CreatedBy,CreatedOn)
			Values
			(@StartDate,@EndDate,DATEDIFF(DAY,@StartDate,@EndDate),@SchoolID,@IsCurrentTerm, @CreatedBy,@CreatedOn)
END



GO
/****** Object:  StoredProcedure [dbo].[Usp_ManageFirstAidAnalytics]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_ManageFirstAidAnalytics]
	@VisitorId INT,
	@VisitedOn DATETIME
	
AS
BEGIN
			INSERT INTO FirstAidAnalytics
			(VisitorId,VisitedOn)
			VALUES
			(@VisitorId,@VisitedOn)
END


GO
/****** Object:  StoredProcedure [dbo].[Usp_Rpt_AdminYearlyReport]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Usp_Rpt_AdminYearlyReport]
	@SchoolID int
	
AS
BEGIN

IF OBJECT_ID('dbo.#TempMonth', 'U') IS NOT NULL
  DROP TABLE #TempMonth; 
IF OBJECT_ID('dbo.#TempTotalStudents', 'U') IS NOT NULL
  DROP TABLE #TempTotalStudents; 

CREATE TABLE #TempMonth
(
	MonthNumber INT,
	FirstDayOfMonth DATE,
	LastDayOfMonth DATE,
	WorkingDays INT NULL DEFAULT 0
)

CREATE TABLE #TempTotalStudents
(
	Gender CHAR,
	Total INT
)

	
DECLARE @start DATE 
DECLARE @end DATE 

DECLARE @TermStartDate DATE 
DECLARE @TermEndDate DATE 

-- DECLARE @TotalStudents INT = 0

SELECT @TermStartDate = Start_Date,@TermEndDate = End_Date FROM AcademicTermDetails WHERE SchoolId = @SchoolID  AND IsCurrentTerm = 1


INSERT INTO #TempTotalStudents(Gender,TOtal) SELECT CM.Gender,COUNT(CM.ID) AS Total FROM ChildrenMAster CM 
						INNER JOIN ClassMaster CLSM ON CLSM.ID = CM.ClassRoomID
						WHERE CLSM.SchoolId = @SchoolID 
						GROUP BY CM.GENDER




SET @start = @TermStartDate
SET @end = GETDATE()

	IF GETDATE() > @TermEndDate
		BEGIN
			SET @end = @TermEndDate
		END



;with months (date)
AS
(
    SELECT @start
    UNION ALL
    SELECT DATEADD(month,1,date)
    from months
    where DATEADD(month,1,date) < @end
)
INSERT INTO #TempMonth(MonthNumber,FirstDayOfMonth,LastDayOfMonth) SELECT  
			 
			-- [MonthName]    = DATENAME(mm ,Date)  
			           -- ,[LastDayOfMonth]  = DATEPART(dd,EOMONTH(Date))
			  [MonthNumber]  = DATEPART(mm ,Date) 
		     ,[FirstDayOfMonth] = CONVERT(DATE, DATEADD(MONTH, DATEDIFF(MONTH, 0, EOMONTH(Date)), 0)) 
		     ,[LastDayOfMonth]  = EOMONTH(Date)
FROM months


UPDATE #TempMonth SET FirstDayOfMonth = @start WHERE MonthNumber =  MONTH(@start) 
UPDATE #TempMonth SET LastDayOfMonth = @end WHERE MonthNumber =  MONTH(@end) 


UPDATE #TempMonth SET WorkingDays =  dbo.Fn_GetNumberOfWorkingDays(@SChoolID,FirstDayOfMonth,LastDayOfMonth)

--SELECT * FROM #TempMonth
--SELECT * FROM #TempTotalStudents


SELECT A.AttendanceMonth,PresentCount,AbscentCount
,CAST(A.PresentCount AS FLOAT) / (SELECT SUM(Total) FROM #TempTotalStudents ) * (SELECT WorkingDays FROM #TempMonth WHERE MOnthNumber = A.AttendanceMonth) AS AttendanceRate
,CAST(A.AbscentCount AS FLOAT) / (SELECT SUM(Total) FROM #TempTotalStudents ) * (SELECT WorkingDays FROM #TempMonth WHERE MOnthNumber = A.AttendanceMonth) AS AbsenteeismRate
FROM (
		SELECT MONTH(AttendanceDate) AS AttendanceMonth
				,SUM(CASE WHEN ISNULL( AM.IsPresent,0) = 1 THEN 1 ELSE 0 END) AS PresentCount
				,SUM(CASE WHEN ISNULL( AM.IsPresent,0) = 0 THEN 1 ELSE 0 END) AS AbscentCount	
				
		FROM AttendanceMaster AM
			INNER JOIN ClassMaster CM ON CM.ID = AM.ClassRoomID
			INNER JOIN SchoolMaster SM ON SM.ID = CM.SchoolID
			WHERE SM.ID = @SchoolID
			GROUP BY MONTH(AttendanceDate)
		) AS A

IF OBJECT_ID('dbo.#TempMonth', 'U') IS NOT NULL
  DROP TABLE #TempMonth; 
IF OBJECT_ID('dbo.#TempTotalStudents', 'U') IS NOT NULL
  DROP TABLE #TempTotalStudents; 


END

--   EXEC Usp_Rpt_AdminYearlyReport  23

GO
/****** Object:  StoredProcedure [dbo].[Usp_Rpt_GetAbsenceChildrenList]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Usp_Rpt_GetAbsenceChildrenList]
	@ClassRoomID INT,
--	@AttendanceDate DATE,
	@FromDate DATE,
	@ToDate DATE
	
AS
BEGIN


IF OBJECT_ID('dbo.#Temp', 'U') IS NOT NULL
  DROP TABLE #Temp; 
 

CREATE TABLE #Temp
(
	ID INT,
    ChildID INT, 
    TeacherID INT,
	ClassRoomID INT,
    AttendanceDate DATE,
	LastPresentDate DATE NULL
)



	DECLARE @SChoolID int
	DECLARE @TermStartDate DATE
	DECLARE @TermEndDate DATE
	DECLARE @WorkingDays INT

	--DECLARE @FromDate DATE
	--DECLARE @TODate DATE


	SELECT @SChoolID =  SchoolId FROM ClassMaster WHERE ID =  @ClassRoomID

	SELECT @TermStartDate = Start_Date,@TermEndDate = End_Date FROM AcademicTermDetails WHERE SchoolId = @SchoolID  AND IsCurrentTerm = 1


	
	IF @FromDate < @TermStartDate
		BEGIN
			SET @FromDate = @TermStartDate
		END

	IF @ToDate > CAST(GETDATE() AS DATE)	--@TermEndDate
		BEGIN
			SET @ToDate = CAST(GETDATE() AS DATE) --@TermEndDate
		END

		

	SELECT @WorkingDays = dbo.Fn_GetNumberOfWorkingDays(@SChoolID,ISNULL(@TermStartDate,GETDATE()),GETDATE()) 


		INSERT INTO #Temp(ID,ChildID,AttendanceDate) SELECT AM.ID, AM.ChildID,AM.AttendanceDate FROM AttendanceMaster AM WHERE AM.ClassRoomID = @ClassRoomID AND AM.IsPresent = 0  AND AM.AttendanceDate BETWEEN @FromDate AND @ToDate 	
		ORDER BY AM.ChildID,AM.AttendanceDate
		--GROUP BY  AM.ChildID 


	UPdate #Temp SET ClassRoomID =  @ClassRoomID

	/*
	UPDATE t1
		SET t1.AttendanceDate = t2.AttendanceDate
		FROM #Temp t1
			CROSS APPLY (SELECT MAX(AM.AttendanceDate) AS AttendanceDate FROM AttendanceMaster AM WHERE					AM.ClassRoomID = @ClassRoomID AND AM.IsPresent = 0  AND AM.AttendanceDate BETWEEN @FromDate AND @ToDate AND AM.ChildID = t1.ChildID) AS t2

			*/
			
	UPDATE #Temp SET LastPresentDate = ( SELECT A.LastPresentDate FROM ( 	
									SELECT TOP 1 AM.AttendanceDate LastPresentDate FROM AttendanceMaster AM   
									WHERE AM.ClassRoomID = @ClassRoomID  AND AM.IsPresent = 1
									AND CAST(AM.AttendanceDate AS date) <= #Temp.AttendanceDate  /* BETWEEN @FromDate AND @ToDate */
									AND #Temp.ChildID = AM.ChildID
									ORDER BY AttendanceDate DESC	
									) AS A
							)


SELECT CM.Name, CM.Surname,CM.Image,CM.ClassroomID,Class.ClassName,CM.SchoolId,#Temp.AttendanceDate, 
--ISNULL(#Temp.LastPresentDate,#Temp.AttendanceDate) AS LastPresentDate,
SM.SchoolName,#Temp.ChildID AS ID, #Temp.ID as AttendanceMasterID
	,CM.ParentName,CM.Phone,CM.Address, 
	#Temp.LastPresentDate,
	/*
	dbo.Fn_GetNumberOfWorkingDays(@SChoolID, DATEADD(DAY,1,ISNULL(#Temp.LastPresentDate,#Temp.AttendanceDate)), CASE WHEN @ToDate > GETDATE() THEN GETDATE() ELSE @ToDate END) As AbsentDays
	*/
	
	AbsentReason.ReasonID, AbsentReason.InformationProvider,
	ReasonMaster.ReasonType,reasontype.Code AS AbsenceReason,AbsenceInformerMaster.Description As AbsenteeInformer,
	DATEDIFF(HOUR,#Temp.AttendanceDate,GETDATE()) AS ElapsedTime ,
	AbsentReason.Notes,ReasonMaster.Description AS ReasonDescription
	
	/*
	,CAST(ISNULL((CAST(AttendanceSummary.PresentCount AS float) / CAST(@WorkingDays AS float) * 100),0) as decimal(10,2)) As AttendancePercent	*/	
	/*,0 AS AttendancePercent*/

	/*
	,(SELECT COUNT(IsPresent)  FROM AttendanceMaster WHERE ChildID = #Temp.ChildID AND CAST(AttendanceDate AS DATE) > ISNULL(#Temp.LastPresentDate,CAST(GETDATE() AS DATE)) AND IsPresent = 0
	) as AbsentDays
	*/
	,1 as AbsentDays
	,AbsentReason.Notes,AbsentReason.Notes_Indonesian
	 FROM #Temp

		INNER JOIN ChildrenMaster CM ON CM.ID = #Temp.ChildID
		--INNER JOIN TeacherClassMapping TCM ON TCM.ClassroomID = CM.ClassRoomID
		--INNER JOIN TeacherClassMapping TCM ON TCM.ClassroomID =@ClassRoomID AND TCM.IsActive=1

		INNER JOIN SchoolMaster SM ON SM.ID = CM.SchoolId
		INNER JOIN ClassMaster Class ON Class.ID = #Temp.ClassRoomID
		LEFT JOIN AbsenteeReasonDetails AbsentReason ON AbsentReason.ChildID = #Temp.ChildID AND CAST(AbsentReason.CreatedOn AS DATE)= #Temp.AttendanceDate
		LEFT JOIN ReasonMaster ON ReasonMaster.ID = AbsentReason.ReasonID
		LEFT JOIN reasontype ON reasontype.ID = ReasonMaster.ReasonType
		LEFT JOIN AbsenceInformerMaster ON AbsenceInformerMaster.ID = AbsentReason.InformationProvider
		--AND CAST(AbsentReason.CreatedOn AS date) = #Temp.AttendanceDate

		/*
		LEFT JOIN (
							 SELECT ChildID,count(IsPresent)  as PresentCount
							 FROM AttendanceMaster 
							 WHERE AttendanceDate BETWEEN @TermStartDate AND @TermEndDate 
								AND  IsPresent = 1
							 GROUP BY ChildID
						) AS AttendanceSummary on AttendanceSummary.ChildID = #Temp.ChildID

		*/
		ORDER BY #Temp.AttendanceDate DESC ,CM.Name ASC

IF OBJECT_ID('dbo.#Temp', 'U') IS NOT NULL
  DROP TABLE #Temp; 
 

END



--	EXEC Usp_Rpt_GetAbsenceChildrenList @ClassRoomID = 66,@FromDate = '2019-02-01' ,@ToDate = '2019-02-03'

-- EXEC Usp_Rpt_GetAbsenceChildrenList @ClassRoomID = 45,@FromDate = '2019-01-01' ,@ToDate = '2019-01-26'




GO
/****** Object:  StoredProcedure [dbo].[Usp_Rpt_TeacherYearlyReport]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Usp_Rpt_TeacherYearlyReport]
	@ClassID INT,
	@TeacherID INT
	
AS
BEGIN

IF OBJECT_ID('dbo.#TempMonth', 'U') IS NOT NULL
  DROP TABLE #TempMonth; 

IF OBJECT_ID('dbo.#TempTotalStudents', 'U') IS NOT NULL
  DROP TABLE #TempTotalStudents; 

CREATE TABLE #TempMonth
(
	MonthNumber INT,
	FirstDayOfMonth DATE,
	LastDayOfMonth DATE,
	WorkingDays INT NULL DEFAULT 0
)

CREATE TABLE #TempTotalStudents
(
	Gender CHAR,
	Total INT
)

	
DECLARE @start DATE 
DECLARE @end DATE 

DECLARE @TermStartDate DATE 
DECLARE @TermEndDate DATE 
DECLARE @SchoolID INT
DECLARE  @MonthNames VARCHAR(100)


-- DECLARE @TotalStudents INT = 0

SELECT  @SchoolID  = SchoolId FROM ClassMaster WHERE ID = @ClassID




INSERT INTO #TempMonth(MonthNumber,FirstDayOfMonth,LastDayOfMonth,WorkingDays) SELECT  * FROM dbo.Fn_GetYearly_Report_DateRange(@SChoolID)

SELECT @start = MIN(FirstDayOfMonth), @end = MAX(LastDayOfMonth) FROM #TempMonth




INSERT INTO #TempTotalStudents(Gender,TOtal) SELECT Gender, COUNT(ID) FROM ChildrenMAster  WHERE ClassRoomID = @ClassID AND ISActive =1  GROUP BY GENDER


SELECT A.ChildID,CM.Name, A.MonthNumber as AttendanceMonth, A.PresentDays 
,(#TempMonth.WorkingDays - ISNULL(A.PresentDays,0)) as AbsentDays
,( CAST(A.PresentDays AS FLOAT) / #TempMonth.WorkingDays) * 100 AS AttendancePercent 

--,( CAST(A.PresentCount AS FLOAT) / (SELECT WorkingDays FROM #TempMonth WHERE MonthNumber = A.AttendanceMonth)) * 100 
--,CAST(A.AbscentCount AS FLOAT) / ((SELECT SUM(Total) FROM #TempTotalStudents ) * (SELECT WorkingDays FROM #TempMonth WHERE MOnthNumber = A.AttendanceMonth)) AS AbsenteeismRate
FROM (
		SELECT AM.ChildID,MONTH(AttendanceDate) AS MonthNumber
		,COUNT(ID) PresentDays
				
		FROM AttendanceMaster AM		
			WHERE AM.ClassRoomID = @ClassID AND AM.TeacherID = @TeacherID AND IsPresent = 1
			AND AM.AttendanceDate BETWEEN @start AND @end
			GROUP BY MONTH(AM.AttendanceDate),AM.ChildID
		)  AS A

LEFT JOIN #TempMonth ON #TempMonth.MonthNumber = A.MonthNumber
INNER JOIN ChildrenMaster CM ON CM.ID = A.ChildID
ORDER BY A.ChildID,A.MonthNumber

--SELECT * FROM #TempMonth 
--SELECT STRING_AGG( MonthNumber, ', ') as MonthNames  
--select *,datename(MONTH,MonthNumber) from  #TempMonth

SELECT LEFT(datename(MONTH,FirstDayOfMonth),3) as AttendanceMonth FROM #TempMonth

IF OBJECT_ID('dbo.#TempMonth', 'U') IS NOT NULL
  DROP TABLE #TempMonth; 

IF OBJECT_ID('dbo.#TempTotalStudents', 'U') IS NOT NULL
  DROP TABLE #TempTotalStudents; 


END


	
--   EXEC Usp_Rpt_TeacherYearlyReport @ClassID = 35,@TeacherID = 1


--  select * from 




GO
/****** Object:  StoredProcedure [dbo].[Usp_Rpt_TwilioSchdular_AbsenteeList]    Script Date: 19/02/2019 19:49:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Usp_Rpt_TwilioSchdular_AbsenteeList]
	@FromDate DATE,
	@ToDate DATE,
	@Days INT
	
AS
BEGIN

WITH AbsenteeList
AS
(
  SELECT amst.ID, AttendanceDate FromDate,IsPresent,ChildId,
         LEAD(AttendanceDate) OVER (PARTITION BY ChildId ORDER BY AttendanceDate ASC) ToDate		 
  FROM   [dbo].[AttendanceMaster] amst
  INNER JOIN [dbo].[ClassMaster] cmst ON amst.ClassRoomID = cmst.ID 
  WHERE IsPresent = 0 --AND ChildID = 54
    AND amst.AttendanceDate NOT IN (SELECT HolidayDate FROM [dbo].[YearlyHolidayMaster] hmst WHERE hmst.SchoolId = cmst.SchoolId)
	AND amst.AttendanceDate BETWEEN @FromDate AND @ToDate
)

SELECT A.*,ChildrenMaster.Name,ChildrenMaster.Phone FROM
(
	SELECT  DENSE_RANK() OVER(PARTITION BY ChildID ORDER BY ToDate DESC) as RowNum, AbsenteeList.Id, FromDate ,ToDate, DATEDIFF(d, FromDate ,ToDate) as days_diff, ChildId
	FROM AbsenteeList 	
	WHERE DATEDIFF(d, FromDate ,ToDate) = @Days
) AS A
INNER JOIN [dbo].[ChildrenMaster] ChildrenMaster ON ChildrenMaster.ID = A.ChildID
WHERE A.RowNum = 1;
END


--	EXEC Usp_Rpt_TwilioSchdular_AbsenteeList @FromDate = '2019-01-01' ,@ToDate = '2019-02-11',@Days =2



GO
USE [master]
GO
ALTER DATABASE [DB_A39427_WalikuDev] SET  READ_WRITE 
GO
