﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalikuWebSite
{

    public partial class ImportStudentData : System.Web.UI.Page
    {
        public string DefaultLanguage = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            string Language = Common.CommonFunction.GetUserLanguageFromCookie();

            if (Language != "")
            {
                string[] str = Language.Split('-');
                DefaultLanguage = (str.Length == 2) ? DefaultLanguage = str[0] : "en";
            }
            else
            {
                Language = "en-us";
            }
            Session["Language"] = Language;

            Session["CurrentCulture"] = DefaultLanguage;

            if(DefaultLanguage == "en")
            {
                eng.Visible = true;
                ind.Visible = false;
            }
            else
            {
                eng.Visible = false;
                ind.Visible = true;
            }
        }

        public void StudentsDataFileImport()
        {
            //do something
        }
    }
}