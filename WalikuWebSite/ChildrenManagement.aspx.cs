﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using WalikuWebSite.App_Start;
using WalikuWebSite.DataAccess;

public partial class ChildrenManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }
    }


    //[WebMethod]
    //public static List<ChildrenMaster> GetAllChildrenForMe()
    //{
    //    var uid = Common.CommonFunction.GetUserIdFromCookieByID();
    //    if (uid == 0)
    //        return new List<ChildrenMaster>();
    //    var udata = SqlHelper.GetUserByID(uid);
    //    return SqlHelper.GetAllChildrenForMe(uid, int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()));
    //}

    [WebMethod]
    public static List<ChildrenMaster> GetAllChildrenForMe(int SchoolID, int ClassRoomID)
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        if (uid == 0)
            return new List<ChildrenMaster>();
        var udata = SqlHelper.GetUserByID(uid);
        if(SchoolID == 0 && ClassRoomID == 0)
            return SqlHelper.GetAllChildrenForMe(uid, int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()), Common.CommonFunction.GetCurrentCulture());
        else
            return SqlHelper.GetAllChildrenForMeByFilter(uid, int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()), SchoolID, ClassRoomID,Common.CommonFunction.GetCurrentCulture());
    }

    [WebMethod]
    public static string GetChildImageByID(int ChildID)
    {
        var s = SqlHelper.GetChildImageByID(ChildID);
        byte[] bytes = Convert.FromBase64String(s);
        string filePath = "~/Photos/" + ChildID.ToString() + "_Child.jpg";
        File.WriteAllBytes(HttpContext.Current.Server.MapPath(filePath), bytes);
        return filePath.Replace("~", "..");

    }

    [WebMethod]
    public static List<UserMaster> GetAllCommunityWorker(int PageIndex)
    {
        return SqlHelper.GetAllUsers();


    }

    [WebMethod]
    public static List<UserMaster> GetTeacherClassMapping(int ClassRoomID)
    {
        return SqlHelper.GetTeacherClassMappingDetails(ClassRoomID);
    }



    [WebMethod]
    public static List<SchoolMaster> GetAllSchool(int PageIndex)
    {
        return SqlHelper.GetAllSchools(); 
    }

    //[WebMethod]
    //public static List<ClassMaster> GetAllClassRoom(int PageIndex)
    //{
    //    return SqlHelper.GetAllClasses();

    //   // HttpFileCollectionBase files = Request.Files;
    //}

    [WebMethod]
    public static List<ClassMaster> GetAllClassRoomForMe(int SchoolID)
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        return SqlHelper.GetAllClassesForMe(SchoolID, uid);
    }

    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        //if (this.FileUploadControl.HasFile)
        //{
            
        //    this.FileUploadControl.SaveAs(@"C:\temp\" + FileUploadControl.FileName);
            
        //}
        //else
        //{
            
        //}
    }

    [WebMethod]
    public static void ManageChild(int ID, string Name, string Surname, string ParentName, int CommunityWorkerID, string Email, string  Image, int SchoolID, 
        int ClassroomID, string Address, string Phone, string Note,string Gender, string Nisn, int GradeID, int DeleteReasonID,string Date_of_Birth )
    {
        
        SqlHelper.ManageChildren(new ChildrenMaster()
        {
            ID = ID,
            Name = Name.Trim(),
            Surname = Surname.Trim(),
            ParentName = ParentName.Trim(),
            CommunityWorkerID = CommunityWorkerID,
            Email = Email.Trim(),
            Image = string.Empty,
            SchoolID = SchoolID,
            ClassroomID = ClassroomID,
            Address = Address.Trim(),
            Phone = Phone.Trim(),
            Note = Note.Trim(),
            Gender = Gender,
            NISN = Nisn,
            GradeID = GradeID,
            DeleteReasonID = DeleteReasonID,
            Date_of_Birth = Convert.ToDateTime(Date_of_Birth)

        });
    }

    [WebMethod]
    public static List<GradeMaster> GetAllGrade()
    {
        return SqlHelper.GetAllGrade();
    }
    [WebMethod]
    public static List<DelteReasonMaster> GetAllReasonForDelete()
    {
        return SqlHelper.GetAllReasonForDelete(Common.CommonFunction.GetCurrentCulture());
    }

    [WebMethod]
    public static void DeleteChildByStudentID(int ID,int DelID)
    {
        SqlHelper.DeleteChildByStudentID(ID,DelID);
    }

    [WebMethod]
    public static VmChildren GetChildDetailsByID(int ID)
    {
        return ChildrenDataAccess.GetChildDetailsByID(ID);
    }

    [WebMethod]
    public static List<ChartMaster.ExistingNisn> GetExistingNISN(int SchoolID, int ClassRoomID, string Nisn)
    {
        var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
        return SqlHelper.GetExistingNISN(TeacherId, SchoolID, ClassRoomID, Nisn);
    }
}