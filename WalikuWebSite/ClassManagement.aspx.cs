﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ClassManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }
    }

    [WebMethod]
    public static List<ClassMaster> GetAllClasses(int PageIndex)
    {
        return SqlHelper.GetAllClasses();

    }
    [WebMethod]
    public static VmResponseData ManageClass(int ID, string ClassName, int SchoolId)
    {
        VmResponseData VMResponseData = new VmResponseData();
        int result =  SqlHelper.ManageClass(new ClassMaster()
        {
            ID = ID,
            ClassName = ClassName.Trim(),
            SchoolId = SchoolId,
            CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
            CreatedOn = DateTime.Now,
            IsActive =1
        });
        if(result == -1)
            VMResponseData.Message = "Class name already exists";
        else
            VMResponseData.Message = "Success";

        return VMResponseData;
    }
    [WebMethod]
    public static void DeleteClassByID(int ID)
    {
        SqlHelper.DeleteClassByID(ID);
    }
}