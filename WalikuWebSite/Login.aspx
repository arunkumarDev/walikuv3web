﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>
<html class="">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery-ui.min.css">
    <link rel="stylesheet" href="css/animate.min.css" />
    <link rel="stylesheet" href="css/bootstrap-material-design.min.css">
    <link rel="stylesheet" href="css/theme.css" />
    <link rel="stylesheet" href="css/style.css?1" />
    <link rel="stylesheet" href="css/custom.css" /> 

</head>

<body>
    <div class="loginpage custom-material">
        <div class="login-img"></div>
        <div class="login-form">
            <div class="login-content">
                <form>
                    <div class="text-center mb-15">
                        <div class="img-circle img_logo">
                            <img src="images/logo.png">
                        </div>
                    </div>
                    <h1 class="text-center mb-15">Waliku</h1>
                    <div class="bmd-form-group ">
                        <label class="bmd-label-floating required" id="Lbl_SelectLanguage"><%=Resources.Resource.Lbl_SelectLanguage %></label>
                        <select id="ddlLanguage" class="form-control select-validate">                            
                            <option value="id-ID">Indonesian</option>
                            <option value="en-us">English</option>
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                    <div class="bmd-form-group">
                      <label class="bmd-label-floating required" id="Lbl_UerName"><%=Resources.Resource.Lbl_UerName %></label>
                         <input type="text" id="txtUserName" value="" class="form-control input-validate" required>
                    </div>
                    <div class="bmd-form-group">
                       <label class="bmd-label-floating required" id="Lbl_Password"><%=Resources.Resource.Lbl_Password %></label>
                         <input type="password" id="txtPassword" value="" class="form-control input-validate">
                    </div>
                    <p><input type="checkbox" id="chkRemember" checked><span id="Lbl_RememberMe"><%=Resources.Resource.Lbl_RememberMe %></span></p>
                    <button type="button" id="btnSignIn" class="btn btn-primary btn-block mt-20"><%=Resources.Resource.Lbl_Login %> </button>                     
                </form>
            </div>
        </div>


    </div>



    
<div id="LgRolePopup" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">
                   <%-- <%=Resources.Resource.Title_DeleteStudent %>--%>
                    Login User Roles
                </h4>
            </div>
            <div class="modal-body custom-scroll mh-500 custom-material">
                <%--<p class="mt-5 col-md-12"><%=Resources.Resource.Lbl_Delmsg %></p>--%>
                <div class="col-md-12">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required mb-10">
                                        Select Role </label>
                                       <%-- <%=Resources.Resource.Lbl_ReasonForDelete %></label>--%>
                                    <select id="ddlPopupRole" class="form-control select-validate">
                                     </select>
                                     <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
            </div>
             <div class="modal-footer" style="padding:0px;">
                <div class="col-md-12 text-right">
                    <button type="button"  style="min-width: 97px;" class="btn btn-default mr-5" title="" data-dismiss="modal"> 
                        <%--<%=Resources.Resource.Lbl_Cancel %>--%> 
                        Cancel
                    </button>
                    <button type="button"  style="min-width: 97px;" class="btn btn-primary" title="" id="btnLoginOk"> OK </button>
                </div>
            </div>
        </div>
    </div>
</div>
    



</body>


     


<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/popper.js"></script>
<script src="js/highcharts.js"></script>
<script src="js/bootstrap-material-design.js"></script>
<script src="js/custom.js"></script>
 <script src="Scripts/Common.js?1"></script>
 <script src="Scripts/Login.js?6"></script>

<script>$(document).ready(function () { $('body').bootstrapMaterialDesign(); });</script>


</html>




