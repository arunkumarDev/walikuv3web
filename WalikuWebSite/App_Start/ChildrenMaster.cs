﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChildrenMaster
/// </summary>
public class ChildrenMaster
{
    public int ID { get; set; }

    public string Name { get; set; }

    public string Middlename { get; set; }

    public string Surname { get; set; }

    public string ParentName { get; set; }

    public int CommunityWorkerID { get; set; }

    public string CommunityWorkerName { get; set; }

    public string Email { get; set; }

    public string Image { get; set; }

    public int Age { get; set; }

    public string ImageBase { get; set; }

    public int SchoolID { get; set; }

    public string SchoolName { get; set; }

    public int ClassroomID { get; set; }

    public string ClassroomName { get; set; }

    public string Address { get; set; }

    public string Phone { get; set; }

    public string Note { get; set; }

    public string GeoLocation { get; set; }

    public string Gender { get; set; }

    //public string AttendanceType { get; set; }

   // public int AttendanceDate { get; set; }

    public int CreatedBy { get; set; }

    public DateTime CreatedOn { get; set; } = DateTime.MinValue;

    public int IsActive { get; set; }

    public Single AttendancePercent { get; set; }

    public string Note_Indonesian { get; set; }

    public string NISN { get; set; }
    public int GradeID { get; set; }
    public int DeleteReasonID { get; set; }

    public string Student_Registration_Number { get; set; }
    public string Birthplace { get; set; }
    public Nullable<System.DateTime> Date_of_Birth { get; set; }
    public string ID_Number { get; set; }
    public string Religion { get; set; }
    public string RT { get; set; }
    public string RW { get; set; }
    public string Village { get; set; }
    public string Village_Office { get; set; }
    public string Sub_District { get; set; }
    public string Postal_Code { get; set; }
    public string Mobile_No { get; set; }
    public string Living_Situation { get; set; }
    public string Transportation_Mode { get; set; }
    public string Certificate_of_National_Examination_Result { get; set; }
    public string Recipient_of_Social_Security_Card { get; set; }
    public string Social_Security_No { get; set; }
    public string F_Name { get; set; }
    public string F_Birth_Year { get; set; }
    public string F_Education_Level { get; set; }
    public string F_Occupation { get; set; }
    public string F_Income { get; set; }
    public string F_ID_Number { get; set; }
    public string M_Name { get; set; }
    public string M_Birth_Year { get; set; }
    public string M_Education_Level { get; set; }
    public string M_Occupation { get; set; }
    public string M_Income { get; set; }
    public string M_ID_Number { get; set; }
    public string G_Name { get; set; }
    public string G_Birth_Year { get; set; }
    public string G_Education_Level { get; set; }
    public string G_Occupation { get; set; }
    public string G_Income { get; set; }
    public string G_ID_Number { get; set; }
    public string Current_Class { get; set; }
    public string National_Examination_Participant_No { get; set; }
    public string Certificate_Serial_No { get; set; }
    public string Recipient_of_Indonesian_Smart_Card { get; set; }
    public string Smart_Card_No { get; set; }
    public string Name_on_Smart_Card { get; set; }
    public string Family_Card_No { get; set; }
    public string Birth_Certificate_No { get; set; }
    public string Bank { get; set; }
    public string Bank_Account_Number { get; set; }
    public string Name_of_Account_Holder { get; set; }
    public string Suitable_for_Smart_Indonesia_Program { get; set; }
    public string Reason_for_Smart_Indonesia_Program { get; set; }
    public string Special_Needs { get; set; }
    public string Previous_School { get; set; }
    public string Position_of_Child_among_Siblings { get; set; }
    public string Latitude { get; set; }
    public string Longitude { get; set; }
    public string Family_Register_No { get; set; }
    public string Weight { get; set; }
    public string Height { get; set; }
    public string Head_circumference { get; set; }
    public string No_of_Siblings_related_by_blood { get; set; }
}