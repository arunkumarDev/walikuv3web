﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.App_Start
{
    public class AbsenteeReason
    {
        public int ID { get; set; }

        public int ChildID { get; set; }

        public int TeacherID { get; set; }

        public bool IsHealthReason { get; set; }

        public string OtherHealthIssue { get; set; }

        public string OtherNonHealthIssue { get; set; }

        public int ReasonID { get; set; }

        public int InformationProvider { get; set; }

        public bool HasPermission { get; set; }

        public int CreatedBy { get; set; }

    }
}