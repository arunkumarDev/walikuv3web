﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.App_Start
{
    public class StudentImport
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ParentName { get; set; }
        public int CommunityWorkerID { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public int SchoolID { get; set; }
        public int ClassroomID { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
        public string GeoLocation { get; set; }
        public string Gender { get; set; }
        public string ParentPhone { get; set; }
        public string Middlename { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsActive { get; set; }
        public string Note_Indonesian { get; set; }
        public string NISN { get; set; }
        public int PreviousClassRoomId { get; set; }
        public int GradeID { get; set; }
        public int DeleteReasonID { get; set; }
        public long NUPTK { get; set; }
        public string Place_of_Birth { get; set; }
        public DateTime DateTime_of_Birth { get; set; }
        public long NIP { get; set; }
        public string Employee_Status { get; set; }
        public string Type_of_Teacher { get; set; }
        public string Religion { get; set; }
        public string Street { get; set; }
        public string RT { get; set; }
        public string RW { get; set; }
        public string Hamlet_Village { get; set; }
        public string Subdistrict { get; set; }
        public string Postal_Code { get; set; }
        public string Mobile_No { get; set; }
        public string Additional_Assignments { get; set; }
        public string SK_CPNS { get; set; }
        public DateTime SK_DateTime { get; set; }
        public string SK_Appointment { get; set; }
        public DateTime TMT_Appointment { get; set; }
        public string TMT { get; set; }
        public string Appointment_Agency { get; set; }
        public string Rank_Group { get; set; }
        public string Salary_Source { get; set; }
        public string Biological_mothers_name { get; set; }
        public string Marital_status { get; set; }
        public string Husband___Wife_Name { get; set; }
        public string Husband___Wife_NIP { get; set; }
        public string Husband___Wife_Occupation { get; set; }
        public DateTime TMT_PNS { get; set; }
        public string Already_a_Principals_License { get; set; }
        public string Supervision_Training { get; set; }
        public string Braille_expertise { get; set; }
        public string Sign_Language_Expertise { get; set; }
        public string NPWP { get; set; }
        public string The_name_of_the_taxpayer { get; set; }
        public string Citizenship { get; set; }
        public string Bank { get; set; }
        public string Bank_account_number { get; set; }
        public string Account_Name { get; set; }
        public string NIK { get; set; }
        public string No_KK { get; set; }
        public string Karpeg { get; set; }
        public string Karis_Karsu { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string NUKS { get; set; }
    }
}