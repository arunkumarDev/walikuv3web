﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SchoolMaster
/// </summary>
public class AcademicCalenderMaster
{
    public int ID { get; set; }

    public string Reason { get; set; }

    public DateTime HolidayDate { get; set; }

    public int AcademicYear { get; set; }

    public int SchoolID { get; set; }

    public bool IsActive { get; set; }

    public int CreatedBy { get; set; }

    public DateTime CreatedOn { get; set; } = DateTime.MinValue;

    public string Reason_Indonesian { get; set; }
    

}