﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Teacher Assign Substitute
/// </summary>
public class AssignSubstitute
{
    public int ID { get; set; }

    public int AcademicYear { get; set; }

    public int SchoolID { get; set; }

    public int ClassRoomID { get; set; }

    public int SubstituteTeacherID { get; set; }

    public DateTime AssignFromDate { get; set; }

    public DateTime AssignToDate { get; set; }

    public int IsActive { get; set; }

    public int CreatedBy { get; set; }

    public DateTime CreatedOn { get; set; } = DateTime.MinValue;

    public string Name { get; set; }

    public string SchoolName { get; set; }

    public string ClassName { get; set; }

}