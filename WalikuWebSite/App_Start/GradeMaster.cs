﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GreadeMaster
/// </summary>
public class GradeMaster
{
    public int ID { get; set; }
    public string Name { get; set; }
    public int IsActive { get; set; }
}