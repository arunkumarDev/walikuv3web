﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.App_Start
{
    public class AcademicYear
    {

        public int ID { get; set; }

        public string Name { get; set; }
        public DateTime CreatedOn { get; set; }

        public DateTime Start_Date { get; set; }

        public DateTime End_Date { get; set; }

        public bool IsActive { get; set; }

        public bool IsCurrentTerm { get; set; }

    }
}