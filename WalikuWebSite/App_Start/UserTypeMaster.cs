﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserTypeMaster
/// </summary>
public class UserTypeMaster
{

    public int ID { get; set; }

    public string UserType { get; set; }

    public string UserType_Indonesian { get; set; }

}