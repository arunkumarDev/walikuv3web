﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.App_Start
{
    public class ChartMaster
    {
        public class PresentAbsetReport
        {
            public int ClassRoomID { get; set; }

            public int AYID { get; set; }

            public double Abstsmratesickness { get; set; }

            public double Abstsmratenonhealth { get; set; }

            public double AbstsmrateUnknown { get; set; }

            public bool IsHistory { get; set; }

            public string AYName { get; set; }


            public string ClassName { get; set; }

            public int Present { get; set; }

            public int Absent { get; set; }

            public int AbsentBoys { get; set; }

            public int AbsentGirls { get; set; }

            public int TotalBoys { get; set; }

            public int TotalStudents { get; set; }
            public int TotalDays { get; set; }

            public int TotalGirls { get; set; }

            public string AttendanceDate { get; set; }

            public int StudentCount1 { get; set; }

            public int StudentCount2 { get; set; }

            public string Code { get; set; }
            
            public int ReasonCount { get; set; }

            public string Description { get; set; }

            public int ReasonType { get; set; }

            public string ReasonCode { get; set; }            

            public Single TotalAbsStudentDays { get; set; }

            public Single AbsenteesimRate { get; set; }

            public Single AttendanceRate { get; set; }

            public Single BoysAttendanceRate { get; set; }

            public Single GirlsAttendanceRate { get; set; }

            public Single BoysAbsenteeRate { get; set; }

            public Single GirlsAbsenteeRate { get; set; }

            public Single HealthAbsenteeRate { get; set; }

            public Single NonHealthAbsenteeRate { get; set; }

            public Single UnKnownAbsenteeRate { get; set; }


            public int AttendanceMonth { get; set; }

            public int ChildID { get; set; }

            public string Name { get; set; }

            public int PresentDays { get; set; }

            public int AbsentDays { get; set; }

            public Single AttendancePercent { get; set; }

            public int HolidyId { get; set; }

            public DateTime HolidayDate { get; set; }

            public decimal PresentPercent { get; set; }

            public decimal AbsentPercent { get; set; }

            public string AttendanceMonthName { get; set; }

            public int ChronicallyAbsent { get; set; }

            public int SeverelyChronicallyAbsent { get; set; }

            public string TopThreeReasons { get; set; }

            public Single ThreeDaysAbsenteeismRate { get; set; }

            public Single ThreeDaysAttendanceRate { get; set; }

            public int Health { get; set; }

            public int NonHealth { get; set; }

            public int UnKnown { get; set; }

            public int MonthlyAbsence { get; set; }

            public int Reasoncount { get; set; }

            public int Count { get; set; }





        }
        public class VmMonthList
        {
            public int AttendanceMonth { get; set; }
        }

        public class HolidayResponse
        {
            public DateTime HolidayDate { get; set; }
            public string Reason { get; set; }

        }

        public class AcademicYearResponse
        {
            public string AYName { get; set; }

        }
        public class AcademicYearGenderResponse
        {
            public int ClassRoomId { get; set; }
            public string ClassRoomName { get; set; }
            public int TotalBoys { get; set; }
            public int TotalGirls { get; set; }

        }
        public class AbsentismMonthResponse
        {
            public string AttendanceMonthName { get; set; }
            public string Code { get; set; }
            public Single Health { get; set; }
            public Single NonHealth { get; set; }
            public Single UnKnown { get; set; }

        }

        public class LoadAbsentDetailsForChildResponse
        {
            public DateTime AbsentDate { get; set; }
            public string Code { get; set; }
            public string Notes { get; set; }
        }

        public class LoadYearlyTopReasonDetails
        {
            public string Description { get; set; }
            public int Reasoncount { get; set; }
        }
        public class LoadMonthlyStudentDetails
        {
            public int ID { get; set; }
            public string NISN { get; set; }
            public string Name { get; set; }
            public DateTime CreatedOn { get; set; } = DateTime.MinValue;
            public string ClassroomName { get; set; }
            public string Status { get; set; }
            public string Reason { get; set; }

        }

        public class LoadConsecutiveresponse
        {
            public int GChildId { get; set; }
            public int LChildId { get; set; }
        }
        public class LoadStaffSummaryresponse
        {
            public Single AttendanceRate { get; set; }
            public int TotalStudents { get; set; }
            public Single HealthAbsenteeRate { get; set; }
            public Single NonHealthAbsenteeRate { get; set; }
            public Single UnKnownAbsenteeRate { get; set; }
            public int TotalBoys { get; set; }
            public int TotalGirls { get; set; }
            public Single AssignmentsAbsenteeRate { get; set; }

        }
        public class ExistingNisn
        {
            public int IsExistingNisn { get; set; }
            public int ChildId { get; set; }
        }

        public class TeacherDateDetailsResponse
        {
            public int Userid { get; set; }
            public bool IsPresent { get; set; }
            public string Code { get; set; }
        }
        
        public class TeacherSubmittedDetailsResponse
        {
            public int ID { get; set; }
            public int UserID { get; set; }
            public string SchoolName { get; set; }
            public string UserName { get; set; }
            public string Name { get; set; }
            public string Surname { get; set; }
            public int UserTypeID { get; set; }
            public string UserType { get; set; }
            public int SchoolID { get; set; }
            public string Code { get; set; }
            public bool IsPresent { get; set; }
            public int ReasonId { get; set; }
        }

        public class LoadAcademicMonthDetails
        {
            public string Name { get; set; }
            public string StartMonth { get; set; }
            public string EndMonth { get; set; }

        }

    }
}