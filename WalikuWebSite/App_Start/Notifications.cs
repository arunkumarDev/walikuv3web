﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.App_Start
{
    public class Notifications
    {
        public bool IsStaffAttendanceTaken { get; set; }

        public int CountOfClassesSynched { get; set; }

        public int LastDayOfMonth { get; set; }

        public int LastDayOfWeek { get; set; }

        public string GetSchoolName { get; set; }


        public int Transfercount { get; set; }

        public int Dropcount { get; set; }

        public int Diedcount { get; set; }

        public int Othercount { get; set; }

        public string NotifyMonthName { get; set; }

        public int NotifyYearName { get; set; }

    }
    

}