﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SchoolMaster
/// </summary>
public class SchoolAdminMappingMaster
{
    public int ID { get; set; }

    public int SchoolId { get; set; } = 0;

    public int UserId { get; set; } = 0;

    public string SchoolName { get; set; }

    public string UserName { get; set; }

    public int CreatedBy { get; set; } = 0;

    public DateTime CreatedOn { get; set; } = DateTime.MinValue;

    public bool IsActive { get; set; }


}