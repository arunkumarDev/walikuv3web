﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.DataAccess;

public partial class UserManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }
      //  Thread.CurrentThread.CurrentCulture = new CultureInfo("id-ID");
    }
    [WebMethod]
    public static List<UserMaster> GetAllUsers(int PageIndex)
    {
        return SqlHelper.GetAllUsers();

    }

    [WebMethod]
    public static List<UserTypeMaster> GetAllUserTypes(int PageIndex)
    {
        var myUser = SqlHelper.GetUserByID(Common.CommonFunction.GetUserIdFromCookieByID());
       
            int uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());

            List<UserTypeMaster> lst = SqlHelper.GetAllUserTypes(uType);
           string CurrentCulture = Common.CommonFunction.GetCurrentCulture();
            foreach (var item in lst)
            {
            if (CurrentCulture == "id")
                item.UserType = item.UserType_Indonesian;
            else
                item.UserType = item.UserType;

            item.UserType_Indonesian = "";
        }
        return lst; // SqlHelper.GetAllUserTypes(uType);
    }

    //[WebMethod]
    //public static void ManageUser(System.Web.HttpContext fileData, int ID, string UserName, string Password, string Name, string Surname, string Image, int UserType, string Address, string Phone, string Note)
    //{

    //    SqlHelper.ManageUser(new UserMaster()
    //    {
    //        ID = ID,
    //        UserName = UserName.Trim(),
    //        Password = Password.Trim(),
    //        Name = Name.Trim(),
    //        Surname = Surname.Trim(),
    //        Image = Image.Trim(),
    //        UserType = UserType,
    //        Address = Address.Trim(),
    //        Phone = Phone.Trim(),
    //        Note = Note.Trim()
    //    });
    //}


    [WebMethod]
    //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = = false)]
    public static void ManageUser()
    {
        var httpRequest = HttpContext.Current.Request;

        //var userRequest = new UserRequest
        //{
        var RequestMessage = httpRequest.Form["ID"];
        var Photo = httpRequest.Files["fileData"];

        //};


    }


    [WebMethod]
    public static void DeleteUserByID(int ID, int DelID)
    {
        SqlHelper.DeleteUserByID(ID,DelID);
    }


    [WebMethod]
    public static List<UserMaster> GetMyUserList()
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();

        return UserDataAccess.GetMyUserList(uid, Common.CommonFunction.GetCurrentCulture());
    }
    [WebMethod]
    public static List<DelteReasonMaster> GetAllReasonForDeleteTeacher()
    {
        return SqlHelper.GetAllReasonForDeleteTeacher(Common.CommonFunction.GetCurrentCulture());
    }

    [WebMethod]
    public static List<UserExistcount> GetUserExistscount(string Name)
    {
        return SqlHelper.GetUserExistscount(Name);
    }
    [WebMethod]
    public static List<UserExistcount> GetChildrenExistscount(string Name)
    {
        return SqlHelper.GetChildrenExistscount(Name);
    }
}