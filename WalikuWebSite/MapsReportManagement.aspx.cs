﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

/*
 * 
 * ALTER TABLE SchoolMaster ADD GeoLocation VARCHAR(50) NOT NULL DEFAULT '';*/

public partial class MapsReportManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
           
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }
    } 


    [WebMethod]
    public static Dictionary<string, object> GetData()
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        if (uid == 0)
            throw new Exception("UID not valid");
        var udata = SqlHelper.GetUserByID(uid); 

        var d = new Dictionary<string, object>();
        d.Add("Schools", SqlHelper.GetAllSchools());
        d.Add("Children", SqlHelper.GetAllChildrenForMe(uid, int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()),Common.CommonFunction.GetCurrentCulture()));
        return d;
    }

}
