﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.UI;
using WalikuWebSite.DataAccess;

public partial class Feedback : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }

    }
    [WebMethod]
    public static List<SchoolMaster> GetAllSchool(int PageIndex)
    {
        return SqlHelper.GetAllSchools();
    }

    //[WebMethod]
    //public List<VmFeedBack> GetFeedBackList(int PageIndex)
    //{
    //    return new List<VmFeedBack>();
    //}

    [WebMethod]
    public static List<VmFeedBack> GetFeedBackList(int SchoolID, DateTime StartDate, DateTime EndDate)
    {
        return FeedBackDataAccess.GetFeedBackData(SchoolID, StartDate, EndDate, Common.CommonFunction.GetCurrentCulture());
    }

}