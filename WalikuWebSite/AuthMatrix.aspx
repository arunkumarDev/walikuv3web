﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AuthMatrix.aspx.cs" MasterPageFile="~/Site.master" Inherits="WalikuWebSite.AuthMatrix" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

    <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="page-content">
                <div class="table-responsive">
                



                    <table id="tblProviders" class="table table-striped table-bordered data-table mb-0" style="width: 100%" role="grid">
                         <thead id="theadList">
                            <%--<tr>

                                <th style="position: relative; vertical-align: middle;" rowspan="3"><span><%=Resources.Resource.Lbl_Page %></span></th>
                            </tr>
                            <tr>
                                <th class="text-center" colspan="5">Waliku Roles</th>
                            </tr>--%>
                             
                             </thead>
                        <tbody id="tbodyList">

                        </tbody>
</table>

                 </div>

                     <div class="row">
                        <div class="col-md-12 text-right">
                           <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveMatrix"> <%=Resources.Resource.Lbl_Save %> </button>
                   
                 </div>
                </div>
                
            </div>
        </div>
    </main>

    <script>document.write("<script type='text/javascript' src='Scripts/AuthMatrix.js?v=" + JsVerion + "'><\/script>");</script>

  
    <style>
        .table {
    border: 1px solid #ddd;
}

.table thead tr th, .table tbody tr td{
    border-right: 1px solid #ddd;
}
.table thead .text-center th span{
    text-align: center;
}
.table thead tr th{
    background: #0b507a;
    color: #fff;
}
.table tbody tr td{
    color: #0b507a;
}
.mt-20{
    margin-top: 20px;
}

    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

</asp:Content>
