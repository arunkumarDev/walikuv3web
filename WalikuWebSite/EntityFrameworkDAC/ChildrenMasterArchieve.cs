//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WalikuWebSite.EntityFrameworkDAC
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChildrenMasterArchieve
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ParentName { get; set; }
        public Nullable<int> CommunityWorkerID { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
        public Nullable<int> SchoolID { get; set; }
        public Nullable<int> ClassroomID { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
        public string GeoLocation { get; set; }
        public string Gender { get; set; }
        public string ParentPhone { get; set; }
        public string Middlename { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string Note_Indonesian { get; set; }
        public string NISN { get; set; }
        public Nullable<int> PreviousClassRoomId { get; set; }
        public Nullable<int> GradeID { get; set; }
        public Nullable<int> DeleteReasonID { get; set; }
    }
}
