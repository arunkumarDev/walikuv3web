//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WalikuWebSite.EntityFrameworkDAC
{
    using System;
    using System.Collections.Generic;
    
    public partial class FirstAidAnalytic
    {
        public int Id { get; set; }
        public int VisitorId { get; set; }
        public System.DateTime VisitedOn { get; set; }
    
        public virtual UserMaster UserMaster { get; set; }
    }
}
