//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WalikuWebSite.EntityFrameworkDAC
{
    using System;
    using System.Collections.Generic;
    
    public partial class AttendanceMaster
    {
        public int ID { get; set; }
        public Nullable<int> ClassRoomID { get; set; }
        public Nullable<int> TeacherID { get; set; }
        public Nullable<int> ChildID { get; set; }
        public Nullable<System.DateTime> AttendanceDate { get; set; }
        public Nullable<bool> IsPresent { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<bool> IsSubmitted { get; set; }
    
        public virtual ChildrenMaster ChildrenMaster { get; set; }
        public virtual ClassMaster ClassMaster { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public virtual UserMaster UserMaster1 { get; set; }
    }
}
