//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WalikuWebSite.EntityFrameworkDAC
{
    using System;
    using System.Collections.Generic;
    
    public partial class ConfigureWeekday
    {
        public int ID { get; set; }
        public string ConfigureName { get; set; }
        public Nullable<bool> IsMandatory { get; set; }
        public Nullable<int> OrderBy { get; set; }
    }
}
