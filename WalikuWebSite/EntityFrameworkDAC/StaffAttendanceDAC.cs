﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WalikuWebSite.EntityFrameworkDAC
{
    public class StaffAttendanceDAC
    {
        public static void SaveStaffAttendance(int UserId,List<TeacherAttendanceMaster> StaffList)
        {
            using (WalikuEntities context = new WalikuEntities())
            {
                foreach(var staff in StaffList)
                {

                    using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
                    {
                        using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageStaffAttendance, con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@UserID", staff.UserID);
                            cmd.Parameters.AddWithValue("@AttendanceDate", staff.AttendanceDate);
                            cmd.Parameters.AddWithValue("@IsPresent", staff.IsPresent);
                            cmd.Parameters.AddWithValue("@CreatedBy",staff.UserID);
                            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                            cmd.Parameters.AddWithValue("@IsSubmitted", staff.IsSubmitted);
                            cmd.Parameters.AddWithValue("@SchoolID", staff.SchoolID);
                            cmd.Parameters.AddWithValue("@ReasonTypeID", staff.ReasonTypeID);

                            con.Open();
                       
                            cmd.ExecuteNonQuery();
                        }
                    }   //context.TeacherAttendanceMasters.Add(staff);
                }

                context.SaveChanges();
            }
        }

        public static void UpdateStaffAttendance(int UserId, List<TeacherAttendanceMaster> StaffList)
        {
            using (WalikuEntities context = new WalikuEntities())
            {
                foreach (var staff in StaffList)
                {

                    using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
                    {
                        using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.UpdateStaffAttendance, con))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@ID", staff.ID);
                            cmd.Parameters.AddWithValue("@UserID", staff.UserID);
                            cmd.Parameters.AddWithValue("@AttendanceDate", staff.AttendanceDate);
                            cmd.Parameters.AddWithValue("@IsPresent", staff.IsPresent);
                            cmd.Parameters.AddWithValue("@CreatedBy", staff.UserID);
                            cmd.Parameters.AddWithValue("@CreatedOn", DateTime.Now);
                            cmd.Parameters.AddWithValue("@IsSubmitted", staff.IsSubmitted);
                            cmd.Parameters.AddWithValue("@SchoolID", staff.SchoolID);
                            cmd.Parameters.AddWithValue("@ReasonTypeID", staff.ReasonTypeID);

                            con.Open();

                            cmd.ExecuteNonQuery();
                        }
                    }   //context.TeacherAttendanceMasters.Add(staff);
                }

                context.SaveChanges();
            }
        }



    }
}


//public Nullable<int> UserID { get; set; }
//public Nullable<System.DateTime> AttendanceDate { get; set; }
//public Nullable<bool> IsPresent { get; set; }
//public Nullable<int> CreatedBy { get; set; }
//public Nullable<System.DateTime> CreatedOn { get; set; }
//public Nullable<bool> IsSubmitted { get; set; }
//public Nullable<int> SchoolID { get; set; }
//public Nullable<int> ReasonTypeID { get; set; }