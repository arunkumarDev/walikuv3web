//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WalikuWebSite.EntityFrameworkDAC
{
    using System;
    using System.Collections.Generic;
    
    public partial class AbsenteeReasonDetail
    {
        public int ID { get; set; }
        public Nullable<int> ChildID { get; set; }
        public Nullable<int> TeacherID { get; set; }
        public Nullable<System.DateTime> VisitHomeDate { get; set; }
        public Nullable<bool> IsHealthReason { get; set; }
        public Nullable<int> ReasonID { get; set; }
        public Nullable<int> InformationProvider { get; set; }
        public Nullable<bool> HasPermission { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string Notes { get; set; }
        public string Notes_Indonesian { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
    
        public virtual ReasonMaster ReasonMaster { get; set; }
        public virtual ChildrenMaster ChildrenMaster { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        public virtual UserMaster UserMaster1 { get; set; }
    }
}
