﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using WebGrease.Css.Extensions;
using WalikuWebSite.App_Start;
using WalikuWebSite.Model;
using WalikuWebSite.Entities.DataModels;
using System.Data.SqlClient;
using System.Data;

namespace WalikuWebSite.EntityFrameworkDAC
{
    public class ImportData
    {
        public void ImportStudentData(List<ChildrenMaster> children)
        {
            List<string> data = new List<string>();
          var uid = Common.CommonFunction.GetUserIdFromCookieByID();
            using (WalikuEntities context = new WalikuEntities())
            {
                try
                {
                   
                    foreach (var childrenMaster in children)
                    {

                        
                            using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
                        {
                            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageBulkChildren, con))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@ID", childrenMaster.ID);
                                cmd.Parameters.AddWithValue("@Name", childrenMaster.Name);
                                cmd.Parameters.AddWithValue("@Surname", string.IsNullOrEmpty(childrenMaster.Surname) || string.IsNullOrWhiteSpace(childrenMaster.Surname) ? "" : childrenMaster.Surname);
                                cmd.Parameters.AddWithValue("@ParentName", string.IsNullOrEmpty(childrenMaster.F_Name) || string.IsNullOrWhiteSpace(childrenMaster.F_Name) ? "" : childrenMaster.F_Name);
                                cmd.Parameters.AddWithValue("@CommunityWorkerID", childrenMaster.CommunityWorkerID == null ? 0 : childrenMaster.CommunityWorkerID);
                                cmd.Parameters.AddWithValue("@Email", string.IsNullOrEmpty(childrenMaster.Email)||  string.IsNullOrWhiteSpace(childrenMaster.Email) ? "" : childrenMaster.Email);
                                cmd.Parameters.AddWithValue("@Image", string.IsNullOrEmpty(childrenMaster.Image) || string.IsNullOrWhiteSpace(childrenMaster.Image) ? "" : childrenMaster.Image);
                                cmd.Parameters.AddWithValue("@SchoolID", childrenMaster.SchoolID == null ? 7 : childrenMaster.SchoolID);
                                cmd.Parameters.AddWithValue("@ClassroomID", childrenMaster.ClassroomID == null ? 1071 : childrenMaster.ClassroomID);
                                cmd.Parameters.AddWithValue("@Address", string.IsNullOrEmpty(childrenMaster.Address) || string.IsNullOrWhiteSpace(childrenMaster.Address) ? "" : childrenMaster.Address);
                                cmd.Parameters.AddWithValue("@Phone", string.IsNullOrEmpty(childrenMaster.Phone) || string.IsNullOrWhiteSpace(childrenMaster.Phone) ? "" : childrenMaster.Phone);
                                cmd.Parameters.AddWithValue("@Note", string.IsNullOrEmpty(childrenMaster.Note) || string.IsNullOrWhiteSpace(childrenMaster.Note) ? "" : childrenMaster.Note);
                                cmd.Parameters.AddWithValue("@GeoLocation", string.IsNullOrEmpty(childrenMaster.GeoLocation) || string.IsNullOrWhiteSpace(childrenMaster.GeoLocation) ? string.Empty : childrenMaster.GeoLocation);
                                cmd.Parameters.AddWithValue("@Gender", string.IsNullOrEmpty(childrenMaster.Gender) || string.IsNullOrWhiteSpace(childrenMaster.Gender) ? "" : childrenMaster.Gender);
                                cmd.Parameters.AddWithValue("@IsActive", childrenMaster.IsActive == null ? true: childrenMaster.IsActive);
                                cmd.Parameters.AddWithValue("@CreatedBy", childrenMaster.CreatedBy == null ? uid : childrenMaster.CreatedBy);
                                cmd.Parameters.AddWithValue("@CreatedOn", childrenMaster.CreatedOn == null ? DateTime.Now : childrenMaster.CreatedOn);
                                cmd.Parameters.AddWithValue("@Note_Indonesian", string.IsNullOrEmpty(childrenMaster.Note_Indonesian) || string.IsNullOrWhiteSpace(childrenMaster.Note_Indonesian) ? "" : childrenMaster.Note_Indonesian);
                                cmd.Parameters.AddWithValue("@NISN", string.IsNullOrEmpty(childrenMaster.NISN) || string.IsNullOrWhiteSpace(childrenMaster.NISN) ? "" : childrenMaster.NISN);
                                cmd.Parameters.AddWithValue("@GradeID", childrenMaster.GradeID == null ? 0 : childrenMaster.GradeID);
                                cmd.Parameters.AddWithValue("@DeleteReasonID", childrenMaster.DeleteReasonID == null ? 7 : childrenMaster.DeleteReasonID);

                                cmd.Parameters.AddWithValue("@Birthplace", string.IsNullOrEmpty(childrenMaster.Birthplace) || string.IsNullOrWhiteSpace(childrenMaster.Birthplace) ? "" : childrenMaster.Birthplace);
                                cmd.Parameters.AddWithValue("@Date_of_Birth", childrenMaster.Date_of_Birth == null ? DateTime.Now : childrenMaster.Date_of_Birth);
                                cmd.Parameters.AddWithValue("@ID_Number", string.IsNullOrEmpty(childrenMaster.ID_Number) || string.IsNullOrWhiteSpace(childrenMaster.ID_Number) ? "" : childrenMaster.ID_Number);
                                cmd.Parameters.AddWithValue("@Religion", childrenMaster.Religion == null ? "" : childrenMaster.Religion);
                                cmd.Parameters.AddWithValue("@RT", string.IsNullOrEmpty(childrenMaster.RT) || string.IsNullOrWhiteSpace(childrenMaster.RT) ? "" : childrenMaster.RT);
                                cmd.Parameters.AddWithValue("@RW", string.IsNullOrEmpty(childrenMaster.RW) || string.IsNullOrWhiteSpace(childrenMaster.RW) ? "" : childrenMaster.RW);
                                cmd.Parameters.AddWithValue("@Village", childrenMaster.Village == null ? "" : childrenMaster.Village);
                                cmd.Parameters.AddWithValue("@Village_Office", childrenMaster.Village_Office == null ? "" : childrenMaster.Village_Office);
                                cmd.Parameters.AddWithValue("@Sub_District", string.IsNullOrEmpty(childrenMaster.Sub_District) || string.IsNullOrWhiteSpace(childrenMaster.Sub_District) ? "" : childrenMaster.Sub_District);
                                cmd.Parameters.AddWithValue("@Postal_Code", string.IsNullOrEmpty(childrenMaster.Postal_Code) || string.IsNullOrWhiteSpace(childrenMaster.Postal_Code) ? "" : childrenMaster.Postal_Code);
                                cmd.Parameters.AddWithValue("@Living_Situation", string.IsNullOrEmpty(childrenMaster.Living_Situation) || string.IsNullOrWhiteSpace(childrenMaster.Living_Situation) ? "" : childrenMaster.Living_Situation);
                                cmd.Parameters.AddWithValue("@Transportation_Mode", string.IsNullOrEmpty(childrenMaster.Transportation_Mode) || string.IsNullOrWhiteSpace(childrenMaster.Transportation_Mode) ? string.Empty : childrenMaster.Transportation_Mode);
                                cmd.Parameters.AddWithValue("@F_Name", string.IsNullOrEmpty(childrenMaster.F_Name) || string.IsNullOrWhiteSpace(childrenMaster.F_Name) ? "" : childrenMaster.F_Name);
                                cmd.Parameters.AddWithValue("@F_Birth_Year", childrenMaster.F_Birth_Year == null ? "" : childrenMaster.F_Birth_Year);
                                cmd.Parameters.AddWithValue("@F_Education_Level", childrenMaster.F_Education_Level == null ? "" : childrenMaster.F_Education_Level);
                                cmd.Parameters.AddWithValue("@F_Occupation", childrenMaster.F_Occupation == null ? "" : childrenMaster.F_Occupation);
                                cmd.Parameters.AddWithValue("@F_Income", string.IsNullOrEmpty(childrenMaster.F_Income) || string.IsNullOrWhiteSpace(childrenMaster.F_Income) ? "" : childrenMaster.F_Income);
                                cmd.Parameters.AddWithValue("@F_ID_Number", string.IsNullOrEmpty(childrenMaster.F_ID_Number) || string.IsNullOrWhiteSpace(childrenMaster.F_ID_Number) ? "" : childrenMaster.F_ID_Number);
                                cmd.Parameters.AddWithValue("@M_Name", childrenMaster.M_Name == null ? "" : childrenMaster.M_Name);
                                cmd.Parameters.AddWithValue("@M_Birth_Year", childrenMaster.M_Birth_Year == null ? "" : childrenMaster.M_Birth_Year);

                                cmd.Parameters.AddWithValue("@M_Education_Level", childrenMaster.M_Education_Level == null ? "" : childrenMaster.M_Education_Level);
                                cmd.Parameters.AddWithValue("@M_Occupation", string.IsNullOrEmpty(childrenMaster.M_Occupation) || string.IsNullOrWhiteSpace(childrenMaster.M_Occupation) ? "" : childrenMaster.M_Occupation);
                                cmd.Parameters.AddWithValue("@M_ID_Number", string.IsNullOrEmpty(childrenMaster.M_ID_Number) || string.IsNullOrWhiteSpace(childrenMaster.M_ID_Number) ? "" : childrenMaster.M_ID_Number);
                                cmd.Parameters.AddWithValue("@M_Income", childrenMaster.M_Income == null ? "" : childrenMaster.M_Income);
                                cmd.Parameters.AddWithValue("@G_Name", string.IsNullOrEmpty(childrenMaster.G_Name) || string.IsNullOrWhiteSpace(childrenMaster.G_Name) ? "" : childrenMaster.G_Name);
                                cmd.Parameters.AddWithValue("@G_Birth_Year", string.IsNullOrEmpty(childrenMaster.G_Birth_Year) || string.IsNullOrWhiteSpace(childrenMaster.G_Birth_Year) ? "" : childrenMaster.G_Birth_Year);
                                cmd.Parameters.AddWithValue("@G_Education_Level", childrenMaster.G_Education_Level == null ? "" : childrenMaster.G_Education_Level);
                                cmd.Parameters.AddWithValue("@G_Occupation", childrenMaster.G_Occupation == null ? "" : childrenMaster.G_Occupation);
                                cmd.Parameters.AddWithValue("@G_Income", string.IsNullOrEmpty(childrenMaster.G_Income) || string.IsNullOrWhiteSpace(childrenMaster.G_Income) ? "" : childrenMaster.G_Income);
                                cmd.Parameters.AddWithValue("@G_ID_Number", string.IsNullOrEmpty(childrenMaster.G_ID_Number) || string.IsNullOrWhiteSpace(childrenMaster.G_ID_Number) ? "" : childrenMaster.G_ID_Number);
                                cmd.Parameters.AddWithValue("@Current_Class", string.IsNullOrEmpty(childrenMaster.Current_Class) || string.IsNullOrWhiteSpace(childrenMaster.Current_Class) ? "" : childrenMaster.Current_Class);
                                cmd.Parameters.AddWithValue("@National_Examination_Participant_No", string.IsNullOrEmpty(childrenMaster.National_Examination_Participant_No) || string.IsNullOrWhiteSpace(childrenMaster.National_Examination_Participant_No) ? string.Empty : childrenMaster.National_Examination_Participant_No);
                                cmd.Parameters.AddWithValue("@Certificate_Serial_No", string.IsNullOrEmpty(childrenMaster.Certificate_Serial_No) || string.IsNullOrWhiteSpace(childrenMaster.Certificate_Serial_No) ? "" : childrenMaster.Certificate_Serial_No);
                                cmd.Parameters.AddWithValue("@Recipient_of_Indonesian_Smart_Card", childrenMaster.Recipient_of_Indonesian_Smart_Card == null ? "" : childrenMaster.Recipient_of_Indonesian_Smart_Card);
                                cmd.Parameters.AddWithValue("@Smart_Card_No", childrenMaster.Smart_Card_No == null ? "" : childrenMaster.Smart_Card_No);
                                cmd.Parameters.AddWithValue("@Name_on_Smart_Card", childrenMaster.Name_on_Smart_Card == null ? "" : childrenMaster.Name_on_Smart_Card);
                                cmd.Parameters.AddWithValue("@Special_Needs", string.IsNullOrEmpty(childrenMaster.Special_Needs) || string.IsNullOrWhiteSpace(childrenMaster.Special_Needs) ? "" : childrenMaster.Special_Needs);
                                cmd.Parameters.AddWithValue("@Previous_School", string.IsNullOrEmpty(childrenMaster.Previous_School) || string.IsNullOrWhiteSpace(childrenMaster.Previous_School) ? "" : childrenMaster.Previous_School);
                                cmd.Parameters.AddWithValue("@Position_of_Child_among_Siblings", childrenMaster.Position_of_Child_among_Siblings == null ? "0" : childrenMaster.Position_of_Child_among_Siblings);
                                cmd.Parameters.AddWithValue("@Latitude", childrenMaster.Latitude == null ? "" : childrenMaster.Latitude);

                                cmd.Parameters.AddWithValue("@Longitude", string.IsNullOrEmpty(childrenMaster.Longitude) || string.IsNullOrWhiteSpace(childrenMaster.Longitude) ? "" : childrenMaster.Longitude);
                                cmd.Parameters.AddWithValue("@Family_Register_No", string.IsNullOrEmpty(childrenMaster.Family_Register_No) || string.IsNullOrWhiteSpace(childrenMaster.Family_Register_No) ? "" : childrenMaster.Family_Register_No);
                                cmd.Parameters.AddWithValue("@Weight", string.IsNullOrEmpty(childrenMaster.Weight) || string.IsNullOrWhiteSpace(childrenMaster.Weight) ? "" : childrenMaster.Weight);
                                cmd.Parameters.AddWithValue("@Height", string.IsNullOrEmpty(childrenMaster.Height) || string.IsNullOrWhiteSpace(childrenMaster.Height) ? string.Empty : childrenMaster.Height);
                                cmd.Parameters.AddWithValue("@Head_circumference", string.IsNullOrEmpty(childrenMaster.Head_circumference) || string.IsNullOrWhiteSpace(childrenMaster.Head_circumference) ? "" : childrenMaster.Head_circumference);
                                cmd.Parameters.AddWithValue("@No_of_Siblings_related_by_blood", childrenMaster.No_of_Siblings_related_by_blood == null ? "" : childrenMaster.No_of_Siblings_related_by_blood);
                                cmd.Parameters.AddWithValue("@dst_frm_houseto", childrenMaster.dst_frm_houseto == null ? "" : childrenMaster.dst_frm_houseto);
                                cmd.Parameters.AddWithValue("@TimeTaken_Hrs", childrenMaster.TimeTaken_Hrs == null ? "" : childrenMaster.TimeTaken_Hrs);
                                cmd.Parameters.AddWithValue("@TimeTaken_Mins", string.IsNullOrEmpty(childrenMaster.TimeTaken_Mins) || string.IsNullOrWhiteSpace(childrenMaster.TimeTaken_Mins) ? "" : childrenMaster.TimeTaken_Mins);
                               

                                con.Open();
                                cmd.ExecuteNonQuery();
                            }
                        }


                        // SqlHelper.ManageChildren(new ChildrenMaster()
                        // {

                        //     ID = tmp.ID,
                        //     Name = tmp.Name.Trim(),
                        //     Surname = tmp.Surname.Trim(),
                        //     ParentName = tmp.ParentName.Trim(),
                        //     CommunityWorkerID = tmp.CommunityWorkerID,
                        //     Email = tmp.Email.Trim(),
                        //     Image = string.Empty,
                        //     SchoolID = tmp.SchoolID,
                        //     ClassroomID = tmp.ClassroomID,
                        //     Address = tmp.Address.Trim(),
                        //     Phone = tmp.Phone.Trim(),
                        //     Note = tmp.Note.Trim(),
                        //     Gender = tmp.Gender,
                        //     NISN = tmp.NISN,
                        //     GradeID = tmp.GradeID,
                        //     DeleteReasonID = tmp.DeleteReasonID

                        // });

                        //context.ChildrenMasters.Add(child);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }
        }

        public void UpdateStudentData(List<ChildrenMaster> children)
        {
            using (WalikuEntities context = new WalikuEntities())
            {
                foreach (var child in children)
                {
                    context.ChildrenMasters.ForEach(x => { 
                    if(x.ID == child.ID)
                     {
                            x.Name = child.Name;
                            x.Surname = child.Surname;
                            x.ParentName = child.ParentName;
                            x.CommunityWorkerID = child.CommunityWorkerID == null ? 0 : (int)child.CommunityWorkerID;
                            x.ClassroomID = child.ClassroomID;
                            x.SchoolID = child.SchoolID;
                            x.Email = child.Email;
                            x.Image = child.Image;
                            x.Address = child.Address;
                            x.Phone = child.Phone;
                            x.Note = child.Note;
                            x.GeoLocation = child.GeoLocation;
                            x.Gender = child.Gender;
                            x.Middlename = child.Middlename;
                            x.Note_Indonesian = child.Note_Indonesian;
                            x.NISN = child.NISN;
                            x.GradeID = child.GradeID == null ? 0 : (int)child.GradeID;
                            x.DeleteReasonID = child.DeleteReasonID == null ? 0 : (int)child.DeleteReasonID;
                            x.Student_Registration_Number = child.Student_Registration_Number;
                            x.Birthplace = child.Birthplace;
                            x.Date_of_Birth = child.Date_of_Birth;
                            x.ID_Number = child.ID_Number;
                            x.Religion = child.Religion;
                            x.RT = child.RT;
                            x.RW = child.RW;
                            x.Village = child.Village;
                            x.Village_Office = child.Village_Office;
                            x.Sub_District = child.Sub_District;
                            x.Postal_Code = child.Postal_Code;
                            x.Living_Situation = child.Living_Situation;
                            x.Transportation_Mode = child.Transportation_Mode;
                            x.Certificate_of_National_Examination_Result = child.Certificate_of_National_Examination_Result;
                            x.Recipient_of_Social_Security_Card = child.Recipient_of_Social_Security_Card;
                            x.Social_Security_No = child.Social_Security_No;
                            x.F_Name = child.F_Name;
                            x.F_Birth_Year = child.F_Birth_Year;
                            x.F_Education_Level = child.F_Education_Level;
                            x.F_Occupation = child.F_Occupation;
                            x.F_Income = child.F_Income;
                            x.F_ID_Number = child.F_ID_Number;
                            x.M_Name = child.M_Name;
                            x.M_Birth_Year = child.M_Birth_Year;
                            x.M_Education_Level = child.M_Education_Level;
                            x.M_Occupation = child.M_Occupation;
                            x.M_Income = child.M_Income;
                            x.M_ID_Number = child.M_ID_Number;
                            x.G_Name = child.G_Name;
                            x.G_Birth_Year = child.G_Birth_Year;
                            x.G_Education_Level = child.G_Education_Level;
                            x.G_Occupation = child.G_Occupation;
                            x.G_Income = child.G_Income;
                            x.G_ID_Number = child.G_ID_Number;
                            x.Current_Class = child.Current_Class;
                            x.National_Examination_Participant_No = child.National_Examination_Participant_No;
                            x.Certificate_Serial_No = child.Certificate_Serial_No;
                            x.Recipient_of_Indonesian_Smart_Card = child.Recipient_of_Indonesian_Smart_Card;
                            x.Smart_Card_No = child.Smart_Card_No;
                            x.Name_on_Smart_Card = child.Name_on_Smart_Card;
                            x.Family_Card_No = child.Family_Card_No;
                            x.Birth_Certificate_No = child.Birth_Certificate_No;
                            x.Bank = child.Bank;
                            x.Bank_Account_Number = child.Bank_Account_Number;
                            x.Name_of_Account_Holder = child.Name_of_Account_Holder;
                            x.Suitable_for_Smart_Indonesia_Program = child.Suitable_for_Smart_Indonesia_Program;
                            x.Reason_for_Smart_Indonesia_Program = child.Reason_for_Smart_Indonesia_Program;
                            x.Special_Needs = child.Special_Needs;
                            x.Previous_School = child.Previous_School;
                            x.Position_of_Child_among_Siblings = child.Position_of_Child_among_Siblings;
                            x.Latitude = child.Latitude;
                            x.Longitude = child.Longitude;
                            x.Family_Register_No = child.Family_Register_No;
                            x.Weight = child.Weight;
                            x.Height = child.Height;
                            x.Head_circumference = child.Head_circumference;
                            x.No_of_Siblings_related_by_blood = child.No_of_Siblings_related_by_blood;
                            x.Mobile_No = child.Mobile_No;
                            x.dst_frm_houseto = child.dst_frm_houseto;
                            x.TimeTaken_Hrs = child.TimeTaken_Hrs;
                            x.TimeTaken_Mins = child.TimeTaken_Mins;
                    }
                    });
                }
                context.SaveChanges();
            }
        }
        public void ImportTeacherData(List<UserMaster> teachers)
        {
            using (WalikuEntities context = new WalikuEntities())
            {
                try
                {
                    foreach (var teacher in teachers)
                    {
                        context.UserMasters.Add(teacher);
                    }
                    context.SaveChanges();
                }
                catch(Exception e)
                {
                    throw e;
                }
            }
        }
    }
}