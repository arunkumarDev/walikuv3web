﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.EntityFrameworkDAC { 
    public class ExportData
    {
        public List<ChildrenMaster> ExportStudentData()
        {
            using (WalikuEntities context = new WalikuEntities())
            {
                var childrenList = context.ChildrenMasters.ToList();
                return childrenList.Where(x => x.IsActive == true).ToList();

            }
        }
        public List<UserMaster> ExportTeacherData()
        {
            using (WalikuEntities context = new WalikuEntities())
            {
                return context.UserMasters.Where(x => x.IsActive == true).ToList();
            }
        }
    }
}