﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="MapsReportManagement.aspx.cs" Inherits="MapsReportManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        #map {
            height: 640px;
            width: 100%;
        }
    </style> 
     
    <div id="content">
        <div class="outer">
            <div class="inner bg-light lter">
                <!--Begin Datatables-->
                <div class="row">
                    <div class="col-lg-12 ui-sortable">
                        <div class="box ui-sortable-handle">
                            <header>
                                <div class="icons"><i class="fa fa-table"></i></div>
                                <h5>Children on map</h5> 
                            </header>
                            <div class="body"> 
                                <div id="map"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!--End Datatables-->
            </div>
            <!-- /.inner -->
        </div>
        <!-- /.outer -->
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV6jv36xrNX23a9RWk-9ZsCSCOmSU-iys"></script>
    <script>
        var map = null; 

        $(document).ready(function () {
            initMap();
        });  
         
        function initMap() {  
            var latlng = new google.maps.LatLng(-34.397, 150.644);
            var mapOptions = {
                zoom: 14,
                center: latlng
            }; 
            map = new google.maps.Map(document.getElementById("map"), mapOptions); 

            LoadData();
        } 


    function createMarketAt(t, name, strLatLng, icon) { 
        var myLatlng = new google.maps.LatLng(strLatLng.split("/")[0], strLatLng.split("/")[1]); 
         
        // Crea il marker
        marker = new google.maps.Marker({
            map: map,
            title: name,
            position: myLatlng,
            draggable: false,
            icon: icon
        }); 
        marker.setMap(map);
        return marker;
    }

        
    function LoadData() {

        $.ajax({
            type: "POST",
            url: "MapsReportManagement.aspx/GetData",
            data: '',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                console.log(data);
                // Crea le varie scuole
                var lastMarker;
                for (var i = 0; i < data.d.Schools.length; i++)
                {
                    // Crea il marker
                    lastMarker = createMarketAt(data.d.Schools[i], data.d.Schools[i].Name, data.d.Schools[i].GeoLocation, "https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_blueS.png"); 
                }
                if(lastMarker != null)
                    map.panTo(lastMarker.getPosition());
                
                for (var i = 0; i < data.d.Children.length; i++)
                {
                    // Crea il marker
                    createMarketAt(data.d.Children[i], data.d.Children[i].Name + " " + data.d.Children[i].Middlename + " " + data.d.Children[i].Surname , data.d.Children[i].GeoLocation, "https://raw.githubusercontent.com/Concept211/Google-Maps-Markers/master/images/marker_yellowC.png");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

    </script>


</asp:Content>

