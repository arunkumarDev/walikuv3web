﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using Waliku.GoogleTranslator;

namespace WalikuWebSite
{
    /// <summary>
    /// Summary description for UserManagement
    /// </summary>
    public class UserManagement : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string strImageName = string.Empty;
            string strTemp = "1";
            string result = "";
            if (context.Request.Files.Count > 0)
            {


                HttpFileCollection files = context.Request.Files;

                for (int i = 0; i < files.Count; i++)
                {
                    try
                    {


                        HttpPostedFile file = files[i];
                        strImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                        string filePath = context.Server.MapPath("~/Photos/" + strImageName);
                        file.SaveAs(filePath);
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                        //strTemp = ex.Message + "---" + ex.InnerException;
                    }

                }

            }
            else
            {
                if (Convert.ToInt32(context.Request.Form["ID"]) != 0)
                {
                    strImageName = context.Request.Form["hdnImage"].Trim();
                }

            }
            try
            {
               string Note = CheckEmpty(context.Request.Form["Note"]);

                string CurrentCulture = Common.CommonFunction.GetCurrentCulture(), ToCulture = Common.CommonFunction.GetToCulture();
                Dictionary<string, string> Dict = GoogleTranslator.TranslateText(Note, CurrentCulture,ToCulture);


                SqlHelper.ManageUser(new UserMaster()
                {
                    ID = Convert.ToInt32(context.Request.Form["ID"]),
                    UserName = CheckEmpty(context.Request.Form["UserName"]),
                    Password = CheckEmpty(context.Request.Form["Password"]),
                    Name = CheckEmpty(context.Request.Form["Name"]),
                    Surname = CheckEmpty(context.Request.Form["Surname"]),
                    Image = strImageName,
                    UserType = Convert.ToInt32(context.Request.Form["UserType"]),
                    Address = CheckEmpty(context.Request.Form["Address"]),
                    Phone = CheckEmpty(context.Request.Form["Phone"]),
                    Note = Dict["en"],  //Note,
                    GeoLocation = CheckEmpty(context.Request.Form["GeoLocation"]),
                    Gender = CheckEmpty(context.Request.Form["Gender"]),
                    CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                    CreatedOn = DateTime.Now,
                    IsActive = 1,
                    Email = CheckEmpty(context.Request.Form["Email"]),
                    Note_Indonesian = Dict["id"],
                    NIK = CheckEmpty(context.Request.Form["NIK"]),
                    DeleteReasonID = context.Request.Form["DeleteReasonID"] == "null" ? 0 : Convert.ToInt32(context.Request.Form["DeleteReasonID"])
                });
                result = "Success";
            }
            catch (SqlException SqlEx)
            {
                if (SqlEx.Number == 2601)
                    result = "User Name already Taken";
                else
                    result = "User Name already exists ";
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "text/plain";
                strTemp = ex.Message + "---" + ex.InnerException;
                result = strTemp;
               
            }
            context.Response.Write(result);
        }

        private string CheckEmpty(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            else
                return input.Trim();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}