﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using Waliku.Web.RestClient;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Common.CommonFunction.IsUserLoggedIn())
        {
          //  Response.Redirect("Report-schoolsummary");
        }
        SetLanguage("id-ID"); //id-ID
    }

    private void SetLanguage(string Language)
    {
        if (Language != null)
        {
            string Lang = Language.ToString();
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Lang);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Lang);
            Page.Culture = Lang;
        }
        
    }

    private async Task Authenticate(string userName, string password)
    {
        IAuthenticationRestService authenticationRestService = new AuthenticationRestService("","en");
        await authenticationRestService.Login(new Waliku.Domain.Models.WebModel.RequestModel.LoginRequest {
        UserName  = userName,
        Password = password
        });

        

    }

    [WebMethod]
    public static bool ValidateUser(string UserName,string Password,string Language= "en-us", bool RememberMe=false)
    {
        int userID = SqlHelper.ValidateUser(UserName, Password);
        HttpContext.Current.Session["Language"] = Language;
        
        if (userID > 0)
        {
         
            Common.CommonFunction.CreateCookie(userID, RememberMe,Language);
            return true;
        }
        else
        {
            return false;
        }
    }

    [WebMethod]
    public static int GetUserType()
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        int usertype = SqlHelper.GetUserType(uid);
        return usertype;
    }

}