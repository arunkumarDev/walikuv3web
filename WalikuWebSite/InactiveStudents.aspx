﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="InactiveStudents.aspx.cs" Inherits="WalikuWebSite.InactiveStudents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <main class="l-main">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <h3><%=Resources.Resource.Menu_InactiveStudents %></h3>
            </div>
            <div class="page-content">
                        <div class="row">
                        <div class="col-sm-12">
                            <div class="configuration-details">
                                <div class="configuration_data">
                                    <div class="bmd-form-group">
                                        <div class="col-md-12">
                                            <div id= "configDataGroup">
                                         
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                             <div class="data-table-wrapper sort-table">
                            <table id="inactiveStudentsDataTable" " class="table table-hover mb-0 user-tbl" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th><span><%=Resources.Resource.Lbl_StudentName %></span></th>
                                        <th><span><%=Resources.Resource.Lbl_Gender %></span></th>
                                        <th><span><%=Resources.Resource.Lbl_SchoolName %></span></th>
                                        <th><span><%=Resources.Resource.Lbl_ClassName %></span></th>
                                        <th><span><%=Resources.Resource.Lbl_Action %></span></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                                 </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="Scripts/Common.js?0"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/AdminOperations/InactiveStudents.js'><\/script>");</script>
    <script>
    </script>
</asp:Content>

