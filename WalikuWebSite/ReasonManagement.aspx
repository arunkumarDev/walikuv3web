﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReasonManagement.aspx.cs" Inherits="WalikuWebSite.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="ReasonModel" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md custom-material">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Title_ReasonManagement %></h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12 pr-0">
                    <form class="custom-material form-horizontal">
                         <input type="hidden" id="hdnReasonID" value="0" />
                        <div class="bmd-form-group is-filled">
                            <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SelectReasonType %></label>
                            <span>
                                <select id="ddlReasonType"  class="form-control select-validate">                                    
                                </select>
                                <i class="fa fa-angle-down fa-icons text-18"></i>
                            </span>
                        </div>
                        <div class="bmd-form-group is-filled">
                            <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Description %></label>
                            <span><br /></span>
                            <input type="text" id="txtDescription" class="form-control input-validate">
                        </div>
                    </form>
                </div>

            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal"> <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveReason"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
            </div>
        </div>
    </div>
</div>
 <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_ReasonManagement %></h1>
                </div>
            </div>
            <div class="row">
                <!-- <div class="col-md-3 mb-5">
                    <div class="search-form"><span class="bmd-form-group"><input type="text" placeholder="Search..."
                                class="form-control"></span>
                        <i class="fa fa-search fa-icons"></i>
                    </div>
                </div> -->
                <div class="col-md-12 mb-5 text-right">
                    <button type="button" class="btn btn-primary " title="" data-toggle="modal" id="btnAddReason"><%=Resources.Resource.Lbl_AddReason %></button>
                </div>
            </div>
            <div class="page-content">
                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-striped table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th><%=Resources.Resource.Lbl_SerialNo %></th>
                                            <th><%=Resources.Resource.Lbl_ReasonType %></th>
                                            <th><%=Resources.Resource.Lbl_Description %></th>
                                            <th><%=Resources.Resource.Lbl_Action %></th>
                                        </tr>
                                    </thead>
                                   <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script>
        var Lbl_AddReason = '<%=Resources.Resource.Lbl_AddReason %>'
         var Lbl_EditReason = '<%=Resources.Resource.Lbl_EditReason %>'
    </script>
   <script>document.write("<script type='text/javascript' src='Scripts/ReasonManagement.js?v=" + JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

</asp:Content>
