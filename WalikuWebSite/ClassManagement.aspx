﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ClassManagement.aspx.cs" Inherits="ClassManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
   <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Menu_ClassManagement %></h1>
                </div>
            </div>
            <div class="row">
                <!-- <div class="col-md-3 mb-5">
                    <div class="search-form"><span class="bmd-form-group"><input type="text" placeholder="Search..."
                                class="form-control"></span>
                        <i class="fa fa-search fa-icons"></i>
                    </div>
                </div> -->
                <div class="col-md-12 mb-5 text-right">
                    <button type="button" class="btn btn-primary " title="" data-toggle="modal" id="btnAddClass" ><%=Resources.Resource.Lbl_AddClass %></button>
                </div>
            </div>
            <div class="page-content">
                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-striped table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width:60px;"><%=Resources.Resource.Lbl_SerialNo %></th>
                                            <th><%=Resources.Resource.Lbl_ClassName %></th>
                                            <th style="width:150px;"><%=Resources.Resource.Lbl_Action %></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

       <!-- Add User -->
<div id="ClassManagementModel" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md custom-material">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Lbl_AddClass %></h4>
            </div>
            <div class="modal-body">
                <form class="custom-material">      
                      <input type="hidden" id="hdnClassID" value="0" />
                    <div class="col-md-12 pr-0">
                        <div class="row">                           
                            <div class="col-md-6">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required "><%=Resources.Resource.Lbl_ClassName %></label>
                                    <span><br/></span>
                                    <input type="text" id="txtClassName" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SelectSchool %> </label>
                                    <span><br/></span>
                                    <select id="ddlSchool" class="form-control select-validate">                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                                                    
                          
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal"> <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveClass"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
            </div>
        </div>
    </div>
</div>


    
<div id="DelReasonPopup" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><%=Resources.Resource.Title_DeleteClass %></h4>
            </div>
            <div class="modal-body custom-scroll mh-500 custom-material">
                <p class="mt-5 col-md-12"><%=Resources.Resource.Lbl_Delmsg %></p>
               
            </div>
             <div class="modal-footer" style="padding:0px;">
                <div class="col-md-12 text-right">
                    <button type="button"  style="min-width: 97px;" class="btn btn-default mr-5" title="" data-dismiss="modal"> <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button"  style="min-width: 97px;" class="btn btn-primary" title="" id="btndelChild"> <%=Resources.Resource.Lbl_Ok%> </button>
                </div>
            </div>
        </div>
    </div>
</div>


 <script>document.write("<script type='text/javascript' src='Scripts/ClassManagement.js?v=" + JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

</asp:Content>

