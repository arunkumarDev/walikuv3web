﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Web;
using Waliku.GoogleTranslator;

namespace WalikuWebSite
{
    /// <summary>
    /// Summary description for SchoolAcademicCalendar
    /// </summary>
   
    public class SchoolAcademicCalendar : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string strImageName = string.Empty;
            string strTemp = "1";
            string result = "";
            try
            {

            
                SqlHelper.SaveSchoolAcademicCalendar(new SchoolAcademicCalendarMaster()
                {
                    ID = Convert.ToInt32(context.Request.Form["ID"]),
                    Name = CheckEmpty(context.Request.Form["Name"]),
                    Name_Indonesian = CheckEmpty(context.Request.Form["Name"]),
                    CalendarTypeId = Convert.ToInt32(context.Request.Form["CalendarTypeId"]),
                    SchoolId = Convert.ToInt32(context.Request.Form["SchoolId"]),
                    CalendarDate = Convert.ToDateTime(context.Request.Form["CalendarDate"]),
                   CalendarToDate = Convert.ToDateTime(context.Request.Form["CalendarToDate"]),
                    CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                    CreatedOn = DateTime.Now,
                    IsActive = 1
                });
                result = "Success";
            }
            catch (SqlException SqlEx)
            {
                if (SqlEx.Number == 2601)
                    result = "User Name already Taken";
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "text/plain";
                strTemp = ex.Message + "---" + ex.InnerException;
                result = strTemp;

            }
            context.Response.Write(result);
        }

        private string CheckEmpty(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            else
                return input.Trim();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}