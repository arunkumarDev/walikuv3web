﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExportStudentData.aspx.cs" Inherits="WalikuWebSite.ExportStudentData" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
     <main class="l-main custom-material">
    <script src="Scripts/ImportData/ExportStudentData.js">
     
    </script>

        <div>
            <div class="col-sm-3">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Select %></label>
                       <select id="ddlExportType" class="form-control">
                           <option value="0" title="Student">Student</option>
                           <option value="1" title="Teacher" style="display:none">Teacher</option>
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
             <div>
                              <input type="button" class="btn btn-success" id="btnExportData" value="<%=Resources.Resource.Lbl_Export %>">
                        </div>
            <div class="col-md-6" id="exportOutput" style="display:none;">
                <ul>
                    <li>
                        <p>Please Download the Exported File from <a href="ImportExport/ExportedFiles/ChildrenExport.xlsx">Here</a></p>
                    </li>
                </ul>
                
            </div>
        </div>
</main>
</asp:Content>