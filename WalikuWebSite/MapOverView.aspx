﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="MapOverView.aspx.cs" Inherits="WalikuWebSite.MapOverView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_MapOverview %></h1>
                </div>
            </div>
            <div class="row">
               
                <div class="col-sm-3">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select class="form-control" id="ddlSchool">
                          
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ClassName %></label>
                        <select class="form-control" id="ddlClass">
                           
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                <div class="col-sm-2 mt-15">
                    <button type="button" class="btn btn-primary" title="" id="btnSearch"> <%=Resources.Resource.Lbl_Search %> </button>
                </div>
            </div>
            <div class="page-content">
                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                                               
                            <div id="map-canvas" style="width: 100%; height: 400px"></div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

     <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV6jv36xrNX23a9RWk-9ZsCSCOmSU-iys">
    </script>

        <script>document.write("<script type='text/javascript' src='Scripts/MapOverView.js?v=" +JsVerion + "'><\/script>");</script>
</asp:Content>