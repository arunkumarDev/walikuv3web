﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report-TrackStudents.aspx.cs" MasterPageFile="~/Site.master" Inherits="WalikuWebSite.Report_TrackStudents" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
                <div class="row">
                        <div class="col-sm-2">
                            <div class="bmd-form-group">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectDate %></label>
                                   <input type="text" class="form-control date-icon hasDatepicker" id="datepicker">
                                    <i class="fa fa-calendar fa-icons"></i>
                                
                            </div>
                        </div>
                       
                    
                        <div class="col-sm-2 mt-15">
                            <button type="button" class="btn btn-primary" title="" id="btnFilter">Filter</button>
                        </div>

                    <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-striped table-hover" style="width:100%">
                                        <thead>
                                                <tr>
                                                    <th><%=Resources.Resource.Lbl_SerialNo %></th>
                                                    <th>ID</th>
                                                    <th>Name</th>
                                                    <th>Status</th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>

            
                                            </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>



                    </div>
            
            
        </div>
    </main>

     <script src="Scripts/bootstrap-datepicker.js"></script>
      <script>
        var FormLoad = true;
  function noSunday(date) {
        var day = date.getDay();
        return [(day > 0), ''];
    };


        $('#datepicker').datepicker({
            autoclose: true,
             beforeShowDay: noSunday
        });
      
        var date = new Date();
        $('#datepicker').datepicker('setDate', date);

    </script>

     <script>document.write("<script type='text/javascript' src='Scripts/TrackStudents.js?v=" + JsVerion + "'><\/script>");</script>
   

</asp:Content>
