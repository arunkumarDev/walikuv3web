﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WalikuWebSite.DataAccess
{
    public class AcademicCalendarDataAccess
    {
        public static List<SchoolMaster> GetMySchoolList(int UserId)
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                DataSet ds = new DataSet();
                sqlCnn.Open();


                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetMySchoolList, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = UserId;
                  
                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "SchoolList");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<SchoolMaster> lst = ds.Tables[0].ToList<SchoolMaster>();
                        return lst;
                    }

                }
            }
            return new List<SchoolMaster>();

        }

        public static List<SchoolAcademicCalendarMaster> GetSchoolAcademicCalendarList(int UserId, int ID)
        {
            var LangID = Common.CommonFunction.GetCurrentCulture();
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                DataSet ds = new DataSet();
                sqlCnn.Open();


                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetSchoolAcademicCalendarList, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = UserId;
                    sqlCmd.Parameters.Add(new SqlParameter("@SchoolID", SqlDbType.Int)).Value = ID;
                    sqlCmd.Parameters.Add(new SqlParameter("@LangId", SqlDbType.Char)).Value = LangID;


                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "SchoolAcademicCalendarList");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<SchoolAcademicCalendarMaster> lst = ds.Tables[0].ToList<SchoolAcademicCalendarMaster>();
                        return lst;
                    }

                }
            }
            return new List<SchoolAcademicCalendarMaster>();

        }
    }
}