﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace WalikuWebSite.DataAccess
{
    public class FeedBackDataAccess
    {
        public static List<VmFeedBack> GetFeedBackData(int SchoolID, DateTime StarDate, DateTime EndDate, string LangID)
        {

            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
                {
                    DataSet ds = new DataSet();
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetFeedbackList, sqlCnn);

                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.Add(new SqlParameter("@SchoolID", SqlDbType.Int)).Value = SchoolID;
                    sqlCmd.Parameters.Add(new SqlParameter("@StarDate", SqlDbType.DateTime)).Value = StarDate;
                    sqlCmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime)).Value = EndDate;
                    sqlCmd.Parameters.Add(new SqlParameter("@LangID", SqlDbType.Char)).Value = LangID;

                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "FeedBack");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<VmFeedBack> lst = ds.Tables[0].ToList<VmFeedBack>();
                        return lst;
                    }
                    return new List<VmFeedBack>();
                }
            }
            catch (Exception ex)
            {
                return new List<VmFeedBack>();
            }
        }
    }
}