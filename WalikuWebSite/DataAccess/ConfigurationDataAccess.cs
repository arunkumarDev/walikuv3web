﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WalikuWebSite.Entities.DataModels;

namespace WalikuWebSite.DataAccess
{
    public class ConfigurationDataAccess
    {

        public enum ConfigType { Student = 'C', Teacher = 'T', Users = 'U' };

        public static List<ConfigurationData> GetStudentConfigurationFields()
        {
            SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
            DataSet ds = new DataSet();
            try
            {
                sqlCnn.Open();
                SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetConfigurationData, (char)ConfigType.Student), sqlCnn);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds, "StudentConfiguration");

                adapter.Dispose();
                sqlCmd.Dispose();
                sqlCnn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<ConfigurationData> lst = ds.Tables[0].ToList<ConfigurationData>();
                    return lst;
                }
                return new List<ConfigurationData>();
            }
            catch (Exception ex)
            {
                return new List<ConfigurationData>();
            }
        }
    }
}