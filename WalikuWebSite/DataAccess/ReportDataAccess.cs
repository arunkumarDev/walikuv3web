﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WalikuWebSite.Entities.DataModels;

namespace WalikuWebSite.DataAccess
{
    public class ReportDataAccess
    {

        public static VmYearlyReport GetTeacherTearly(int ClassID, int TeacherID)
        {
            VmYearlyReport lst = new VmYearlyReport();

            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                DataSet ds = new DataSet();
                using (SqlCommand sqlCmd = new SqlCommand("Usp_Rpt_TeacherYearlyReport", sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@ClassID", ClassID);
                    sqlCmd.Parameters.AddWithValue("@TeacherID", TeacherID);
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = sqlCmd;
                    adapter.Fill(ds, "Child");
                    adapter.Dispose();
                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lst.GridData = ds.Tables[0].ToList <VmGridData>();
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        lst.MonthList = ds.Tables[0].ToList<VmMonthList>();
                    }

                }
                return new VmYearlyReport();
            }

        }

        //public static List<TrackStudents> GetTrackStudents(string Date)
        //{
        //    SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        sqlCnn.Open();
        //        SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.LoadConfigureWeekdays), sqlCnn);
        //        SqlDataAdapter adapter = new SqlDataAdapter();
        //        adapter.SelectCommand = sqlCmd;
        //        adapter.Fill(ds, "ConfigureWeekdays");

        //        adapter.Dispose();
        //        sqlCmd.Dispose();
        //        sqlCnn.Close();

        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            List<TrackStudents> lst = ds.Tables[0].ToList<TrackStudents>();
        //            return lst;
        //        }
        //        return new List<TrackStudents>();
        //    }
        //    catch (Exception ex)
        //    {
        //        return new List<TrackStudents>();
        //    }
        //}
    }
}