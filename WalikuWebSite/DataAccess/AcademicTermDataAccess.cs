﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WalikuWebSite.Model;

namespace WalikuWebSite.DataAccess
{
    public class AcademicTermDataAccess
    {
        public static string ConnectionString = Common.SqlQueries.CONNECTION_STRING;

        public static List<AcademicTerm> GetAcademicTermDetails(int USerId, int UserType, int SchoolID)
        {
            using (SqlConnection sqlCnn = new SqlConnection(ConnectionString))
            {
                DataSet ds = new DataSet();
                sqlCnn.Open();
                using (
                    SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAcademicTermList, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = USerId;
                    sqlCmd.Parameters.Add(new SqlParameter("@UserType", SqlDbType.Int)).Value = UserType;
                    sqlCmd.Parameters.Add(new SqlParameter("@SchoolID", SqlDbType.Int)).Value = SchoolID;


                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "Academic Term");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<AcademicTerm> lst = ds.Tables[0].ToList<AcademicTerm>();

                        for(var i=0; i < lst.Count;i++)
                        {
                            lst[i].usertype = UserType;
                        }

                        //lst.Add(new AcademicTerm() { usertype = Convert.ToInt32(UserType) });
                        return lst;
                    }

                }
            }
            return new List<AcademicTerm>();

        }

        public static bool DeleteAcademicTermByID(int ID)
        {           
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(ConnectionString))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteAcademicTermByID, ID), sqlCnn);
                    sqlCmd.Connection = sqlCnn;
                    sqlCmd.CommandType = CommandType.Text;
                    sqlCmd.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static void ManageAcademicTerm(AcademicTerm ObjAcademicTerm)
        {
            using (SqlConnection sqlCnn = new SqlConnection(ConnectionString))
            {
                sqlCnn.Open();
                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.ManageAcademTermDetails, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Value = ObjAcademicTerm.ID;
                    sqlCmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime)).Value = ObjAcademicTerm.Start_Date;
                    sqlCmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime)).Value = ObjAcademicTerm.End_Date;
                    sqlCmd.Parameters.Add(new SqlParameter("@SchoolID", SqlDbType.Int)).Value = ObjAcademicTerm.SchoolId;
                    sqlCmd.Parameters.Add(new SqlParameter("@IsCurrentTerm", SqlDbType.Bit)).Value = ObjAcademicTerm.IsCurrentTerm;
                    sqlCmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.Int)).Value = ObjAcademicTerm.CreatedBy;
                    sqlCmd.Parameters.Add(new SqlParameter("@CreatedOn", SqlDbType.DateTime)).Value = ObjAcademicTerm.CreatedOn;
                    sqlCmd.Parameters.Add(new SqlParameter("@AcademicYearName", SqlDbType.VarChar)).Value = ObjAcademicTerm.AcademicYearName;

                    sqlCmd.Connection = sqlCnn;
                    sqlCmd.ExecuteNonQuery();

                }
            }
        }

        public static void UpdateAcademicTerm(AcademicTerm ObjAcademicTerm)
        {
            using (SqlConnection sqlCnn = new SqlConnection(ConnectionString))
            {
                sqlCnn.Open();
                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.UpdateAcademicTerm, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Value = ObjAcademicTerm.ID;
                    sqlCmd.Parameters.Add(new SqlParameter("@StartDate", SqlDbType.DateTime)).Value = ObjAcademicTerm.Start_Date;
                    sqlCmd.Parameters.Add(new SqlParameter("@EndDate", SqlDbType.DateTime)).Value = ObjAcademicTerm.End_Date;
                    sqlCmd.Parameters.Add(new SqlParameter("@SchoolID", SqlDbType.Int)).Value = ObjAcademicTerm.SchoolId;
                    sqlCmd.Parameters.Add(new SqlParameter("@IsCurrentTerm", SqlDbType.Bit)).Value = ObjAcademicTerm.IsCurrentTerm;
                    sqlCmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.Int)).Value = ObjAcademicTerm.CreatedBy;
                    sqlCmd.Parameters.Add(new SqlParameter("@CreatedOn", SqlDbType.DateTime)).Value = ObjAcademicTerm.CreatedOn;
                    sqlCmd.Parameters.Add(new SqlParameter("@AcademicYearName", SqlDbType.VarChar)).Value = ObjAcademicTerm.AcademicYearName;

                    sqlCmd.Connection = sqlCnn;
                    sqlCmd.ExecuteNonQuery();

                }
            }

        }


            public static bool IsCurrentTermExist(int SchoolID)
        {     
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(ConnectionString))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand(string.Format("SELECT COUNT(1) AS cnt FROM AcademicTermDetails WHERE SchoolId={0} AND IsCurrentTerm =1", SchoolID), sqlCnn);
                    sqlCmd.Connection = sqlCnn;
                    sqlCmd.CommandType = CommandType.Text;
                    int count = Convert.ToInt32(sqlCmd.ExecuteScalar());
                    return (count > 0 ? true : false);
                }
            }
            catch (Exception ex)
            {
                return true;
            }

        }

        public static bool IsCurrentTermExist1(int SchoolID,int ID)
        {
            try
            {
                using (SqlConnection sqlCnn = new SqlConnection(ConnectionString))
                {
                    sqlCnn.Open();
                    SqlCommand sqlCmd = new SqlCommand(string.Format("SELECT Id FROM AcademicTermDetails WHERE SchoolId={0} AND IsCurrentTerm =1", SchoolID), sqlCnn);
                    sqlCmd.Connection = sqlCnn;
                    sqlCmd.CommandType = CommandType.Text;
                    int count = Convert.ToInt32(sqlCmd.ExecuteScalar());
                    if(ID == count)
                    {
                        return false;
                    }
                    else
                    {
                        return true;

                    }
                }
            }
            catch (Exception ex)
            {
                return true;
            }

        }

        public static void InsertHolidays(string hname)
        {
            using (SqlConnection sqlCnn = new SqlConnection(ConnectionString))
            {
                sqlCnn.Open();
                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.InsertHolidays, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@Name", SqlDbType.VarChar)).Value = hname;
                    sqlCmd.Parameters.Add(new SqlParameter("@IsActive", SqlDbType.Bit)).Value = 1;


                    sqlCmd.Connection = sqlCnn;
                    sqlCmd.ExecuteNonQuery();

                }
            }
        }
    }
}