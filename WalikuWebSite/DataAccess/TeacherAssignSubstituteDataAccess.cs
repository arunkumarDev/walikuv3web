﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WalikuWebSite.DataAccess
{
    public class TeacherAssignSubstituteDataAccess
    {
       
        public static List<AssignSubstitute> GetTeacherAssignSubstituteList(int UserId)
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                DataSet ds = new DataSet();
                sqlCnn.Open();


                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetTeacherAssignSubstituteList, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = UserId;

                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "TeacherAssignSubstituteList");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<AssignSubstitute> lst = ds.Tables[0].ToList<AssignSubstitute>();
                        return lst;
                    }

                }
            }
            return new List<AssignSubstitute>();

        }
    }
}