﻿using System;

public class VmFeedBack
{
    public int ID { get; set; }

    public string Name { get; set; }

    public string Content { get; set; }

    public DateTime CreatedOn { get; set; }

    public int CreatedBy { get; set; }
}