﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.Entities.DataModels
{
    public class ConfigurationData
    {
        public string FieldID { get; set; }

        public string FieldName { get; set; }

        public bool IsMandatory { get; set; }
        public char ConfigType { get; set; }

        public int OrderBY { get; set; }

        public string DataType { get; set; }

    }
}