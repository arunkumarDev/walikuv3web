﻿using System;

namespace WalikuWebSite.Model
{
    public class AbsenteeReason
    {
        public int ID { get; set; }

        public int ChildID { get; set; }

        public int TeacherID { get; set; }

        public bool IsHealthReason { get; set; }

        public int ReasonID { get; set; }

        public int InformationProvider { get; set; }

        public bool HasPermission { get; set; }

        public int CreatedBy { get; set; }

        public DateTime CreatedOn { get; set; }

        public string Notes { get; set; }

        public int ReasonType { get; set; }

        public int AttendanceMasterID { get;set; }

        public bool IsPresent { get; set; }

        public int ClassroomID { get; set; }
    }
}