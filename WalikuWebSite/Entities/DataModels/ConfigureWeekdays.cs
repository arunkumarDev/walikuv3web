﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Calendar Configure Weekdays
/// </summary>
public class ConfigureWeekdays
{
    public int ID { get; set; }
    public string ConfigureName { get; set; }
    public bool IsMandatory { get; set; }
    public int OrderBy { get; set; }
  
}