﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Services;
using System.Web.UI;
using WalikuWebSite.Model;

namespace WalikuWebSite
{
    public partial class AbsenceManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Common.CommonFunction.IsUserLoggedIn())
                {
                    Response.Redirect("login");
                }
            }
        }


        //[WebMethod]
        //public static void ManageAbsence(string ID, string ChildID, string TeacherID, string AbsenceReason, string ReasonID,
        //    string InformationProvider, string HasPermission,string Notes)
        //{

        //}

        //[WebMethod]
        //public static void ManageAbsence()
        //{

        //}


        [WebMethod]
        public static VmResponseData ManageAbsence(string AttendanceMasterID, string ChildID, string ReasonType, string ReasonID, string TeacherID,
            string InformationProvider, string Notes,bool HasPermission, int IsPresent,DateTime CreatedOn, string ClassroomID)
        {
                    VmResponseData result = new VmResponseData();
            try
            {
                AbsenteeReason ObjAbsenteeReason = new AbsenteeReason();
                ObjAbsenteeReason.AttendanceMasterID = !string.IsNullOrEmpty(AttendanceMasterID) ? Convert.ToInt32(AttendanceMasterID) : 0;
                ObjAbsenteeReason.ChildID = !string.IsNullOrEmpty(ChildID) ? Convert.ToInt32(ChildID) : 0;
                ObjAbsenteeReason.ReasonType = !string.IsNullOrEmpty(ReasonType) ? Convert.ToInt32(ReasonType) : 0;
                ObjAbsenteeReason.ReasonID = !string.IsNullOrEmpty(ReasonID) ? Convert.ToInt32(ReasonID) : 0;
                ObjAbsenteeReason.InformationProvider = !string.IsNullOrEmpty(InformationProvider) ? Convert.ToInt32(InformationProvider) : 0;
                ObjAbsenteeReason.Notes = Notes.Trim();
                ObjAbsenteeReason.HasPermission = HasPermission;
                ObjAbsenteeReason.CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID();
                ObjAbsenteeReason.IsPresent = Convert.ToBoolean(IsPresent);
                ObjAbsenteeReason.CreatedOn = CreatedOn;
                ObjAbsenteeReason.ClassroomID = !string.IsNullOrEmpty(ClassroomID) ? Convert.ToInt32(ClassroomID) : 0;
               


                using (var client = new HttpClient())
                {
                    if (HttpContext.Current.Request.Headers["Lang"] != null && !string.IsNullOrEmpty(HttpContext.Current.Request.Headers["Lang"].ToString().Trim()))
                    {
                        client.DefaultRequestHeaders.Add("Lang", HttpContext.Current.Request.Headers["Lang"].ToString().Trim());                      
                    }

                    client.BaseAddress = new Uri(Common.AppSettings.WebApiUrl);

                    //   ArrayList paramList = new ArrayList();
                    Dictionary<string, object> paramList = new Dictionary<string, object>();
                    paramList.Add("AttendanceMasterID", ObjAbsenteeReason.AttendanceMasterID);
                    paramList.Add("ChildID", ObjAbsenteeReason.ChildID);
                    paramList.Add("ReasonType", ObjAbsenteeReason.ReasonType);
                    paramList.Add("ReasonID", ObjAbsenteeReason.ReasonID);
                    paramList.Add("InformationProvider", ObjAbsenteeReason.InformationProvider);
                    paramList.Add("Notes", ObjAbsenteeReason.Notes);
                    paramList.Add("HasPermission", HasPermission);
                    paramList.Add("CreatedBy", ObjAbsenteeReason.CreatedBy);
                    paramList.Add("IsPresent", ObjAbsenteeReason.IsPresent);
                    paramList.Add("CreatedOn", ObjAbsenteeReason.CreatedOn);
                    paramList.Add("ClassroomID", ObjAbsenteeReason.ClassroomID);

                    //   paramList.Add("data", JsonConvert.SerializeObject(data));

                    string data = JsonConvert.SerializeObject(paramList);

                    HttpResponseMessage response = client.PostAsync("api/Attendance/EditAbsence", new StringContent(data, Encoding.UTF8, "application/json")).Result;
                    if(response.IsSuccessStatusCode)
                    {
                        result.Message = "Success" ;
                    }
                    else
                    {
                        result.Message = "Failed";
                    }
                    return result;
                }
             
            }


           
            catch (Exception ex)
            {

                result.Message = "Failed";
            }
            return result;
        }


    }
    

    


}