﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Feedback.aspx.cs" Inherits="Feedback" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <%--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" />--%>
    <link id="bs-css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet">



    <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_FeedBackManagement %></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <div class="bmd-form-group">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_StartDate %></label>
                        <input type="text" class="form-control date-icon hasDatepicker" id="datepicker1">
                        <i class="fa fa-calendar fa-icons"></i>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="bmd-form-group">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_EndDate %></label>
                        <input type="text" class="form-control date-icon hasDatepicker" id="datepicker2">
                        <i class="fa fa-calendar fa-icons"></i>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchool" class="form-control select-validate">
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                <div class="col-sm-2 mt-15">
                    <button id="btnSearch" type="button" class="btn btn-primary" title=""> <%=Resources.Resource.Lbl_Submit %> </button>
                </div>
              
            </div>
            <div class="page-content">

                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-hover mb-0 " role="grid">
                                    <thead>
                                        <tr>
                                            <th style="width:100px;"><%=Resources.Resource.Lbl_SerialNo %></th>
                                            <th style="width:250px;"><%=Resources.Resource.Lbl_UserName %></th>
                                            <th><%=Resources.Resource.Lbl_Description %></th>
                                            <th style="width:150px;"><%=Resources.Resource.Lbl_CreatedDate %></th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script src="Scripts/bootstrap-datepicker.js"></script>
    <!--
    <script src="Scripts/bootstrap.min.js"></script>

    <script src="http://www.chartjs.org/dist/2.7.1/Chart.bundle.js"></script>
    <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
    -->
    
 <script>document.write("<script type='text/javascript' src='Scripts/Feedback.js?v=" + JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.id.min.js"></script>

    <script>
        $('#datepicker1').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: localStorage.getItem('loglang')

        });
        $('#datepicker2').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy',
            language: localStorage.getItem('loglang')

        });
        var date = new Date();
        $('#datepicker1').datepicker('setDate', date);
         $('#datepicker2').datepicker('setDate', date);
    </script>
</asp:Content>
