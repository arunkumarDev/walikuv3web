﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using Waliku.GoogleTranslator;

namespace WalikuWebSite
{
    /// <summary>
    /// Summary description for TeacherAssignSubstitute
    /// </summary>

    public class TeacherAssignSubstitute : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string strImageName = string.Empty;
            string strTemp = "1";
            string result = "";
            try
            {
                SqlHelper.SaveTeacherAssignSubstitute(new AssignSubstitute()
                {
                    ID = Convert.ToInt32(context.Request.Form["ID"]),
                    AssignFromDate = Convert.ToDateTime(context.Request.Form["AssignFromDate"]),
                    AssignToDate = Convert.ToDateTime(context.Request.Form["AssignToDate"]),
                    SchoolID = Convert.ToInt32(context.Request.Form["SchoolID"]),
                    ClassRoomID = Convert.ToInt32(context.Request.Form["ClassRoomID"]),
                    SubstituteTeacherID = Convert.ToInt32(context.Request.Form["SubstituteTeacherID"]),
                    CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                    CreatedOn = DateTime.Now,
                    IsActive = 1
                });
                result = "Success";
            }
            catch (SqlException SqlEx)
            {
                if (SqlEx.Number == 2601)
                    result = "Teacher Assign Substitute";
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "text/plain";
                strTemp = ex.Message + "---" + ex.InnerException;
                result = strTemp;

            }
            context.Response.Write(result);
        }

        private string CheckEmpty(string input)
        {
            if (string.IsNullOrEmpty(input))
                return "";
            else
                return input.Trim();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}