﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="SchoolAcademicCalendar.aspx.cs" Inherits="SchoolAcademicCalendar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link id="bs-css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet">
  <%--  <link rel="Stylesheet" href=" " "text="" javascript""="" src=""https://code.jquery.com/jquery-1.12.3.js""> " target="_blank">https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />  --%>

    <!-- Add User -->
   <div id="viewprofile" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><%=Resources.Resource.Title_UserProfile %></h4>
            </div>
            <div class="modal-body ">
                <form class="custom-material">
                    
                    <div class="col-md-10 right-content no-pad">
                        <div class="col-md-8">
                            <div class="row mt-20">
                                <div class="col-md-12">
                                    <strong class="pr-15 mb-10" style="display:block;" id="viewName"></strong>
                                    
                                </div>
                            </div>
                            
                            <div class="row mt-10">
                                <div class="col-md-12">
                                    <i class="fa fa-map-marker site-color pr-15" aria-hidden="true"></i><span class="info-content" id="viewAddress"></span>
                                </div>
                            </div>
                            <div class="row mt-10">
                                <div class="col-md-12">
                                    <i class="fa fa-comments site-color pr-15" aria-hidden="true"></i><span class="info-content" id="viewNotes"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="viewprofileMap" style="width: 100%; height:280px"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<div id="SchoolAcademicCalendarModel" class="modal fade custom-modal" role="dialog"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg custom-material" >
        <div class="modal-content" >
           
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Lbl_AddCalendarEvent %></h4>
            </div>
            <div class="modal-body custom-scroll mh-400">
                <form class="custom-material">
                   
                    <div class="col-md-9 pr-0">
                        <div class="row" style="display:none;" > <%--style="display:none;"--%>
                            <div class="col-sm-5" >
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchoolSearch" class="form-control">
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                            
                       
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_CalendarName %></label>
                                    <input type="text" id="txtName" class="form-control input-validate">
                                     <input type="hidden" id="hdnUserID" value="0" />
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_CalendarEventType %></label>
                                    <select id="ddlCalendarEventType" class="form-control select-validate">
                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="bmd-form-group">
                                 <label class="bmd-label-floating"><%=Resources.Resource.Lbl_CalendarFromDate %></label>
                                 <input type="text" class="form-control date-icon hasDatepicker" id="dtCalendarDate">
                                 <i class="fa fa-calendar fa-icons"></i>
                                 </div>
                           </div>
                            <div class="col-md-3">
                                <div class="bmd-form-group">
                                 <label class="bmd-label-floating"><%=Resources.Resource.Lbl_CalendarToDate %></label>
                                 <input type="text" class="form-control date-icon hasDatepicker" id="dtCalendarToDate">
                                 <i class="fa fa-calendar fa-icons"></i>
                                 </div>
                           </div>
                           
                        </div>
                        <div class="row">
                            
                            <div class="col-md-12 ">
                                <div class="bmd-form-group">
                                 <div class="col-md-12 text-right">
                     <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveCalendar"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
                                 </div>
                           </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>


       <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
           

            <div class="row mb-10">

                <div class="col-sm-12">
            <ul class="nav nav-pills" >
              <li><a  href="SchoolAcademicCalendarNew.aspx" ><%=Resources.Resource.Lbl_CalendarView%></a></li>
              <li  class="active"><a  href="SchoolAcademicCalendar.aspx"><%=Resources.Resource.Lbl_CalendarList%></a></li>
              
            </ul>
        </div>
                <div class="col-sm-4 mt-20" >
                    <div id="schoolsearch">
                  <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchoolSearch1" class="form-control">
                        </select>
                        </div>
                      </div>
                <div class="col-md-8 mt-20">
                    <button type="button" class="btn btn-primary pull-right" title="" data-toggle="modal" id="btnAddSchoolAcademicCalendar"><%=Resources.Resource.Lbl_AddCalendarEvent %></button>
                    <button type="button" class="btn btn-primary pull-right" style="display:none"  title="" data-toggle="modal" id="btnDownloadSchoolAcademicCalendar"><%=Resources.Resource.Lbl_DownloadCalendarEvent %></button> 
                    
                    <%--<p class="student-count"><%=Resources.Resource.Lbl_TotalUsers %> : <span id="total">0</span></p>--%>
                </div>
            </div>
            <div class="page-content">
                <div class="data-table-wrapper sort-table">
                    <table id="dataTableUser" class="table table-hover mb-0 user-tbl" style="width:100%">
                        <thead>
                            <tr>
                                <th><span><%=Resources.Resource.Lbl_SerialNo %></span></th>   
                                <th><span><%=Resources.Resource.Lbl_CalendarName %></span></th>
                                <th><%=Resources.Resource.Lbl_CalendarEventType %></th>
                                <th><%=Resources.Resource.Lbl_CalendarFromDate %></th>
                                <th><%=Resources.Resource.Lbl_CalendarToDate %></th>
                                <th><%=Resources.Resource.Lbl_Action%></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <script src="Scripts/bootstrap-datepicker.js"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/SchoolAcademicCalendar.js?v=" + JsVerion + "'><\/script>");</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
        
    <script>
        var Lbl_AddCalendarEvent = '<%=Resources.Resource.Lbl_AddCalendarEvent %>'
        var Lbl_EditCalendarEvent = '<%=Resources.Resource.Lbl_EditCalendarEvent %>'

        $('#dtCalendarDate').datepicker({
            autoclose: true,
            format: 'mm/dd/yyyy'
        });
        var date = new Date(); //'2018-01-01'
        $('#dtCalendarDate').datepicker('setDate', date);

        $('#dtCalendarToDate').datepicker({
            autoclose: true,
            format: 'mm/dd/yyyy'
        });
        var date = new Date(); //'2018-01-01'
        $('#dtCalendarToDate').datepicker('setDate', date);

    </script>

</asp:Content>

