﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.master"  CodeBehind="UserRoleMapping.aspx.cs" Inherits="WalikuWebSite.UserRoleMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <!-- Modal -->
    <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_UsrMap %></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 mb-5">
                    <div class="search-form">
                    </div>
                </div>
                <div class="col-md-9 mb-5 text-right">
                    <button type="button" class="btn btn-primary " title="" data-toggle="modal" id="btnAssignAdmin"><%=Resources.Resource.Title_UsrMap %></button>
                </div>
            </div>
             <div class="page-content">
                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTableUser" class="table table-hover mb-0 " role="grid">
                                    <thead>
                                        <tr>
                                            <th style="width:60px;"><%=Resources.Resource.Lbl_SerialNo %></th>
                                            <th style="width:300px;"><%=Resources.Resource.Lbl_SchoolName %></th>
                                            <th><%=Resources.Resource.Lbl_AdminName %></th>
                                            <th style="width:100px;"><%=Resources.Resource.Lbl_Action %></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </main>

        <!-- Add User -->
<div id="Assign-admin" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md custom-material">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Title_UsrMap %></h4>
            </div>
            <div class="modal-body">
                <form class="custom-material">
                    <div class="col-md-12 pr-0">
                       
                             <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectUser %> </label>
                                     <select id="ddlUser" class="form-control select-validate">                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>

                             <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectRole %> </label>
                                     <select id="ddlRole" class="form-control select-validate">                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectDistrict %> </label>
                                     <select id="ddlDistrict" class="form-control select-validate">                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="bmd-form-group is-filled">

                                     <input type="hidden" id="hdnAdminID" value="0" />
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SelectSchool %> </label>
                                     <select id="ddlSchool" class="form-control select-validate">                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                           
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal"> <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button id="btnSaveAdmin" type="button" class="btn btn-primary mr-15" title=""> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
            </div>
        </div>
    </div>
</div>

     <script>document.write("<script type='text/javascript' src='Scripts/UserRoleMapping.js?v=" + JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>


     <script>
         var Title_AssignAdmin = '<%=Resources.Resource.Title_UsrMap %>'
      
     </script>

    </asp:Content>