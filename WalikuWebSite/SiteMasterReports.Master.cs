﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalikuWebSite
{
    public partial class SiteMasterReports : System.Web.UI.MasterPage
    {
        public string DefaultLanguage = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            SetLanguage(GetLanguageFromCookie());
        }


        private string GetLanguageFromCookie()
        {
            string Language = Common.CommonFunction.GetUserLanguageFromCookie();

            if (Language != "")
            {
                string[] str = Language.Split('-');
                DefaultLanguage = (str.Length == 2) ? DefaultLanguage = str[0] : "en";
            }
            else
            {
                Language = "en-us";
            }
            Session["Language"] = Language;

            Session["CurrentCulture"] = DefaultLanguage;// Common.CommonFunction.GetCurrentCulture();

            return Language;
        }

        private void SetLanguage(string Language)
        {
            if (Language != null)
            {
                string Lang = Language.ToString();
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Lang);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Lang);
                Page.Culture = Lang;
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
                Page.Culture = "en-us";
            }
        }
    }
}