﻿var language = {
    "en-us": {
        "Lbl_SelectLanguage": "Select Language", "Lbl_UerName": "UserName", "Lbl_Password": "Password", "Lbl_RememberMe": "Remember Me","Lbl_Login":"Login"},
    "id-ID": {
        "Lbl_SelectLanguage": "Pilih bahasa", "Lbl_UerName": "Nama pengguna", "Lbl_Password": "Kata sandi", "Lbl_RememberMe": "Ingat saya", "Lbl_Login": "Masuk"
    }
};



function ValidateForm() {
    var FieldValue = $.trim($('#txtUserName').val());
    if (IsNullOrEmpty(FieldValue)) {
        IsInputValid('#txtUserName', false);
        return false;
    }
    else {
        $('#txtUserName').parent().removeClass("invalid");
    }

    FieldValue = $.trim($('#txtPassword').val());
    if (IsNullOrEmpty(FieldValue)) {
        IsInputValid('#txtPassword', false);
        return false;
    }
    else {
        $('#txtPassword').parent().removeClass("invalid");
    }

    return true;
}



$(document).ready(function () {
    //localStorage.removeItem('NotifyText');
    //localStorage.removeItem('NotifyDate');
    //localStorage.removeItem('notificationCount');

    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    })

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val()))  {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    })

    var lang;
    var userrole = '';
    $("#ddlLanguage").change(function () {
        var selectedValue = $(this).val();
        lang = language[selectedValue];
        ChangeLangulage(lang);
    });
})

function ChangeLangulage(lang) {
    $('#Lbl_SelectLanguage').text(lang.Lbl_SelectLanguage);
    $('#Lbl_UerName').text(lang.Lbl_UerName);
    $('#Lbl_Password').text(lang.Lbl_Password);
    $('#Lbl_RememberMe').text(lang.Lbl_RememberMe);
    $('#btnSignIn').text(lang.Lbl_Login);
}

$("#btnSignIn").bind("click", function () {
    if (!ValidateForm()) {
        return;
    }
    submitLogin();
});

$('.input-validate').keypress(function (e) {
    if (e.which == 13) {
        if (!ValidateForm()) {
            return;
        }
        submitLogin();
        e.preventDefault();
        e.stopPropagation();
        return false;    
    }
});


function submitLogin() {
    localStorage.setItem('loglang', $("#ddlLanguage").val());
    $.ajax({
        type: "POST",

        url: "/Login.aspx/ValidateUser",
        data: '{UserName: "' + $("#txtUserName").val() + '" , Password: "' + $("#txtPassword").val() + '", Language: "' + $("#ddlLanguage").val() + '" , RememberMe: ' + $("#chkRemember").prop('checked') + '}',

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.d) {

                
               GetuserType();

               // window.location = "Report-schoolsummary";
            }
            else {
                alert("Invalid UserName/password");
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function GetUserRoles() {
    var userinfo = getCookie("UserInfo");
    var username = userinfo.split('&')[0].split('=')[1]
    console.log(userinfo);
    $.ajax({
        type: "GET",
        url: "https://waliku-dev-api.azurewebsites.net/api/permission/getuserroles/" + username,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: { 'Authorization': "Bearer " + '59wDnOw8fhg7_FJy4kr1QxE93OhKZSFAAVqP3ph8Krmj7PJSkjKr3EAS7V_63pC6CJrtjEtTab_0mDV_jdFxvfyHOiZws21bYatyo9F1Vu7fBMu4ab4sOY28WCtCajt_etqo6MAilGAPgjwfO5BGInafAMaWw1lN9VcIyhFEPX9WOU82-tBP02V-Aiq4fr4b5_qtUUjaGGxHk0LkMz1RAIgte5ZFuytLGS3M2l_Tn9OSraoknLYqy6XCszOjcNPbeS1OFli8GBW_P7vJCWVZGNffj5XjdkR_dEbjBPFSsiL88TeYzZZND6O6O8_z1ImMjyoYnNP4YVnkiXUemSU7zUty7EBe0LQxsYebPZlFakzlGKlyWkuNvlgi-NuhDqiJMMduoB_OEXbYsjmgGr5yfLnz91KtaXuOxGMe0a3UdPeQ-LgmAXhVeTSmRA4Fem0htecvRP0gfYh2yNf96iw8QPtaY0hky1SeYVAS-6SPOH3SFJ-6h3t8LbPuwGreuRoEOiy8ghczvzjvRVzvCPwzMQ' },

        success: function (data) {
            console.log(data);
            if (data.length > 1) {
                var appenddata;
                $.each(data, function (i, item) {
                    appenddata += "<option value = '" + item.Name + "'>" + item.Name + " </option>";
                });

                $('#ddlPopupRole').html(appenddata);
                $('#LgRolePopup').modal('show');
            }
            if (data.length == 1) {
            
                var userrole1 = $("#ddlPopupRole").val();
                if (userrole1 == null) {
                    userrole = data[0].Name;
                }
                else {
                    userrole = userrole1;
                }

                localStorage.setItem('usrRoleName', userrole);
                LoadDefaultUrl();
               // window.location = "Report-schoolsummary";
            }
            //if (data.d) {
            //    if (data.d != 2)
            //        window.location = "Report-schoolsummary";
            //}
            //else {
            //    alert("Invalid UserName/password");
            //}
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

 function GetuserType() {
    $.ajax({
        type: "POST",
        url: "/Login.aspx/GetuserType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.d) {
                localStorage.setItem('Usertypefrimport', data.d);
            }
                GetUserRoles();
            //    if (data.d != 2)
            //      window.location = "Report-schoolsummary";
            //}
            //else {
            //    alert("Invalid UserName/password");
            //}
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


$('#btnLoginOk').click(function () {
    debugger;


     userrole = $("#ddlPopupRole").val();  
   
    localStorage.setItem('usrRoleName', userrole);

    LoadDefaultUrl();

});

function LoadDefaultUrl() {
    debugger;
    var userinfo = getCookie("UserInfo");
    var username = userinfo.split('&')[0].split('=')[1]
    console.log(userinfo);
  

    $.ajax({
        type: "GET",
        url: "https://waliku-dev-api.azurewebsites.net/api/permission/userrolecomponents/" + username + '/' + userrole,
        //data: '{SelectedDate: "' + inputDate + '" ,SchoolId: "' + Schoolid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: { 'Authorization': "Bearer " + '59wDnOw8fhg7_FJy4kr1QxE93OhKZSFAAVqP3ph8Krmj7PJSkjKr3EAS7V_63pC6CJrtjEtTab_0mDV_jdFxvfyHOiZws21bYatyo9F1Vu7fBMu4ab4sOY28WCtCajt_etqo6MAilGAPgjwfO5BGInafAMaWw1lN9VcIyhFEPX9WOU82-tBP02V-Aiq4fr4b5_qtUUjaGGxHk0LkMz1RAIgte5ZFuytLGS3M2l_Tn9OSraoknLYqy6XCszOjcNPbeS1OFli8GBW_P7vJCWVZGNffj5XjdkR_dEbjBPFSsiL88TeYzZZND6O6O8_z1ImMjyoYnNP4YVnkiXUemSU7zUty7EBe0LQxsYebPZlFakzlGKlyWkuNvlgi-NuhDqiJMMduoB_OEXbYsjmgGr5yfLnz91KtaXuOxGMe0a3UdPeQ-LgmAXhVeTSmRA4Fem0htecvRP0gfYh2yNf96iw8QPtaY0hky1SeYVAS-6SPOH3SFJ-6h3t8LbPuwGreuRoEOiy8ghczvzjvRVzvCPwzMQ' },
        success: function (data) {
            if (data.length > 0) {
                window.location = data[1].Path;
            }
            else if (usertype == 4)
            {
                $.ajax({
                    type: "GET",
                    url: "https://waliku-dev-api.azurewebsites.net/api/permission/allrolematrix",
                    //data: '{SelectedDate: "' + inputDate + '" ,SchoolId: "' + Schoolid + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    headers: { 'Authorization': "Bearer " + '59wDnOw8fhg7_FJy4kr1QxE93OhKZSFAAVqP3ph8Krmj7PJSkjKr3EAS7V_63pC6CJrtjEtTab_0mDV_jdFxvfyHOiZws21bYatyo9F1Vu7fBMu4ab4sOY28WCtCajt_etqo6MAilGAPgjwfO5BGInafAMaWw1lN9VcIyhFEPX9WOU82-tBP02V-Aiq4fr4b5_qtUUjaGGxHk0LkMz1RAIgte5ZFuytLGS3M2l_Tn9OSraoknLYqy6XCszOjcNPbeS1OFli8GBW_P7vJCWVZGNffj5XjdkR_dEbjBPFSsiL88TeYzZZND6O6O8_z1ImMjyoYnNP4YVnkiXUemSU7zUty7EBe0LQxsYebPZlFakzlGKlyWkuNvlgi-NuhDqiJMMduoB_OEXbYsjmgGr5yfLnz91KtaXuOxGMe0a3UdPeQ-LgmAXhVeTSmRA4Fem0htecvRP0gfYh2yNf96iw8QPtaY0hky1SeYVAS-6SPOH3SFJ-6h3t8LbPuwGreuRoEOiy8ghczvzjvRVzvCPwzMQ' },
                    success: function (data) {
                        $("#myUl").empty();
                        window.location = data.Components[1].Path;
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
            else {
                $('#LgRolePopup').modal('hide');
                alert("Please contact Super Admin for access the application");
                // location.reload();

            }

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}