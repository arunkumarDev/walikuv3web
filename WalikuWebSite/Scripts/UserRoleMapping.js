﻿
$(document).ready(function () {
    InitEvents();
     LoadSchool();
    LoadNotifications();
    LoadRoles();
    LoadDistricts();
    LoadUsers();
});

function InitEvents() {
    $('#btnAssignAdmin').click(function () {
        $('#Assign-admin').modal('show');
    });
}

function LoadSchool() {

    $.ajax({
        type: "POST",
        url: "AssignAdmin.aspx/GetSchoolListForUser",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        headers: { 'Authorization': "Bearer " + '59wDnOw8fhg7_FJy4kr1QxE93OhKZSFAAVqP3ph8Krmj7PJSkjKr3EAS7V_63pC6CJrtjEtTab_0mDV_jdFxvfyHOiZws21bYatyo9F1Vu7fBMu4ab4sOY28WCtCajt_etqo6MAilGAPgjwfO5BGInafAMaWw1lN9VcIyhFEPX9WOU82-tBP02V-Aiq4fr4b5_qtUUjaGGxHk0LkMz1RAIgte5ZFuytLGS3M2l_Tn9OSraoknLYqy6XCszOjcNPbeS1OFli8GBW_P7vJCWVZGNffj5XjdkR_dEbjBPFSsiL88TeYzZZND6O6O8_z1ImMjyoYnNP4YVnkiXUemSU7zUty7EBe0LQxsYebPZlFakzlGKlyWkuNvlgi-NuhDqiJMMduoB_OEXbYsjmgGr5yfLnz91KtaXuOxGMe0a3UdPeQ-LgmAXhVeTSmRA4Fem0htecvRP0gfYh2yNf96iw8QPtaY0hky1SeYVAS-6SPOH3SFJ-6h3t8LbPuwGreuRoEOiy8ghczvzjvRVzvCPwzMQ' },
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--Select School--</option>";
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

            });
            $('#ddlSchool').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function LoadRoles() {

    $.ajax({
        type: "GET",
        url: 'https://waliku-dev-api.azurewebsites.net/api/common/getroles',
     //   data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        headers: { 'Authorization': "Bearer " + '59wDnOw8fhg7_FJy4kr1QxE93OhKZSFAAVqP3ph8Krmj7PJSkjKr3EAS7V_63pC6CJrtjEtTab_0mDV_jdFxvfyHOiZws21bYatyo9F1Vu7fBMu4ab4sOY28WCtCajt_etqo6MAilGAPgjwfO5BGInafAMaWw1lN9VcIyhFEPX9WOU82-tBP02V-Aiq4fr4b5_qtUUjaGGxHk0LkMz1RAIgte5ZFuytLGS3M2l_Tn9OSraoknLYqy6XCszOjcNPbeS1OFli8GBW_P7vJCWVZGNffj5XjdkR_dEbjBPFSsiL88TeYzZZND6O6O8_z1ImMjyoYnNP4YVnkiXUemSU7zUty7EBe0LQxsYebPZlFakzlGKlyWkuNvlgi-NuhDqiJMMduoB_OEXbYsjmgGr5yfLnz91KtaXuOxGMe0a3UdPeQ-LgmAXhVeTSmRA4Fem0htecvRP0gfYh2yNf96iw8QPtaY0hky1SeYVAS-6SPOH3SFJ-6h3t8LbPuwGreuRoEOiy8ghczvzjvRVzvCPwzMQ' },
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--Select Role--</option>";
            $.each(data, function (i, item) {
                appenddata += "<option value = '" + item.Id + "'>" + item.Name + " </option>";

            });
            $('#ddlRole').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function LoadDistricts() {

    $.ajax({
        type: "GET",
        url: 'https://waliku-dev-api.azurewebsites.net/api/common/getdistricts',
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        headers: { 'Authorization': "Bearer " + '59wDnOw8fhg7_FJy4kr1QxE93OhKZSFAAVqP3ph8Krmj7PJSkjKr3EAS7V_63pC6CJrtjEtTab_0mDV_jdFxvfyHOiZws21bYatyo9F1Vu7fBMu4ab4sOY28WCtCajt_etqo6MAilGAPgjwfO5BGInafAMaWw1lN9VcIyhFEPX9WOU82-tBP02V-Aiq4fr4b5_qtUUjaGGxHk0LkMz1RAIgte5ZFuytLGS3M2l_Tn9OSraoknLYqy6XCszOjcNPbeS1OFli8GBW_P7vJCWVZGNffj5XjdkR_dEbjBPFSsiL88TeYzZZND6O6O8_z1ImMjyoYnNP4YVnkiXUemSU7zUty7EBe0LQxsYebPZlFakzlGKlyWkuNvlgi-NuhDqiJMMduoB_OEXbYsjmgGr5yfLnz91KtaXuOxGMe0a3UdPeQ-LgmAXhVeTSmRA4Fem0htecvRP0gfYh2yNf96iw8QPtaY0hky1SeYVAS-6SPOH3SFJ-6h3t8LbPuwGreuRoEOiy8ghczvzjvRVzvCPwzMQ' },
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--Select District--</option>";
            $.each(data, function (i, item) {
                appenddata += "<option value = '" + item.Id + "'>" + item.Name + " </option>";

            });
            $('#ddlDistrict').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function LoadUsers() {

    $.ajax({
        type: "GET",
        url: 'https://waliku-dev-api.azurewebsites.net/api/user/allusers',
       
        contentType: "application/json; charset=utf-8",
        headers: { 'Authorization': "Bearer " + '59wDnOw8fhg7_FJy4kr1QxE93OhKZSFAAVqP3ph8Krmj7PJSkjKr3EAS7V_63pC6CJrtjEtTab_0mDV_jdFxvfyHOiZws21bYatyo9F1Vu7fBMu4ab4sOY28WCtCajt_etqo6MAilGAPgjwfO5BGInafAMaWw1lN9VcIyhFEPX9WOU82-tBP02V-Aiq4fr4b5_qtUUjaGGxHk0LkMz1RAIgte5ZFuytLGS3M2l_Tn9OSraoknLYqy6XCszOjcNPbeS1OFli8GBW_P7vJCWVZGNffj5XjdkR_dEbjBPFSsiL88TeYzZZND6O6O8_z1ImMjyoYnNP4YVnkiXUemSU7zUty7EBe0LQxsYebPZlFakzlGKlyWkuNvlgi-NuhDqiJMMduoB_OEXbYsjmgGr5yfLnz91KtaXuOxGMe0a3UdPeQ-LgmAXhVeTSmRA4Fem0htecvRP0gfYh2yNf96iw8QPtaY0hky1SeYVAS-6SPOH3SFJ-6h3t8LbPuwGreuRoEOiy8ghczvzjvRVzvCPwzMQ' },
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--Select User--</option>";
            $.each(data, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";

            });
            $('#ddlUser').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}