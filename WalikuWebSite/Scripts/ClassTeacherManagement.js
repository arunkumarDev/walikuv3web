﻿$(window).load(function () {
    LoadClassTeacher();
    LoadNotifications();
    LoadClasses();
    LoadTeachers();
    //LoadNotifications();
});
$("#btnSaveClassTeacher").bind("click", function () {
    if (!ValidateClassTeacher()) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "ClassTeacherManagement.aspx/ManageClassTeacher",
        data: '{ID: "' + $("#hdnClassTeacherID").val() + '",ClassRoomID: "' + $("#ddlClassName").val() + '",TeacherID: "' + $("#ddlTeacherName").val() + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadClassTeacher();
            ////ClearControl();
            if (data.d.Message == 'Success')
                window.location.href = "ClassTeacherManagement.aspx";
            else
                alert(data.d.Message)
        },
        failure: function (response) {
            alert(response.d);
        }
    });
});


function ValidateClassTeacher() {
    
    if ($.trim($("#ddlClassName").val()) === "") {
        IsInputValid("#ddlClassName", false);
        return false;
    }
    
    if ($.trim($("#ddlTeacherName").val()) === "") {
        IsInputValid("#ddlTeacherName", false);
        return false;
    }

    return true;
}

$(document).ready(function () {
    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });
});

function LoadClassTeacher() {
    teacherList = [];
    //$('#dataTable').dataTable({
    //    "bDestroy": true
    //}).fnDestroy();
    //$('#dataTable tbody').empty();
    $.ajax({
        type: "POST",
        url: "ClassTeacherManagement.aspx/GetAllClassTeacher",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;

            $.each(data.d, function (i, item) {
                teacherList[item.ID] = item;
                $('<tr id="tr' + item.ID + '">').html("<td>" + (++slNo) + "</td>" +
                    "<td data-classID =" + item.ClassRoomID + "> " + item.ClassName + "</td >" +
                    "<td data-teacherID =" + item.TeacherID + ">" + item.Name + "</td>" +
                    "<td><a onclick='return OpenModalPopup(" + item.ID + ");' class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></a> <a href='javascript:;' data-toggle='modal' onclick='return DeleteClassTeacher(" + item.ID + ");'><i class='fa fa-trash site-color' aria-hidden='true'></i></a></td>").appendTo('#dataTable tbody');
             
            });
            InitializeDataTable('dataTable');

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function DeleteClassTeacher(ID) {
    if (!confirm("Are you sure want to delete?")) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "ClassTeacherManagement.aspx/DeleteClassTeacherByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadClassTeacher();
            window.location.href = "ClassTeacherManagement.aspx";

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}
function OpenModalPopup(ctrl) {
    var teacherData = teacherList[ctrl];
    if (teacherData != undefined) {
        $("#hdnClassTeacherID").val(teacherData.ID);
        $("#ddlClassName").val(teacherData.ClassRoomID);
        $("#ddlTeacherName").val(teacherData.TeacherID);
        $('#txtHeader').text("Edit Assign Mobile Login");


        $('#AssignTeacherModel').modal('show');
    }
   


}
function ClearControl() {
    $("#ddlClassName").val(0);
    $("#ddlTeacherName").val(0);
    $("#hdnClassTeacherID").val('0');
}
function LoadClasses() {
    var schoolid = localStorage.getItem('StaffSchoolId');
    $.ajax({
        type: "POST",
     //   url: "ClassTeacherManagement.aspx/GetAllClasses",
     //   data: '{PageIndex: "' + 1 + '"  }',
        url: "ChildrenManagement.aspx/GetAllClassRoomForMe",
        data: '{SchoolID: "' + schoolid + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--" + Lbl_SelectClass +"--</option>";
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";

            });
            $('#ddlClassName').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function LoadTeachers() {

    $.ajax({
        type: "POST",
        url: "ClassTeacherManagement.aspx/GetMyTeacherList",
       // data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--"+Lbl_SelectTeacher+"--</option>";
            $.each(data.d, function (i, item) {
                if (item.UserType == 2) {
                    appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";
                }

            });
            $('#ddlTeacherName').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}