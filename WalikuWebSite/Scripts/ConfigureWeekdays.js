﻿$(document).ready(function () {

    LoadConfigureWeekdays();
    LoadSchool();
});
var selectedFields = [];
var masterConfig = [];

function checkboxClick(curElem) {
    if (curElem.checked)
        selectedFields.push(curElem.value);
    else {
        var newFields = [];
        selectedFields.forEach(function (val, ind) {
            if (val != curElem.value)
                newFields.push(val);
        });
        selectedFields = newFields;
    }
}
$('#btnSave').click(function (event) {

   // if ( selectedFields.length == 0) {
    selectedFields.forEach(function (val, ind) {
    if (selectedFields.length == 0) // && val.IsMandatory
    {
        break;
    }   
    else {
        selectedFields.push(val.ID);
    }
                
       });
    //}
    LoadConfigureWeekdays();
});

function LoadConfigureWeekdays() {
    $.ajax({
        type: "POST",
        url: "CalendarConfigureWeekdays.aspx/LoadConfigureWeekdays",
        contentType: "application/json; charset=utf-8",
        //data: '{ID: "' + $("#ddlSchool").val() + '"}',
        dataType: "json",
        success: function (data) {
            var checkboxList = '';
            masterConfig = data.d;
            $.each(masterConfig, function (ind, val) {
                if (val.IsMandatory) {
                    selectedFields.push(val.ID);
                    checkboxList += '<div class="md-checkbox row col-md-1 "><label> <input checked="true" value="' + val.ID + '" onClick="checkboxClick(this)" type="checkbox" id="chk_' + val.ID + '"></input>' +
                        '<span class="md-checkbox-material">' + val.ConfigureName + '</span></label></div>';
                }
                else {
                    checkboxList += '<div class="md-checkbox row col-md-1 "><label> <input value="' + val.ID + '" onClick="checkboxClick(this)" type="checkbox" id="chk_' + val.ID + '"></input>' +
                        '<span class="md-checkbox-material">' + val.ConfigureName + '</span></label></div>';
                }
            });
            $('#configureWeekdaysGroup').append(checkboxList);
        }
    });

}
    
function LoadSchool() {
    AjaxCall("CalendarConfigureWeekdays.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";
    });
    $('#ddlSchool').html(appenddata);

}

function FailureCallBack() {

}
