﻿$(document).ready(function () {
    LoadSchool();
    LoadNotifications();
    $("#txtImportFile").change(function () {
        readURL(this);
    });
     let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
        document.getElementById("Engtext").style.display = "block"; 
        document.getElementById("Idtext").style.display = "none";
        document.getElementById("EngFile").style.display = "block"; 
        document.getElementById("IdFile").style.display = "none"; 
        $('#Filename').text("No file chosen");
        $('#Filename1').text("");
    }
    else {
        document.getElementById("Idtext").style.display = "block"; 
        document.getElementById("Engtext").style.display = "none";
        document.getElementById("EngFile").style.display = "none";
        document.getElementById("IdFile").style.display = "block"; 
        $('#Filename1').text("Belum ada dokumen");
        $('#Filename').text("");

    }
    var impid = localStorage.getItem('Usertypefrimport');
    if (impid == 4)
        document.getElementById("imp-school").style.display = "block";
    else
        document.getElementById("imp-school").style.display = "none";

    $('#btnImportData').click(function (event) {
        let lang = localStorage.getItem('loglang');
       

        //var url = "../ImportStudentData.aspx/StudentsDataFileImport";
        $('.loaderDiv').show();
        var formData = new FormData();
        var file = $('#txtImportFile')[0].files[0];
        formData.append("name", file.name);
        formData.append("size", file.size);
        formData.append("type", file.type);
        formData.append("file", file);
        formData.append("schid", parseInt($('#ddlSchoolSearch').val()));

        debugger;
        $.ajax({
            url: "ImportStudentData.ashx",
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                debugger;
                if (data == 'Error1') {
                    $('#comp-Fields').modal('show');
                    $('.loaderDiv').hide();
                   
                }
                else if (data == 'Error2') {

                    $('#Nisn-Existing').modal('show');
                    $('.loaderDiv').hide();
                }
                else if (data == 'Error3')
                {
                    $('#Data-Existing').modal('show');
                    $('.loaderDiv').hide();
                }
                else if (data == 'Error4') {
                    $('#File-Error').modal('show');
                    $('.loaderDiv').hide();
                }
                else if (data == 'Error') {
                   
                    $('#comp-Fields').modal('show');
                    $('.loaderDiv').hide();
                }
                else if (data == 'ClassError') {

                    $('#class-Match').modal('show');
                    $('.loaderDiv').hide();
                }
                
                else {
                    $('.loaderDiv').hide();
                    $('#alertAction').modal('show');
                   
                }
                $('#Filename').text('');
                $('#Filename1').text('');
            }
        });
    });

    $('#btnclosePopup').click(function (event) {
        $('#comp-Fields').modal('hide');
        location.reload();
    });
    $('#btnclosePopup1').click(function (event) {
        $('#Nisn-Existing').modal('hide');
        location.reload();
    });
    $('#btnclosePopup2').click(function (event) {
        $('#alertAction').modal('hide');
        location.reload();
    });
    $('#btnclosePopup3').click(function (event) {
        $('#Data-Existing').modal('hide');
        location.reload();
    });
    $('#btnclosePopup4').click(function (event) {
        $('#Data-Existing').modal('hide');
        location.reload();
    });

    
  
});

function readURL(input) {
    if (input.files && input.files[0]) {
        if (input.files[0].name.indexOf('.xls') == -1 || input.files[0].name.indexOf('.xlsx') == -1) {
            alert("Please Import .xls or .xlsx files only!");
            $('#Filename').text('');
            $('#Filename1').text('');
            return;
        }
        else {
            var reader = new FileReader();
            let lang = localStorage.getItem('loglang');
            if (lang == "en-us") {
                $('#Filename').text(input.files[0].name);
            }
            else {
                $('#Filename1').text(input.files[0].name);

            }
            //reader.onload = function (e) {
            //    //alert(e.target.result);
            //    if (e.target.result.files[0].name.indexOf('.xls') != -1 || e.target.result.files[0].name.indexOf('.xlsx') != -1) {
            //        alert("Please Import .xls or .xlsx files only!");
            //    }
            //}

            reader.readAsDataURL(input.files[0]);
        }
    }
}

function LoadSchool() {
    AjaxCall("SchoolAcademicCalendar.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

    });
    $('#ddlSchoolSearch').html(appenddata);
  
}

function FailureCallBack(data) {
    alert(data.d);
}