﻿$(document).ready(function () {
    $("#txtExportFile").change(function () {
        readURL(this);
    });
    $('#btnExportData').click(function (event) {
        $('.loaderDiv').show();
        var formData = new FormData();
        formData.append("FilePath", "/ImportExport/Template/ChildrenExport.xlsx");
        formData.append("ExportType", $('#ddlExportType').val());

        $.ajax({
            url: "ExportStudentData.ashx",
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                $('.loaderDiv').hide();
                $('#exportOutput').show();

            }
        });
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //alert(e.target.result);
            if (e.target.result.files[0].name.indexOf('.xls') != -1 || e.target.result.files[0].name.indexOf('.xlsx') != -1) {
                alert("Please Import .xls or .xlsx files only!");
            }
        }

        reader.readAsDataURL(input.files[0]);
    }
}
