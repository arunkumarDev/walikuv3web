﻿$(document).ready(function () {
    InitEvents();
    LoadNotifications();
})


function InitEvents() { 
    $("#btnCounter").bind("click", function () {
        Counterpopup();
        $('#view-count').modal('show');
    });
}


function Counterpopup() {
    $.ajax({
        type: "POST",
        url: "FirstAid.aspx/FirstAidCounterList",
      //  data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            let SerialNum = 0;
            $.each(data.d, function (i, item) {
                $('<tr>').html("<td>" + (++SerialNum) + "</td>" +
                    "<td>" + (item.Name + ' ' + item.Surname) + "</td >" +
                    "<td>" + item.SchoolName + "</td>" +
                    "<td><span style='display:none'>" + FormatDate(item.VisitedOn,3)+"</span>" + FormatDate(item.VisitedOn) + "</td>"
                    // "<td>" + item.DescriptionIndian + "</td>" +
                ).appendTo('#CounterTable tbody');
            });
            InitializeDataTable('CounterTable');
        },
        failure: function (response) {
          //  alert(response.d);
        }
    });
}





