﻿


$(document).ready(function () {

    LoadNotifications();
    LoadAuthMatrixList();

});

var checkedArray = [];
var ComponentsList = [];
var RolesList = [];
var permissionList = [];
var uniqueArray = [];



function checkboxClick(curElem, CmpId, RoleId) {

    var roleIndex = RolesList.findIndex(role => role.Id1 === RoleId);
    if (curElem.checked == false) {
        checkedArray.filter(o => {

            if (CmpId == o.ComponentId && RolesList[roleIndex].Id == o.RoleId) {
                o.Allow = curElem.checked;
            }
        });
    }
    else {
        var obj = {
            "ComponentId": CmpId,
            "RoleId": RolesList[roleIndex].Id,
            "Allow": curElem.checked,
            "Readonly": false
        }
        checkedArray.push(obj);
        console.log(checkedArray);
    }
}

function LoadAuthMatrixList() {

    teacherAttendanceList = [];
    $('#dataTableAuthMatrixList').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();
    $('#dataTableAuthMatrixList tbody').empty();

    $.ajax({
        type: "GET",
        url: "https://waliku-dev-api.azurewebsites.net/api/permission/allrolematrix",
        //data: '{SelectedDate: "' + inputDate + '" ,SchoolId: "' + Schoolid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: { 'Authorization': "Bearer " + localStorage.getItem('access_token') },
        success: function (data) {

            ComponentsList = data.Components;
            RolesList = data.Roles;
            permissionList = data.Permissions;
            var SerialNo = 0;
            // $("#tblProviders").html("");
            var removeArray = ["Community Worker", "Super Admin"]

            for (var i = 0; i < RolesList.length; i++) {
                for (var j = 0; j < removeArray.length; j++) {
                    if (RolesList[i].Name == removeArray[j]) {
                        RolesList.splice(i, 1);
                        i--;
                    }
            }
        };

        $("#theadList").empty();
        $("#tbodyList").empty();

        var columnKeys = RolesList;

        var newRowContent = '<tr> <th style="position: relative; vertical-align: middle;" rowspan="3"><span>Page</span></th>  </tr> ' +
            '<tr> <th class="text-center" colspan="5">Waliku Roles</th> </tr> ';
        for(var j = 0; j<columnKeys.length; j++) {
        newRowContent += "<th>" + columnKeys[j].Name + "</th>";
    }
    //break;
    //newRowContent += "</tr>"
    $("#tblProviders thead").append(newRowContent);


    var columnKeys1 = ComponentsList;
    var columnKeys2 = RolesList;

    for (var m = 0; m < columnKeys2.length; m++) {
        columnKeys2[m].Id1 = m;
    }

    var newRowContent = "";
    var a, b;
            for (var j = 0; j < columnKeys1.length; j++) {
                   if (columnKeys1[j].MenuId == 0) {
                newRowContent += "<tr><td>" + columnKeys1[j].Name + "</td>";
                for (var k = 0; k < columnKeys2.length; k++) {
                    for (var m = 0; m < permissionList.length; m++) {

                        if (permissionList[m].ComponentId == columnKeys1[j].Id && columnKeys2[k].Id == permissionList[m].RoleId && permissionList[m].Allow == true) {
                            a = j; b = k;
                            newRowContent += "<td class='text-center'> <input type='checkbox' checked id='chk_" + k + "' " +
                                " onClick='return checkboxClick(this," + columnKeys1[j].Id + "," + columnKeys2[k].Id1 + ");' > " +
                                "</td>";
                            break;
                        }
                        else {

                        }

                    }
                    if (a == j && b == k) {

                        var obj = {
                            "ComponentId": columnKeys1[j].Id,
                            "RoleId": columnKeys2[k].Id,
                            "Allow": true,
                            "Readonly": false
                        }
                        checkedArray.push(obj);
                        console.log(checkedArray);
                    }
                    else {
                        newRowContent += "<td class='text-center'> <input type='checkbox' id='chk_" + k + "' " +
                            " onClick='return checkboxClick(this," + columnKeys1[j].Id + "," + columnKeys2[k].Id1 + ");' > " +
                            "</td>";
                    }

                }

                //break;
                newRowContent += "</tr>"
            }
    }

    $("#tblProviders tbody").append(newRowContent);


},
failure: function (response) {
    alert(response.d);
}
    });


}


$('#btnSaveMatrix').click(function () {
    debugger;
    $("#divLoader").show();
    var AuthList = [];
    var inputDate = moment($("#dtCalendarDate").val()).format('MM-DD-YYYY');




    //const unique = []

    //const duplicates = checkedArray.filter(o => {

    //    if (unique.find(i => i.ComponentId == o.ComponentId && i.RoleId == o.RoleId)) {

    //        return true
    //    }

    //    unique.push(o)
    //    return false;
    //})
    //console.log(duplicates);


    //$.each(checkedArray, function (i, uitem) {
    //    $.each(duplicates, function (i, ditem) {
    //        if (uitem.ComponentId == ditem.ComponentId && uitem.RoleId == ditem.RoleId)
    //            uitem.Allow = ditem.Allow

    //    });
    //});


    for (var i = 0; i < checkedArray.length; i++) {
        if (checkedArray[i].Allow != true) {
            checkedArray.splice(i, 1);
            i--;
        }
    };
    console.log(checkedArray);
    if (checkedArray.length > 0) {
        $.ajax({
            type: "POST",
            url: "https://waliku-dev-api.azurewebsites.net/api/permission/save",
            data: JSON.stringify(checkedArray),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
           // headers: { 'Authorization': "Bearer " + '59wDnOw8fhg7_FJy4kr1QxE93OhKZSFAAVqP3ph8Krmj7PJSkjKr3EAS7V_63pC6CJrtjEtTab_0mDV_jdFxvfyHOiZws21bYatyo9F1Vu7fBMu4ab4sOY28WCtCajt_etqo6MAilGAPgjwfO5BGInafAMaWw1lN9VcIyhFEPX9WOU82-tBP02V-Aiq4fr4b5_qtUUjaGGxHk0LkMz1RAIgte5ZFuytLGS3M2l_Tn9OSraoknLYqy6XCszOjcNPbeS1OFli8GBW_P7vJCWVZGNffj5XjdkR_dEbjBPFSsiL88TeYzZZND6O6O8_z1ImMjyoYnNP4YVnkiXUemSU7zUty7EBe0LQxsYebPZlFakzlGKlyWkuNvlgi-NuhDqiJMMduoB_OEXbYsjmgGr5yfLnz91KtaXuOxGMe0a3UdPeQ-LgmAXhVeTSmRA4Fem0htecvRP0gfYh2yNf96iw8QPtaY0hky1SeYVAS-6SPOH3SFJ-6h3t8LbPuwGreuRoEOiy8ghczvzjvRVzvCPwzMQ' },
            headers: { 'Authorization': "Bearer " + localStorage.getItem('access_token') },

            success: function (data) {
                alert('Success');
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        alert('Select atleast one Menu and SubMenu');
        return;
    }

});





