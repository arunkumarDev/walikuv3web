﻿var map;
var SchoolList = [];

$(document).ready(function () {
    InitEvents();
    SetDefaultMapLocation();
    //LoadSchool();
    GetData();   
});

function InitEvents() {
    $("#btnSearch").bind("click", function () {
        LoadDataByClass(parseInt($('#ddlSchool').val()), parseInt($('#ddlClass').val()))
    });

    $("#ddlSchool").on("change", function () {
        LoadClassRoom(parseInt($(this).val()));
    })
}

function GetData() {
    AjaxCall("MapOverView.aspx/GetData", null, "POST", GetDataResponse, FailureCallBack);
}

function GetDataResponse(data) {
    LoadSchool(data.d.Schools);
    LoadClassList(data.d.Classes)
}


function LoadSchool(data) {
    var appenddata;// = "<option value=''>--Select School--</option>";
    $.each(data, function (i, item) {
      SchoolList[item.ID] = item;
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";
    });
    $('#ddlSchool').html(appenddata);
   
}

function LoadClassList(data) {
    var appenddata;//= "<option value=''>--Select Informer--</option>";
    var data = data.d != undefined ? data.d : data
    $.each(data, function (i, item) {      
        appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";
    });
  
    $('#ddlClass').html(appenddata);
   
}

function LoadClassRoom(SchoolId) {
    AjaxCall("ChildrenManagement.aspx/GetAllClassRoomForMe", '{SchoolID: "' + SchoolId + '"  }', "POST", LoadClassList, FailureCallBack);
}


function LoadDataByClass(SchoolId, ClassId) {
    var params = new Object()
    params.SchoolID = SchoolId;
    params.ClassID = ClassId; 
    AjaxCall("MapOverView.aspx/GetDataBySchool", JSON.stringify(params), "POST", ResponseData, FailureCallBack);
}

function ResponseData(data) {
    var locations = [];
  
    $.each(data.d.Children, function (i, item) {
       
        if (!IsNullOrEmpty(item.GeoLocation))
        {
            var Geo = item.GeoLocation.split('/');          
          //  var temp = new Array(item.Name + ' ' + item.Surname, Geo[0], Geo[1], "yellow");
            
            var temp = { lat: Geo[0], lng: Geo[1], id: i, img: "home1.png", remarks: item.Name + ' ' + item.Surname }
            locations.push(temp);
        }      
      
    });


    if (locations.length > 0) {
        var schoolData = SchoolList[parseInt($('#ddlSchool').val())];
        var Geo = schoolData.GeoLocation.split('/');
        var temp = { lat: Geo[0], lng: Geo[1], id: (locations.length + 1), img: "school1.png", remarks: schoolData.SchoolName }
        locations.push(temp);
        initialize(locations);
    }
    else {
        SetDefaultMapLocation();
    }
   
}

function SetDefaultMapLocation() {
    var locations = [];
    initialize(locations.push({ lat: -34.397, lng: 150.644, id: 1, img: "", remarks: "" }));
}


function initialize(markerPositions) {

    var center = new google.maps.LatLng(-4.520, 121.787); //50.940749, 4.2033035
    var radius_circle = 30000; // 30km
   
    var markers = [];
    // draw map
    var mapOptions = {
        center: center,
        zoom: 6,  //20
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    var circle = drawCircle(mapOptions.center, radius_circle);
    var infowindow = new google.maps.InfoWindow();

    // markers
    for (var i = 0; i < markerPositions.length; i++) {
       
        //let url = "http://maps.google.com/mapfiles/ms/icons/";
       // url += markerPositions[i].color + "-dot.png";
    //   let url = location.protocol + '/'+location.hostname+ '/images/' + markerPositions[i].img;
        let url = '/images/' + markerPositions[i].img;
        markers = new google.maps.Marker({
            position: new google.maps.LatLng(markerPositions[i].lat, markerPositions[i].lng),
            map: map,
            icon: {
                url: url
            },
            title: markerPositions[i].remarks,
            draggable: true
        });
        google.maps.event.addListener(markers, 'click', function (markers, ) {
            for (var i = 0; i < markerPositions.length; i++) {
                infowindow.setContent('<div><strong>' + markerPositions[i].remarks + '</strong><br>' +
                    'Address1: ' + markerPositions[i].remarks + '</div>');
                infowindow.open(map, this);
            }
        });
        google.maps.event.addListener(markers, 'click', (function (markers, i) {
            return function () {
                infowindow.setContent('<div><strong>' + markerPositions[i].remarks + '</strong><br>' +
                    'Address1: ' + markerPositions[i].remarks + '</div>');
                infowindow.open(map, this);
            }
        })(markers, i));
    }

    // client clicks on button, we will check for the markers within the circle


    function drawCircle(center, radius) {
        return new google.maps.Circle({
            strokeColor: '#0000FF',
            strokeOpacity: 0.7,
            strokeWeight: 1,
            fillColor: '#0000FF',
            fillOpacity: 0.15,
            draggable: true,
            map: map,
            center: center,
            radius: radius
        });
    }
}


function FailureCallBack(data) {
    alert(data.d);
}

