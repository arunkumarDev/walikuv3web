﻿
var HolidaysList = [];
var HolidayDates = [];
var WeekDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

function DrawTeacherReportGrid(data) {
  
   // console.log(data);
    var UserList = [];
    var outList = [];
    var header = '<tr>' +
        '<th style="min-width:50px !important;text-align: center; max-width:50px;">' + GlobalResources.Lbl_SerialNo+'</th>' +
        ' <th style="min-width:150px !important;">' + LocalResources.Lbl_StudentName +'</th>' +
        //'<th style="min-width:120px;">ID No</th>' +
        ' <th style="min-width:100px;">' + LocalResources.Lbl_Gender +'</th>';

    var GridDateRange;
    

    GridDateRange = GetCalendarStartAndEndDate($("#datepicker").val(), 3);    

    var inputDate = $("#datepicker").val();
    var dt = inputDate.split('/');
    selectedDate =  new Date(dt[2] + '-' + dt[0] + '-' + dt[1]);
      
    for (var day = GridDateRange[0]; day <= GridDateRange[1]; day++) {
       // var date = selectedDate.setDate(day);
        header += '<th style="white-space:pre-line;width:50px;">' + day + '</th>';
    }


    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
        header += '<th style="white-space:pre-line;width:50px;">' + '# Days Absent for health reasons (Sick)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + '# Days Absent for social reasons (Social)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + '# Days Absent without information (Unknown)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'Total Absent Days' + '</th>';

    }
    else {
        header += '<th style="white-space:pre-line;width:50px;">' + '# Hari Absen untuk alasan kesehatan (Sakit)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + '# Hari Absen untuk alasan pribadi (Izin)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + '# Hari Absen tanpa keterangan (Alpa)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'Jumlah Hari Absen' + '</th>';

    }

    header += '</tr>';

    var slNo = 0;
    var row = "";
    $('#tblhead').html('');
    $('#tblBody').html('');

    $.each(data, function (i, item) {

        var tempObj = [];

        if (UserList[item.ID] !== undefined)
            tempObj = UserList[item.ID];
        else {
            tempObj = [];
            tempObj[0] = item;
            UserList[item.ID] = tempObj;
            outList.push({ id: item.ID });
        }
        tempObj[item.AttendanceDate] = item;
    });
    var HCount = 0;
    var NHCount = 0;
    var ULCount = 0;
    var AbsentDays = 0;
    row = "";
    $.each(outList, (i, elem) => {
        var HCount = 0;
        var NHCount = 0;
        var ULCount = 0;
        var AbsentDays = 0;

        var key = elem.id
        row += '<tr>';
        if (UserList.hasOwnProperty(key)) {
            // console.log(UserList[key]);
            var tempData = UserList[key];
            //    console.log(tempData);
           
            row += ' <td style="min-width:50px; max-width:50px;">' + (++slNo) + '</td>' +
                ' <td style="min-width:150px;">' + tempData[0].Name + '</td>' +
                ' <td style="min-width:100px;">' + GlobalResources[tempData[0].Gender] + '</td>';

            
            for (var day = GridDateRange[0]; day <= GridDateRange[1]; day++) {
                

                if (tempData.hasOwnProperty(day)) {
                    var obj = tempData[day];
                    if (obj.AttendanceType == 'H') {
                        HCount++;
                        AbsentDays++;
                    }
                    else if (obj.AttendanceType == 'NH') {
                        NHCount++;
                        AbsentDays++;
                    }
                    else if (obj.AttendanceType == 'UL') {
                        ULCount++;
                        AbsentDays++;
                    }

                    
                    // row += ' <td class=' + tdClass[obj.AttendanceType] + '>' + LocalResources[obj.AttendanceType] + '</td>';
                    row += '<td>' + LocalResources[obj.AttendanceType] + '</td>';
                }
                else {
                    if ($.inArray(day, HolidayDates) >= 0) {
                        // row += ' <td class="holiday">' + LocalResources['Holiday'] + '</td>';
                        row += ' <td>' + LocalResources['Holiday'] + '</td>';
                    }
                    else {
                        var date = selectedDate.setDate(day);
                        var d = new Date(date)
                        var dayName = WeekDays[d.getDay()];
                        if (dayName == 'Sunday')
                            row += ' <td>' + LocalResources['Holiday'] + '</td>';
                        else
                            row += ' <td>  </td>';
                    }
                }
            }
        }
        row += ' <td>' + HCount + '</td>';
        row += ' <td>' + NHCount + '</td>';
        row += ' <td>' + ULCount + '</td>';
        row += ' <td>' + AbsentDays + '</td>';
        row += '</tr>';
    });

    $('#tblhead').html(header);    
    $('#tblBody').html(row);

    ////Set Summaries
    //row += "<tr></tr>";
    //LoadMonthlyAttendanceRateSummaryForGrid(row, GridDateRange[1], header);

   

    

}

function LoadMonthlyAttendanceRateSummaryForGrid(row, TotalDays,header) {
    var inputDate = $("#datepicker").val();
    dataTotalCount = [];
    var classValue = $("#ddlClassSearch").val() == undefined ? 0 : $("#ddlClassSearch").val();
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetReasonSummaryForGrid",
        data: '{ClassId: "' + classValue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + 3 + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            
            $.each(data.d, function (ind, s) {
                row += "<tr><td>Total Students</td><td>" + s.TotalStudents + "</td><td>&nbsp;</td><td>&nbsp;</td>" +
                    "<td>Total Absenteeism Rate</td><td>" + s.AbsenteesimRate + "% </td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>P: Present</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                for (var i = 0; i < TotalDays-4; i++) {
                    row += "<td>&nbsp;</td>";
                }
                row +="</tr > "
                row += "<tr><td>Male</td><td>" + s.TotalBoys+ "</td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>Absenteeism due to sickness</td><td>" + s.HealthAbsenteeRate+"% </td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>A: Absent</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                for (var i = 0; i < TotalDays - 4; i++) {
                    row += "<td>&nbsp;</td>";
                }
                row += "</tr > "
                row += "<tr><td>Female</td><td>" + s.TotalGirls + "</td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>Absenteeism due to Social reasons</td><td>" + s.NonHealthAbsenteeRate + "% </td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>S: Absence due to social reason</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                for (var i = 0; i < TotalDays - 4; i++) {
                    row += "<td>&nbsp;</td>";
                }
                row += "</tr > "
                row += "<tr><td>Total Effective Days</td><td>" + TotalDays + "</td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>Absenteeism due to sickness</td><td>" + s.UnKnownAbsenteeRate + "% </td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>U: Unknown Reasons</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                for (var i = 0; i < TotalDays - 4; i++) {
                    row += "<td>&nbsp;</td>";
                }
                row += "</tr > "
                row += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
                    + "<td>O: Holiday</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
                for (var i = 0; i < TotalDays - 4; i++) {
                    row += "<td>&nbsp;</td>";
                }
                row += "</tr > "
            });
            $('#tblhead').html(header);
            $('#tblBody').html(row);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function InitDataTableReportForMonthly(ID, title) {

    var zeroRecText = "";
    var zeroFilterText = "";
    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
        zeroRecText = "No matching records found";
        zeroFilterText = "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json";

    }
    else {
        zeroRecText = "Tidak ada data ditemukan";
        zeroFilterText = "//cdn.datatables.net/plug-ins/1.10.22/i18n/Indonesian.json"

    }

    console.log(zeroFilterText);

    var table = $('#' + ID).DataTable({
        scrollY: "425px",
        scrollX: "520px",
        scrollCollapse: false,
        paging: true,
        destroy: true,
      
        //fixedColumns: {
        //    leftColumns: 2
        //},

        dom: 'Bfrtip',
        "stripeClasses": ['tr.odd'],
        drawCallback: function () { // this gets rid of duplicate headers
          
        },
        //buttons: [
        //    'copyHtml5',
        //    'excelHtml5',
        //    'csvHtml5',
        //    'pdfHtml5'
        //],
        /*
        columnDefs: [
            { "width": "50px", "targets": [0] },
            { "width": "250px", "targets": [1] }
        ],
        */

        buttons: [
            {
                extend: 'copyHtml5',
                title: title
            },
            {
                extend: 'excelHtml5',
                title: title
            },
            {
                extend: 'csvHtml5',
                title: title
            },
            {
                extend: 'pdfHtml5',
                title: title,
                orientation: 'landscape',//orientamento stampa
                pageSize: 'A4', //formato stampa
                alignment: "center", //non serve a nnt ma gli dice di stampare giustificando centralmente
                titleAttr: 'PDF',   //titolo bottone    
                exportOptions: {
                    stripNewlines: false
                }
                //,

                //customize: function (doc) {
                //    for (var r = 1; r < doc.content[1].table.body.length; r++) {
                //        var row = doc.content[1].table.body[r];
                //        for (c = 0; c < row.length; c++) {
                //            var exportColor = table
                //                .cell({ row: r - 1, column: c })
                //                .nodes()
                //                .to$()
                //                //.attr('class');
                //                .hasClass('holiday');
                //            if (exportColor) {
                //                row[c].fillColor = '#c1c1c1';
                //                //row[c].maxWidth = '10px';
                //            }
                //        }
                //    }
                //}
            }
        ],

        language: {
            url: zeroFilterText
        },


        //"oLanguage": {
        //    "sLengthMenu": GlobalResources.Lbl_Page_Showing + " _MENU_ " + GlobalResources.Lbl_Page_Entries,
        //    "sInfo": GlobalResources.Lbl_Page_Showing + ' ' + GlobalResources.Lbl_Page + "  _PAGE_ " + GlobalResources.Lbl_Page_Of + " _PAGES_",
        //    "infoEmpty": GlobalResources.Lbl_No_Record,
        //    "sSearch": GlobalResources.Lbl_Search,
        //    "sEmptyTable": GlobalResources.Lbl_No_Data,
        //    "zeroRecords": "Nothing found - sorry",
        //    "sZeroRecords": zeroRecText,
        //    //  "infoFiltered": "(filtered from _MAX_ total records)",
        //    "oPaginate": {
        //        "sFirst": GlobalResources.Lbl_Page_First, // This is the link to the first page
        //        "sPrevious": GlobalResources.Lbl_Page_Previous, // This is the link to the previous page
        //        "sNext": GlobalResources.Lbl_Page_Next, // This is the link to the next page
        //        "sLast": GlobalResources.Lbl_Page_Last // This is the link to the last page
        //    }
        //}
        //, 
        
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
         
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                       
                        //to select and search from grid  
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    if (d == "&nbsp;") {
                        var a = 1;
                        select.append('<option value="' + a + '">' + a + '</option>')
                    }
                    else
                        select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    }).columns.adjust();
}

function InitDataTableReport(ID, title) {
    var zeroRecText = "";
    var zeroFilterText = "";
    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
        zeroRecText = "No matching records found";
        zeroFilterText = "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json";

    }
    else {
        zeroRecText = "Tidak ada data ditemukan";
        zeroFilterText = "//cdn.datatables.net/plug-ins/1.10.22/i18n/Indonesian.json"

    }

    console.log(zeroFilterText);
    var table = $('#' + ID).DataTable({
        scrollY: "300px",
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        destroy: true,
        fixedColumns: {
            leftColumns: 2
        },
        dom: 'Bfrtip',
        "stripeClasses": ['tr.odd'],
        //buttons: [
        //    'copyHtml5',
        //    'excelHtml5',
        //    'csvHtml5',
        //    'pdfHtml5'
        //],
        /*
        columnDefs: [
            { "width": "50px", "targets": [0] },
            { "width": "250px", "targets": [1] }
        ],
        */

        buttons: [
            {
                extend: 'copyHtml5',
                title: title
            },
            {
                extend: 'excelHtml5',
                title: title
            },
            {
                extend: 'csvHtml5',
                title: title
            },
            {
                extend: 'pdfHtml5',
                title: title,
                orientation: 'landscape',//orientamento stampa
                pageSize: 'A4', //formato stampa
                alignment: "center", //non serve a nnt ma gli dice di stampare giustificando centralmente
                titleAttr: 'PDF',   //titolo bottone    
                exportOptions: {                  
                    stripNewlines: false
                }
                //,

                //customize: function (doc) {
                //    for (var r = 1; r < doc.content[1].table.body.length; r++) {
                //        var row = doc.content[1].table.body[r];
                //        for (c = 0; c < row.length; c++) {
                //            var exportColor = table
                //                .cell({ row: r - 1, column: c })
                //                .nodes()
                //                .to$()
                //                //.attr('class');
                //                .hasClass('holiday');
                //            if (exportColor) {
                //                row[c].fillColor = '#c1c1c1';
                //                //row[c].maxWidth = '10px';
                //            }
                //        }
                //    }
                //}
            }
        ],

        //"oLanguage": {
        //    "sLengthMenu": GlobalResources.Lbl_Page_Showing + " _MENU_ " + GlobalResources.Lbl_Page_Entries,
        //    "sInfo": GlobalResources.Lbl_Page_Showing + ' ' + GlobalResources.Lbl_Page + "  _PAGE_ " + GlobalResources.Lbl_Page_Of + " _PAGES_",
        //    "infoEmpty": GlobalResources.Lbl_No_Record,
        //    "sSearch": GlobalResources.Lbl_Search,
        //    "sEmptyTable": GlobalResources.Lbl_No_Data,
        //    "zeroRecords": "Nothing found - sorry",
        //    "sZeroRecords": zeroRecText,

        //    //  "infoFiltered": "(filtered from _MAX_ total records)",
        //    "oPaginate": {
        //        "sFirst": GlobalResources.Lbl_Page_First, // This is the link to the first page
        //        "sPrevious": GlobalResources.Lbl_Page_Previous, // This is the link to the previous page
        //        "sNext": GlobalResources.Lbl_Page_Next, // This is the link to the next page
        //        "sLast": GlobalResources.Lbl_Page_Last // This is the link to the last page
        //    }
        //}
        //,

        language: {
            url: zeroFilterText
        },


        initComplete: function () {
            this.api().columns().every(function () {
                debugger;
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                       
                        //to select and search from grid  
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
}

function InitDataTableReportWithoutFixedCol(ID) {



    var table = $('#' + ID).DataTable({
        scrollY: "300px",
        scrollX: true,
        scrollCollapse: true,
        paging: false,
        destroy: true,
      
        dom: 'Bfrtip',
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });
}

function GetHolidayList(inputDate, classId, selectedType) {
    AjaxCall("../Default.aspx/GetHolidayList", '{ClassId: "' + classId + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + selectedType + '"  }', "POST", HolidayListResponse, null);
}

function HolidayListResponse(data) {   
    HolidaysList = [];
    HolidayDates = [];
    $.each(data.d, function (i, item) {    
        var holidayDate = new Date(parseInt(item.HolidayDate.substr(6))).toISOString();   
        date = new Date(holidayDate);
        HolidaysList.push({ "HolidayDate": FormatDate(item.HolidayDate, 1), "Reason": item.Reason, "Day": date.getDate() });
        HolidayDates.push(date.getDate());
    });

    //console.log(HolidayDates);
}





function DrawTeacherReportGridStaff(data) {

    // console.log(data);
    var UserList = [];
    var outList = [];
    var header = '<tr>' +
        '<th style="min-width:30px !important;text-align: center; max-width:50px;">' + GlobalResources.Lbl_SerialNo + '</th>' +
        ' <th style="min-width:150px !important;">' + LocalResources.Lbl_Staff + '</th>' +
        //'<th style="min-width:120px;">ID No</th>' +
        ' <th style="min-width:100px;">' + LocalResources.Lbl_Gender + '</th>';

    var GridDateRange;


    GridDateRange = GetCalendarStartAndEndDate($("#datepicker").val(), 3);

    var inputDate = $("#datepicker").val();
    var dt = inputDate.split('/');
    selectedDate = new Date(dt[2] + '-' + dt[0] + '-' + dt[1]);

    for (var day = GridDateRange[0]; day <= GridDateRange[1]; day++) {
        // var date = selectedDate.setDate(day);
        header += '<th style="white-space:pre-line;width:50px;">' + day + '</th>';
    }

    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
        header += '<th style="white-space:pre-line;width:50px;">' + 'HEALTH (H)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'SOCIAL (S)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'UNKNOWN (U)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'Assignment (A)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'Total Absent Days' + '</th>';
        header += '<th style="white-space:pre-line;min-width:38px;">' + '%' + '</th>';
        
    }
    else {
        header += '<th style="white-space:pre-line;width:50px;">' + 'Sakit (S)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'Izin (I)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'Tanpa Berita (TB)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'Tugas Luar (TL)' + '</th>';
        header += '<th style="white-space:pre-line;width:50px;">' + 'Jumlah Hari Absen' + '</th>';
        header += '<th style="white-space:pre-line;min-width:38px;">' + '%' + '</th>';
    }


    header += '</tr>';

    var slNo = 0;
    var row = "";
    $('#tblhead').html('');
    $('#tblBody').html('');

    $.each(data, function (i, item) {

        var tempObj = [];

        if (UserList[item.ID] !== undefined)
            tempObj = UserList[item.ID];
        else {
            tempObj = [];
            tempObj[0] = item;
            UserList[item.ID] = tempObj;
            outList.push({ id: item.ID });
        }
        tempObj[item.AttendanceDate] = item;
    });
    var HCount = 0;
    var NHCount = 0;
    var ULCount = 0;
    var TotalDays = 0;
    row = "";
    $.each(outList, (i, elem) => {
        var HCount = 0;
        var NHCount = 0;
        var ULCount = 0;
        var AssCount = 0;
        var AbsentDays = 0;

        var key = elem.id
        row += '<tr>';
        if (UserList.hasOwnProperty(key)) {
            // console.log(UserList[key]);
            var tempData = UserList[key];
            console.log(tempData);
            TotalDays = tempData[0].TotalDays;
            row += ' <td style="min-width:50px; max-width:50px;">' + (++slNo) + '</td>' +
                ' <td style="min-width:150px;">' + tempData[0].Name + '</td>' +
                ' <td style="min-width:100px;">' + GlobalResources[tempData[0].Gender] + '</td>';

            debugger;
            for (var day = GridDateRange[0]; day <= GridDateRange[1]; day++) {

                
                if (tempData.hasOwnProperty(day)) {
                    var obj = tempData[day];
                    if (obj.AttendanceType == 'H') {
                        HCount++;
                        AbsentDays++;
                    }
                    else if (obj.AttendanceType == 'NH') {
                        NHCount++;
                        AbsentDays++;
                    }
                    else if (obj.AttendanceType == 'UL') {
                        ULCount++;
                        AbsentDays++;
                    }
                    else if (obj.AttendanceType == 'A') {
                        AssCount++;
                        AbsentDays++;
                    }


                    // row += ' <td class=' + tdClass[obj.AttendanceType] + '>' + LocalResources[obj.AttendanceType] + '</td>';
                    row += '<td>' + LocalResources[obj.AttendanceType] + '</td>';
                }
                else {
                    if ($.inArray(day, HolidayDates) >= 0) {
                        // row += ' <td class="holiday">' + LocalResources['Holiday'] + '</td>';
                        row += ' <td>' + LocalResources['Holiday'] + '</td>';
                    }
                    else {
                        var date = selectedDate.setDate(day);
                        var d = new Date(date)
                        var dayName = WeekDays[d.getDay()];
                        if (dayName == 'Sunday')
                            row += ' <td>' + LocalResources['Holiday'] + '</td>';
                        else
                            row += ' <td> </td>';
                    }
                }
            }
        }
        var aa = HCount - NHCount - ULCount - AssCount;
        row += ' <td>' + HCount + '</td>';
        row += ' <td>' + NHCount + '</td>';
        row += ' <td>' + ULCount + '</td>';
        row += ' <td>' + AssCount + '</td>';
        row += ' <td>' + AbsentDays + '</td>';
        row += ' <td>' + (AbsentDays / TotalDays * 100).toFixed(2) + '%' + '</td>';

        row += '</tr>';
    });

    $('#tblhead').html(header);
    $('#tblBody').html(row);

    ////Set Summaries
    //row += "<tr></tr>";
    //LoadMonthlyAttendanceRateSummaryForGrid(row, GridDateRange[1], header);





}
