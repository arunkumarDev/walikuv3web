﻿$(document).ready(function () {
  
    LoadClassRoom();
    LoadData();
    //LoadSchool();
    $("#btnSearchReport").click(function () {       
             LoadData();
    });
});
var SelectedType;
//function LoadSchool() {
//    ajaxcall("schoolmanagement.aspx/GetMySchools", '{ pageindex: "' + 1 + '" }', "post", LoadSchoolSuccess, failurecallback);
//}
var menuType = 1;
$('#DailyContainer').click(function (id) {
    this.classList.add('is-active');
    SelectedType = 1;
    LoadDataForPanel();
});
$('#MonthlyContainer').click(function (id) {
    this.classList.add('is-active');
    SelectedType = 2;
    LoadDataForPanel();
});
$('#WeeklyContainer').click(function (id) {
    this.classList.add('is-active');
    SelectedType = 3;
    LoadDataForPanel();
}); 
$('#YearlyContainer').click(function (id) {
    this.classList.add('is-active');
    SelectedType = 4;
    LoadDataForPanel();
});

function LoadDataForPanel() {
    var inputDate = $("#datepicker").val();
    var url = "";
    if (SelectedType == 1)
        url = 'Views/TeacherReportDaily.aspx?inputDate=' + inputDate + '&SelectedType=' + SelectedType;
    else if (SelectedType == 2)
        url = 'Views/TeacherReportMonthly.aspx?inputDate=' + inputDate + '&SelectedType=' + SelectedType;
    else if (SelectedType == 3)
        url = 'Views/TeacherReportWeekly.aspx?inputDate=' + inputDate + '&SelectedType=' + SelectedType;
    else if (SelectedType == 4)
        url = 'Views/TeacherReportYearly.aspx?inputDate=' + inputDate + '&SelectedType=' + SelectedType;

    $("#ReportContainer").load(url, function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success")
            //   alert("External content loaded successfully!");
            if (statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
    });
}
function LoadData() {
    
    var inputDate = $("#datepicker").val();
    var SelectedType = 0;
    var url = "";
    if (SelectedType == 0)
        url = 'Views/TeacherReportDaily.aspx?inputDate=' + inputDate + '&SelectedType=' + SelectedType;
    else if (SelectedType == 1)
        url = 'Views/TeacherReportMonthly.aspx?inputDate=' + inputDate + '&SelectedType=' + SelectedType;
    else if (SelectedType == 2)
        url = 'Views/TeacherReportWeekly.aspx?inputDate=' + inputDate + '&SelectedType=' + SelectedType;
    else if (SelectedType == 3)
        url = 'Views/TeacherReportYearly.aspx?inputDate=' + inputDate + '&SelectedType=' + SelectedType;

    $("#ReportContainer").load(url, function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success")
            //   alert("External content loaded successfully!");
            if (statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
    });
}


function LoadSchoolSuccess(data) {
    
    var appenddata;// = "<option value=''>--Select School--</option>";
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";
    });
    $('#ddlSchool').html(appenddata);
    LoadClassRoom($('#ddlSchool').val());
}
function LoadClassRoom() {

    $.ajax({
        type: "POST",
        //url: "Default.aspx/GetClassRoomForMe",
        //data: '',
        url: "../ChildrenManagement.aspx/GetAllClassRoomForMe",
        data: '{SchoolID: 0}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata = '';// '<option value="">--' + LocalResources.Lbl_Select + '--</option>';
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";
            });
            $('#ddlClassRoom').html(appenddata);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}