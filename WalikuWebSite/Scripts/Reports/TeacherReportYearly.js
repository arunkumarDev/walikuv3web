﻿
////https://canvasjs.com/javascript-charts/multiple-axis-column-chart/
$(document).ready(function () {
    
    LoadMonthlyAttendanceRateSummary();
    LoadReport();
    LoadStudentAttendanceReport();
    LoadReasonsChart();
    LoadDaysStudentAbsent();

})

var classData = [];

var tdClass = { "H": "present", "K": "absent", "NK": "sick_leave", "C": "unknown_leave", "L": "holyday" }

function LoadReport() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    dataPointsPresent = [];
    dataPointsAbsent = [];
    dataPointsClassName = [];
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetClassWisePresentAbsentReport",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + $('#ddlMenuType').val() + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {

                drawMonthlyReportChart(data);                
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function LoadStudentAttendanceReport() {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    var SelectedType = SelectedType;

    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetStudentWisePresentAbsentReport",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
            $('#total').text(data.d.length);
            if (data.d.length > 0) {
                DrawTeacherYearlyGrid(data.d);
                InitDataTableReport('dataTableAttendance', $("#ddlClassRoom option:selected").text() + ' - ' + thisMonth + ' ' + $('#datepicker').datepicker('getDate').getFullYear());
            }

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function LoadMonthlyAttendanceRateSummary() {
    var inputDate = $("#datepicker").val();
    var SelectedType = SelectedType;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyAttendanceRateSummary",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            
            for (var i = 0; i < data.d.length; i++) {
                $("#YearlyAttendance").text(data.d[i].AttendanceRate + ' %');
                $("#AbsenteesimRate").text(data.d[i].AbsenteesimRate + ' %');
                $("#BoysAbsenteeRate").text(data.d[i].BoysAbsenteeRate + ' %');
                $("#GirlsAbsenteeRate").text(data.d[i].GirlsAbsenteeRate + ' %');
                $("#HealthAbsenteeRate").text(data.d[i].HealthAbsenteeRate + ' %');
                $("#NonHealthAbsenteeRate").text(data.d[i].NonHealthAbsenteeRate + ' %');
                $("#UnKnownAbsenteeRate").text(data.d[i].UnKnownAbsenteeRate + ' %');
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function LoadDaysStudentAbsent() {
    var inputDate = $("#datepicker").val();
    var SelectedType = SelectedType;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyStudentAbsent",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.d.length; i++) {
                $("#StudentCount1").text(data.d[i].StudentCount1);
                $("#StudentCount2").text(data.d[i].StudentCount2);
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function LoadReasonsChart() {
    var inputDate = $("#datepicker").val();
    var SelectedType = SelectedType;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyReasonCountNew",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            

            var top3HealthReson = "";
            var top3NonHealthReson = "";
            var healthData = [];
            var nonHealthData = [];
            var healthCount = 0;
            var nonHealthCount = 0;

            if (data.d.Top3Reason) {

                for (var i = 0; i < data.d.Top3Reason.length; i++) {
                    if (data.d.Top3Reason[i].ReasonCode == "Health") {

                        if (healthCount < 3) {
                            top3HealthReson += '<span class="badge badge-success">' + data.d.Top3Reason[i].Description + '<span class="badge badge-warning">' + data.d.Top3Reason[i].ReasonCount + '</span></span>';
                        }
                        healthCount++;

                    } else if (data.d.Top3Reason[i].ReasonCode == "Non-Health") {

                        if (nonHealthCount < 3) {
                            top3NonHealthReson += '<span class="badge badge-success">' + data.d.Top3Reason[i].Description + '<span class="badge badge-warning">' + data.d.Top3Reason[i].ReasonCount + '</span></span>';
                        }
                        nonHealthCount++;

                    }
                }
            }

            if (data.d.PieChart) {
                for (var i = 0; i < data.d.PieChart.length; i++) {
                    if (data.d.PieChart[i].ReasonCode == "Health") {

                        healthData.push(data.d.PieChart[i]);

                    } else if (data.d.PieChart[i].ReasonCode == "Non-Health") {

                        nonHealthData.push(data.d.PieChart[i]);
                    }
                }
            }

            drawReasonChartforHealth(healthData);
            drawReasonChartforNonHealth(nonHealthData);

            $('#dvHealthReasonCount').html(top3HealthReson);
            $('#dvNonHealthReasonCount').html(top3NonHealthReson);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function drawReasonChartforHealth(data) {
    var ReasonName = [];
    var ReasonCount = [];


    for (var i = 0; i < data.length; i++) {
        var ReasonObj = new Object();
        ReasonObj.name = data[i].Description;
        ReasonObj.y = data[i].ReasonCount;
        ReasonCount.push(ReasonObj);

    }
    Highcharts.chart('burden-healthabsence', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: LocalResources.Lbl_AbsentDueToHealth //'Student absent days due to health'
        },
        tooltip: {
            //pointFormat: '{series.name}: {point.y}',
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' + '<br/>' + '{series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Total Count',
            colorByPoint: true,
            data: ReasonCount
        }]
    });

}

function drawReasonChartforNonHealth(data) {
    
    var ReasonName = [];
    var ReasonCount = [];

    for (var i = 0; i < data.length; i++) {
        var ReasonObj = new Object();
        ReasonObj.name = data[i].Description;
        ReasonObj.y = data[i].ReasonCount;
        ReasonCount.push(ReasonObj);

    }

    Highcharts.chart('burden-non-healthabsence', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: LocalResources.Lbl_AbsentDueToNonHealth  //d'Student absent days due to non health'
        },
        tooltip: {
            //pointFormat: '{series.name}: {point.y}',
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' + '<br/>' + '{series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Total Count',
            colorByPoint: true,
            data: ReasonCount
        }]
    });

}


function DrawTeacherYearlyGrid(data) {
    
    var header = '<tr>' +
        '<th style="min-width:70px; text-align: center;">' + GlobalResources.Lbl_SerialNo +'</th>' +
        ' <th style="min-width:180px;">' +LocalResources.Lbl_StudentName+'</th>' +
        '<th style="min-width:120px;">' + LocalResources.Lbl_AbsentDays +'</th>' +
        ' <th>' + LocalResources.Lbl_AbsenceType +'</th></tr>';


    var slNo = 0;
    var row = "";
    $('#tblhead').html('');
    $('#tblBody').html('');

    row = "";

    for (var i = 0; i < data.length; i++) {
        row += '<tr>';

        var tempData = data[i];

        row += ' <td>' + (++slNo) + '</td>' +
            ' <td>' + tempData.Name + '</td>' +
            ' <td>' + tempData.AbsentDays + '</td>' +
            ' <td>' + LocalResources[tempData.ReasonType] + '</td>';

        row += '</tr>';
    }

    $('#tblhead').html(header);
    $('#tblBody').html(row);
}




function drawMonthlyReportChart(data) {
    var ChronicalData = [];
    var SeverelyChronicalData = [];
    var CategoryData = [];

    for (var i = 0; i < data.d.length; i++) {
        ChronicalData.push(data.d[i].ChronicallyAbsent);
        SeverelyChronicalData.push(data.d[i].SeverelyChronicallyAbsent);
        switch ($('#ddlMenuType').val()) {
            case "1":
                break;
            case "2":
                CategoryData.push(data.d[i].ClassName);
                break;
            default:
                CategoryData.push(data.d[i].AttendanceMonthName);
                break;
        }

    }

    Highcharts.chart('attendance-chart', {
        title: {
            text: ''
        },
        xAxis: {
            categories: CategoryData
        },
        yAxis: {
            title: {
                text: LocalResources.Lbl_TotalStudents //'Total Students'
            }
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';
                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '';
                });
                return s;
            },
            shared: true
        },
        series: [{
            name: LocalResources.Lbl_ChronicAbsentChildren,    //'CHRONICALLY ABSENT CHILDREN ',
            color: '#ffb212',
            data: ChronicalData
        }, {
                name: LocalResources.Lbl_SevereChronicAbsentChildren,    //'SEVERELY CHRONICALLY ABSENT CHILDREN',
            color: '#ff0707',
            data: SeverelyChronicalData
        }]
    });


}


