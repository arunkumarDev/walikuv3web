﻿$(document).ready(function () {
   // LoadClassRoom();
    document.getElementById("hideAll").style.display = "none";
    LoadSchool();
    LoadNotifications();
    debugger;
   
    $('#divStudentFilter').hide();
    $("#btnSearchReport1").click(function () {       
             LoadData();
    });
    $("#ddlMenuType").change(function () {
        if ($(this).val() == 5) {
            $("#DailyContainer").hide();
            $("#WeeklyContainer").hide();
            $("#YearlyContainer").hide();
            $("#MonthlyContainer").show();
            $('#divStudentFilter').hide();
        }
        else if ($(this).val() == 4) {
            $("#DailyContainer").hide();
            $("#WeeklyContainer").hide();
            $("#MonthlyContainer").hide();
            $("#YearlyContainer").show();
            $('#divStudentFilter').show();
            LoadChildrenForClass();
        }
        else {
            $("#DailyContainer").show();
            $("#WeeklyContainer").show();
                $("#YearlyContainer").show();
            $("#MonthlyContainer").show();
            $('#divStudentFilter').hide();
        }
    });
    $('#ddlSchoolSearch').on('change', function () {
        LoadClassRoom($(this).val());
    });

    $("#datepicker").on("changeDate", function () {
        LoadData();
    });

});
var SelectedType=1;
//function LoadSchool() {
//    ajaxcall("schoolmanagement.aspx/GetMySchools", '{ pageindex: "' + 1 + '" }', "post", LoadSchoolSuccess, failurecallback);
//}
$('#DailyContainer').click(function (id) {
    this.classList.add('is-active');
    SelectedType = 1;
    LoadData();
});
$('#MonthlyContainer').click(function (id) {
    this.classList.add('is-active');
    SelectedType = 3;
    LoadData();
});
$('#WeeklyContainer').click(function (id) {
    this.classList.add('is-active');
    SelectedType = 2;
    LoadData();
});
$('#YearlyContainer').click(function (id) {
    this.classList.add('is-active');
    SelectedType = 4;
    LoadData();
});


function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

    });
    $('#ddlSchoolSearch').html(appenddata);
    $('#ddlSchoolSearch').parent().hide();
    LoadClassRoom(parseInt($('#ddlSchoolSearch').val()));

}

var StudentsForSchool = [];
$('#ddlClassSearch').change(function () {
    LoadChildrenForClass();
    LoadData();
});

function LoadChildrenSuccess(data) {
    StudentsForSchool = data.d;
    $('#ddlStudentSearch')
        .find('option')
        .remove()
        .end();
    var appenddata = '';//'<option value="">--' + LocalResources.Lbl_Select + '--</option>';
    var ClassID = $('#ddlClassSearch').val();
    $.each(StudentsForSchool, function (ind, child) {
        if (child.ClassroomID == ClassID) {
            appenddata += "<option value = '" + child.ID + "'>" + child.Name + " </option>";
        }
    });
    $('#ddlStudentSearch').html(appenddata);
    $('#divStudentFilter').show();
}
function LoadChildrenForClass() {
    var SchoolID = $('#ddlSchoolSearch').val();
    var ClassID = $('#ddlClassSearch').val();
    AjaxCall("Default.aspx/GetAllChildrenForSchool", '{SchoolID: "' + SchoolID + '",ClassID: "' + ClassID +  '"}', "POST", LoadChildrenSuccess, FailureCallBack);
}
function LoadSchool() {
    AjaxCall("SchoolManagement.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}
function LoadClassRoom(SchoolId) {
    AjaxCall("ChildrenManagement.aspx/GetAllClassRoomForMe", '{SchoolID: "' + SchoolId + '"  }', "POST", LoadClassRoomSuccess, FailureCallBack);
}
function LoadClassRoomSuccess(data) {
    $('#ddlClassSearch')
        .find('option')
        .remove()
        .end();
    MyClassList = data.d;
    var appenddata = '';//'<option value="">--' + LocalResources.Lbl_Select + '--</option>';
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";

    });
    $('#ddlClassSearch').html(appenddata);

    var reportTypes = "<option value = '1'>School Summary</option>" +
        "<option value = '2'>Compare Classes</option>" ;
    $('#ddlReportType').html(reportTypes);
    if (FormLoad) {
        FormLoad = false;
        setTimeout(function () {
        }, 500)

    }
    LoadData();
    LoadChildrenForClass();
}
function LoadData() {
    var inputDate = $("#datepicker").val();
    var url = "";
    debugger;
    var menutypevalue = 0;
    debugger;
    if (window.location.pathname == '/Report-schoolsummary' || window.location.pathname == '/Report-schoolsummary.aspx') {
        menutypevalue = 1;
    }
    else if (window.location.pathname == '/Report-CompareClasses' || window.location.pathname == '/Report-CompareClasses.aspx') {
        menutypevalue = 2;
    }
    else if (window.location.pathname == '/Reports-IndClassSummary' || window.location.pathname == '/Reports-IndClassSummary.aspx') {
        menutypevalue = 3;
    }
    else if (window.location.pathname == '/Report-IndChildSummary' || window.location.pathname == '/Report-IndChildSummary.aspx') {
        menutypevalue = 4;
    }
    else {
        menutypevalue = 5;
        $("#DailyContainer").hide();
        $("#WeeklyContainer").hide();
        $("#YearlyContainer").hide();
        $("#MonthlyContainer").show();
        $('#divStudentFilter').hide();
    }

    if (userType == 1) {
        if (menutypevalue == 4) {
            debugger;
            url = 'Views/AdminReportChildSummary.aspx';
            $('#divStudentFilter').show();
            //LoadChildrenForClass();
        }
        else {
            $('#divStudentFilter').hide();
            if (window.location.pathname == '/Report-StaffSummary' || window.location.pathname == '/Report-StaffSummary.aspx')
            {
                url = 'Views/AdminReportMonthly.aspx';
            }  
            else {
                if (SelectedType == 1)
                    url = 'Views/AdminReportDaily.aspx';
                else if (SelectedType == 2)
                    url = 'Views/AdminReportWeekly.aspx';
                else if (SelectedType == 3)
                    url = 'Views/AdminReportMonthly.aspx';
                else if (SelectedType == 4)
                    url = 'Views/AdminReportYearly.aspx';
            }
        }
    } else if (userType == 4) {
        if (menutypevalue == 4) {
            url = 'Views/AdminReportChildSummary.aspx';
        }
        else {
            if (window.location.pathname == '/Report-StaffSummary' || window.location.pathname == '/Report-StaffSummary.aspx') {
                url = 'Views/SuperAdminReportMonthly.aspx';
            }
            else {
                if (SelectedType == 1)
                    url = 'Views/SuperAdminReportDaily.aspx';
                else if (SelectedType == 2)
                    url = 'Views/SuperAdminReportWeekly.aspx';
                else if (SelectedType == 3)
                    url = 'Views/SuperAdminReportMonthly.aspx';
                else if (SelectedType == 4)
                    url = 'Views/SuperAdminReportYearly.aspx';
            }
        }
    }

    localStorage.setItem('Usertypefrimport', userType);
    

    $("#ReportContainer").load(url, function (responseTxt, statusTxt, xhr) {
        if (statusTxt == "success")
            //   alert("External content loaded successfully!");
            if (statusTxt == "error")
                alert("Error: " + xhr.status + ": " + xhr.statusText);
    });

    //LoadNotifications();
}

function FailureCallBack(data) {

}