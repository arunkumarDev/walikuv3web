﻿var thisMonth = '';
////https://canvasjs.com/javascript-charts/multiple-axis-column-chart/
$(document).ready(function () {
  //      
    LoadMonthlyAttendanceRateSummary();
    LoadReport();
    LoadStudentAttendanceReport();
    LoadReasonsChart();
    //LoadReasonCountHealth();
    //LoadReasonCountNonHealth();
    // LoadMonthlyReasonReportRate();
    LoadDaysStudentAbsent();    
   
})




var classData = [];

var tdClass = {
    "P": "present", "H": "absent", "NH": "sick_leave", "UL": "unknown_leave", "L": "holyday", "L": "holyday" };

function LoadReport() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 3;
    dataPointsPresent = [];
    dataPointsAbsent = [];
    dataPointsClassName = [];
    dataTotalCount = [];
    $("#monthName").text('');

    GetHolidayList(inputDate, $("#ddlClassRoom").val(), 1);
    thisMonth = getMonthName(inputDate);

    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetClassWisePresentAbsentReport",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + $('#ddlMenuType').val() + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#monthName").text(thisMonth);            

            if (data.d.length > 0) {

                drawMonthlyReportChart(data);
               
                dataTotalCount.sort(function (a, b) { return a - b });
                var maxYvalue1 = dataTotalCount[dataTotalCount.length - 1];
                maxYvalue1 = maxYvalue1 + 10;
                maxYvalue1 = maxYvalue1.toString().substr(0, maxYvalue1.toString().length - 1);
                var maxYvalue = parseInt(maxYvalue1 * 10);

                var barChartData = {
                    labels: dataPointsClassName,
                    datasets: [{
                        label: LocalResources.Lbl_Present, //'Present',
                        backgroundColor: "#83A7D0",
                        yAxisID: "y-axis-1",
                        data: dataPointsPresent
                    }, {
                            label: LocalResources.Lbl_Absent,  //'Absent',
                        backgroundColor: "#D38483",
                        yAxisID: "y-axis-2",
                        data: dataPointsAbsent
                    }]

                };

                var ctx = document.getElementById("canvas").getContext("2d");
                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        onClick: function (cc) {
                           // console.log(cc);
                        },
                        responsive: true,
                        title: {
                            display: true,
                            text: "Class wise attendance report for " + $("#txtDatePicker").val()
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: true
                        },
                        scales: {
                            yAxes: [{
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: true,
                                position: "left",
                                id: "y-axis-1",

                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 5,
                                    max: maxYvalue
                                }
                            }, {
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: false,
                                position: "right",
                                id: "y-axis-2",
                                gridLines: {
                                    drawOnChartArea: false
                                },
                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 5,
                                    max: maxYvalue
                                }
                            }],
                        }
                    }
                });

                $("#trigo").show();
                $("#fullReport").hide();
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function LoadStudentAttendanceReport() {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    var SelectedType = 3;
    
    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
        url: "../Default.aspx/GetStudentWisePresentAbsentReport",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
            $('#total').text(data.d.length);
            if (data.d.length > 0) {
                DrawTeacherReportGrid(data.d);           
                InitDataTableReport('dataTableAttendance', $("#ddlClassRoom option:selected").text() + ' - ' + thisMonth + ' ' + $('#datepicker').datepicker('getDate').getFullYear());
            }
         
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function LoadMonthlyAttendanceRateSummary() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyAttendanceRateSummary",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        
            
            
            for (var i = 0; i < data.d.length; i++) {
                $("#MonthlyAttendance").text(data.d[i].AttendanceRate +' %');
                $("#AbsenteesimRate").text(data.d[i].AbsenteesimRate + ' %');
                $("#BoysAbsenteeRate").text(data.d[i].BoysAbsenteeRate + ' %');
                $("#GirlsAbsenteeRate").text(data.d[i].GirlsAbsenteeRate + ' %');
                $("#HealthAbsenteeRate").text(data.d[i].HealthAbsenteeRate + ' %');
                $("#NonHealthAbsenteeRate").text(data.d[i].NonHealthAbsenteeRate + ' %');
                $("#UnKnownAbsenteeRate").text(data.d[i].UnKnownAbsenteeRate + ' %');
            }

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


//function LoadMonthlyReasonReportRate() {
//    var inputDate = $("#datepicker").val();
//    var SelectedType = SelectedType;
//    dataTotalCount = [];
//    $.ajax({
//        type: "POST",
//        url: "../Default.aspx/GetMonthlyReasonReportRate",
//        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            for (var i = 0; i < data.d.length; i++) {
//                $("#Health").text(data.d[i].Abstsmratesickness.toFixed(3) + '%');
//                $("#NonHealth").text(data.d[i].Abstsmratenonhealth.toFixed(3) + '%');
//                $("#Unknown").text(data.d[i].AbstsmrateUnknown.toFixed(3) + '%');
//            }
//        },
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}

function LoadDaysStudentAbsent() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyStudentAbsent",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.d.length; i++) {
                $("#StudentCount1").text(data.d[i].StudentCount1);
                $("#StudentCount2").text(data.d[i].StudentCount2);                
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function LoadReasonsChart() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyReasonCountNew",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            

            var top3HealthReson = "";
            var top3NonHealthReson = "";
            var healthData = [];
            var nonHealthData = [];
            var healthCount = 0;
            var nonHealthCount = 0;

            if (data.d.Top3Reason) {
                for (var i = 0; i < data.d.Top3Reason.length; i++) {
                    if (data.d.Top3Reason[i].ReasonCode == "Health") {

                        if (healthCount < 3) {
                            top3HealthReson += '<span class="badge badge-success">' + data.d.Top3Reason[i].Description + '<span class="badge badge-warning">' + data.d.Top3Reason[i].ReasonCount + '</span></span>';
                        }
                        healthCount++;

                    } else if (data.d.Top3Reason[i].ReasonCode == "Non-Health") {

                        if (nonHealthCount < 3) {
                            top3NonHealthReson += '<span class="badge badge-success">' + data.d.Top3Reason[i].Description + '<span class="badge badge-warning">' + data.d.Top3Reason[i].ReasonCount + '</span></span>';
                        }
                        nonHealthCount++;

                    }
                }
            }

            if (data.d.PieChart) {
                for (var i = 0; i < data.d.PieChart.length; i++) {
                    if (data.d.PieChart[i].ReasonCode == "Health") {

                        healthData.push(data.d.PieChart[i]);

                    } else if (data.d.PieChart[i].ReasonCode == "Non-Health") {

                        nonHealthData.push(data.d.PieChart[i]);
                    }
                }
            }

            drawReasonChartforHealth(healthData);
            drawReasonChartforNonHealth(nonHealthData);

            $('#dvHealthReasonCount').html(top3HealthReson);
            $('#dvNonHealthReasonCount').html(top3NonHealthReson);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function LoadReasonCountHealth() {

    var inputDate = $("#datepicker").val();
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyReasonCount1",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            drawReasonChartforHealth(data);
            
            var top3HealthReson="";
            for (var i = 0; i < data.d.length; i++) {
                top3HealthReson += '<span class="badge badge-success">' + data.d[i].Description + '<span class="badge badge-warning">' + data.d[i].ReasonCount+'</span></span>';
                 
            }
            $('#dvHealthReasonCount').html(top3HealthReson);
            
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function drawReasonChartforHealth(data) {
    var ReasonName = [];
    var ReasonCount = [];
    

    for (var i = 0; i < data.length; i++) {
        var ReasonObj = new Object();
        ReasonObj.name = data[i].Description;
        ReasonObj.y = data[i].ReasonCount;
        ReasonCount.push(ReasonObj);
      
    }
       Highcharts.chart('burden-healthabsence', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: LocalResources.Lbl_AbsentDueToHealth //'Student absent days due to health'
        },
        tooltip: {
            //pointFormat: '{series.name}: {point.y}',
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' + '<br/>' + '{series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Total Count',
            colorByPoint: true,
            data: ReasonCount
        }]
    });
   
}

function drawReasonChartforNonHealth(data) {
    
    var ReasonName = [];
    var ReasonCount = [];

    for (var i = 0; i < data.length; i++) {
        var ReasonObj = new Object();
        ReasonObj.name = data[i].Description;
        ReasonObj.y = data[i].ReasonCount;
        ReasonCount.push(ReasonObj);
       
    }

    Highcharts.chart('burden-non-healthabsence', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: LocalResources.Lbl_AbsentDueToNonHealth  //'Student absent days due to non health'
        },
        tooltip: {
            //pointFormat: '{series.name}: {point.y}',
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' + '<br/>' + '{series.name}: {point.y}'    
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Total Count',
            colorByPoint: true,
            data: ReasonCount
        }]
    });

}



function LoadReasonCountNonHealth() {
    
    var inputDate = $("#datepicker").val();
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyReasonCount2",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            drawReasonChartforNonHealth(data);
            var top3NonHealthReson = "";
            for (var j = 0; j < data.d.length; j++) {
                top3NonHealthReson += '<span class="badge badge-success">' + data.d[j].Description + '<span class="badge badge-warning">' + data.d[j].ReasonCount + '</span></span>';
                //  $("#sickness1Name").text(data.d[i].Description);
                //   $("#sickness1count").text(data.d[i].Reasoncount);
                // $("#Unknown").text(data.d[i].AbstsmrateUnknown);
            }
            $('#dvNonHealthReasonCount').html(top3NonHealthReson);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function drawMonthlyReportChart(data) {
    //alert(3);
    var presentData = [];
    var absentData = [];
    var CategoryData = [];
    //var totalPresentCount = 0;
    //var totalAbsentCount = 0;
    //var totalBoysCount = 0;
    //var totalGirlsCount = 0;
    //var absentGirlsCount = 0;
    //var absentBoysCount = 0;


    for (var i = 0; i < data.d.length; i++) {
        presentData.push(data.d[i].Present);
        absentData.push(data.d[i].Absent);
        switch ($('#ddlMenuType').val()) {
            case "2":
                CategoryData.push(data.d[i].ClassName);
                break;
            default:
                CategoryData.push(data.d[i].AttendanceMonthName);
                break;
        }
        

    }

    //$("#presentCount").text(totalPresentCount);
    //$("#absentCount").text(totalAbsentCount);
    $('#grid').show();
    $('#MonthlyLeftSummary').show();
    $('#YearlyGraphHeading').hide();
    $('#attendance-chart').hide();
    $('#attendance-chart3').hide();
    $('#attendance-chart4').hide();
    $('#MonthlyGraphHeading').show();
    $('#burden-non-healthabsence').show();
    $('#burden-healthabsence').show();
    $('#attendance-chart2').show();
    $('#Monthlygraph').show();
    $('#Monthlyleftsec').show();
    $('#MonthlySummary').show();
    $('#MonthlySummary1').show();
    $('#MonthlySummary2').show();
    $('#MonthlySummary3').show();
    $('#MonthlySummary5').show();
    $('#MonthlySummary4').show();
    $('#leftsumarry').show();


    Highcharts.chart('attendance-chart2', {
        title: {
            text: ''
        },
        xAxis: {
            categories: CategoryData
        },
        yAxis: {
            title: {
                text: LocalResources.Lbl_TotalStudents  //'Total Students'
            }
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';
                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '';
                });
                return s;
            },
            shared: true
        },
        series: [{
            name: LocalResources.Lbl_Present,  //'Present',
            color: '#01a051',
            data: presentData,
        }, {
                name: LocalResources.Lbl_Absent,   //'Absent ',
            color: '#ff0716',
            data: absentData,
        }]
    });


 

}


