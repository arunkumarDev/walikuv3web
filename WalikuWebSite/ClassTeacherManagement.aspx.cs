﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.UI;
using WalikuWebSite.DataAccess;

public partial class ClassTeacherManagement : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }
    }
    [WebMethod]
    public static List<TeacherClassMapping> GetAllClassTeacher(int PageIndex)
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        return SqlHelper.GetAllTeacherClasses(uid,false);
    }
    [WebMethod]
    public static List<ClassMaster> GetAllClasses(int PageIndex)
    {
        return SqlHelper.GetAllClasses();

    }
    [WebMethod]
    public static List<UserMaster> GetAllTeachers(int PageIndex)
    {
        return SqlHelper.GetAllTeachers();

    }

    [WebMethod]
    public static List<UserMaster> GetMyTeacherList()
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();

        return UserDataAccess.GetMyUserList(uid,Common.CommonFunction.GetCurrentCulture());
    }

    [WebMethod]
    public static VmResponseData ManageClassTeacher(int ID, int ClassRoomID,int TeacherID)
    {
        VmResponseData VMResponseData = new VmResponseData();

        try
        {
            SqlHelper.ManageClassTeacher(new TeacherClassMapping()
            {
                ID = ID,
                ClassRoomID = ClassRoomID,
                TeacherID = TeacherID,
                CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                CreatedOn = DateTime.Now,
                IsActive = 1
            });
            VMResponseData.Message = "Success";
        }
        
        catch (SqlException ex)
        {
            if (ex.Number == 2627)
            {
                VMResponseData.Message = "This Class is already mapped to another teacher";
            }
        }
        catch (Exception e)
        {
            VMResponseData.Message = "Failed";
        }
        return VMResponseData;
    }
    [WebMethod]
    public static void DeleteClassTeacherByID(int ID)
    {
        SqlHelper.DeleteClassTeacherByID(ID);
    }
}