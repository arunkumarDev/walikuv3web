﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using WalikuWebSite.Model;
using WalikuWebSite.DataAccess;
using System.Data.SqlClient;

namespace WalikuWebSite
{
    public partial class AcademicTermDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<AcademicTerm> GetAllAcademicTermList(int SchoolID)
        {
            var myUser = SqlHelper.GetUserByID(Common.CommonFunction.GetUserIdFromCookieByID());

            int uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());
            var uid = Common.CommonFunction.GetUserIdFromCookieByID();
            return AcademicTermDataAccess.GetAcademicTermDetails(uid, uType,SchoolID);

        }

        [WebMethod]
        public static VmResponseData DeleteAcademicTerm(int ID)
        {      
            AcademicTermDataAccess.DeleteAcademicTermByID(ID);
            return new VmResponseData() { Message = "Success" };
        }

        [WebMethod]
        public static VmResponseData ManageAcademicTerm(int SchoolID, DateTime StartDate, DateTime EndDate)
        {
            VmResponseData response = new VmResponseData();
            try
            {
                if(AcademicTermDataAccess.IsCurrentTermExist(SchoolID))
                {
                    response.Message = "Academic Term Already Set For this school";
                    return response;
                }

                AcademicTermDataAccess.ManageAcademicTerm(new AcademicTerm()
                {
                    ID =0,
                    SchoolId = SchoolID,
                    Start_Date = StartDate,
                    End_Date = EndDate,
                    IsCurrentTerm = true,
                    CreatedOn = DateTime.Now,
                    CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID()
                });
                response.Message = "Success";
            }
            catch (SqlException SqlEx)
            {
                if (SqlEx.Number == 2601)
                    response.Message = "Term Details Already Set For this school";
            }
            catch (Exception ex)
            {
                response.Message = "Failed";

            }
            return response;
        }
    }
}