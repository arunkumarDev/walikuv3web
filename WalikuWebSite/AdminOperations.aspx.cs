﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.DataAccess;
using WalikuWebSite.Entities.DataModels;

namespace WalikuWebSite
{
    public partial class AdminOperations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<ConfigurationData> LoadStudentConfigurationData()
        {
            try
            {
                List<ConfigurationData> studentConfig = ConfigurationDataAccess.GetStudentConfigurationFields();
                return studentConfig;
            }
           catch(Exception e)
            {
                return null;
            }

        }

        }
}