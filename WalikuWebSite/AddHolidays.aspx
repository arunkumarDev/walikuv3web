﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master"  CodeBehind="AddHolidays.aspx.cs" Inherits="WalikuWebSite.AddHolidays" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
        <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
                <div class="row">
                        <div class="col-sm-2">
                            <div class="bmd-form-group">
                                <label class="bmd-label-floating">Name</label>
                                <input type="text" class="form-control" id="holname">
                                
                            </div>
                        </div>
                       
                    
                        <div class="col-sm-2 mt-15">
                            <button type="button" class="btn btn-primary" title="" id="btnAdd">Add Holiday</button>
                        </div>
                    </div>
            
            
        </div>
    </main>

     <script>document.write("<script type='text/javascript' src='Scripts/AddHolidays.js?v=" + JsVerion + "'><\/script>");</script>
   

</asp:Content>
