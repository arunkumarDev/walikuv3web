﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalikuWebSite
{
    public partial class SickChildReasonManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Common.CommonFunction.IsUserLoggedIn())
                {
                    Response.Redirect("login");
                }
            }
        }
        [WebMethod]
        public static List<FirstAidMaster> GetAllReason(int PageIndex)
        {
            // var l = SqlHelper.GetAllReason();
            var l = SqlHelper.GetAllFirstAidList();
            
            foreach (var r in l)
            {
                try
                {
                    r.Content = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(r.Content));
                }
                catch(Exception e)
                {

                }
            }
            return l;
        }
        [WebMethod]
        public static void ManageReason(int ID, string Title,string Content, string ContentIndian)
        {
            SqlHelper.ManageReason(new FirstAidMaster()
            {
                ID = ID,
                Title = Title.Trim(),
                Content = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(Content.Trim())),
                ContentIndian = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(ContentIndian.Trim())),
            });
        }
        [WebMethod]
        public static void DeleteReasonByID(int ID)
        {
            SqlHelper.DeleteReasonByID(ID);
        }
    }
}