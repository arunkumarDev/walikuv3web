﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalikuWebSite
{
    public partial class MapOverView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Common.CommonFunction.IsUserLoggedIn())
                {
                    Response.Redirect("login");
                }
            }
        }

        [WebMethod]
        public static Dictionary<string, object> GetData()
        {
            var d = new Dictionary<string, object>();
            try
            {
                var uid = Common.CommonFunction.GetUserIdFromCookieByID();
                if (uid == 0)
                    throw new Exception("UID not valid");
                var udata = SqlHelper.GetUserByID(uid);

                List<SchoolMaster> SchoolList = SqlHelper.GetSchoolListForUser(uid, Common.CommonFunction.GetCurrentCulture());
                List<ClassMaster> ClassList = new List<ClassMaster>();
                List<ChildrenMaster> ChildrenList = new List<ChildrenMaster>();

                if (SchoolList.Count > 0)
                {
                    int SchoolID = SchoolList[0].ID;
                    ClassList = SqlHelper.GetAllClassesForMe(SchoolID, uid);

                    if (ClassList.Count > 0)
                    {
                        int ClassRoomID = ClassList[0].ID;

                        ChildrenList = SqlHelper.GetAllChildrenForMeByFilter(uid, int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()), SchoolID, ClassRoomID,Common.CommonFunction.GetCurrentCulture());
                    }
                }

                d.Add("Schools", SchoolList);

                d.Add("Classes", ClassList);

                d.Add("Children", ChildrenList);
            }
            catch(Exception ex)
            {

            }
            return d;
        }

        [WebMethod]
        public static Dictionary<string, object> GetDataBySchool(int SchoolID, int ClassID)
        {
            var d = new Dictionary<string, object>();
            try
            {

                var uid = Common.CommonFunction.GetUserIdFromCookieByID();
                if (uid == 0)
                    throw new Exception("UID not valid");
                var udata = SqlHelper.GetUserByID(uid);

                List<ChildrenMaster> ChildrenList = new List<ChildrenMaster>();

                if (SchoolID > 0 && ClassID > 0)
                {

                    ChildrenList = SqlHelper.GetAllChildrenForMeByFilter(uid, int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()), SchoolID, ClassID,Common.CommonFunction.GetCurrentCulture());
                }

                d.Add("Children", ChildrenList);
            }
            catch (Exception ex)
            {

            }
            return d;
        }

    }
}