﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace WalikuWebSite
{
    /// <summary>
    /// Descrizione di riepilogo per PhotoUploader
    /// </summary>
    public class PhotoUploader : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string strImageName = string.Empty;
            List<string> outs = new List<string>();
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;

                for (int i = 0; i < files.Count; i++)
                {
                    try
                    {
                        HttpPostedFile file = files[i];
                        if (!file.ContentType.Contains("image"))
                            continue;
                        if (Path.GetExtension(file.FileName) != ".jpg")
                            continue;

                        strImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                        string filePath = context.Server.MapPath("~/Photos/" + strImageName);

                        file.SaveAs(filePath);


                        using (var fStream = System.IO.File.Open(filePath, FileMode.Create))
                        {
                            file.InputStream.CopyTo(fStream);
                        }
                        outs.Add("Photos/" + strImageName);
                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    } 
                }
            }
            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(outs));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}