﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="AcademicYears.aspx.cs" Inherits="WalikuWebSite.AcademicYears" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div id="editTerm" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.ConfigureAcademicYear %></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="bmd-form-group">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_StartDate %></label>
                                <input type="text" class="form-control date-icon hasDatepicker" id="dtStartDate">

                                <i class="fa fa-calendar fa-icons"></i>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="bmd-form-group">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_EndDate %></label>
                                <input type="text" class="form-control date-icon hasDatepicker" id="dtEndDate">

                                <i class="fa fa-calendar fa-icons"></i>
                            </div>
                        </div>
                        <div class="col-sm-6 mt-20">
                            <div class="bmd-form-group">
                                <select class="form-control" id="ddlSchool1">
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bmd-form-group">
                                <label class="bmd-label-floating"><%=Resources.Resource.AcademicYearName %></label>
                                <input type="text" id="txtYearName" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-6 yearlabel mt-20">
                            <label class="switch">
                                <input type="checkbox" id="chkIsCurrentTerm"><span class="slider round"></span></label>
                            <label class="ctsyear"><%=Resources.Resource.IsCurrentYear %></label>
                        </div>
                        <div class="col-md-6 mt-20" id="add">
                            <div class="mt-20  pull-right">
                                <button type="button" class="btn btn-primary" title="" id="btnAdd"><%=Resources.Resource.Lbl_Add %></button>
                            </div>
                        </div>
                        <div class="col-md-6 mt-20" id="update">
                            <div class="mt-20  pull-right">
                                <button type="button" class="btn btn-primary" title="" id="btnUpdate"><%=Resources.Resource.Lbl_Update %></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">

            <div class="page-content">
                <div class="col-sm-6 mt-0" id="schoolsearch">
                    <select class="form-control" id="ddlSchool">
                    </select>
                </div>
                <div class="col-sm-6 mt-0">
                    <div class="pull-right mb-10">
                        <button type="button" class="btn btn-primary" title="" id="btnOpenAdd">
                            <%=Resources.Resource.Lbl_Add %></button>
                    </div>
                </div>
                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-striped table-hover" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th><%=Resources.Resource.Lbl_SerialNo %></th>
                                            <th><%=Resources.Resource.AcademicYearName %></th>
                                            <th><%=Resources.Resource.Lbl_StartDate %></th>
                                            <th><%=Resources.Resource.Lbl_EndDate %></th>
                                            <th id="statusid"><%=Resources.Resource.Lbl_Status %></th>
                                            <th id="actionid"><%=Resources.Resource.Lbl_Action %></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script src="Scripts/bootstrap-datepicker.js"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/AcademicYears.js?v=" + JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.id.min.js"></script>

    <script>
        $('#dtStartDate').datepicker({
            autoclose: true,
            format: 'mm/dd/yyyy',
            language: localStorage.getItem('loglang')

        });
        var date = new Date();
        $('#dtStartDate').datepicker('setDate', date);

        $('#dtEndDate').datepicker({
            autoclose: true,
            format: 'mm/dd/yyyy',
            language: localStorage.getItem('loglang')

        });
        date = new Date();
        $('#dtEndDate').datepicker('setDate', date);

        var Lbl_Active = '<%=Resources.Resource.Lbl_Active %>'

    </script>

</asp:Content>
