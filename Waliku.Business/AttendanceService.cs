﻿using System;
using System.Linq;
using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
namespace Waliku.Business
{
    public class AttendanceService : IAttendanceService
    {
        private readonly IUnitOfWork _uow;
        private readonly IAttendanceRepository _repository;
        private readonly IReasonRepository _reasonRepository;
        public AttendanceService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = _uow.AttendanceRepository;
            _reasonRepository = _uow.ReasonRepository;
        }

        public VmAbsenteeReason GetAbsenteeReasonDetails(int childId, int classroomId, DateTime createOn) => _repository.GetAbsenteeReasonDetails(childId, classroomId, createOn);

        public AttendanceMaster GetAttendanceMasterDetails(int id) => _repository.GetAttendanceMasterDetails(id);

        public bool IsAttendanceExists(int childId, int classRoomId, DateTime attendanceDate) => _repository.IsAttendanceExists(childId, classRoomId, attendanceDate);

        public VmAbsenceReasonResponse ManageStudentAbsence(VmAbsenteeReason absence, string currentCulture, string toCulture)
        {
            var absenceReasonResponse = new VmAbsenceReasonResponse();
            var absenteeReason = _repository.GetAbsenteeReasonDetails(absence.ChildID, absence.ClassroomID, absence.CreatedOn);
            absence.ID = absenteeReason.ID;
            absence.CreatedBy = absence.TeacherID;
            if (!absence.IsPresent)
            {
                var reasonTypes = _reasonRepository.GetAllReasonType(currentCulture);
                string reasonType = reasonTypes.SingleOrDefault(x => x.ID == absence.ReasonType)?.Code.ToLower();
                var reasons = _reasonRepository.GetAllReasonMasterByReasonType(absence.ReasonType, currentCulture);
                foreach (var reason in reasons)
                {
                    if (reason.ID == absence.ReasonID && reason.Description.Trim().ToLower() != "other")
                    {
                        absence.Notes = string.Empty;
                        break;
                    }
                    if (reason.Description.Trim().ToLower() == "unknown" && reasonType == "unknown")
                    {
                        absence.ReasonID = reason.ID;
                        break;
                    }
                }
                if (reasonType == "unknown")
                {
                    absence.IsHealthReason = false;
                    absence.InformationProvider = 0;
                    absence.HasPermission = false;
                    absence.Notes = string.Empty;
                }
                else if (reasonType == "health")
                {
                    absence.IsHealthReason = true;
                    absence.HasPermission = false;
                }
                else if (reasonType == "non-health")
                {
                    absence.IsHealthReason = false;
                }

                //  ObjAbsence.CreatedOn = DateTime.Now;

                Dictionary<string, string> dict = GoogleTranslator.GoogleTranslator.TranslateText(absence.Notes, currentCulture, toCulture);
                absence.Notes = dict["en"];
                absence.Notes_Indonesian = dict["id"];

                bool result = _repository.ManageStudentAbsence(absence);
                if (result)
                {
                    absenceReasonResponse.ClassID = absence.ClassroomID;
                    absenceReasonResponse.TeacherID = absence.TeacherID;
                    absenceReasonResponse.StudentID = absence.ChildID;
                    absenceReasonResponse.Date = absence.CreatedOn;
                }
            }
            _uow.Commit();
            return absenceReasonResponse;
        }

        public bool ManageStudentAttendance(List<AttendanceMaster> attendances)
        {
            _repository.ManageStudentAttendance(attendances);
            _uow.Commit();
            return true;
        }

        public VmResponseData ManageStudentAttendance(VmAbsenteeReason absence, string currentCulture, string toCulture)
        {
            var responseData = new VmResponseData();
            VmAbsenteeReason tmpAbsenteeReason = _repository.GetAbsenteeReasonDetails(absence.ChildID, absence.ClassroomID, absence.CreatedOn);
            absence.ID = tmpAbsenteeReason.ID;
            absence.CreatedBy = absence.CreatedBy;
            absence.TeacherID = absence.CreatedBy;
            if (!absence.IsPresent)
            {
                var reasonTypes = _reasonRepository.GetAllReasonType(currentCulture);
                string reasonType = reasonTypes.SingleOrDefault(x => x.ID == absence.ReasonType)?.Code.ToLower();
                var reasons = _reasonRepository.GetAllReasonMasterByReasonType(absence.ReasonType, currentCulture);
                foreach (var reason in reasons)
                {
                    if (reason.ID == absence.ReasonID && reason.Description.Trim().ToLower() != "other")
                    {
                        absence.Notes = string.Empty;
                        break;
                    }
                    if (reason.Description.Trim().ToLower() == "unknown" && reasonType == "unknown")
                    {
                        absence.ReasonID = reason.ID;
                        break;
                    }
                }
                if (reasonType == "unknown")
                {
                    absence.IsHealthReason = false;
                    absence.InformationProvider = 0;
                    absence.HasPermission = false;
                    absence.Notes = string.Empty;
                }
                else if (reasonType == "health")
                {
                    absence.IsHealthReason = true;
                    absence.HasPermission = false;
                }
                else if (reasonType == "non-health")
                {
                    absence.IsHealthReason = false;
                }


                Dictionary<string, string> dict = GoogleTranslator.GoogleTranslator.TranslateText(absence.Notes, currentCulture, toCulture);
                absence.Notes = dict["en"];
                absence.Notes_Indonesian = dict["id"];

                _repository.ManageStudentAbsence(absence);
                responseData.Message = "Success";
            }
            else
            {
                var attendances = new List<AttendanceMaster>();
                var attendance = _repository.GetAttendanceMasterDetails(absence.AttendanceMasterID);
                attendance.IsPresent = true;
                attendances.Add(attendance);
                _repository.ManageStudentAttendance(attendances);
                responseData.Message = "Success";
            }
            _uow.Commit();
            return responseData;
        }
    }
}
