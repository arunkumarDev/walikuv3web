﻿using System;
using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel.ViewModel;

namespace Waliku.Business
{
    public class FeedbackService : IFeedbackService
    {
        private readonly IUnitOfWork _uow;
        private readonly IFeedbackRepository _repository;
        public FeedbackService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.FeedbackRepository;
        }

        public IEnumerable<VMFeedBack> GetFeedBackData(int schoolId, DateTime starDate, DateTime endDate) => _repository.GetFeedBackData(schoolId, starDate, endDate);

        public IEnumerable<WebModel.VmFeedBack> GetFeedBackData(int schoolId, DateTime starDate, DateTime endDate, string langId)
            => _repository.GetFeedBackData(schoolId, starDate, endDate, langId);

        public VmFeedBack Manage(FeedBack entity, string currentCulture, string toCulture)
        {
            var transalationDictionary = GoogleTranslator.GoogleTranslator.TranslateText(entity.Content, currentCulture, toCulture);

            entity.Content = transalationDictionary["en"];
            entity.CreatedOn = DateTime.Now;
            entity.Content_Indonesian = transalationDictionary["id"];
            _repository.Manage(entity);
            _uow.Commit();
            return new VmFeedBack() { Message = "Success" };
        }
                
              public IEnumerable<FeedBackTypeMaster> GetFeedBackTypes(string langId)  => _repository.GetFeedBackTypes(langId);

    }
}
