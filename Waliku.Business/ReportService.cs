﻿using System;
using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Business
{
    public class ReportService : IReportService
    {
        private readonly IReportRepository _repository;
        public ReportService(IUnitOfWork uow)
        {
            _repository = uow.ReportRepository;
        }

     

        public IEnumerable<WebModel.AbsentOrPresentMaster> GetClassPresentsOrAbsentsChildren(string classRoomId, DateTime selectedDate)
            => _repository.GetClassPresentsOrAbsentsChildren(classRoomId, selectedDate);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReport(int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetClassWisePresentAbsentReport(classId, selectedDate, selectedType, teacherId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportForAdmin(int schoolId, int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetClassWisePresentAbsentReportForAdmin(schoolId, classId, selectedDate, selectedType, teacherId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportOnload(string selectedDate, int selectedType, int teacherId)
            => _repository.GetClassWisePresentAbsentReportOnload(selectedDate, selectedType, teacherId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetHolidayList(int classId, string selectedDate, int selectedType)
            => _repository.GetHolidayList(classId, selectedDate, selectedType);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummary(int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetMonthlyAttendanceRateSummary(classId, selectedDate, selectedType, teacherId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForAdmin(int schoolId, int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetMonthlyAttendanceRateSummaryForAdmin(schoolId, classId, selectedDate, selectedType, teacherId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetMonthlyAttendanceRateSummaryForSa(schoolId, classId, selectedDate, selectedType, teacherId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonCount(int classId, string selectedDate, int selectedType, int teacherId, string langId)
            => _repository.GetMonthlyReasonCount(classId, selectedDate, selectedType, teacherId, langId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonCount2(string selectedDate)
            => _repository.GetMonthlyReasonCount2(selectedDate);

        public IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId)
            => _repository.GetMonthlyReasonCountForSa(schoolId, classId, selectedDate, selectedType, teacherId, langId);

        public IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountNew(int classId, string selectedDate, int selectedType, int teacherId, string langId)
            => _repository.GetMonthlyReasonCountNew(classId, selectedDate, selectedType, teacherId, langId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonReportRate(int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetMonthlyReasonReportRate(classId, selectedDate, selectedType, teacherId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsent(int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetMonthlyStudentAbsent(classId, selectedDate, selectedType, teacherId);

        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsentForSa(int schoolId, int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetMonthlyStudentAbsentForSa(schoolId, classId, selectedDate, selectedType, teacherId);

        public IEnumerable<VmChildrenReport> GetStudentWisePresentAbsentReport(int classId, string selectedDate, int selectedType, int teacherId)
            => _repository.GetStudentWisePresentAbsentReport(classId, selectedDate, selectedType, teacherId);

        public WebViewModel.VmYearlyReport GetTeacherYearly(int classId, int teacherId)
            => _repository.GetTeacherYearly(classId, teacherId);

        public IEnumerable<dynamic> GetYearlyHealthReason(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId)
            => _repository.GetYearlyHealthReason(schoolId, classId, selectedDate, selectedType, teacherId, langId);

        public IEnumerable<dynamic> GetYearlyNonHealthReason(int schoolId, int classId, string selectedDate, int selectedType, int teacherId, string langId)
            => _repository.GetYearlyNonHealthReason(schoolId, classId, selectedDate, selectedType, teacherId, langId);

        public IEnumerable<YearlyTopFiveAbsenteesSummary> GetYearlyTopFiveHealthChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int ReasonType, string LangID)
        => _repository.GetYearlyTopFiveHealthChartReport(TeacherID, AttendanceDate, ClassRoomID, ReasonType, LangID);

        public IEnumerable<YearlyAbsenteesSummary> GetYearlyAbsenteesChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID)
        => _repository.GetYearlyAbsenteesChartReport(TeacherID, AttendanceDate, ClassRoomID);

        public IEnumerable<YearlyChronicAbsenteesSummary> GetYearlyChronicAbsentChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID)
        => _repository.GetYearlyChronicAbsentChartReport(TeacherID, AttendanceDate, ClassRoomID);


        public List<ChildDashboardSummary> GetChildDashboardSummary(int ChildID, DateTime AttendanceDate)
        => _repository.GetChildDashboardSummary(ChildID, AttendanceDate);

       public List<ReasonSummary> GetChildReasonSummary(int ChildID, DateTime AttendanceDate, string langId)
       => _repository.GetChildReasonSummary(ChildID, AttendanceDate, langId);

        public IEnumerable<ClassDashboardSummary> GetWeeklyChartReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
            => _repository.GetWeeklyChartReport(TeacherID, AttendanceDate, ClassRoomID, Type, LangID);

        public IEnumerable<ClassDashboardSummary> GetClassDashboardReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
            => _repository.GetClassDashboardReport(TeacherID, AttendanceDate, ClassRoomID, Type, LangID);
        public IEnumerable<ClassTopReasonsForAbsence> GetClassTopReasonsForAbsence(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
            => _repository.GetClassTopReasonsForAbsence(TeacherID, AttendanceDate, ClassRoomID, Type, LangID);
        public IEnumerable<ChildDashboardSummary> GetMonthlyListReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type)
        => _repository.GetMonthlyListReport(TeacherID, AttendanceDate, ClassRoomID, Type);

        public SummaryData GetMonthlySummaryReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
        => _repository.GetMonthlySummaryReport(TeacherID, AttendanceDate, ClassRoomID, Type, LangID);

        public IEnumerable<VmChildrenReport> GetWeeklyListReport(int ClassRoomID, int TeacherID, DateTime AttendanceDate, int Type)
        => _repository.GetWeeklyListReport(ClassRoomID, TeacherID, AttendanceDate, Type);

        public SummaryData GetWeeklySummaryReport(int TeacherID, DateTime AttendanceDate, int ClassRoomID, int Type, string LangID)
        => _repository.GetWeeklySummaryReport(TeacherID, AttendanceDate, ClassRoomID, Type, LangID);
    }
}
