﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business
{
    public class ClassService : IClassService
    {
        private readonly IUnitOfWork _uow;
        private readonly IClassRepository _repository;
        public ClassService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.ClassRepository;
        }

        public void DeleteClassById(int classId) => _repository.DeleteClassById(classId);

        public IEnumerable<ClassMaster> GetAllClassBySchool(int schoolId) => _repository.GetAllClassBySchool(schoolId);

        public IEnumerable<WebModel.ClassMaster> GetAllClasses() => _repository.GetAllClasses();

        public IEnumerable<WebModel.ClassMaster> GetAllClassesForMe(int schoolId, int userId) => _repository.GetAllClassesForMe(schoolId, userId);

        public IEnumerable<WebModel.TeacherClassMapping> GetAllTeacherClasses(int userId, bool flag) => _repository.GetAllTeacherClasses(userId, flag);

        public IEnumerable<WebModel.ClassMaster> GetAllTeacherClasses(int teacherId) => _repository.GetAllTeacherClasses(teacherId);

        public IEnumerable<ClassMaster> GetClassesByTeacherId(int teacherId) => _repository.GetClassesByTeacherId(teacherId);

        public void ManageClass(WebModel.ClassMaster classMaster)
        {
            _repository.ManageClass(classMaster);
            _uow.Commit();
        }

       public IEnumerable<VmClassAttendanceSummary> GetMyClassAttendanceSummary(int TeacherId, int ClassRoomId) => 
            _repository.GetMyClassAttendanceSummary(TeacherId, ClassRoomId);
    }
}
