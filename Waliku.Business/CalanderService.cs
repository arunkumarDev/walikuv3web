﻿using System;
using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;
using System.Linq;
namespace Waliku.Business
{
    public class CalanderService : ICalendarService
    {
        private readonly IUnitOfWork _uow;
        private readonly ICalendarRepository _repository;
        public CalanderService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.CalendarRepository;
        }

        public IList<CalendarEvents> GetAllCalendarEventsBySchool(string LanguageID, int SchoolID, DateTime calendarDate)
        {
            return _repository.GetAllCalendarEventsBySchool(LanguageID, SchoolID, calendarDate).ToList();
        }

    }
}
