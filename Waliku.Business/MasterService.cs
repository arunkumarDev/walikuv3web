﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Business
{
    public class MasterService : IMasterService
    {
        private readonly IMasterRepository _repository;
        public MasterService(IUnitOfWork uow)
        {
            _repository = uow.MasterRepository;
        }

        public IEnumerable<ReasonType> GetAllActiveReasonTypes() => _repository.GetAllActiveReasonTypes();

        public IEnumerable<ReasonType> GetAllReasonType() => _repository.GetAllReasonType();

        public IEnumerable<SickChildMaster> GetAllSickChildReason() => _repository.GetAllSickChildReason();

        public IEnumerable<vmRole> GetAllActiveRoles() => _repository.GetAllActiveRoles();

        public IEnumerable<vmRole> GetUnderprivilegedRoles(string roleName) => _repository.GetUnderprivilegedRoles(roleName);

        public IEnumerable<vmDistrict> GetAllActiveDistricts() => _repository.GetAllActiveDistricts();
    }
}
