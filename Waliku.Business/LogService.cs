﻿using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;

namespace Waliku.Business
{
    public class LogService : ILogService
    {
        private readonly IUnitOfWork _uow;
        private readonly ILogRepository _repository;
        public LogService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.LogRepository;
        }

        public long AddErrorLog(Error error)
        {
            var result = _repository.AddErrorLog(error);
            _uow.Commit();
            return result;
        }
        public long AddActionLog(Action action)
        {
            var result = _repository.AddActionLog(action);
            _uow.Commit();
            return result;
        }
        public long AddAuditLog(Audit audit)
        {
            var result = _repository.AddAuditLog(audit);
            _uow.Commit();
            return result;
        }
    }
}
