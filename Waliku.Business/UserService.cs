﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _uow;
        private readonly IUserRepository _repository;
        public UserService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.UserRepository;
        }

        public IEnumerable<UserMaster> GetAllCommunityWorker() => _repository.GetAllCommunityWorker();
        public IEnumerable<UserMaster> GetAllActiveUsers() => _repository.GetAllActiveUsers();
        public IEnumerable<UserMaster> GetUsersByType(int userType) => _repository.GetUsersByType(userType);

        public bool Ping() => _repository.Ping() == 1;

        public UserMaster ValidateUser(string userName, string password) => _repository.ValidateUser(userName, password);

        public dynamic GetUserById(int userId) => _repository.GetUserById(userId);

        public void DeleteUserById(int userId)
        {
            _repository.DeleteUserById(userId);
            _uow.Commit();
        }

        public IEnumerable<WebModel.WebUserMaster> GetMyUserList(int userId, string langId) => _repository.GetMyUserList(userId, langId);

        public IEnumerable<UserMaster> GetAllUsers() => _repository.GetAllUsers();

        public IEnumerable<WebModel.WebUserMaster> GetAllTeachers() => _repository.GetAllTeachers();

        public IEnumerable<WebModel.SchoolAdminMappingMaster> GetAllSchoolAdmin() => _repository.GetAllSchoolAdmin();

        public IEnumerable<WebModel.SchoolAdminMapping> GetSchoolAdminMapping(int schoolId) => _repository.GetSchoolAdminMapping(schoolId);

        public IEnumerable<WebModel.WebUserMaster> GetTeacherClassMappingDetails(int classRoomId) => _repository.GetTeacherClassMappingDetails(classRoomId);

        public IEnumerable<WebModel.UserTypeMaster> GetAllUserTypes() => _repository.GetAllUserTypes();

        public IEnumerable<WebModel.UserTypeMaster> GetAllUserTypes(int userType) => _repository.GetAllUserTypes(userType);

        public void ManageUser(WebModel.WebUserMaster userMaster)
        {
            _repository.ManageUser(userMaster);
            _uow.Commit();
        }

        public void ManageAdmin(WebModel.SchoolAdminMappingMaster schoolAdmin)
        {
            _repository.ManageAdmin(schoolAdmin);
            _uow.Commit();
        }

        public void ManageAdminMapping(WebModel.SchoolAdminMapping schoolAdminMapping)
        {
            _repository.ManageAdminMapping(schoolAdminMapping);
            _uow.Commit();
        }

        public void ManageClassTeacher(WebModel.TeacherClassMapping teacherClassMapping)
        {
            _repository.ManageClassTeacher(teacherClassMapping);
            _uow.Commit();
        }

        public void DeleteSchoolAdminById(int id)
        {
            _repository.DeleteSchoolAdminById(id);
            _uow.Commit();
        }

        public void DeleteClassTeacherById(int id)
        {
            _repository.DeleteClassTeacherById(id);
            _uow.Commit();
        }
    }
}
