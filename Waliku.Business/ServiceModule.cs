﻿using Autofac;
using Waliku.Business;
using Waliku.Business.Interface;
using Module = Autofac.Module;

namespace Waliku.Data
{
    public class ServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AcademicTermService>().As<IAcademicTermService>();
            builder.RegisterType<AttendanceService>().As<IAttendanceService>();
            builder.RegisterType<ClassService>().As<IClassService>();
            builder.RegisterType<CommonService>().As<ICommonService>();
            builder.RegisterType<FeedbackService>().As<IFeedbackService>();
            builder.RegisterType<FirstAidService>().As<IFirstAidService>();
            builder.RegisterType<HolidayService>().As<IHolidayService>();
            builder.RegisterType<MasterService>().As<IMasterService>();
            builder.RegisterType<ReasonService>().As<IReasonService>();
            builder.RegisterType<ReportService>().As<IReportService>();
            builder.RegisterType<SchoolService>().As<ISchoolService>();
            builder.RegisterType<StudentService>().As<IStudentService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<VisitorHomeService>().As<IVisitorHomeService>();
            builder.RegisterType<CalanderService>().As<ICalendarService>();

            #region "Web"
            builder.RegisterType<PermissionService>().As<IPermissionService>();
            #endregion

            builder.RegisterType<CalanderService>().As<ICalendarService>();
            builder.RegisterType<ChildAssesmentService>().As<IChildAssesmentService>();
            builder.RegisterType<LogService>().As<ILogService>();
        }
    }
}
