﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
namespace Waliku.Business
{
    public class ConfigurationService : IConfigurationService
    {
        private readonly IConfigurationRepository _repository;
        public ConfigurationService(IUnitOfWork uow)
        {
            _repository = uow.ConfigurationRepository;
        }


        public IEnumerable<ConfigurationGroup> GetAllConfigurationGroup() => _repository.GetAllConfigurationGroup();

        public ConfigurationGroup GetConfigurationGroupById(int id) => _repository.GetConfigurationGroupById(id);

        public IEnumerable<Configuration> GetAllConfigurations() => _repository.GetAllConfigurations();

        public IEnumerable<Configuration> GetConfigurationByGroupId(int configurationGroupId) =>
            _repository.GetConfigurationByGroupId(configurationGroupId);

        public IEnumerable<Configuration> GetConfigurationByGroupName(string configurationGroupName) =>
            _repository.GetConfigurationByGroupName(configurationGroupName);

        public Configuration GetConfigurationByKey(string key) => _repository.GetConfigurationByKey(key);
    }
}
