﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Business
{
    public class FirstAidService : IFirstAidService
    {
        private readonly IUnitOfWork _uow;
        private readonly IFirstAidRepository _repository;
        public FirstAidService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.FirstAidRepository;
        }

        public IEnumerable<WebModel.FirstAidMaster> GetAllFirstAidList() => _repository.GetAllFirstAidList();

        public IEnumerable<WebViewModel.VmFirstAidAnalytics> GetFirstAidCounterList() => _repository.GetFirstAidCounterList();

        public bool ManageFirstAidAnalytics(FirstAidAnlytics entity)
        {
            _repository.ManageFirstAidAnalytics(entity);
            _uow.Commit();
            return true;
        }
    }
}
