﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using WebModel = Waliku.Domain.Models.WebModel;

namespace Waliku.Business
{
    public class AcademicTermService : IAcademicTermService
    {
        private readonly IUnitOfWork _uow;
        private readonly IAcademicTermRepository _repository;
        public AcademicTermService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = _uow.AcademicTermRepository;
        }

        public bool DeleteAcademicTermById(int id)
        {
            var result = _repository.DeleteAcademicTermById(id);
            _uow.Commit();
            return result;
        }

        public IEnumerable<WebModel.DataModels.AcademicTerm> GetAcademicTermDetails(int userId, int userType) => _repository.GetAcademicTermDetails(userId, userType);

        public bool IsCurrentTermExist(int schoolId) => _repository.IsCurrentTermExist(schoolId);

        public void ManageAcademicTerm(WebModel.DataModels.AcademicTerm academicTerm)
        {
            _repository.ManageAcademicTerm(academicTerm);
            _uow.Commit();
        }
    }
}
