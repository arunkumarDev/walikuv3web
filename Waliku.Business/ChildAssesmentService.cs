﻿using System;
using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Business
{
    public class ChildAssesmentService : IChildAssesmentService
    {
        private readonly IUnitOfWork _uow;
        private readonly IChildAssesmentRepository _repository;
        public ChildAssesmentService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.childAssesmentRepository;
        }


        public bool CheckIfChildIsAbsentForThreedays(int ChildID, DateTime AttendanceDate)
       => _repository.CheckIfChildIsAbsentForThreedays(ChildID, AttendanceDate);

        public List<ExplanatoryNotes> GetAllExplanatoryNotes(int TeacherID, int AssesmentType, string LangId)
        => _repository.GetAllExplanatoryNotes(TeacherID, AssesmentType, LangId);

        public List<SubComplaintAdvice> GetAllMappedAdvices(int TeacherID, int AssesmentType, string LangId)
               => _repository.GetAllMappedAdvices(TeacherID, AssesmentType, LangId);


        public List<Recommendations> GetAllRecommendations(int TeacherID, int AssesmentType, string LangId)
       => _repository.GetAllRecommendations(TeacherID, AssesmentType, LangId);

        public List<SubComplaintsMaster> GetAllSubComplaints(int TeacherID, int AssesmentType, string LangId)
               => _repository.GetAllSubComplaints(TeacherID, AssesmentType, LangId);


        public List<ChildAssesmentDetails> GetChildAssesmentDetails(int TeacherID, int AssesmentType, string LangId) 
            => _repository.GetChildAssesmentDetails(TeacherID, AssesmentType, LangId);

        
        public VmChildAssesmentResponse UpdateChildAssesmentDetails(VmChildAssesmentSummary summary)
        {
           
            _repository.UpdateChildAssesmentDetails(summary);
            _uow.Commit();
            return new VmChildAssesmentResponse() { Message = "Success" };
        }
    }
}
