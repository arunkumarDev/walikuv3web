﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business.Interface
{
    public interface IUserService
    {
        bool Ping();
        UserMaster ValidateUser(string userName, string password);
        IEnumerable<UserMaster> GetUsersByType(int userType);
        IEnumerable<UserMaster> GetAllCommunityWorker();
        IEnumerable<UserMaster> GetAllActiveUsers();

        dynamic GetUserById(int userId);
        void DeleteUserById(int userId);
        IEnumerable<WebModel.WebUserMaster> GetMyUserList(int userId, string langId);
        IEnumerable<UserMaster> GetAllUsers();
        IEnumerable<WebModel.WebUserMaster> GetAllTeachers();
        IEnumerable<WebModel.SchoolAdminMappingMaster> GetAllSchoolAdmin();
        IEnumerable<WebModel.SchoolAdminMapping> GetSchoolAdminMapping(int schoolId);
        IEnumerable<WebModel.WebUserMaster> GetTeacherClassMappingDetails(int classRoomId);
        IEnumerable<WebModel.UserTypeMaster> GetAllUserTypes();
        IEnumerable<WebModel.UserTypeMaster> GetAllUserTypes(int userType);
        void ManageUser(WebModel.WebUserMaster userMaster);
        void ManageAdmin(WebModel.SchoolAdminMappingMaster schoolAdmin);
        void ManageAdminMapping(WebModel.SchoolAdminMapping schoolAdminMapping);
        void ManageClassTeacher(WebModel.TeacherClassMapping teacherClassMapping);
        void DeleteSchoolAdminById(int id);
        void DeleteClassTeacherById(int id);
    }
}