﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business.Interface
{
    public interface IReasonService
    {
        IEnumerable<ReasonType> GetAllReasonType(string langId);
        IEnumerable<ReasonMaster> GetAllReasonMasterByReasonType(int reasonType, string langId);
        IEnumerable<AbsenceInformerMaster> GetAllAbsenceInformerMaster(string langId); 
        IEnumerable<WebModel.ReasonMaster> GetAllReasonMaster(string langId);
        void ManageReasonType(WebModel.ReasonMaster reason);
        void ManageReason(WebModel.FirstAidMaster reason);
        void DeleteReasonTypeById(int reasonTypeId);
        void DeleteReasonById(int reasonId);
    }
}