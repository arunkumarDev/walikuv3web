﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Business.Interface
{
    public interface IAttendanceService
    {
        VmResponseData ManageStudentAttendance(VmAbsenteeReason absence, string currentCulture, string toCulture);
        VmAbsenceReasonResponse ManageStudentAbsence(VmAbsenteeReason absence, string currentCulture, string toCulture);
        bool ManageStudentAttendance(List<AttendanceMaster> attendances);
        VmAbsenteeReason GetAbsenteeReasonDetails(int childId, int classroomId, DateTime createOn);
        AttendanceMaster GetAttendanceMasterDetails(int id);
        bool IsAttendanceExists(int childId, int classRoomId, DateTime attendanceDate);
    }
}