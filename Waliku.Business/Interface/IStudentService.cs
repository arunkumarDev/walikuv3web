﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business.Interface
{
    public interface IStudentService
    {
        IEnumerable<VmChildren> GetAbsentChildrenByClassId(int classId, DateTime startDate, DateTime endDate);
        IEnumerable<VmChildren> GetAbsentChildrenReport(int classId, DateTime startDate, DateTime endDate);
        IEnumerable<ChildrenMaster> GetAllChildren();
        ChildrenMaster GetChildDetailById(int childId);
        IEnumerable<VmChildren> GetChildrenByClassId(int classId, DateTime attendanceDate);
        IEnumerable<ChildrenMaster> GetChildrenByTeacherId(int teacherId);
        IEnumerable<AttendanceMaster> GetChildrenByTeacherIdAndClassIdAndDateWise(int teacherId, int classRoomId, DateTime attendanceDate);
        VmCount GetTeacherClassCountAndAbsenceCount(int teacherId, DateTime attendanceDate);
        void ManageChildren(ChildrenMaster entity);
        IEnumerable<ChildrenMaster> GetAllChildrenForMe(int userId, int userType, string langId);
        IEnumerable<ChildrenMaster> GetAllChildrenForMeByFilter(int userId, int userType, int schoolId, int classRoomId, string langId);
        string GetChildImageById(int childId);
        void DeleteChildById(int childId);
        VmChildren GetChildDetailsById(int childId);
        List<VMChildAbsenceDetails> GetStudentDashboardAbsenceDetails(int childId);
    }
}