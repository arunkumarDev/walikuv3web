﻿using Waliku.Domain.Models;

namespace Waliku.Business.Interface
{
    public interface ILogService
    {
        long AddErrorLog(Error error);
        long AddActionLog(Action action);
        long AddAuditLog(Audit audit);        
    }
}