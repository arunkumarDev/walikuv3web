﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
namespace Waliku.Business.Interface
{
    public interface ICalendarService
    {
        IList<CalendarEvents> GetAllCalendarEventsBySchool(string LanguageID, int SchoolID, DateTime calendarDate);
    }
}