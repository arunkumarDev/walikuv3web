﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Business.Interface
{
    public interface IFeedbackService
    {
        VmFeedBack Manage(FeedBack entity, string currentCulture, string toCulture);
        IEnumerable<VMFeedBack> GetFeedBackData(int schoolId, DateTime starDate, DateTime endDate);
        IEnumerable<WebModel.VmFeedBack> GetFeedBackData(int schoolId, DateTime starDate, DateTime endDate, string langId);

        IEnumerable <FeedBackTypeMaster> GetFeedBackTypes(string langId);
    }
}