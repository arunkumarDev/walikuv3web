﻿using System.Collections.Generic;
using Waliku.Domain.Models;

namespace Waliku.Business.Interface
{
    public interface IVisitorHomeService
    {
        void ManageVisitHome(VisitHomeMaster visitHomeMaster);
        IEnumerable<VisitHomeMaster> GetVisitHomeDetailByTeacherId(int teacherId);
    }
}