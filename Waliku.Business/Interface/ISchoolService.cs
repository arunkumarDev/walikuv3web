﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business.Interface
{
    public interface ISchoolService
    {
        IEnumerable<SchoolMaster> GetAllSchool();
        IEnumerable<SchoolMaster> GetSchoolListForUser(int userId, string langId);
        int ManageSchool(WebModel.SchoolMaster schoolMaster);
        void DeleteSchoolById(int schoolId);
        IEnumerable<SchoolMaster> GetDistrictSchools(string district);
    }
}