﻿using System.Collections.Generic;
using WebModel = Waliku.Domain.Models.WebModel.DataModels;

namespace Waliku.Business.Interface
{
    public interface IAcademicTermService
    {
        IEnumerable<WebModel.AcademicTerm> GetAcademicTermDetails(int userId, int userType);
        bool DeleteAcademicTermById(int id);
        void ManageAcademicTerm(WebModel.AcademicTerm academicTerm);
        bool IsCurrentTermExist(int schoolId);
    }
}