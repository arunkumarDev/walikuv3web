﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Business.Interface
{
   public interface IChildAssesmentService
    {
        bool CheckIfChildIsAbsentForThreedays(int ChildID, DateTime AttendanceDate);

        List<ChildAssesmentDetails> GetChildAssesmentDetails(int TeacherID, int AssesmentType, string LangId);

       List<ExplanatoryNotes> GetAllExplanatoryNotes(int TeacherID, int AssesmentType, string LangId);
        List<Recommendations> GetAllRecommendations(int TeacherID, int AssesmentType, string LangId);
        List<SubComplaintsMaster> GetAllSubComplaints(int TeacherID, int AssesmentType, string LangId);
        List<SubComplaintAdvice> GetAllMappedAdvices(int TeacherID, int AssesmentType, string LangId);

        VmChildAssesmentResponse UpdateChildAssesmentDetails(VmChildAssesmentSummary summary);

    }
}
