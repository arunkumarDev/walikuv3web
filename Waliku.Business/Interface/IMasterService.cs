﻿using System.Collections.Generic;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Business.Interface
{
    public interface IMasterService
    {
        IEnumerable<SickChildMaster> GetAllSickChildReason();
        IEnumerable<ReasonType> GetAllReasonType();
        IEnumerable<ReasonType> GetAllActiveReasonTypes();
        IEnumerable<vmRole> GetAllActiveRoles();
        IEnumerable<vmRole> GetUnderprivilegedRoles(string roleName);
        IEnumerable<vmDistrict> GetAllActiveDistricts();
    }
}