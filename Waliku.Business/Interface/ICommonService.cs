﻿using System.Collections.Generic;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Business.Interface
{
    public interface ICommonService
    {
        IDictionary<string, object> GetOfflineData(int userId, string currentCulture);
        IDictionary<string, object> GetOfflineData(ChildRequestParams childRequest, string currentCulture);

        #region "V2 Offline"
        IDictionary<string, object> GetOfflineDataV2(OfflineDataRequestParams childRequest);
        IDictionary<string, object> GetAssessmentOfflineDataV2(OfflineDataRequestParams childRequest);
        #endregion
    }
}