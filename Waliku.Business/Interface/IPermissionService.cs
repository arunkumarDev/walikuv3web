﻿using System.Collections.Generic;
using Waliku.Domain.Models;
namespace Waliku.Business.Interface
{
    public interface IPermissionService
    {
        RoleMatrix GetRoleMatrix(int[] roleIds);
        RoleMatrix GetUserRoleMatrix(string userName);
        RoleMatrix GetAllRoleMatrix();
        IEnumerable<Component> GetUserComponents(string userName);
        IEnumerable<Component> GetUserRoleComponents(string userName, string roleName);
        bool SavePermission(IEnumerable<Permission> permissions);
        IEnumerable<vmRole> GetUserRoles(string userName);
    }
}