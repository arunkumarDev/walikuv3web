﻿using System.Collections.Generic;
using Waliku.Domain.Models;
namespace Waliku.Business.Interface
{
    public interface IConfigurationService
    {
        IEnumerable<ConfigurationGroup> GetAllConfigurationGroup();
        ConfigurationGroup GetConfigurationGroupById(int id);
        IEnumerable<Configuration> GetAllConfigurations();
        IEnumerable<Configuration> GetConfigurationByGroupId(int configurationGroupId);
        IEnumerable<Configuration> GetConfigurationByGroupName(string configurationGroupName);
        Configuration GetConfigurationByKey(string key);
    }
}