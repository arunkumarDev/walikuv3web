﻿using System;
using System.Collections.Generic;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business.Interface
{
    public interface IHolidayService
    {
        IEnumerable<YearlyHolidayMaster> GetHolidayListByMonthForTeacher(DateTime attendanceDate, int userId, int schoolID, string currentCulture);
        IEnumerable<WebModel.AcademicCalenderMaster> GetAllHoliday(int schoolId, string langIg);
        void ManageHoliday(WebModel.AcademicCalenderMaster academicCalender);
        void DeleteHolidayById(int id);
    }
}