﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business
{
    public class ReasonService : IReasonService
    {
        private readonly IUnitOfWork _uow;
        private readonly IReasonRepository _repository;
        public ReasonService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.ReasonRepository;
        }

        public void DeleteReasonById(int reasonId)
        {
            _repository.DeleteReasonById(reasonId);
            _uow.Commit();
        }

        public void DeleteReasonTypeById(int reasonTypeId)
        {
            _repository.DeleteReasonTypeById(reasonTypeId);
            _uow.Commit();
        }

        public IEnumerable<AbsenceInformerMaster> GetAllAbsenceInformerMaster(string langId) => _repository.GetAllAbsenceInformerMaster(langId);

        public IEnumerable<WebModel.ReasonMaster> GetAllReasonMaster(string langId) => _repository.GetAllReasonMaster(langId);

        public IEnumerable<ReasonMaster> GetAllReasonMasterByReasonType(int reasonType, string langId) => _repository.GetAllReasonMasterByReasonType(reasonType, langId);

        public IEnumerable<ReasonType> GetAllReasonType(string langId) => _repository.GetAllReasonType(langId);

        public void ManageReason(WebModel.FirstAidMaster reason)
        {
            _repository.ManageReason(reason);
            _uow.Commit();
        }

        public void ManageReasonType(WebModel.ReasonMaster reason)
        {
            _repository.ManageReasonType(reason);
            _uow.Commit();
        }
    }
}
