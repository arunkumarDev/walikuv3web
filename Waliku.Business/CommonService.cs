﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Business
{
    public class CommonService : ICommonService
    {
        private readonly IClassRepository _classRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IReasonRepository _reasonRepository;
        private readonly IChildAssesmentRepository _childAssesmentRepository;
        private readonly IFeedbackRepository _feedbackRepository;
        private readonly IReportRepository _reportRepository;
        public CommonService(IUnitOfWork uow)
        {
            _classRepository = uow.ClassRepository;
            _studentRepository = uow.StudentRepository;
            _reasonRepository = uow.ReasonRepository;
            _childAssesmentRepository = uow.childAssesmentRepository;
            _feedbackRepository = uow.FeedbackRepository;
            _reportRepository = uow.ReportRepository;
        }


        public IDictionary<string, object> GetOfflineData(int userId, string currentCulture)
        {
            var offlineDataDictionary = new Dictionary<string, object>();
            var reasonData = new VmReasonData();
            var students = new List<VmChildren>();
            var absenses = new List<VmChildren>();
            offlineDataDictionary.Add("ClassList", new List<object>());
            offlineDataDictionary.Add("StudentList", new List<object>());
            offlineDataDictionary.Add("AbsenceList", new List<object>());
            offlineDataDictionary.Add("ReasonData", new object());
            var classes = _classRepository.GetClassesByTeacherId(userId).ToList();

            foreach (var classItem in classes)
            {
                var children = _studentRepository.GetChildrenByClassId(classItem.ID, DateTime.Now).ToList();
                foreach (var child in children)
                {
                    students.Add(child);
                }

                children = _studentRepository.GetAbsentChildrenByClassId(classItem.ID, DateTime.Now.AddDays(-30), DateTime.Now).ToList();
                absenses.AddRange(children);
                offlineDataDictionary["ClassList"] = classes;
                offlineDataDictionary["StudentList"] = students;
                offlineDataDictionary["AbsenceList"] = absenses;
            }

            reasonData.ReasonTypeList = _reasonRepository.GetAllReasonType(currentCulture).ToList();
            reasonData.ReasonMasterList = _reasonRepository.GetAllReasonMasterByReasonType(0, currentCulture).ToList();
            reasonData.AbsenceInformerList = _reasonRepository.GetAllAbsenceInformerMaster(currentCulture).ToList();
            offlineDataDictionary["ReasonData"] = reasonData;

            return offlineDataDictionary;

        }

        public IDictionary<string, object> GetOfflineData(ChildRequestParams childRequest, string currentCulture)
        {
            var offlineDataDictionary = new Dictionary<string, object>();
            var reasonData = new VmReasonData();
            var students = new List<VmChildren>();
            var absenses = new List<VmChildren>();
            offlineDataDictionary.Add("ClassList", new List<object>());
            offlineDataDictionary.Add("StudentList", new List<object>());
            offlineDataDictionary.Add("AbsenceList", new List<object>());
            offlineDataDictionary.Add("ReasonData", new object());

            var classes = _classRepository.GetClassesByTeacherId(childRequest.UserId).ToList();
            foreach (var classItem in classes)
            {
                var children = _studentRepository.GetChildrenByClassId(classItem.ID, childRequest.AttendanceDate).ToList();
                foreach (var child in children)
                {
                    students.Add(child);
                }
                children = _studentRepository.GetAbsentChildrenByClassId(classItem.ID, childRequest.AttendanceDate.AddDays(-30), childRequest.AttendanceDate).ToList();
                absenses.AddRange(children);
                offlineDataDictionary["ClassList"] = classes;
                offlineDataDictionary["StudentList"] = students;
                offlineDataDictionary["AbsenceList"] = absenses;
            }

            reasonData.ReasonTypeList = _reasonRepository.GetAllReasonType(currentCulture).ToList();
            reasonData.ReasonMasterList = _reasonRepository.GetAllReasonMasterByReasonType(0, currentCulture).ToList();
            reasonData.AbsenceInformerList = _reasonRepository.GetAllAbsenceInformerMaster(currentCulture).ToList();
            offlineDataDictionary["ReasonData"] = reasonData;

            return offlineDataDictionary;

        }

        #region "V2 Offline"

        public IDictionary<string, object> GetOfflineDataV2(OfflineDataRequestParams childRequest)
        {
            var offlineDataDictionary = new Dictionary<string, object>();
            offlineDataDictionary.Add("ClassList", new List<object>());
            offlineDataDictionary.Add("StudentList", new List<object>());
            offlineDataDictionary.Add("AbsenceList", new List<object>());
            offlineDataDictionary.Add("ClassSummary", new List<object>());
            offlineDataDictionary.Add("AssesmentCommonData", new List<object>());

            offlineDataDictionary.Add("ReasonData", new object());
            offlineDataDictionary.Add("FeedbackData", new object());

            var classes = _classRepository.GetClassesByTeacherId(childRequest.UserId).ToList();
            var studentList = new List<VmChildren>();
            var absenceList = new List<VmChildren>();
            var classSummaryDataList = new List<VmClassAttendanceSummary>();
            foreach (var classItem in classes)
            {
                var children = _studentRepository.GetChildrenByClassId(classItem.ID, childRequest.AttendanceDate).ToList();
                foreach (var child in children)
                {
                    studentList.Add(child);
                }
                children = _studentRepository.GetAbsentChildrenByClassId(classItem.ID, childRequest.AttendanceDate.AddDays(-30), childRequest.AttendanceDate).ToList();
                absenceList.AddRange(children);

                var _absenceStudents = studentList.Where(x => x.IsPresent == false).Select(p => p.ID);

                if (_absenceStudents != null && _absenceStudents.Count() > 0)
                {
                    foreach (var item in _absenceStudents)
                    {
                        var _isAssessmentCompletedToday = _studentRepository.IsAssessmentCompletedToday(item, childRequest.AttendanceDate);
                        studentList.Where(x => x.ID == item).FirstOrDefault().IsAssessmentCompletedToday = _isAssessmentCompletedToday;
                    }
                }

                var result = _studentRepository.GetAbsentChildrenByClassID_Offline(classItem.ID, childRequest.AttendanceDate.AddDays(-30), childRequest.AttendanceDate);

                // Previous day Absence details
                var _previousAttendanceDay = childRequest.AttendanceDate.AddDays(-1);
                var PreviousDayAbsenceResult = _studentRepository.GetAbsentChildrenByClassID_Offline(classItem.ID, _previousAttendanceDay.AddDays(-30), _previousAttendanceDay);

                absenceList.AddRange(result);

                if (result != null && result.Count() > 0)
                {
                    foreach (var item in result)
                    {
                        studentList.Where(x => x.ID == item.ID).FirstOrDefault().AbsentDays = item.AbsentDays;
                    }
                }

                if (PreviousDayAbsenceResult != null && PreviousDayAbsenceResult.Count() > 0)
                {
                    foreach (var item in PreviousDayAbsenceResult)
                    {
                        studentList.Where(x => x.ID == item.ID).FirstOrDefault().PreviousAbsentDays = item.AbsentDays;
                    }
                }

                var _classSummaryData = _classRepository.GetMyClassAttendanceSummary(childRequest.UserId, classItem.ID);
                classSummaryDataList.AddRange(_classSummaryData);
            }

            offlineDataDictionary["ClassList"] = classes;
            offlineDataDictionary["StudentList"] = studentList;
            offlineDataDictionary["AbsenceList"] = absenceList;
            offlineDataDictionary["ClassSummary"] = classSummaryDataList;

            // Fetching Reason Data
            var ObjReasonData = new Offline_V2_ReasonDataResponse();
            ObjReasonData.ReasonTypeList = _reasonRepository.GetAllReasonType_Offline();
            ObjReasonData.ReasonMasterList = _reasonRepository.GetReasonList_Offline();
            ObjReasonData.AbsenceInformerList = _reasonRepository.GetAllAbsenceInformerMaster_Offline();
            ObjReasonData.AbsenceRecordModes = _reasonRepository.GetAllAbsenceRecordModes_Offline();
            offlineDataDictionary["ReasonData"] = ObjReasonData;

            // Fetching Assesment Data
            Offline_V2_ClassAssesmentData _objAssesmentData = new Offline_V2_ClassAssesmentData();
            for (int i = 1; i <= 2; i++)
            {
                var ObjChildAssesmentData = new Offline_V2_VmChildAssesmentData_CommonData();
                ObjChildAssesmentData.ChildAssesmentDetails = _childAssesmentRepository.GetChildAssesmentDetails_Offline(childRequest.UserId, i);
                var explanatoryNotes = _childAssesmentRepository.GetAllExplanatoryNotes_Offline(childRequest.UserId, i);
                var recommendations = _childAssesmentRepository.GetAllRecommendations_Offline(childRequest.UserId, i);
                if (ObjChildAssesmentData.ChildAssesmentDetails != null && ObjChildAssesmentData.ChildAssesmentDetails.Count > 0)
                {
                    foreach (var item in ObjChildAssesmentData.ChildAssesmentDetails)
                    {
                        var _notes = explanatoryNotes.Where(x => x.SubComplaintID == item.SubComplaintID).ToList();
                        var _recommedations = recommendations.Where(x => x.SubComplaintID == item.SubComplaintID).ToList();
                        if (_notes != null && _notes.Count > 0)
                            item.ExplanatoryNotes = _notes[0];
                        if (_recommedations != null && _recommedations.Count > 0)
                            item.Recommendations = _recommedations;
                    }
                }

                ObjChildAssesmentData.SubComplaints = _childAssesmentRepository.GetAllSubComplaints_Offline(childRequest.UserId, i);
                if (ObjChildAssesmentData.SubComplaints != null && ObjChildAssesmentData.SubComplaints.Count > 0)
                {
                    var advices = _childAssesmentRepository.GetAllMappedAdvices_Offline(childRequest.UserId, i);
                    foreach (var subcomplaint in ObjChildAssesmentData.SubComplaints)
                    {
                        var _recommedations = recommendations.Where(x => x.SubComplaintID == subcomplaint.ID).ToList();
                        var _notes = explanatoryNotes.Where(x => x.SubComplaintID == subcomplaint.ID).ToList();
                        var _advices = advices.Where(x => x.SubComplaintID == subcomplaint.ID).ToList();
                        if (_recommedations != null && _recommedations.Count > 0)
                            subcomplaint.Recommendations = _recommedations;
                        if (_notes != null && _notes.Count > 0)
                            subcomplaint.ExplanatoryNotes = _notes[0];
                        if (_advices != null && _advices.Count > 0)
                            subcomplaint.Advices = _advices;
                    }

                }

                if (i == 1)
                    _objAssesmentData.HealthAssesment = ObjChildAssesmentData;
                else if (i == 2)
                    _objAssesmentData.SocialAssesment = ObjChildAssesmentData;
            };
            offlineDataDictionary["AssesmentCommonData"] = _objAssesmentData;

            // Fetching Feedback Data
            var _feedbackResult = _feedbackRepository.GetFeedBackTypes_Offline();
            offlineDataDictionary["FeedbackData"] = _feedbackResult;

            return offlineDataDictionary;


        }

        public IDictionary<string, object> GetAssessmentOfflineDataV2(OfflineDataRequestParams childRequest)
        {
            var offlineDataDictionary = new Dictionary<string, object>();
            offlineDataDictionary.Add("StudentDashboardAbsenceDetails", new List<object>());
            offlineDataDictionary.Add("StudentDashboardDetails", new List<object>());

            var classes = _classRepository.GetClassesByTeacherId(childRequest.UserId).ToList();

            var DashboardAbsenceDetails = new List<List<VMChildAbsenceDetails>>();
            var _childDashboardDetails = new List<Offline_V2_ChildDashboard>();

            foreach (var classItem in classes)
            {
                var StudentList = new List<VmChildren>();
                var children = _studentRepository.GetChildrenByClassId(classItem.ID, childRequest.AttendanceDate).ToList();
                StudentList.AddRange(children);


                var _studentCallGuardianDetails = new List<ChildrenMaster>();
                foreach (var _student in StudentList)
                {
                    var childresult = _studentRepository.GetStudentDashboardAbsenceDetails(_student.ID);
                    if (childresult != null && childresult.Count > 0)
                    {
                        childresult[0].ReasonUnsubmitted = JsonConvert.DeserializeObject<List<ReasonUnsubmitted>>(childresult[0].AttendanceDates);
                    }

                    DashboardAbsenceDetails.Add(childresult);

                    var ChildDashboardResult = new Offline_V2_ChildDashboard();
                    ChildDashboardResult.ChildId = _student.ID;
                    ChildDashboardResult.ClassId = classItem.ID;
                    ChildDashboardResult.DashboardSummary = _reportRepository.GetChildDashboardSummary(_student.ID, childRequest.AttendanceDate);
                    ChildDashboardResult.ReasonSummary = _reportRepository.GetChildReasonSummary_Offline(_student.ID, childRequest.AttendanceDate);

                    _childDashboardDetails.Add(ChildDashboardResult);
                }
            }

            offlineDataDictionary["StudentDashboardAbsenceDetails"] = DashboardAbsenceDetails;
            offlineDataDictionary["StudentDashboardDetails"] = _childDashboardDetails;

            return offlineDataDictionary;
        }

        #endregion
    }
}
