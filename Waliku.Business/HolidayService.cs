﻿using System;
using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business
{
    public class HolidayService : IHolidayService
    {
        private readonly IUnitOfWork _uow;
        private readonly IHolidayRepository _repository;
        public HolidayService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = _uow.HolidayRepository;
        }

        public void DeleteHolidayById(int id)
        {
            _repository.DeleteHolidayById(id);
            _uow.Commit();
        }

        public IEnumerable<WebModel.AcademicCalenderMaster> GetAllHoliday(int schoolId, string langId)
            => _repository.GetAllHoliday(schoolId, langId);

        public IEnumerable<YearlyHolidayMaster> GetHolidayListByMonthForTeacher(DateTime holidayDate, int userId, int schoolId, string currentCulture)
            => _repository.GetHolidayListByMonthForTeacher(holidayDate, userId, schoolId, currentCulture);

        public void ManageHoliday(WebModel.AcademicCalenderMaster academicCalender)
        {
            _repository.ManageHoliday(academicCalender);
            _uow.Commit();
        }
    }
}
