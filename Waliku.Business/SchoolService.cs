﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Business
{
    public class SchoolService : ISchoolService
    {
        private readonly IUnitOfWork _uow;
        private readonly ISchoolRepository _repository;
        public SchoolService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.SchoolRepository;
        }

        public void DeleteSchoolById(int schoolId)
        {
            _repository.DeleteSchoolById(schoolId);
            _uow.Commit();
        }

        public IEnumerable<SchoolMaster> GetAllSchool() => _repository.GetAllSchool();

        public IEnumerable<SchoolMaster> GetSchoolListForUser(int userId, string langId) => _repository.GetSchoolListForUser(userId, langId);

        public int ManageSchool(WebModel.SchoolMaster schoolMaster)
        {
            var result = _repository.ManageSchool(schoolMaster);
            _uow.Commit();
            return result;
        }

        public IEnumerable<SchoolMaster> GetDistrictSchools(string district) => _repository.GetDistrictSchools(district);
    }
}
