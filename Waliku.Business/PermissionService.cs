﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
namespace Waliku.Business
{
    public class PermissionService : IPermissionService
    {
        private readonly IUnitOfWork _uow;
        private readonly IPermissionRepository _repository;
        public PermissionService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.PermissionRepository;
        }

        public RoleMatrix GetAllRoleMatrix() => _repository.GetAllRoleMatrix();

        public RoleMatrix GetRoleMatrix(int[] roleIds) => _repository.GetRoleMatrix(roleIds);

        public IEnumerable<Component> GetUserComponents(string userName) => _repository.GetUserComponents(userName);

        public IEnumerable<Component> GetUserRoleComponents(string userName, string roleName) => _repository.GetUserRoleComponents(userName, roleName);
       
        public RoleMatrix GetUserRoleMatrix(string userName) => _repository.GetUserRoleMatrix(userName);

        public bool SavePermission(IEnumerable<Permission> permissions)
        {
            _repository.ClearPermissions();
            foreach (var permission in permissions)
            {
                _repository.SavePermission(permission);
            }
            _uow.Commit();
            return true;
        }

        public IEnumerable<vmRole> GetUserRoles(string userName) => _repository.GetUserRoles(userName);
    }
}
