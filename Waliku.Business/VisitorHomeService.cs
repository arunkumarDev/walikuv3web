﻿using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;

namespace Waliku.Business
{
    public class VisitorHomeService : IVisitorHomeService
    {
        private readonly IUnitOfWork _uow;
        private readonly IVisitorHomeRepository _repository;
        public VisitorHomeService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.VisitorHomeRepository;
        }

        public void ManageVisitHome(VisitHomeMaster entity)
        {
            _repository.ManageVisitHome(entity);
            _uow.Commit();
        }

        public IEnumerable<VisitHomeMaster> GetVisitHomeDetailByTeacherId(int teacherId) => _repository.GetVisitHomeDetailByTeacherId(teacherId);
    }
}
