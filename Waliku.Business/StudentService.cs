﻿using System;
using System.Collections.Generic;
using Waliku.Business.Interface;
using Waliku.Data;
using Waliku.Data.Repositories.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Business
{
    public class StudentService : IStudentService
    {
        private readonly IUnitOfWork _uow;
        private readonly IStudentRepository _repository;
        public StudentService(IUnitOfWork uow)
        {
            _uow = uow;
            _repository = uow.StudentRepository;
        }

        public ChildrenMaster GetChildDetailById(int childId) => _repository.GetChildDetailById(childId);


        public IEnumerable<AttendanceMaster> GetChildrenByTeacherIdAndClassIdAndDateWise(int teacherId, int classRoomId, DateTime attendanceDate) => 
            _repository.GetChildrenByTeacherIdAndClassIdAndDateWise(teacherId, classRoomId, attendanceDate);

        public IEnumerable<ChildrenMaster> GetChildrenByTeacherId(int teacherId) => _repository.GetChildrenByTeacherId(teacherId);

        public IEnumerable<ChildrenMaster> GetAllChildren() => _repository.GetAllChildren();

        public void ManageChildren(ChildrenMaster entity)
        {
            _repository.ManageChildren(entity);
            _uow.Commit();
        }

        public IEnumerable<VmChildren> GetAbsentChildrenByClassId(int classId, DateTime startDate, DateTime endDate) => 
            _repository.GetAbsentChildrenByClassId(classId, startDate, endDate);

        public IEnumerable<VmChildren> GetAbsentChildrenReport(int classId, DateTime startDate, DateTime endDate) => 
            _repository.GetAbsentChildrenReport(classId, startDate, endDate);

        public IEnumerable<VmChildren> GetChildrenByClassId(int classId, DateTime attendanceDate) => _repository.GetChildrenByClassId(classId, attendanceDate);

        public VmCount GetTeacherClassCountAndAbsenceCount(int teacherId, DateTime attendanceDate) => _repository.GetTeacherClassCountAndAbsenceCount(teacherId, attendanceDate);

        public IEnumerable<ChildrenMaster> GetAllChildrenForMe(int userId, int userType, string langId) => _repository.GetAllChildrenForMe(userId, userType, langId);

        public IEnumerable<ChildrenMaster> GetAllChildrenForMeByFilter(int userId, int userType, int schoolId, int classRoomId, string langId)
            => _repository.GetAllChildrenForMeByFilter(userId, userType, schoolId, classRoomId, langId);

        public string GetChildImageById(int childId) => _repository.GetChildImageById(childId);

        public void DeleteChildById(int childId)
        {
            _repository.DeleteChildById(childId);
            _uow.Commit();
        }

        public VmChildren GetChildDetailsById(int childId) => _repository.GetChildDetailsById(childId);
        public List<VMChildAbsenceDetails> GetStudentDashboardAbsenceDetails(int childId)
        => _repository.GetStudentDashboardAbsenceDetails(childId);
    }
}
