﻿namespace Waliku.Domain.Models
{
    public class AbsenceInformerMaster
    {
        public string Description { get; set; }
        public int ID { get; set; }
    }
}