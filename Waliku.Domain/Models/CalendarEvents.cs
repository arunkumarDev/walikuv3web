﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waliku.Domain.Models
{
    public class CalendarEvents
    {
        public int EventTypeID { get; set; }
        public int EventID { get; set; }
        public string EventType { get; set; }
        public string EventName { get; set; }
        public DateTime EventStartDate { get; set; }
        public DateTime EventEndDate { get; set; }
    }
}
