﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Waliku.Domain.Models
{
    public class YearlyHolidayMaster
    {
        public DateTime HolidayDate { get; set; }
        public int ID { get; set; }
        public string Reason { get; set; }
        public int SchoolId { get; set; }
    }
}