﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Waliku.Domain.Models
{
    public class VisitHomeMaster
    {
        public int AbsentCount { get; set; }
        public int ChildID { get; set; }
        public string Image { get; set; }
        public Nullable<bool> IsHealthReason { get; set; }
        public string Name { get; set; }
        public int? ReasonID { get; set; }
        public int TeacherID { get; set; }
        public DateTime VisitHomeDate { get; set; }
    }
}