﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Waliku.Domain.Models
{
    public class Error
    {
        public long Id { get; set; }

        [Column(TypeName = "text")]
        public string Request { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Message { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Exception { get; set; }

        [StringLength(300)]
        public string Url { get; set; }

        public DateTime LogTime { get; set; }
    }

    public class Action
    {
        public long Id { get; set; }            

        public DateTime AccessedTime { get; set; }
        public int UserId { get; set; }

        [StringLength(50)]
        public string Ip { get; set; }

        [StringLength(50)]
        public string Api { get; set; }

        [StringLength(150)]
        public string Service { get; set; }
    }

    public class Audit
    {
        public long Id { get; set; }

        public DateTime LogTime { get; set; }
        public int UserId { get; set; }

        [StringLength(50)]
        public string DeviceInfo { get; set; }

        [StringLength(150)]
        public string Action { get; set; }

        [Column(TypeName = "text")]
        public string Request { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Response { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Remarks { get; set; }
    }
}
