﻿using System;

namespace Waliku.Domain.Models
{
    public class AttendanceMaster
    {
        public DateTime AttendanceDate { get; set; }
        public int ChildID { get; set; }
        public int ClassRoomID { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ID { get; set; }
        public bool IsPresent { get; set; }
        public bool IsSubmitted { get; set; }
        public string Name { get; set; }
        public int TeacherID { get; set; }
    }
}