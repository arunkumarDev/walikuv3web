﻿namespace Waliku.Domain.Models
{
    public class ReasonMaster
    {
        public string Description { get; set; }
        public int ID { get; set; }

        public int ReasonType { get; set; }
    }
}