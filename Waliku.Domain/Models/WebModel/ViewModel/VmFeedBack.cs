﻿using System;
namespace Waliku.Domain.Models.WebModel.ViewModel
{
    public class VmFeedBack
    {
        public string Content { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int ID { get; set; }

        public string Name { get; set; }
    }
}