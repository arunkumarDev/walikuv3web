﻿using System;
using System.Collections.Generic;
namespace Waliku.Domain.Models.WebModel.ViewModel
{
    public class VmGridData
    {
        public int AbsentDays { get; set; }
        public int AttendanceMonth { get; set; }

        public Single AttendancePercent { get; set; }
        public int ChildID { get; set; }

        public string Name { get; set; }

        public int PresentDays { get; set; }
    }

    public class VmMonthList
    {
        public int AttendanceMonth { get; set; }
    }

    public class VmYearlyReport
    {

        public List<VmGridData> GridData = new List<VmGridData>();
        public List<VmMonthList> MonthList = new List<VmMonthList>();
    }
}
