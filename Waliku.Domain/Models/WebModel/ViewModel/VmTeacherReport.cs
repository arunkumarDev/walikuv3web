﻿namespace Waliku.Domain.Models.WebModel.ViewModel
{
    public class VmTeacherReport
    {
        public int AbsentDays { get; set; }
        public string Attendance { get; set; }
        public int AttendanceDate { get; set; }
        public string AttendanceType { get; set; }
        public int ChildId { get; set; }
        public string Gender { get; set; }
        public int ID { get; set; }

        public string Name { get; set; }
        public string ReasonType { get; set; }
    }
}