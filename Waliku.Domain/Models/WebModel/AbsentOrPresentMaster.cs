﻿using System.Collections.Generic;

namespace Waliku.Domain.Models.WebModel
{
    public class AbsentOrPresentMaster
    {
        public string Address { get; set; }
        public int ClassroomID { get; set; }
        public int CommunityWorkerID { get; set; }
        public int Day { get; set; }
        public string Email { get; set; }
        public string GeoLocation { get; set; }
        public int ID { get; set; }
        public string Image { get; set; }
        public bool IsPresent { get; set; }
        public string Middlename { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string ParentName { get; set; }
        public string Phone { get; set; }
        public int SchoolID { get; set; }
        public string Surname { get; set; }
    }

    public class AbsentOrPresentMasterPacked
    {
        public List<AbsentOrPresentMaster> Presences;
        public int ID { get; set; }

        public string Name { get; set; }
    }
}