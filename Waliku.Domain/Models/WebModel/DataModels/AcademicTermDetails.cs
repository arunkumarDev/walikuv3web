﻿using System;

namespace Waliku.Domain.Models.WebModel.DataModels
{
    public class AcademicTerm
    {
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime End_Date { get; set; }
        public int ID { get; set; }

        public bool IsCurrentTerm { get; set; }
        public int No_Of_Days { get; set; }
        public int SchoolId { get; set; }
        public string SchoolName { get; set; }
        public DateTime Start_Date { get; set; }
    }
}