﻿using System;
namespace Waliku.Domain.Models.WebModel
{
    public class ChildrenMaster
    {
        public string Address { get; set; }
        public Single AttendancePercent { get; set; }
        public int ClassroomID { get; set; }
        public string ClassroomName { get; set; }
        public int CommunityWorkerID { get; set; }
        public string CommunityWorkerName { get; set; }
        public int CreatedBy { get; set; }
        // public int AttendanceDate { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;

        public string Email { get; set; }
        public string Gender { get; set; }
        public string GeoLocation { get; set; }
        public int ID { get; set; }

        public string Image { get; set; }
        public string ImageBase { get; set; }
        //public string AttendanceType { get; set; }
        public int IsActive { get; set; }

        public string Middlename { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }
        public string Note_Indonesian { get; set; }
        public string ParentName { get; set; }
        public string Phone { get; set; }
        public int SchoolID { get; set; }
        public string SchoolName { get; set; }
        public string Surname { get; set; }
    }
}