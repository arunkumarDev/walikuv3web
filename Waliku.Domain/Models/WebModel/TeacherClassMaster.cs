﻿using System;
namespace Waliku.Domain.Models.WebModel
{
    public class TeacherClassMapping
    {
        public string ClassName { get; set; }
        public int ClassRoomID { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;
        public int ID { get; set; }

        public int IsActive { get; set; }
        public string Name { get; set; }
        public int TeacherID { get; set; }
    }
}