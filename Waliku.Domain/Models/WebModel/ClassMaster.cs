﻿using System;
namespace Waliku.Domain.Models.WebModel
{
    public class ClassMaster
    {
        public int ID { get; set; }
        public string ClassName { get; set; }
        public int SchoolId { get; set; } = 0;
        public int CreatedBy { get; set; } = 0;
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;
        public string SchoolName { get; set; } = string.Empty;
        public int IsActive { get; set; }
    }
}