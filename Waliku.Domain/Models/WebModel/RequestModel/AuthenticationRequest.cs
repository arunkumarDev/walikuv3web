﻿namespace Waliku.Domain.Models.WebModel.RequestModel
{
    public class LoginRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string grant_type { get; set; }
    }

    public class LoginResponse
    {
        public string UserName { get; set; }
        public string Access_Token { get; set; }
        public string Refresh_Token { get; set; }
    }

    public class UserInfoViewModel
    {
        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string LoginProvider { get; set; }
    }
}