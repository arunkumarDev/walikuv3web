﻿namespace Waliku.Domain.Models.WebModel.RequestModel
{
    public class ReportRequest
    {
        public int SchoolId { get; set; }
        public int ClassId { get; set; }
        public int TeacherId { get; set; }
        public int SelectedType { get; set; }
        public string SelectedDate { get; set; }
        public string LanguageId { get; set; }
    }
}