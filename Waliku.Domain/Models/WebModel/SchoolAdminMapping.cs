﻿using System;
namespace Waliku.Domain.Models.WebModel
{
    public class SchoolAdminMapping
    {
        public int ID { get; set; }
        public int SchoolId { get; set; }
        public int UserId { get; set; }
        public int IsActive { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;
    }
}