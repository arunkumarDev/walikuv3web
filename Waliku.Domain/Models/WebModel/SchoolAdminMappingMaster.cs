﻿using System;
namespace Waliku.Domain.Models.WebModel
{
    public class SchoolAdminMappingMaster
    {
        public int CreatedBy { get; set; } = 0;
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;
        public int ID { get; set; }

        public bool IsActive { get; set; }
        public int SchoolId { get; set; } = 0;

        public string SchoolName { get; set; }
        public int UserId { get; set; } = 0;
        public string UserName { get; set; }
    }
}