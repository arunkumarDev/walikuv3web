﻿namespace Waliku.Domain.Models.WebModel
{
    public class UserTypeMaster
    {

        public int ID { get; set; }

        public string UserType { get; set; }

        public string UserType_Indonesian { get; set; }
        public int Rank { get; set; }
        public byte WebAccess { get; set; }
        public byte AppAccess { get; set; }
    }
}