﻿using System;

namespace Waliku.Domain.Models.WebModel
{
    public class FirstAidMaster
    {
        public string Content { get; set; }
        public string ContentIndian { get; set; }
        public int CreatedBy { get; set; } = 0;
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;
        public int ID { get; set; }

        public bool IsActive { get; set; }
        public string Title { get; set; }
    }
}
