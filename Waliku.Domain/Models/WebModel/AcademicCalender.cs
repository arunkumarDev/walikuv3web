﻿using System;

namespace Waliku.Domain.Models.WebModel
{
    public class AcademicCalenderMaster
    {
        public int AcademicYear { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;
        public DateTime HolidayDate { get; set; }
        public int ID { get; set; }

        public bool IsActive { get; set; }
        public string Reason { get; set; }
        public string Reason_Indonesian { get; set; }
        public int SchoolID { get; set; }
    } 
}