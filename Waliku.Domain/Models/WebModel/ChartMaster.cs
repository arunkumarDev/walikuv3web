﻿using System;

namespace Waliku.Domain.Models.WebModel
{
    public class ChartMaster
    {
        public class PresentAbsetReport
        {
            public int Absent { get; set; }
            public int AbsentBoys { get; set; }
            public int AbsentDays { get; set; }
            public Single AbsenteesimRate { get; set; }
            public int AbsentGirls { get; set; }
            public decimal AbsentPercent { get; set; }
            public int AttendanceDate { get; set; }
            public int AttendanceMonth { get; set; }
            public string AttendanceMonthName { get; set; }
            public Single AttendancePercent { get; set; }
            public Single AttendanceRate { get; set; }
            public Single BoysAbsenteeRate { get; set; }
            public Single BoysAttendanceRate { get; set; }
            public int ChildID { get; set; }
            public int ChronicallyAbsent { get; set; }
            public string ClassName { get; set; }
            public int ClassRoomID { get; set; }
            public int Code { get; set; }
            public string Description { get; set; }
            public Single GirlsAbsenteeRate { get; set; }
            public Single GirlsAttendanceRate { get; set; }
            public Single HealthAbsenteeRate { get; set; }
            public DateTime HolidayDate { get; set; }
            public int HolidyId { get; set; }
            public string Name { get; set; }
            public Single NonHealthAbsenteeRate { get; set; }
            public int Present { get; set; }
            public int PresentDays { get; set; }
            public decimal PresentPercent { get; set; }
            public string ReasonCode { get; set; }
            public int ReasonCount { get; set; }
            public int ReasonType { get; set; }
            public int SeverelyChronicallyAbsent { get; set; }
            public int StudentCount1 { get; set; }
            public int StudentCount2 { get; set; }
            public Single TotalAbsStudentDays { get; set; }
            public int TotalBoys { get; set; }

            public int TotalGirls { get; set; }
            public Single UnKnownAbsenteeRate { get; set; }
        }
        public class VmMonthList
        {
            public int AttendanceMonth { get; set; }
        }
    }
}