﻿namespace Waliku.Domain.Models.WebModel
{
    public class ReasonMaster
    {
        public string Description { get; set; }
        // public string DescriptionIndian { get; set; }
        public string Description_Indonesian { get; set; }

        public int ID { get; set; }

        public int ReasonType { get; set; }
        public string ReasonTypeName { get; set; }
    }

    public class ReasonType
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int ID { get; set; }
    }
}