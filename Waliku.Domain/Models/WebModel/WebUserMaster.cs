﻿using System;

namespace Waliku.Domain.Models.WebModel
{
    public class WebUserMaster
    {
        public string Address { get; set; } = "";
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.MaxValue;
        public string Email { get; set; }
        public string Gender { get; set; }
        public string GeoLocation { get; set; }
        public int ID { get; set; }

        public string Image { get; set; } = "";
        public int IsActive { get; set; }
        public string Name { get; set; } = "";
        public string Note { get; set; } = "";
        public string Note_Indonesian { get; set; }
        public string Password { get; set; } = "";
        public string Phone { get; set; } = "";
        public string Surname { get; set; } = "";
        public string UserName { get; set; } = "";
        public int UserType { get; set; } = 0;
        public string UserTypeName { get; set; }
    }
}