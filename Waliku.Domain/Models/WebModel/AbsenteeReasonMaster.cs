﻿namespace Waliku.Domain.Models.WebModel
{
    public class AbsenteeReason
    {
        public int ChildID { get; set; }
        public int CreatedBy { get; set; }
        public bool HasPermission { get; set; }
        public int ID { get; set; }
        public int InformationProvider { get; set; }
        public bool IsHealthReason { get; set; }
        public string OtherHealthIssue { get; set; }
        public string OtherNonHealthIssue { get; set; }
        public int ReasonID { get; set; }
        public int TeacherID { get; set; }
    }
}