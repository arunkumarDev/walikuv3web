﻿namespace Waliku.Domain.Models
{
    public class ReasonType
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public int ID { get; set; }
    }
}