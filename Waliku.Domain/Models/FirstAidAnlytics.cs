﻿using System;

namespace Waliku.Domain.Models
{
    public class FirstAidAnlytics
    {
        public int ID { get; set; }

        public DateTime VisitedOn { get; set; } = DateTime.MinValue;
        public int VisitorId { get; set; }
    }
}