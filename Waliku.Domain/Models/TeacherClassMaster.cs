﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Waliku.Domain.Models
{
    public class TeacherClassMaster
    {
        public string ClassName { get; set; }
        public int ClassRoomID { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public int TeacherID { get; set; }
    }
}