﻿using System.Collections.Generic;

namespace Waliku.Domain.Models
{
    public class RoleMatrix
    {
        public IEnumerable<Component> Components { get; set; }
		public IEnumerable<Role> Roles { get; set; }
		public IEnumerable<Permission> Permissions { get; set; }
	}

	public class Component
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string PrimaryLanguageTitle { get; set; }
		public string SecondaryLanguageTitle { get; set; }
		public bool IsMenu { get; set; }
		public bool IsPage { get; set; }
		public int? ParentId { get; set; }
		public int Order { get; set; }
		public string Path { get; set; }
		public bool IsActive { get; set; }
		public int MenuId { get; set; }
		public string IconClass { get; set; }
	}

	public class Role
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}

	public class Permission
	{
		public int ComponentId { get; set; }
		public string RoleId { get; set; }
		public bool Allow { get; set; }
		public bool Readonly { get; set; }
	}
	public class vmRole
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string NameIndonesian { get; set; }
	}
}