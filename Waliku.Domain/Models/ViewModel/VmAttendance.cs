﻿using System;
using Waliku.Domain.Models;

namespace Waliku.Domain.Models.ViewModel
{
    public class VmAbsenteeReason : AbsenteeReason
    {
        public int ReasonType { get; set; }  

        public string Message { get; set; }

        public int ClassroomID { get; set; }

        public int AttendanceMasterID { get; set; }

        public bool IsPresent { get; set; }

    }
}