﻿using System;
using System.Collections.Generic;

namespace Waliku.Domain.Models.ViewModel
{
    public class VmChildren 
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Image { get; set; }
        public int ClassroomID { get; set; }
        public string ClassName { get; set; }
        public int AbsentDays { get; set; }
        public int PreviousAbsentDays { get; set; }
        public string ParentName { get; set; }
        public string F_Name { get; set; }
        public string M_Name { get; set; }
        public string G_Name { get; set; }
        public string Phone { get; set; }
        public string F_Phone_No { get; set; }
        public string M_Phone_No { get; set; }
        public string G_Phone_No { get; set; }
        public string Address { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string SchoolName { get; set; }
        public int ReasonID { get; set; }
        public int InformationProvider { get; set; }
        public int AttendanceMasterID { get; set; }
        public string AbsenceReason { get; set; }
        public string AbsenteeInformer { get; set; }
        public int ElapsedTime { get; set; }
        public string ReasonDescription { get; set; }
        public bool IsSubmitted { get; set; }
        public int AttendanceStatus { get; set; }
        public bool IsPresent { get; set; }
        public Single AttendancePercent { get; set; }
        public string GeoLocation { get; set; }
        public string Gender { get; set; }
        public bool RedDot { get; set; }
        public bool ReasonUnSubmitted { get; }
        public bool IsAssessmentCompletedToday { get; set; }
    }



    public class VMChildAbsenceDetails
    {
        public int Id { get; set; }
        public int ChildId { get; set; }

        public string ChildName { get; set; }


        public string ClassName { get; set; }

        public string RegistrationNumber { get; set; }

        public float AttendancePercent { get; set; }

        public int? Age { get; set; }

        public string Gender { get; set; }

        public string AttendanceDates { get; set; }

        public List<ReasonUnsubmitted> ReasonUnsubmitted { get; set; }
    }

    public class ReasonUnsubmitted
    {
        private DateTime _attendanceDate;

        private bool _reasonUnSubmitted;

        public bool IsAbsentToday { get; set; }

        public bool ReasonUnSubmitted
        {
            get
            {
                return _reasonUnSubmitted;
            }
            set
            {
                _reasonUnSubmitted = value;
            }
        }

        public DateTime AttendanceDate
        {
            get
            {
                return _attendanceDate;
            }
            set
            {
                _attendanceDate = value;
            }
        }
    }
}