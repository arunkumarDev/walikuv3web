﻿using System.Collections.Generic;

namespace Waliku.Domain.Models.ViewModel
{
    public class VmReasonData
    {
        public List<ReasonType> ReasonTypeList { get; set; }
        public List<ReasonMaster> ReasonMasterList { get; set; }

        public List<AbsenceInformerMaster> AbsenceInformerList { get; set; }

    }
}