﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Waliku.Domain.Models.ViewModel
{
    public class VmCount
    {
        public int ClassCount { get; set; } = 0;
        public int AbsenceCount { get; set; } = 0;
    }
}