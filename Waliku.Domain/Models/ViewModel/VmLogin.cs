﻿namespace Waliku.Domain.Models.ViewModel
{
    public class VmLogin
    {
        public UserMaster User { get; set; } = new UserMaster();

        public SchoolMaster School { get; set; } = new SchoolMaster();

        public int ClassCount { get; set; } = 0;

        public int AbsenceCount { get; set; } = 0;

        public string WebSiteUrl { get; set; }

        public string PhotoPath { get; set; }

    }
}