﻿using System;
using System.Collections.Generic;

namespace Waliku.Domain.Models.ViewModel
{
    public class ChildRequestParams
    {
        public int UserId { get; set; }

        public int ClassRoomId { get; set; }

        public DateTime AttendanceDate { get; set; }
    }

    public class AbsenceChildRequestParams
    {
        public int UserId { get; set; }

        public int ClassRoomId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int Type { get; set; } = 1;
    }

    public class HolidayRequestParams
    {
        public int UserId { get; set; }

        public DateTime HolidayDate { get; set; }

        public int SchoolId { get; set; } = 0;
    }

    public class CalendarEventsParams
    {
        public string CalendarDate { get; set; }
        public int SchoolID { get; set; }
        public string LanguageID { get; set; }
    }

    #region "Yearly Absenteese Chart Report"
    public class ClassDashboardRequestParams
    {
        public int TeacherID { get; set; }

        public DateTime AttendanceDate { get; set; }

        public int ClassRoomID { get; set; }

        public int Type { get; set; }
    }
    #endregion

    public class ChildSummaryRequestParams
    {
        public int ChildID { get; set; }
        public DateTime AttendanceDate { get; set; }

    }

    public class VmChildAssesmentRequest
    {
        public int TeacherID { get; set; }
        public int AssesmentType { get; set; }

        public int ChildID { get; set; }

        public DateTime AttendanceDate { get; set; }
    }

    public class WeeklyChartReportRequestParams
    {
        public int TeacherID { get; set; }

        public DateTime AttendanceDate { get; set; }

        public int ClassRoomID { get; set; }

        public int Type { get; set; }

        public string LangID { get; set; }
    }
    #region "UpdateChildAssesmentDetails"

    public class VmChildAssesmentSummary
    {
        public int ComplaintID { get; set; }
        public int SubComplaintID { get; set; }
        public int ChildID { get; set; }
        public int TeacherID { get; set; }
        public string FreeText { get; set; }
        public DateTime AttendanceDate { get; set; }
        public string LangID { get; set; }
        public DateTime? TeacherSubmissionDate { get; set; }
    }

    #endregion

    #region "MyClassSummary"
    public class MyClassSummaryRequestParams
    {
        public int TeacherId { get; set; }

        public int ClassRoomId { get; set; }
    }

    #endregion

    #region "V2 Offline"

    public class OfflineDataRequestParams
    {
        public int UserId { get; set; }
        public DateTime AttendanceDate { get; set; }
    }

    #endregion




















}

