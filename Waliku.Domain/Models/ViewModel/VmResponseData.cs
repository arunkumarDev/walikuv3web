﻿using System;
using System.Collections.Generic;

namespace Waliku.Domain.Models.ViewModel
{
    public class VmResponseData
    {
        public string Message { get; set; }
    }

    public class VmAttendanceResponse
    {
        public int ClassID { get; set; } = 0;
        public DateTime Date { get; set; } = DateTime.MaxValue;
    }

    public class VmAbsenceReasonResponse
    {
        public int ClassID { get; set; }

        public int TeacherID { get; set; }

        public int StudentID { get; set; }

        public DateTime Date { get; set; }
    }

    public class VmCalendarEventsResponse
    {
        public IList<CalendarEvents> CalendarEventsList { get; set; }
    }


    public class ChildDashboard
    {
        public List<ChildDashboardSummary> DashboardSummary { get; set; }

        public List<ReasonSummary> ReasonSummary { get; set; }

    }
    public class ChildDashboardSummary
    {
        public string AttendanceDate { get; set; }

        public int Present { get; set; }
        public int Absent { get; set; }
        public float PresentPercent { get; set; }
        public float AbsentPercent { get; set; }

        public int Health { get; set; }

        public int NonHealth { get; set; }

        public int UnKnown { get; set; }

        public int MonthlyAbsence { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public int TotalWorkingDays { get; set; }
    }

    public class ReasonSummary
    {
        public string Description { get; set; }

        public int Count { get; set; }
    }


    public class VmChildAssesmentData
    {
        public List<ChildAssesmentDetails> ChildAssesmentDetails { get; set; }

        public List<SubComplaintsMaster> SubComplaints { get; set; }

        public bool IsAbsentConsecutively { get; set; }
    }

    #region "Yearly Absenteese Chart Report"
    public class YearlyAbsenteesSummary
    {
        public string AttendanceYear { get; set; }
        public float BoysAbsent { get; set; }
        public float GirlsAbsent { get; set; }
        public float OverAllAbsent { get; set; }
        public float BoysPercent { get; set; }
        public float GirlsPercent { get; set; }
        public float OverAllPercent { get; set; }
    }

    public class YearlyTopFiveAbsenteesSummary
    {
        public int NoOfStudentsAffected { get; set; }
        public string Description { get; set; }
        public int ReasonID { get; set; }
        public int NoOfTimesAffectedByStudent { get; set; }
    }

    public class YearlyChronicAbsenteesSummary
    {
        public int ChildID { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string ParentName { get; set; }
        public float TotalDays { get; set; }
        public float AbsentDays { get; set; }
        public float AbsentPercent { get; set; }
    }

    public class YearlyAbsenteesChartSummary
    {
        public IEnumerable<YearlyAbsenteesSummary> YearlyAbsenteesSummary { get; set; }
        public IEnumerable<YearlyTopFiveAbsenteesSummary> YearlyTopFiveHealthAbsenteesSummary { get; set; }
        public IEnumerable<YearlyTopFiveAbsenteesSummary> YearlyTopFiveNonHealthAbsenteesSummary { get; set; }
        public IEnumerable<YearlyChronicAbsenteesSummary> YearlyChronicAbsenteesSummary { get; set; }
    }
    #endregion


    #region Reports - Weekly Chart
    public class ClassDashboardSummary
    {
        public string AttendanceDate { get; set; }
        public int Present { get; set; }
        public int Absent { get; set; }
        public float PresentPercent { get; set; }
        public float AbsentPercent { get; set; }
        public decimal BoysAbsentPercent { get; set; }
        public decimal GirlsAbsentPercent { get; set; }
        public decimal OverallAbsentPercent { get; set; }
        public string AttendanceDateFormatted { get; set; }
    }

    #endregion

    #region Reports -  Class Dashboard
    public class ClassDashboard
    {
        public int UnknownAbsence { get; set; }
        public ClassDashboard()
        {
            ClassDashboardSummary = new List<ClassDashboardSummary>();
            ClassTopReasonsForAbsence = new List<ClassTopReasonsForAbsence>();
        }
        public List<ClassDashboardSummary> ClassDashboardSummary { get; set; }

        public List<ClassTopReasonsForAbsence> ClassTopReasonsForAbsence { get; set; }
    }
    public class ClassTopReasonsForAbsence
    {
        public string Description { get; set; }
        public int ReasonCount { get; set; }
    }

    #endregion

    #region "UpdatechildAssesment"
    public class VmChildAssesmentResponse
    {
        public string Message { get; set; }
    }

    #endregion

    #region "GetMyClassSummary"
    public class VmClassAttendanceSummary
    {
        public int WalikuAttendanceDays { get; set; }

        public int WalikuNonAttendanceDays { get; set; }

        public int TotalDays { get; set; }

        public int AttendancePercent { get; set; }

        public int NoWalikuAttendancePercent { get; set; }
    }
    #endregion

    #region "FeedBackTypeMaster"
    public class FeedBackTypeMaster
    {
        public int ID { get; set; }

        public string Description { get; set; }

    }
    #endregion

    #region "Child Dashboard Summary"
    public class SummaryData
    {
        public SummaryData()
        {
            ReasonSummaryData = new List<ClassChartReasonsForAbsence>();
            TopReasonData = new List<ClassChartReasonsForAbsence>();
        }

        public List<ClassChartReasonsForAbsence> ReasonSummaryData { get; set; }

        public List<ClassChartReasonsForAbsence> TopReasonData { get; set; }

    }

    public class ClassChartReasonsForAbsence
    {
        public string Description { get; set; }

        public decimal? ReasonCount { get; set; }
    }
    #endregion

    #region "ChildrenReport"
    public class VmChildrenReport
    {
        public VmChildrenReport()
        {
            VmAttendanceDataList = new List<VmAttendanceData>();
            AttendanceData = new VmAttendanceData();
        }
        public int ID { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public List<VmAttendanceData> VmAttendanceDataList { get; set; }

        public VmAttendanceData AttendanceData { get; set; }

    }

    public class VmAttendanceData
    {
        public int AttendanceDate { get; set; }

        public string Attendance { get; set; }

        public string AttendanceType { get; set; }

        public int ChildId { get; set; }

        public int AbsentDays { get; set; }

        public string ReasonType { get; set; }

        public string WeekAttendanceDate { get; set; }
    }
    #endregion

    public class vmDistrict
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    #region "V2 offline"
    public class Offline_V2_ReasonMaster
    {
        public int ID { get; set; }

        public int ReasonType { get; set; }

        public string Description { get; set; }

        public string Description_Indonesian { get; set; }

        public int ReasonSubType { get; set; }

    }
    public class Offline_V2_ReasonType
    {
        public int ID { get; set; }

        public string Code { get; set; }

        public string Description { get; set; }

        public string Description_Indonesian { get; set; }

        public bool IsActive { get; set; }

    }
    public class Offline_V2_ReasonDataResponse
    {
        public IEnumerable<Offline_V2_ReasonType> ReasonTypeList { get; set; }
        public Offline_V2_ReasonListResponse ReasonMasterList { get; set; }
        public IEnumerable<Offline_V2_AbsenceInformerMaster> AbsenceInformerList { get; set; }

        public IEnumerable<Offline_V2_AbsenceRecordMode> AbsenceRecordModes { get; set; }
    }

    public class Offline_V2_ReasonListResponse
    {
        public List<Offline_V2_ReasonMaster> HealthReason { get; set; }
        public List<Offline_V2_ReasonMaster> SocialReason { get; set; }
    }

    public class Offline_V2_AbsenceRecordMode
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string Description_Indonesian { get; set; }
    }
    public class Offline_V2_AbsenceInformerMaster
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string Description_Indonesian { get; set; }
        public int SequenceNo { get; set; }
    }

    public class Offline_V2_ClassAssesmentData
    {
        public Offline_V2_VmChildAssesmentData_CommonData HealthAssesment { get; set; }
        public Offline_V2_VmChildAssesmentData_CommonData SocialAssesment { get; set; }
    }

    public class Offline_V2_VmChildAssesmentData_CommonData
    {
        public List<Offline_V2_ChildAssesmentDetails> ChildAssesmentDetails { get; set; }

        public List<Offline_V2_SubComplaintsMaster> SubComplaints { get; set; }

        // public bool IsAbsentConsecutively { get; set; }
    }
    public class FeedBackTypeMaster_Offline
    {
        public int ID { get; set; }

        public string Description { get; set; }

        public string Description_Indonesian { get; set; }

    }
    public class Offline_V2_ReasonSummary
    {
        public string Description { get; set; }
        public string Description_Indonesian { get; set; }
        public int Count { get; set; }
    }
    public class Offline_V2_ChildDashboard
    {
        public int ChildId { get; set; }
        public int ClassId { get; set; }
        public List<ChildDashboardSummary> DashboardSummary { get; set; }
        public List<Offline_V2_ReasonSummary> ReasonSummary { get; set; }
    }

    #endregion
}