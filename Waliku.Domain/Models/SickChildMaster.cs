﻿namespace Waliku.Domain.Models
{
    public class SickChildMaster
    {
        public string Content { get; set; }
        public int ID { get; set; }
        public string Title { get; set; }
    }
}