﻿using System;

namespace Waliku.Domain.Models
{
    public class FeedBack
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public string Content_Indonesian { get; set; }
    }

    public class VMFeedBack
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
    }
}