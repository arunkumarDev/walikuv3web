﻿namespace Waliku.Domain.Models
{
    public class ClassMaster
    {
        public int ID { get; set; }

        public string ClassName { get; set; }
    }
}