﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Waliku.Domain.Models
{
  
        public class ChildAssesmentDetails
        {

            public int QuestionID { get; set; }
            public int AnswerID { get; set; }

            public int AssesmentID { get; set; }

            public string QuestionDescription { get; set; }

            public string AnswerDescription { get; set; }
            public string AssesmentName { get; set; }

            public int? SubComplaintID { get; set; }

            public List<Recommendations> Recommendations { get; set; }

            public ExplanatoryNotes ExplanatoryNotes { get; set; }

        }
        public class SubComplaintsMaster
        {
            public int ComplaintID { get; set; }
            public string Description { get; set; }
            public int ComplaintType { get; set; }
            public List<Recommendations> Recommendations { get; set; }

            public List<SubComplaintAdvice> Advices { get; set; }

            public ExplanatoryNotes ExplanatoryNotes { get; set; }
            public int ID { get; set; }
        }
        public class SubComplaintAdvice
        {
            public int SubComplaintID { get; set; }
            public string Description { get; set; }
        }
        public class Recommendations
        {
            public int SubComplaintID { get; set; }
            public string Description { get; set; }
        }
        public class ExplanatoryNotes
        {
            public int SubComplaintID { get; set; }
            public string Title { get; set; }
            public string Description { get; set; }
        }

        #region " Versin 2 Offline Model"

        public class Offline_V2_ChildAssesmentDetails
        {

            public int QuestionID { get; set; }
            public int AnswerID { get; set; }

            public int AssesmentID { get; set; }

            public string QuestionDescription { get; set; }
            public string QuestionDescription_Indonesian { get; set; }
            public string AnswerDescription { get; set; }
            public string AnswerDescription_Indonesian { get; set; }
            public string AssesmentName { get; set; }

            public int? SubComplaintID { get; set; }

            public List<Offline_V2_Recommendations> Recommendations { get; set; }

            public Offline_V2_ExplanatoryNotes ExplanatoryNotes { get; set; }

        }

        public class Offline_V2_Recommendations
        {
            public int SubComplaintID { get; set; }
            public string Description { get; set; }
            public string Description_Indonesian { get; set; }
        }
        public class Offline_V2_ExplanatoryNotes
        {
            public int SubComplaintID { get; set; }
            public string Title { get; set; }
            public string Title_Indonesian { get; set; }
            public string Description { get; set; }
            public string Description_Indonesian { get; set; }
        }

        public class Offline_V2_SubComplaintAdvice
        {
            public int SubComplaintID { get; set; }
            public string Description { get; set; }
            public string Description_Indonesian { get; set; }
        }

        public class Offline_V2_SubComplaintsMaster
        {
            public int ComplaintID { get; set; }
            public string Description { get; set; }
            public string Description_Indonesian { get; set; }
            public int ComplaintType { get; set; }
            public List<Offline_V2_Recommendations> Recommendations { get; set; }

            public List<Offline_V2_SubComplaintAdvice> Advices { get; set; }

            public Offline_V2_ExplanatoryNotes ExplanatoryNotes { get; set; }
            public int ID { get; set; }
        }
        #endregion
    }

