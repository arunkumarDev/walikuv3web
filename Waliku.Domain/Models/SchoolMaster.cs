﻿using System;

namespace Waliku.Domain.Models
{
    public class SchoolMaster
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.MinValue;
        public string Email { get; set; }
        public string GeoLocation { get; set; }
        public int ID { get; set; }

        public string Image { get; set; }
        public int IsActive { get; set; }
        public string Note { get; set; }
        public string Note_Indonesian { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string SchoolName { get; set; }
    }
}