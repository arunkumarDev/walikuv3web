﻿namespace Waliku.Domain.Models
{
    public class Configuration
    {
        public int Id { get; set; }

        public int ConfigurationGroupId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }
    }

    public class ConfigurationGroup
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}