﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Waliku.Business.Interface;
using Waliku.WebAPI.Models;

namespace Waliku.WebAPI.Providers
{
    public interface IIdentityDataMigrationProvider
    {
        Task<bool> MigrateUsers();
    }
    public class IdentityDataMigrationProvider : IIdentityDataMigrationProvider
    {
        private readonly IUserService _userService;
        private readonly IdentityDbContext<ApplicationUser> _dbContext;
        public IdentityDataMigrationProvider(IdentityDbContext<ApplicationUser> context, IUserService userService)
        {
            _dbContext = context;
            _userService = userService;
        }

        public async Task<bool> MigrateUsers()
        {
            // string[] roles = new string[] { "Admin", "Teacher", "Community Worker", "Super Admin" };
            var roles = _userService.GetAllUserTypes().Select(x => x.UserType).ToArray();

            foreach (string role in roles)
            {
                var roleStore = new RoleStore<IdentityRole>(_dbContext);

                if (!_dbContext.Roles.Any(r => r.Name == role))
                {
                    await roleStore.CreateAsync(new IdentityRole(role));
                }
            }
            var activeUsers = _userService.GetAllActiveUsers();

            foreach (var activeUser in activeUsers)
            {
                var user = new ApplicationUser
                {
                    Email = activeUser.Email,
                    UserName = activeUser.UserName.ToLowerInvariant(),
                    PhoneNumber = activeUser.Phone,
                    EmailConfirmed = !string.IsNullOrEmpty(activeUser.Email),
                    PhoneNumberConfirmed = !string.IsNullOrEmpty(activeUser.Phone),
                    SecurityStamp = Guid.NewGuid().ToString("D")
                };

                if (!_dbContext.Users.Any(u => u.UserName == user.UserName))
                {
                    user.PasswordHash = HashPassword(activeUser.Password);

                    var userStore = new UserStore<ApplicationUser>(_dbContext);
                    await userStore.CreateAsync(user);
                    await userStore.AddToRoleAsync(user, activeUser.UserTypeName);
                }

            }
            await _dbContext.SaveChangesAsync();

            return true;
        }

        string HashPassword(string password)
        {
            byte[] salt;
            byte[] buffer2;
            if (password == null)
            {
                throw new ArgumentNullException("password");
            }
            using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(password, 0x10, 0x3e8))
            {
                salt = bytes.Salt;
                buffer2 = bytes.GetBytes(0x20);
            }
            byte[] dst = new byte[0x31];
            Buffer.BlockCopy(salt, 0, dst, 1, 0x10);
            Buffer.BlockCopy(buffer2, 0, dst, 0x11, 0x20);
            return Convert.ToBase64String(dst);
        }

    }
}