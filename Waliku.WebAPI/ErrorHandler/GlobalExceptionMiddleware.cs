﻿using Microsoft.Owin;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Waliku.WebAPI.ErrorHandler
{
    public class GlobalExceptionMiddleware : OwinMiddleware
    {
        public GlobalExceptionMiddleware(OwinMiddleware next) : base(next)
        { }

        public override async Task Invoke(IOwinContext context)
        {
            try
            {
                await Next.Invoke(context);
            }
            catch (Exception ex)
            {
                // your handling logic   
                ExceptionLogger.LogErrorToFile(ex);
            }
        }
    }
}