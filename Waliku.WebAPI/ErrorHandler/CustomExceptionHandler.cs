﻿using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace Waliku.WebAPI.ErrorHandler
{
    public class CustomExceptionHandler : IExceptionHandler
    {
        public Task HandleAsync(ExceptionHandlerContext context, CancellationToken cancellationToken)
        {
            // Perform some form of logging
            ExceptionLogger.LogErrorToFile(context.Exception, context.Request.RequestUri.ToString());

            context.Result = new ResponseMessageResult(new HttpResponseMessage
            {
                Content = new StringContent("An unexpected error occurred. Please contact administrator."),
                StatusCode = HttpStatusCode.InternalServerError
            });

            return Task.FromResult(0);
        }
    }
}