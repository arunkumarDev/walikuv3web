using Autofac;
using Autofac.Extensions.DependencyInjection;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Reflection;
using System.Web.Http;
using System.Web.Mvc;
using Waliku.Data;
using Waliku.WebAPI.Models;
using Waliku.WebAPI.Providers;

namespace Waliku.WebAPI
{
    //public static class DIConfig
    //{
    //    public static void RegisterComponents()
    //    {
    //        var container = new ServiceContainer();
    //        container.RegisterApiControllers();

    //        container.Register<IConnectionFactory, ConnectionFactory>(new PerScopeLifetime());
    //        container.Register<IUnitOfWork, UnitOfWork>(new PerScopeLifetime());
    //        container.Register<IdentityDbContext<ApplicationUser>, ApplicationDbContext>(new PerScopeLifetime());
    //        container.Register<IIdentityDataMigrationProvider, IdentityDataMigrationProvider>(new PerScopeLifetime());
    //        //register other services
    //        container.Register<IAttendanceService, AttendanceService>(new PerScopeLifetime());
    //        container.Register<IClassService, ClassService>(new PerScopeLifetime());
    //        container.Register<ICommonService, CommonService>(new PerScopeLifetime());
    //        container.Register<IFeedbackService, FeedbackService>(new PerScopeLifetime());
    //        container.Register<IFirstAidService, FirstAidService>(new PerScopeLifetime());
    //        container.Register<IHolidayService, HolidayService>(new PerScopeLifetime());
    //        container.Register<IMasterService, MasterService>(new PerScopeLifetime());
    //        container.Register<IReasonService, ReasonService>(new PerScopeLifetime());
    //        container.Register<ISchoolService, SchoolService>(new PerScopeLifetime());
    //        container.Register<IStudentService, StudentService>(new PerScopeLifetime());
    //        container.Register<IUserService, UserService>(new PerScopeLifetime());
    //        container.Register<IVisitorHomeService, VisitorHomeService>(new PerScopeLifetime());

    //        //container.EnablePerWebRequestScope();
    //        container.EnableWebApi(GlobalConfiguration.Configuration);
    //    }
    //}

    public class Bootstrapper
    {
        public static void Run()
        {
            SetAutofacContainer();
        }
        static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<IdentityDataMigrationProvider>().As<IIdentityDataMigrationProvider>().InstancePerRequest();
            builder.RegisterType<ApplicationDbContext>().As<IdentityDbContext<ApplicationUser>>().InstancePerRequest();
            builder.RegisterModule<DataModule>();
            builder.RegisterModule<ServiceModule>();
            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver((IContainer)container);
        }

    }
}