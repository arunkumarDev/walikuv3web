﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Waliku.WebAPI.Startup))]

namespace Waliku.WebAPI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            //register middlewares that don't need global exception handling. 
            //app.Use<GlobalExceptionMiddleware>();
            //register other middlewares
        }
    }
}
