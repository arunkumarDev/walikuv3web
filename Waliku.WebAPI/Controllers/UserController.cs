﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using Waliku.WebAPI.Providers;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;
        private readonly ISchoolService _schoolService;
        private readonly IIdentityDataMigrationProvider _identityDataMigration;
        public UserController(IUserService userService, ISchoolService schoolService,
            IIdentityDataMigrationProvider identityDataMigration)
        {
            _userService = userService;
            _schoolService = schoolService;
            _identityDataMigration = identityDataMigration;
        }

        [HttpGet]
        [Route("ping")]
        [AllowAnonymous]
        public bool Ping() => _userService.Ping();

        [HttpPost]
        [Route("login")]
        public VmLogin Post([FromBody] UserMaster loginModel)
        {
            UserMaster userMaster = _userService.ValidateUser(loginModel.UserName, loginModel.Password);
            VmLogin loginResult = new VmLogin();
            userMaster.Password = "";

            if (userMaster.ID > 0)
            {
                loginResult.User = userMaster;
                loginResult.WebSiteUrl = ConfigurationManager.AppSettings["WebSiteUrl"];
                loginResult.PhotoPath = "Photos";
                loginResult.School = _schoolService.GetSchoolListForUser(userMaster.ID, "en").FirstOrDefault();
            }
            else
            {
                loginResult = new VmLogin();
            }

            return loginResult;
        }

        // GET api/Account/MigrateUser
        [HttpPost]
        [Route("migrate")]
        public async Task<bool> MigrateUser()
        {
            return await _identityDataMigration.MigrateUsers();
        }

        [HttpGet]
        [Route("{userId}")]
        public dynamic GetUserById(int userId) => _userService.GetUserById(userId);

        [HttpDelete]
        [Route("{userId}")]
        public void DeleteUserById(int userId) => _userService.DeleteUserById(userId);

        [HttpGet]
        [Route("myuserlist/{userId}/{langId}")]
        public IEnumerable<WebModel.WebUserMaster> GetMyUserList(int userId, string langId) =>
            _userService.GetMyUserList(userId, langId);

        [HttpGet]
        [Route("allusers")]
        public IEnumerable<UserMaster> GetAllUsers() => _userService.GetAllUsers();

        [HttpGet]
        [Route("allteachers")]
        public IEnumerable<WebModel.WebUserMaster> GetAllTeachers() => _userService.GetAllTeachers();

        [HttpGet]
        [Route("allschooladmin")]
        public IEnumerable<WebModel.SchoolAdminMappingMaster> GetAllSchoolAdmin() => _userService.GetAllSchoolAdmin();

        [HttpGet]
        [Route("schooladminmapping/{schoolId}")]
        public IEnumerable<WebModel.SchoolAdminMapping> GetSchoolAdminMapping(int schoolId) =>
            _userService.GetSchoolAdminMapping(schoolId);

        [HttpGet]
        [Route("teacherclassmappingdetails/{classRoomId}")]
        public IEnumerable<WebModel.WebUserMaster> GetTeacherClassMappingDetails(int classRoomId) =>
            _userService.GetTeacherClassMappingDetails(classRoomId);

        [HttpGet]
        [Route("allusertypes")]
        public IEnumerable<WebModel.UserTypeMaster> GetAllUserTypes() =>
         _userService.GetAllUserTypes();

        [HttpGet]
        [Route("allusertypes/{userType}")]
        public IEnumerable<WebModel.UserTypeMaster> GetAllUserTypes(int userType) =>
            _userService.GetAllUserTypes(userType);

        [HttpPost]
        [Route("manage")]
        public void ManageUser([FromBody]WebModel.WebUserMaster userMaster) => _userService.ManageUser(userMaster);

        [HttpPost]
        [Route("manageadmin")]
        public void ManageAdmin([FromBody] WebModel.SchoolAdminMappingMaster schoolAdmin) => _userService.ManageAdmin(schoolAdmin);

        [HttpPost]
        [Route("manageadminmapping")]
        public void ManageAdminMapping([FromBody] WebModel.SchoolAdminMapping schoolAdminMapping) =>
            _userService.ManageAdminMapping(schoolAdminMapping);

        [HttpPost]
        [Route("manageclassteacher")]
        public void ManageClassTeacher([FromBody] WebModel.TeacherClassMapping teacherClassMapping) =>
            _userService.ManageClassTeacher(teacherClassMapping);

        [HttpDelete]
        [Route("schooladmin/{id}")]
        public void DeleteSchoolAdminById(int id) => _userService.DeleteSchoolAdminById(id);

        [HttpDelete]
        [Route("classteacher/{id}")]
        public void DeleteClassTeacherById(int id) => _userService.DeleteClassTeacherById(id);
    }
}