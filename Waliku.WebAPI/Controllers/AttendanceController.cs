﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/attendance")]
    public class AttendanceController : BaseController
    {
        private readonly IAttendanceService _attendanceService;
        public AttendanceController(IAttendanceService attendanceService)
        {
            _attendanceService = attendanceService;
        }

        [HttpPost]
        [Route("manage")]
        public VmAttendanceResponse ManageStudentAttendance([FromBody] List<AttendanceMaster> attendances)
        {
            VmAttendanceResponse response = new VmAttendanceResponse();
            if (attendances.Count > 0)
            {
                bool status = _attendanceService.ManageStudentAttendance(attendances);
                if (status)
                {
                    response.ClassID = attendances.First().ClassRoomID;
                    response.Date = attendances.First().AttendanceDate;
                }
            }
            return response;
        }

        [HttpPost]
        [Route("absencedetails")]
        public VmAbsenteeReason GetAbsenceDetails([FromBody] VmAbsenteeReason absentReason)
        {
            VmAbsenteeReason result = _attendanceService.GetAbsenteeReasonDetails(absentReason.ChildID, absentReason.ClassroomID, absentReason.CreatedOn);
            if (CurrentCulture == "id")
                result.Notes = result.Notes_Indonesian;
            return result;
        }

        [HttpPost]
        [Route("manageabsence")]
        public VmResponseData ManageStudentAbsence([FromBody] VmAbsenteeReason absence)
            => _attendanceService.ManageStudentAttendance(absence, CurrentCulture, ToCulture);

        [HttpPost]
        [Route("editabsence")]
        public VmAbsenceReasonResponse EditStudentAbsence([FromBody] VmAbsenteeReason absence) 
            => _attendanceService.ManageStudentAbsence(absence, CurrentCulture, ToCulture);

        [HttpGet]
        [Route("details/{id}")]
        public AttendanceMaster GetAttendanceMasterDetails(int id) 
            => _attendanceService.GetAttendanceMasterDetails(id);

        [HttpGet]
        [Route("exists/{childId}/{classRoomId}/{attendanceDate}")]
        public bool IsAttendanceExists(int childId, int classRoomId, DateTime attendanceDate) 
            => _attendanceService.IsAttendanceExists(childId, classRoomId, attendanceDate);
    }
}