﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http;

namespace Waliku.WebAPI.Controllers
{
    public class BaseController : ApiController
    {
        protected string CurrentCulture
        {
            get
            {
                if (this.Request.Headers.GetValues("Lang").Count() != 0 && !string.IsNullOrEmpty(this.Request.Headers.GetValues("Lang").FirstOrDefault()))
                {
                    return this.Request.Headers.GetValues("Lang").FirstOrDefault().Trim();
                }
                return "en";
            }
        }

        protected string ToCulture
        {
            get
            {
                if (CurrentCulture != null && !string.IsNullOrEmpty(CurrentCulture))
                {
                    return (CurrentCulture == "en" ? "id" : "en");
                }
                else
                    return "en";
            }
        }
    }
}