﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;

namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/calendar")]
    public class CalendarController : BaseController
    {
        private readonly ICalendarService _calendarService;
        public CalendarController(ICalendarService calendarService)
        {
            _calendarService = calendarService;
        }

        [HttpPost]
        [Route("getcalendarevents")]
        public VmCalendarEventsResponse GetCalendarEvents([FromBody]CalendarEventsParams calendarEventsParam)
        {
            VmCalendarEventsResponse response = new VmCalendarEventsResponse();
            if (calendarEventsParam.CalendarDate != null && calendarEventsParam.CalendarDate != "" && Convert.ToDateTime(calendarEventsParam.CalendarDate) != null)
            {
                var calendarDate = (Convert.ToDateTime(calendarEventsParam.CalendarDate) == DateTime.MinValue) ? DateTime.Now : Convert.ToDateTime(calendarEventsParam.CalendarDate);
                IList<CalendarEvents> result = _calendarService.GetAllCalendarEventsBySchool(calendarEventsParam.LanguageID, calendarEventsParam.SchoolID, calendarDate);

                if (result != null && result.Count > 0)
                {
                    response.CalendarEventsList = result;
                }
            }
            return response;
        }
    }
}
