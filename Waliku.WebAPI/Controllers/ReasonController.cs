﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/reason")]
    public class ReasonController : BaseController
    {
        private readonly IReasonService _reasonService;
        public ReasonController(IReasonService reasonService)
        {
            _reasonService = reasonService;
        }

        [HttpGet]
        [Route("getallreasontype")]
        public IEnumerable<ReasonType> GetAllReasonType() => _reasonService.GetAllReasonType(CurrentCulture);

        [HttpGet]
        [Route("getallreasonbytype/{reasonId}")]
        public IEnumerable<ReasonMaster> GetAllReasonByReasonType(int reasonId) => _reasonService.GetAllReasonMasterByReasonType(reasonId, CurrentCulture);

        [HttpGet]
        [Route("getdata")]
        public VmReasonData GetReasonReasonData()
        {
            return new VmReasonData
            {
                ReasonTypeList = _reasonService.GetAllReasonType(CurrentCulture).ToList(),
                ReasonMasterList = _reasonService.GetAllReasonMasterByReasonType(0, CurrentCulture).ToList(),
                AbsenceInformerList = _reasonService.GetAllAbsenceInformerMaster(CurrentCulture).ToList()
            };
        }

        [HttpGet]
        [Route("getdata/{langId}")]
        public IEnumerable<WebModel.ReasonMaster> GetAllReasonMaster(string langId) =>
            _reasonService.GetAllReasonMaster(langId);

        [HttpPost]
        [Route("managereasontype")]
        public void ManageReasonType([FromBody] WebModel.ReasonMaster reason) => _reasonService.ManageReasonType(reason);

        [HttpPost]
        [Route("managereason")]
        public void ManageReason([FromBody] WebModel.FirstAidMaster reason) => _reasonService.ManageReason(reason);

        [HttpDelete]
        [Route("type/{reasonTypeId}")]
        public void DeleteReasonTypeById(int reasonTypeId) => _reasonService.DeleteReasonTypeById(reasonTypeId);

        [HttpDelete]
        [Route("{reasonId}")]
        public void DeleteReasonById(int reasonId) => _reasonService.DeleteReasonById(reasonId);
    }
}