﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;


namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/childassesment")]
    public class ChildAssesmentController : BaseController
    {
        // GET: ChildAssesment
        private readonly IChildAssesmentService _childassesmentService;


        public ChildAssesmentController(IChildAssesmentService childassesmentService)
        {
            _childassesmentService = childassesmentService;
        }

        [HttpPost]
        [Route("getchildassesmentdetails")]
        public VmChildAssesmentData GetChildAssesmentDetails([FromBody] VmChildAssesmentRequest request)
        {
            var isChildAbsentForThreeDays = _childassesmentService.CheckIfChildIsAbsentForThreedays(request.ChildID, request.AttendanceDate);
            VmChildAssesmentData ObjChildAssesmentData = new VmChildAssesmentData();
            if (isChildAbsentForThreeDays)
            {
                ObjChildAssesmentData.IsAbsentConsecutively = isChildAbsentForThreeDays;
                //ObjChildAssesmentData.ChildAssesmentDetails = ChildAssesmentDataAccess.GetChildAssesmentDetails(request.TeacherID, request.AssesmentType, CurrentCulture);
                ObjChildAssesmentData.ChildAssesmentDetails = _childassesmentService.GetChildAssesmentDetails(request.TeacherID, request.AssesmentType, CurrentCulture);
                var explanatoryNotes = _childassesmentService.GetAllExplanatoryNotes(request.TeacherID, request.AssesmentType, CurrentCulture);
                var recommendations = _childassesmentService.GetAllRecommendations(request.TeacherID, request.AssesmentType, CurrentCulture);

                if (ObjChildAssesmentData.ChildAssesmentDetails != null && ObjChildAssesmentData.ChildAssesmentDetails.Count > 0)
                {
                    foreach (var item in ObjChildAssesmentData.ChildAssesmentDetails)
                    {
                        var _notes = explanatoryNotes.Where(x => x.SubComplaintID == item.SubComplaintID).ToList();
                        var _recommedations = recommendations.Where(x => x.SubComplaintID == item.SubComplaintID).ToList();
                        if (_notes != null && _notes.Count > 0)
                            item.ExplanatoryNotes = _notes[0];
                        if (_recommedations != null && _recommedations.Count > 0)
                            item.Recommendations = _recommedations;
                    }
                }
                ObjChildAssesmentData.SubComplaints = _childassesmentService.GetAllSubComplaints(request.TeacherID, request.AssesmentType, CurrentCulture);
                if (ObjChildAssesmentData.SubComplaints != null && ObjChildAssesmentData.SubComplaints.Count > 0)
                {
                    //var recommendations = ChildAssesmentDataAccess.GetAllRecommendations(request.TeacherID, CurrentCulture, request.AssesmentType);
                    //var explanatoryNotes = ChildAssesmentDataAccess.GetAllExplanatoryNotes(request.TeacherID, CurrentCulture, request.AssesmentType);
                    var advices = _childassesmentService.GetAllMappedAdvices(request.TeacherID, request.AssesmentType, CurrentCulture);
                    foreach (var subcomplaint in ObjChildAssesmentData.SubComplaints)
                    {
                        var _recommedations = recommendations.Where(x => x.SubComplaintID == subcomplaint.ID).ToList();
                        var _notes = explanatoryNotes.Where(x => x.SubComplaintID == subcomplaint.ID).ToList();
                        var _advices = advices.Where(x => x.SubComplaintID == subcomplaint.ID).ToList();
                        if (_recommedations != null && _recommedations.Count > 0)
                            subcomplaint.Recommendations = _recommedations;
                        if (_notes != null && _notes.Count > 0)
                            subcomplaint.ExplanatoryNotes = _notes[0];
                        if (_advices != null && _advices.Count > 0)
                            subcomplaint.Advices = _advices;
                    }

                }

            }
            else
            {
                ObjChildAssesmentData.IsAbsentConsecutively = isChildAbsentForThreeDays;
                ObjChildAssesmentData.ChildAssesmentDetails = _childassesmentService.GetChildAssesmentDetails(request.TeacherID, request.AssesmentType, CurrentCulture);

            }
            return ObjChildAssesmentData;

        }

       

        [HttpPost]
        [Route("UpdateChildAssesmentDetails")]
        public VmChildAssesmentResponse UpdateChildAssesmentDetails([FromBody] VmChildAssesmentSummary summary) =>
            _childassesmentService.UpdateChildAssesmentDetails(summary);



    }
}