﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/feedback")]
    public class FeedbackController : BaseController
    {
        private readonly IFeedbackService _feedbackService;
        public FeedbackController(IFeedbackService feedbackService)
        {
            _feedbackService = feedbackService;
        }

        [HttpPost]
        [Route("manage")]
        public VmFeedBack ManageFeedBack([FromBody] FeedBack feedback) => _feedbackService.Manage(feedback, CurrentCulture, ToCulture);

        [HttpGet]
        [Route("getfeedbacklist/{schoolId}/{startDate}/{endDate}")]
        public IEnumerable<VMFeedBack> GetFeedBackList(int schoolId, string startDate, string endDate)
            => _feedbackService.GetFeedBackData(schoolId, Convert.ToDateTime(startDate), Convert.ToDateTime(endDate));

        [HttpGet]
        [Route("getfeedBacktypes")]
        public IEnumerable<FeedBackTypeMaster> GetFeedBackTypes()
        {
            
                var result = _feedbackService.GetFeedBackTypes(CurrentCulture);
                return result;
            
        }
    }
}