﻿using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/school")]
    public class SchoolController : BaseController
    {
        private readonly ISchoolService _schoolService;
        public SchoolController(ISchoolService schoolService)
        {
            _schoolService = schoolService;
        }

        [HttpGet]
        [Route("allschool")]
        public IEnumerable<SchoolMaster> GetAllSchool() => _schoolService.GetAllSchool();

        [HttpGet]
        [Route("schoollistforuser/{userId}/{langId}")]
        public IEnumerable<SchoolMaster> GetSchoolListForUser(int userId, string langId) =>
            _schoolService.GetSchoolListForUser(userId, langId);

        [HttpPost]
        [Route("manage")]
        public int ManageSchool([FromBody]WebModel.SchoolMaster schoolMaster) => _schoolService.ManageSchool(schoolMaster);

        [HttpDelete]
        [Route("{schoolId}")]
        public void DeleteSchoolById(int schoolId) => _schoolService.DeleteSchoolById(schoolId);

        #region V3 - Req# 3

        [HttpGet]
        [Route("getdistrictschools/{district}")]
        public IEnumerable<SchoolMaster> GetDistrictSchools(string district) => _schoolService.GetDistrictSchools(district);

        #endregion
    }
}