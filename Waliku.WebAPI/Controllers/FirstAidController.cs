﻿using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/firstaid")]
    public class FirstAidController : BaseController
    {
        private readonly IFirstAidService _firstAidService;
        public FirstAidController(IFirstAidService firstAidService)
        {
            _firstAidService = firstAidService;
        }

        [HttpPost]
        [Route("manageanalytics")]
        public bool ManageFirstAidAnalytics([FromBody] FirstAidAnlytics entity) => _firstAidService.ManageFirstAidAnalytics(entity);

        [HttpGet]
        [Route("counterlist")]
        public IEnumerable<WebViewModel.VmFirstAidAnalytics> GetFirstAidCounterList() => _firstAidService.GetFirstAidCounterList();

        [HttpGet]
        [Route("list")]
        public IEnumerable<WebModel.FirstAidMaster> GetAllFirstAidList() => _firstAidService.GetAllFirstAidList();
    }
}