﻿using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/class")]
    public class ClassController : BaseController
    {
        private readonly IClassService _classService;
        public ClassController(IClassService classService)
        {
            _classService = classService;
        }

        [HttpGet]
        [Route("allclasses")]
        public IEnumerable<WebModel.ClassMaster> GetAllClasses() => _classService.GetAllClasses();

        [HttpGet]
        [Route("allteacherclasses/{userId}/{flag}")]
        public IEnumerable<WebModel.TeacherClassMapping> GetAllTeacherClasses(int userId, bool flag) => _classService.GetAllTeacherClasses(userId, flag);

        [HttpGet]
        [Route("allteacherclasses/{teacherId}")]
        public IEnumerable<WebModel.ClassMaster> GetAllTeacherClasses(int teacherId) => _classService.GetAllTeacherClasses(teacherId);

        [HttpGet]
        [Route("allclassesforme/{schoolId}/{userId}")]
        public IEnumerable<WebModel.ClassMaster> GetAllClassesForMe(int schoolId, int userId) => _classService.GetAllClassesForMe(schoolId, userId);

        [HttpDelete]
        [Route("{classId}")]
        public void DeleteClassById(int classId) => _classService.DeleteClassById(classId);

        [HttpPost]
        [Route("manage")]
        public void ManageClass([FromBody] WebModel.ClassMaster classMaster) => _classService.ManageClass(classMaster);

        [HttpPost]
        [Route("getmyclasssummary")]
        public IEnumerable <VmClassAttendanceSummary> GetMyClassSummary([FromBody] MyClassSummaryRequestParams objParams)
          => _classService.GetMyClassAttendanceSummary(objParams.TeacherId, objParams.ClassRoomId);

                
           
        

    }
}