﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
using WebRequestModel = Waliku.Domain.Models.WebModel.RequestModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;


namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/reports")]
    public class ReportController : BaseController
    {

        private readonly IReportService _reportService;
        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpGet]
        [Route("classpresentsorabsentschildren/{classRoomId}/{selectedDate}")]
        public IEnumerable<WebModel.AbsentOrPresentMaster> GetClassPresentsOrAbsentsChildren(string classRoomId, DateTime selectedDate)
            => _reportService.GetClassPresentsOrAbsentsChildren(classRoomId, selectedDate);

        [HttpPost]
        [Route("classwisepresentabsentreport")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReport([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetClassWisePresentAbsentReport(model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("classwisepresentabsentreportforadmin")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportForAdmin([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetClassWisePresentAbsentReportForAdmin(model.SchoolId, model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("classwisepresentabsentreport")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportOnload([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetClassWisePresentAbsentReportOnload(model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("holidaylist")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetHolidayList([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetHolidayList(model.ClassId, model.SelectedDate, model.SelectedType);

        [HttpPost]
        [Route("monthlyattendanceratesummary")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummary([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyAttendanceRateSummary(model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("monthlyattendanceratesummaryforadmin")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForAdmin([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyAttendanceRateSummaryForAdmin(model.SchoolId, model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("monthlyattendanceratesummaryforsa")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForSa([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyAttendanceRateSummaryForSa(model.SchoolId, model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("monthlyreasoncount")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonCount([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyReasonCount(model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId, model.LanguageId);

        [HttpPost]
        [Route("monthlyreasoncount2")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonCount2([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyReasonCount2(model.SelectedDate);

        [HttpPost]
        [Route("monthlyreasoncountforsa")]
        public IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountForSa([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyReasonCountForSa(model.SchoolId, model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId, model.LanguageId);

        [HttpPost]
        [Route("monthlyreasoncountnew")]
        public IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountNew([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyReasonCountNew(model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId, model.LanguageId);

        [HttpPost]
        [Route("monthlyreasonreportrate")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyReasonReportRate([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyReasonReportRate(model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("monthlystudentabsent")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsent([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyStudentAbsent(model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("monthlystudentabsentforsa")]
        public IEnumerable<WebModel.ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsentForSa([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetMonthlyStudentAbsentForSa(model.SchoolId, model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("getstudentwisepresentabsentreport")]
        public IEnumerable<VmChildrenReport> GetStudentWisePresentAbsentReport([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetStudentWisePresentAbsentReport(model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId);

        [HttpPost]
        [Route("teacheryearly")]
        public WebViewModel.VmYearlyReport GetTeacherYearly([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetTeacherYearly(model.ClassId, model.TeacherId);

        [HttpPost]
        [Route("yearlyhealthreason")]
        public IEnumerable<dynamic> GetYearlyHealthReason([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetYearlyHealthReason(model.SchoolId, model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId, model.LanguageId);

        [HttpPost]
        [Route("yearlynonhealthreason")]
        public IEnumerable<dynamic> GetYearlyNonHealthReason([FromBody] WebRequestModel.ReportRequest model)
            => _reportService.GetYearlyNonHealthReason(model.SchoolId, model.ClassId, model.SelectedDate, model.SelectedType, model.TeacherId, model.LanguageId);

        [HttpPost]
        [Route("getyearlyabsenteeschartreport")]
        public YearlyAbsenteesChartSummary GetYearlyAbsenteesChartReport([FromBody] ClassDashboardRequestParams reportParams)
        {
            var yearlyAbsenteesSummary = _reportService.GetYearlyAbsenteesChartReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID);

            var yearlyTopFiveHealthAbsenteesSummary = _reportService.GetYearlyTopFiveHealthChartReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID, 1, CurrentCulture);

            var yearlyTopFiveNonHealthAbsenteesSummary = _reportService.GetYearlyTopFiveHealthChartReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID, 2, CurrentCulture);

            var yearlyChronicAbsenteesSummary = _reportService.GetYearlyChronicAbsentChartReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID);

            var response = new YearlyAbsenteesChartSummary();
            response.YearlyAbsenteesSummary = yearlyAbsenteesSummary;
            response.YearlyTopFiveHealthAbsenteesSummary = yearlyTopFiveHealthAbsenteesSummary;
            response.YearlyTopFiveNonHealthAbsenteesSummary = yearlyTopFiveNonHealthAbsenteesSummary;
            response.YearlyChronicAbsenteesSummary = yearlyChronicAbsenteesSummary;

            return response;
        }

        [HttpPost]
        [Route("getchildsummary")]
        public ChildDashboard GetChildSummary([FromBody] ChildSummaryRequestParams reportParams)
        {
            var result = new ChildDashboard();
            result.DashboardSummary = _reportService.GetChildDashboardSummary(reportParams.ChildID, reportParams.AttendanceDate);
            result.ReasonSummary = _reportService.GetChildReasonSummary(reportParams.ChildID, reportParams.AttendanceDate, CurrentCulture);
            return result;
        }

        [HttpPost]
        [Route("GetWeeklyChartReport")]
        public IEnumerable<ClassDashboardSummary> GetWeeklyChartReport([FromBody] WeeklyChartReportRequestParams reportParams)
        {
            var result = new List<ClassDashboardSummary>();
            result = _reportService.GetWeeklyChartReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID, reportParams.Type, CurrentCulture).ToList();
            return result;
        }

        [HttpPost]
        [Route("GetClassDashboardReport")]
        public HttpResponseMessage GetClassDashboardReport([FromBody] ClassDashboardRequestParams reportParams)
        {
            try
            {
                var result = new ClassDashboard();
                
                var dashboard = _reportService.GetClassDashboardReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID, reportParams.Type, CurrentCulture);
                if (dashboard != null && dashboard.Count() > 0)
                    result.ClassDashboardSummary = dashboard.ToList();

                result.ClassTopReasonsForAbsence = _reportService.GetClassTopReasonsForAbsence(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID, reportParams.Type, CurrentCulture).ToList();

                result.UnknownAbsence = result.ClassTopReasonsForAbsence.Any(x => x.Description.ToUpper() == "UNKNOWN")
                    ? result.ClassTopReasonsForAbsence.Where(w => w.Description.ToUpper() == "UNKNOWN").FirstOrDefault().ReasonCount : 0;

                if (result.ClassTopReasonsForAbsence.Any(x => x.Description.ToUpper() == "UNKNOWN"))
                {
                    var obj = result.ClassTopReasonsForAbsence.Where(x => x.Description.ToUpper() == "UNKNOWN").FirstOrDefault();
                    result.ClassTopReasonsForAbsence.Remove(obj);
                }

                return Request.CreateResponse<ClassDashboard>(HttpStatusCode.OK, result);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        [HttpPost]
        [Route("getweeklysummaryreport")]
        public SummaryData GetWeeklySummaryReport([FromBody]ClassDashboardRequestParams reportParams)
           => _reportService.GetWeeklySummaryReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID, reportParams.Type, CurrentCulture);

        [HttpPost]
        [Route("GetWeeklyListReport")]
        public IEnumerable<VmChildrenReport> GetWeeklyListReport([FromBody]ClassDashboardRequestParams gridParams)
            => _reportService.GetWeeklyListReport(gridParams.ClassRoomID, gridParams.TeacherID, gridParams.AttendanceDate, gridParams.Type);

        [HttpPost]
        [Route("GetMonthlySummaryReport")]
        public SummaryData GetMonthlySummaryReport([FromBody]ClassDashboardRequestParams reportParams)
         => _reportService.GetMonthlySummaryReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID, reportParams.Type, CurrentCulture);

        [HttpPost]
        [Route("GetMonthlyListReport")]
        public IEnumerable<ChildDashboardSummary> GetMonthlyListReport([FromBody]ClassDashboardRequestParams reportParams)
           => _reportService.GetMonthlyListReport(reportParams.TeacherID, reportParams.AttendanceDate, reportParams.ClassRoomID, reportParams.Type);

    }
}