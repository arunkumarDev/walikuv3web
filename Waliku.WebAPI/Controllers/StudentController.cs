﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/children")]
    public class StudentController : BaseController
    {
        private readonly IStudentService _studentService;
        private readonly IClassService _classService;
        public StudentController(IStudentService studentService, IClassService classService)
        {
            _studentService = studentService;
            _classService = classService;
        }

        [HttpGet]
        [Route("getclasscyschool/{id}")]
        public IEnumerable<ClassMaster> GetClassBySchool(int id) => _classService.GetAllClassBySchool(id);

        [HttpPost]
        [Route("getchildrenbyclass")]
        public IEnumerable<VmChildren> GetChildrenByClassId([FromBody] ChildRequestParams request) => _studentService.GetChildrenByClassId(request.ClassRoomId, request.AttendanceDate);

        [HttpPost]
        [Route("getallabsencechildrenbyclass")]
        public IEnumerable<VmChildren> GetAbsenceChildrenByClassId([FromBody] AbsenceChildRequestParams absenceChildRequest)
        {
            absenceChildRequest.StartDate = (absenceChildRequest.StartDate == DateTime.MinValue) ? DateTime.Now : absenceChildRequest.StartDate;
            absenceChildRequest.EndDate = (absenceChildRequest.EndDate == DateTime.MinValue) ? DateTime.Now : absenceChildRequest.EndDate;
            IEnumerable<VmChildren> result;
            if (absenceChildRequest.Type == 2)
                result = _studentService.GetAbsentChildrenReport(absenceChildRequest.ClassRoomId, absenceChildRequest.StartDate, absenceChildRequest.EndDate);
            else
                result = _studentService.GetAbsentChildrenByClassId(absenceChildRequest.ClassRoomId, absenceChildRequest.StartDate, absenceChildRequest.EndDate);
            return result;
        }

        [HttpPost]
        [Route("getcount")]
        public VmCount ClassCountAbsenceCount([FromBody] ChildRequestParams request) => _studentService.GetTeacherClassCountAndAbsenceCount(request.UserId, request.AttendanceDate);

        [HttpGet]
        [Route("allchildrenforme/{userId}/{userType}/{langId}")]
        public IEnumerable<ChildrenMaster> GetAllChildrenForMe(int userId, int userType, string langId)
            => _studentService.GetAllChildrenForMe(userId, userType, langId);

        [HttpGet]
        [Route("allchildrenforme/{userId}/{userType}/{schoolId}/{classRoomId}/{langId}")]
        public IEnumerable<ChildrenMaster> GetAllChildrenForMeByFilter(int userId, int userType, int schoolId,
            int classRoomId, string langId)
            => _studentService.GetAllChildrenForMeByFilter(userId, userType, schoolId, classRoomId, langId);

        [HttpGet]
        [Route("childimage/{childId}")]
        public string GetChildImageById(int childId) => _studentService.GetChildImageById(childId);

        [HttpDelete]
        [Route("{childId}")]
        public void DeleteChildById(int childId) => _studentService.DeleteChildById(childId);

        [HttpGet]
        [Route("{childId}")]
        public VmChildren GetChildDetailsById(int childId) =>
            _studentService.GetChildDetailsById(childId);

        [HttpGet]
        [Route("getdashboardabsencedetails/{childId}")]
        public List<VMChildAbsenceDetails> GetStudentDashboardAbsenceDetails(int childId)
        {
            var result = _studentService.GetStudentDashboardAbsenceDetails(childId);
            if (result != null && result.Count > 0)
            {
                result[0].ReasonUnsubmitted = JsonConvert.DeserializeObject<List<ReasonUnsubmitted>>(result[0].AttendanceDates);
            }

            return  result;
        }
           



    }
}