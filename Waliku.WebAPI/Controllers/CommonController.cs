﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/common")]
    public class CommonController : BaseController
    {
        private readonly IStudentService _studentService;
        private readonly IClassService _classService;
        private readonly IUserService _userService;
        private readonly ISchoolService _schoolService;
        private readonly IVisitorHomeService _visitorHomeService;
        private readonly IMasterService _masterService;
        private readonly ICommonService _commonService;
        public CommonController(IStudentService studentService, IClassService classService,
            IUserService userService, ISchoolService schoolService, IVisitorHomeService visitorHomeService,
            IMasterService masterService, ICommonService commonService)
        {
            _studentService = studentService;
            _classService = classService;
            _userService = userService;
            _schoolService = schoolService;
            _visitorHomeService = visitorHomeService;
            _masterService = masterService;
            _commonService = commonService;
        }

        [HttpPost]
        [Route("getchildrenbyteacheridandclassidanddatewise")]
        public IEnumerable<AttendanceMaster> GetChildrenByTeacherIdAndClassIdAndDateWise([FromBody] AttendanceMaster attendanceMaster)
            => _studentService.GetChildrenByTeacherIdAndClassIdAndDateWise(attendanceMaster.TeacherID, attendanceMaster.ClassRoomID, attendanceMaster.AttendanceDate);

        [HttpGet]
        [Route("getchildrenbyteacherid/{id}")]
        public IEnumerable<ChildrenMaster> GetChildrenByTeacherId(int id) => _studentService.GetChildrenByTeacherId(id);

        [HttpGet]
        [Route("getallchildren")]
        public IEnumerable<ChildrenMaster> GetAllChildren() => _studentService.GetAllChildren();

        [HttpGet]
        [Route("getclassesbyteacherid/{id}")]
        public IEnumerable<ClassMaster> GetClassesByTeacherId(int id) => _classService.GetClassesByTeacherId(id);

        [HttpGet]
        [Route("getchilddetailbyid/{id}")]
        public ChildrenMaster GetChildDetailById(int id) => _studentService.GetChildDetailById(id);

        [HttpGet]
        [Route("getallcommunityworker")]
        public IEnumerable<UserMaster> GetAllCommunityWorker() => _userService.GetAllCommunityWorker();

        [HttpGet]
        [Route("getallschool")]
        public IEnumerable<SchoolMaster> GetAllSchool() => _schoolService.GetAllSchool();

        [HttpGet]
        [Route("getvisithomedetailbyteacherid/{id}")]
        public IEnumerable<VisitHomeMaster> GetVisitHomeDetailByTeacherId(int id) => _visitorHomeService.GetVisitHomeDetailByTeacherId(id);

        [HttpPost]
        [Route("managevisithome")]
        public void ManageVisitHome([FromBody] VisitHomeMaster visitHomeMaster) => _visitorHomeService.ManageVisitHome(visitHomeMaster);

        [HttpGet]
        [Route("getallsickchildreason")]
        public IEnumerable<SickChildMaster> GetAllSickChildReason() => _masterService.GetAllSickChildReason();

        [HttpGet]
        [Route("getallreasontype")]
        public IEnumerable<ReasonType> GetAllReasonType() => _masterService.GetAllReasonType();

        [HttpGet]
        [Route("getuserbytype/{userType}")]
        public IEnumerable<UserMaster> GetAllUserByType(int userType) => _userService.GetUsersByType(userType);

        [HttpPost]
        [Route("managechildren")]
        public bool ManageChildren([FromBody] ChildrenMaster childrenMaster)
        {
            string strImageName = Guid.NewGuid().ToString() + ".jpg";
            if (childrenMaster != null && childrenMaster.Image != null)
            {
                string filePath = "~/Photos/" + strImageName;
                File.WriteAllBytes(HttpContext.Current.Server.MapPath(filePath), Convert.FromBase64String(childrenMaster.Image));

                string destiPath = ConfigurationManager.AppSettings["UploadDestinationPath"] + strImageName;

                File.Copy(filePath, destiPath, true);
            }

            if (childrenMaster != null)
            {
                childrenMaster.Image = strImageName;
                _studentService.ManageChildren(childrenMaster);
            }

            return true;
        }

        [HttpGet]
        [Route("getofflinedata/{id}")]
        public IDictionary<string, object> GetOfflineData(int id) => _commonService.GetOfflineData(id, CurrentCulture);

        [HttpPost]
        [Route("getofflinedata")]
        public IDictionary<string, object> GetOfflineData([FromBody] ChildRequestParams childRequest) => _commonService.GetOfflineData(childRequest, CurrentCulture);

        [HttpGet]
        [Route("allactivereasontypes")]
        public IEnumerable<ReasonType> GetAllActiveReasonTypes() => _masterService.GetAllActiveReasonTypes();

        [HttpPost]
        [Route("getofflinedataV2")]
        public IDictionary<string, object> GetOfflineDataV2([FromBody] OfflineDataRequestParams childRequest)
        => _commonService.GetOfflineDataV2(childRequest);

        [HttpPost]
        [Route("getassessmentofflinedataV2")]
        public IDictionary<string, object> GetAssessmentOfflineDataV2([FromBody] OfflineDataRequestParams childRequest)
       => _commonService.GetAssessmentOfflineDataV2(childRequest);

        #region V3 - Req# 3

        [HttpGet]
        [Route("getroles")]
        public IEnumerable<vmRole> GetAllActiveRoles() => _masterService.GetAllActiveRoles();

        [HttpGet]
        [Route("getrolesbelow/{roleName}")]
        public IEnumerable<vmRole> GetUnderprivilegedRoles(string roleName) => _masterService.GetUnderprivilegedRoles(roleName);

        [HttpGet]
        [Route("getdistricts")]
        public IEnumerable<vmDistrict> GetAllActiveDistricts()
        {
            ///Sample Code to read claims    
            //var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();
            //string uId = claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value.ToString();
            //string uName = claims.First(x => x.Type == ClaimTypes.Name).Value.ToString();
            //var uRole = claims.Where(x => x.Type == ClaimTypes.Role).Select(y => y.Value).ToList();

            ////Read Custom Claims From DB AspNetUserClaims
            //string uCountry = claims.First(x => x.Type == "Country").Value;        
            //string uCustom = claims.First(x => x.Type == "WalikuCustom").Value.ToString();
            ///

            var response = _masterService.GetAllActiveDistricts();

            //Exception exception = new Exception("log new exception");
            //Error error = new Error
            //{
            //    Exception = exception.ToString(),
            //    LogTime = System.DateTime.Now,
            //    Message = exception.Message,
            //    Request = Request.RequestUri.ToString()
            //};
            //var x = _logService.AddErrorLog(error);

            return response;
        }
        #endregion
    }
}