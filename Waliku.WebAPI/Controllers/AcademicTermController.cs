﻿using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/academicterm")]
    public class AcademicTermController : BaseController
    {
        private readonly IAcademicTermService _academicTermService;
        public AcademicTermController(IAcademicTermService academicTermService)
        {
            _academicTermService = academicTermService;
        }
        [HttpDelete]
        [Route("{id}")]
        public bool DeleteAcademicTermById(int id) => _academicTermService.DeleteAcademicTermById(id);

        [HttpGet]
        [Route("{userId}/{userType}")]
        public IEnumerable<WebModel.DataModels.AcademicTerm> GetAcademicTermDetails(int userId, int userType) => _academicTermService.GetAcademicTermDetails(userId, userType);

        [HttpGet]
        [Route("iscurrenttermexist/{schoolId}")]
        public bool IsCurrentTermExist(int schoolId) => _academicTermService.IsCurrentTermExist(schoolId);

        [HttpPost]
        [Route("manage")]
        public void ManageAcademicTerm([FromBody] WebModel.DataModels.AcademicTerm academicTerm) => _academicTermService.ManageAcademicTerm(academicTerm);
    }
}