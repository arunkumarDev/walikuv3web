﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/permission")]
    public class PermissionController : BaseController
    {
        private readonly IPermissionService _permissionService;

        public PermissionController()
        {

        }

        public PermissionController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }

        [HttpPost]
        [Route("rolematrix")]
        public RoleMatrix GetRoleMatrix([FromBody] int[] roleIds) => _permissionService.GetRoleMatrix(roleIds);

        [HttpGet]
        [Route("userrolematrix/{userName}")]
        public RoleMatrix GetUserRoleMatrix(string userName) => _permissionService.GetUserRoleMatrix(userName);

        [HttpGet]
        [Route("allrolematrix")]
        public RoleMatrix GetAllRoleMatrix() => _permissionService.GetAllRoleMatrix();

        [HttpGet]
        [Route("usercomponents/{userName}")]
        public IEnumerable<Component> GetUserComponents(string userName) => _permissionService.GetUserComponents(userName);
      
        [HttpGet]
        [Route("userrolecomponents/{userName}/{roleName}")]
        public IEnumerable<Component> GetUserRoleComponents(string userName, string roleName) => _permissionService.GetUserRoleComponents(userName, roleName);

        [HttpPost]
        [Route("save")]
        public bool SavePermission([FromBody] IEnumerable<Permission> permissions) => _permissionService.SavePermission(permissions);

        [HttpGet]
        [Route("getuserroles/{userName}")]
        public IEnumerable<vmRole> GetUserRoles(string userName) => _permissionService.GetUserRoles(userName);

         }
}