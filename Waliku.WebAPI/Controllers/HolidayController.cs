﻿using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/holiday")]
    public class HolidayController : BaseController
    {
        private readonly IHolidayService _holidayService;
        public HolidayController(IHolidayService holidayService)
        {
            _holidayService = holidayService;
        }

        [HttpPost]
        [Route("get")]
        public IEnumerable<YearlyHolidayMaster> GetHolidayListByMonthForTeacher([FromBody] HolidayRequestParams holidayRequest)
         => _holidayService.GetHolidayListByMonthForTeacher(holidayRequest.HolidayDate, holidayRequest.UserId, 0, CurrentCulture);

        [HttpGet]
        [Route("{schoolId}/{langId}")]
        public IEnumerable<WebModel.AcademicCalenderMaster> GetAllHoliday(int schoolId, string langId)
            => _holidayService.GetAllHoliday(schoolId, langId);

        [HttpPost]
        [Route("manage")]
        public void ManageHoliday([FromBody] WebModel.AcademicCalenderMaster academicCalender) =>
            _holidayService.ManageHoliday(academicCalender);

        [HttpDelete]
        [Route("{id}")]
        public void DeleteHolidayById(int id) => _holidayService.DeleteHolidayById(id);
    }
}