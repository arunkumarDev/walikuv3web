﻿using System.Collections.Generic;
using System.Web.Http;
using Waliku.Business.Interface;
using Waliku.Domain.Models;
namespace Waliku.WebAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/config")]
    public class ConfigurationController : BaseController
    {
        private readonly IConfigurationService _configurationService;
        public ConfigurationController(IConfigurationService configurationService)
        {
            _configurationService = configurationService;
        }

        [HttpGet]
        [Route("allconfigurationgroups")]
        public IEnumerable<ConfigurationGroup> GetAllConfigurationGroup() => _configurationService.GetAllConfigurationGroup();

        [HttpGet]
        [Route("configurationgroup/{id}")]
        public ConfigurationGroup GetConfigurationGroupById(int id) => _configurationService.GetConfigurationGroupById(id);

        [HttpGet]
        [Route("allconfigurations")]
        public IEnumerable<Configuration> GetAllConfigurations() => _configurationService.GetAllConfigurations();

        [HttpGet]
        [Route("configurationsbygroupid/{configurationGroupId}")]
        public IEnumerable<Configuration> GetConfigurationByGroupId(int configurationGroupId) =>
            _configurationService.GetConfigurationByGroupId(configurationGroupId);

        [HttpGet]
        [Route("configurationsbygroupname/{configurationGroupName}")]
        public IEnumerable<Configuration> GetConfigurationByGroupName(string configurationGroupName) =>
            _configurationService.GetConfigurationByGroupName(configurationGroupName);

        [HttpGet]
        [Route("configurationbykey/{key}")]
        public Configuration GetConfigurationByKey(string key) => _configurationService.GetConfigurationByKey(key);
    }
}