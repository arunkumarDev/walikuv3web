﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
using WebRequestModel = Waliku.Domain.Models.WebModel.RequestModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Web.RestClient
{
    public interface IReportRestService
    {
        Task<IEnumerable<WebModel.AbsentOrPresentMaster>> GetClassPresentsOrAbsentsChildren(string classRoomId, DateTime selectedDate);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetClassWisePresentAbsentReport(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetClassWisePresentAbsentReportForAdmin(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetClassWisePresentAbsentReportOnload(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetHolidayList(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyAttendanceRateSummary(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyAttendanceRateSummaryForAdmin(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyAttendanceRateSummaryForSa(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCount(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCount2(WebRequestModel.ReportRequest model);
        Task<IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>> GetMonthlyReasonCountForSa(WebRequestModel.ReportRequest model);
        Task<IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>> GetMonthlyReasonCountNew(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonReportRate(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyStudentAbsent(WebRequestModel.ReportRequest model);
        Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyStudentAbsentForSa(WebRequestModel.ReportRequest model);
        Task<IEnumerable<VmChildrenReport>> GetStudentWisePresentAbsentReport(WebRequestModel.ReportRequest model);
        Task<WebViewModel.VmYearlyReport> GetTeacherYearly(WebRequestModel.ReportRequest model);
        Task<IEnumerable<dynamic>> GetYearlyHealthReason(WebRequestModel.ReportRequest model);
        Task<IEnumerable<dynamic>> GetYearlyNonHealthReason(WebRequestModel.ReportRequest model);
    }

    public class ReportRestService : RestClientBase, IReportRestService
    {
        private const string BaseEndpoint = "api/report/";

        public ReportRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<IEnumerable<WebModel.AbsentOrPresentMaster>> GetClassPresentsOrAbsentsChildren(string classRoomId, DateTime selectedDate)
        {
            var request = new RestRequest(BaseEndpoint + $"classpresentsorabsentschildren/{classRoomId}/{selectedDate}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.AbsentOrPresentMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetClassWisePresentAbsentReport(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"classwisepresentabsentreport", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetClassWisePresentAbsentReportForAdmin(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"classwisepresentabsentreportforadmin", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetClassWisePresentAbsentReportOnload(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"classwisepresentabsentreport", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetHolidayList(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"holidaylist", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyAttendanceRateSummary(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlyattendanceratesummary", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyAttendanceRateSummaryForAdmin(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlyattendanceratesummaryforadmin", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyAttendanceRateSummaryForSa(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlyattendanceratesummaryforsa", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCount(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlyreasoncount", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonCount2(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlyreasoncount2", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>> GetMonthlyReasonCountForSa(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlyreasoncountforsa", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>>(request);
            return response.Data;
        }

        public async Task<IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>> GetMonthlyReasonCountNew(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlyreasoncountnew", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IDictionary<string, IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyReasonReportRate(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlyreasonreportrate", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyStudentAbsent(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlystudentabsent", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>> GetMonthlyStudentAbsentForSa(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"monthlystudentabsentforsa", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ChartMaster.PresentAbsetReport>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<VmChildrenReport>> GetStudentWisePresentAbsentReport(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"studentwisepresentabsentreport", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<VmChildrenReport>>(request);
            return response.Data;
        }

        public async Task<WebViewModel.VmYearlyReport> GetTeacherYearly(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"teacheryearly", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<WebViewModel.VmYearlyReport>(request);
            return response.Data;
        }

        public async Task<IEnumerable<dynamic>> GetYearlyHealthReason(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"yearlyhealthreason", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<dynamic>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<dynamic>> GetYearlyNonHealthReason(WebRequestModel.ReportRequest model)
        {
            var request = new RestRequest(BaseEndpoint + $"yearlynonhealthreason", Method.POST, DataFormat.Json).AddJsonBody(model);
            var response = await RestClient.ExecuteAsync<IEnumerable<dynamic>>(request);
            return response.Data;
        }
    }
}