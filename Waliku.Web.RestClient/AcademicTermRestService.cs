﻿
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

using WebModel = Waliku.Domain.Models.WebModel.DataModels;

namespace Waliku.Web.RestClient
{
    public interface IAcademicTermRestService
    {
        Task<IEnumerable<WebModel.AcademicTerm>> GetAcademicTermDetails(int userId, int userType);
        Task<bool> DeleteAcademicTermById(int id);
        Task<bool> ManageAcademicTerm(WebModel.AcademicTerm academicTerm);
        Task<bool> IsCurrentTermExist(int schoolId);
    }

    public class AcademicTermRestService : RestClientBase, IAcademicTermRestService
    {
        private const string BaseEndpoint = "academicterm/";
        public AcademicTermRestService(string token, string languageId) : base(token, languageId)
        {
        }
        public async Task<bool> DeleteAcademicTermById(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"{id}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<bool>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.AcademicTerm>> GetAcademicTermDetails(int userId, int userType)
        {
            var request = new RestRequest(BaseEndpoint + $"{userId}/{userType}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.AcademicTerm>>(request);
            return response.Data;
        }

        public async Task<bool> IsCurrentTermExist(int schoolId)
        {
            var request = new RestRequest(BaseEndpoint + $"iscurrenttermexist/{schoolId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<bool>(request);
            return response.Data;
        }

        public async Task<bool> ManageAcademicTerm(WebModel.AcademicTerm academicTerm)
        {
            var request = new RestRequest(BaseEndpoint + "manage", Method.POST).AddJsonBody(academicTerm);
            await RestClient.ExecuteAsync(request);
            return true;
        }
    }
}