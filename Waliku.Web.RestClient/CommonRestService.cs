﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
namespace Waliku.Web.RestClient
{
    public interface ICommonRestService
    {
        Task<IEnumerable<AttendanceMaster>> GetChildrenByTeacherIdAndClassIdAndDateWise(AttendanceMaster attendanceMaster);
        Task<IEnumerable<ChildrenMaster>> GetChildrenByTeacherId(int id);
        Task<IEnumerable<ChildrenMaster>> GetAllChildren();
        Task<IEnumerable<ClassMaster>> GetClassesByTeacherId(int id);
        Task<ChildrenMaster> GetChildDetailById(int id);
        Task<IEnumerable<UserMaster>> GetAllCommunityWorker();
        Task<IEnumerable<SchoolMaster>> GetAllSchool();
        Task<IEnumerable<VisitHomeMaster>> GetVisitHomeDetailByTeacherId(int id);
        Task<bool> ManageVisitHome(VisitHomeMaster visitHomeMaster);
        Task<IEnumerable<SickChildMaster>> GetAllSickChildReason();
        Task<IEnumerable<ReasonType>> GetAllReasonType();
        Task<IEnumerable<UserMaster>> GetAllUserByType(int userType);
        Task<bool> ManageChildren(ChildrenMaster childrenMaster);
        Task<IDictionary<string, object>> GetOfflineData(int id);
        Task<IDictionary<string, object>> GetOfflineData(ChildRequestParams childRequest);
        Task<IEnumerable<ReasonType>> GetAllActiveReasonTypes();
    }

    public class CommonRestService : RestClientBase, ICommonRestService
    {
        private const string BaseEndpoint = "api/class/";
        public CommonRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<IEnumerable<AttendanceMaster>> GetChildrenByTeacherIdAndClassIdAndDateWise(AttendanceMaster attendanceMaster)
        {
            var request = new RestRequest(BaseEndpoint + "getchildrenbyteacheridandclassidanddatewise", Method.POST, DataFormat.Json).AddJsonBody(attendanceMaster);
            var response = await RestClient.ExecuteAsync<IEnumerable<AttendanceMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<ChildrenMaster>> GetChildrenByTeacherId(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"getchildrenbyteacherid/{id}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ChildrenMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<ChildrenMaster>> GetAllChildren()
        {
            var request = new RestRequest(BaseEndpoint + $"getallchildren", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ChildrenMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<ClassMaster>> GetClassesByTeacherId(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"getclassesbyteacherid/{id}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ClassMaster>>(request);
            return response.Data;
        }

        public async Task<ChildrenMaster> GetChildDetailById(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"getchilddetailyid/{id}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<ChildrenMaster>(request);
            return response.Data;
        }

        public async Task<IEnumerable<UserMaster>> GetAllCommunityWorker()
        {
            var request = new RestRequest(BaseEndpoint + $"getallcommunityworker", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<UserMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<SchoolMaster>> GetAllSchool()
        {
            var request = new RestRequest(BaseEndpoint + $"getallschool", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<SchoolMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<VisitHomeMaster>> GetVisitHomeDetailByTeacherId(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"getvisithomedetailbyteacherid/{id}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<VisitHomeMaster>>(request);
            return response.Data;
        }

        public async Task<bool> ManageVisitHome(VisitHomeMaster visitHomeMaster)
        {
            var request = new RestRequest(BaseEndpoint + $"managevisithome", Method.POST, DataFormat.Json).AddJsonBody((visitHomeMaster));
            var response = await RestClient.ExecuteAsync<IEnumerable<VisitHomeMaster>>(request);
            return response.IsSuccessful;
        }

        public async Task<IEnumerable<SickChildMaster>> GetAllSickChildReason()
        {
            var request = new RestRequest(BaseEndpoint + $"getallsickchildreason", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<SickChildMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<ReasonType>> GetAllReasonType()
        {
            var request = new RestRequest(BaseEndpoint + $"getallreasontype", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ReasonType>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<UserMaster>> GetAllUserByType(int userType)
        {
            var request = new RestRequest(BaseEndpoint + $"getuserbytype/{userType}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<UserMaster>>(request);
            return response.Data;
        }

        public async Task<bool> ManageChildren(ChildrenMaster childrenMaster)
        {
            var request = new RestRequest(BaseEndpoint + $"managechildren", Method.POST, DataFormat.Json).AddJsonBody(childrenMaster);
            var response = await RestClient.ExecuteAsync<bool>(request);
            return response.Data;
        }

        public async Task<IDictionary<string, object>> GetOfflineData(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"getofflinedata/{id}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IDictionary<string, object>>(request);
            return response.Data;
        }

        public async Task<IDictionary<string, object>> GetOfflineData(ChildRequestParams childRequest)
        {
            var request = new RestRequest(BaseEndpoint + $"getofflinedata", Method.POST, DataFormat.Json).AddJsonBody(childRequest);
            var response = await RestClient.ExecuteAsync<IDictionary<string, object>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<ReasonType>> GetAllActiveReasonTypes()
        {
            var request = new RestRequest(BaseEndpoint + $"allactivereasontypes", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ReasonType>>(request);
            return response.Data;
        }
    }
}