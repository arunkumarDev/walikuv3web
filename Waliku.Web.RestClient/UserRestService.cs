﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Web.RestClient
{
    public interface IUserRestService
    {
        Task<bool> Ping();
        Task<VmLogin> Post(UserMaster loginModel);
        Task<bool> MigrateUser();
        Task<dynamic> GetUserById(int userId);
        Task<bool> DeleteUserById(int userId);
        Task<IEnumerable<WebModel.WebUserMaster>> GetMyUserList(int userId, string langId);
        Task<IEnumerable<UserMaster>> GetAllUsers();
        Task<IEnumerable<WebModel.WebUserMaster>> GetAllTeachers();
        Task<IEnumerable<WebModel.SchoolAdminMappingMaster>> GetAllSchoolAdmin();
        Task<IEnumerable<WebModel.SchoolAdminMapping>> GetSchoolAdminMapping(int schoolId);
        Task<IEnumerable<WebModel.WebUserMaster>> GetTeacherClassMappingDetails(int classRoomId);
        Task<IEnumerable<WebModel.UserTypeMaster>> GetAllUserTypes(int userType);
        Task<bool> ManageUser(WebModel.WebUserMaster userMaster);
        Task<bool> ManageAdmin(WebModel.SchoolAdminMappingMaster schoolAdmin);
        Task<bool> ManageAdminMapping(WebModel.SchoolAdminMapping schoolAdminMapping);
        Task<bool> ManageClassTeacher(WebModel.TeacherClassMapping teacherClassMapping);
        Task<bool> DeleteSchoolAdminById(int id);
        Task<bool> DeleteClassTeacherById(int id);
    }

    public class UserRestService : RestClientBase, IUserRestService
    {
        private const string BaseEndpoint = "api/user/";

        public UserRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<bool> Ping()
        {
            var request = new RestRequest(BaseEndpoint + $"ping", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<bool>(request);
            return response.Data;
        }

        public async Task<VmLogin> Post(UserMaster loginModel)
        {
            var request = new RestRequest(BaseEndpoint + $"login", Method.POST, DataFormat.Json).AddJsonBody(loginModel);
            var response = await RestClient.ExecuteAsync<VmLogin>(request);
            return response.Data;
        }


        public async Task<bool> MigrateUser()
        {
            var request = new RestRequest(BaseEndpoint + $"migrate", Method.POST, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<bool>(request);
            return response.Data;
        }

        public async Task<dynamic> GetUserById(int userId)
        {
            var request = new RestRequest(BaseEndpoint + $"{userId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<dynamic>(request);
            return response.Data;
        }

        public async Task<bool> DeleteUserById(int userId) 
        {
            var request = new RestRequest(BaseEndpoint + $"{userId}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<IEnumerable<WebModel.WebUserMaster>> GetMyUserList(int userId, string langId)
        {
            var request = new RestRequest(BaseEndpoint + $"myuserlist/{userId}/{langId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.WebUserMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<UserMaster>> GetAllUsers()
        {
            var request = new RestRequest(BaseEndpoint + $"allusers", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<UserMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.WebUserMaster>> GetAllTeachers()
        {
            var request = new RestRequest(BaseEndpoint + $"allteachers", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.WebUserMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.SchoolAdminMappingMaster>> GetAllSchoolAdmin()
        {
            var request = new RestRequest(BaseEndpoint + $"allschooladmin", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.SchoolAdminMappingMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.SchoolAdminMapping>> GetSchoolAdminMapping(int schoolId)
        {
            var request = new RestRequest(BaseEndpoint + $"schooladminmapping/{schoolId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.SchoolAdminMapping>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.WebUserMaster>> GetTeacherClassMappingDetails(int classRoomId)
        {
            var request = new RestRequest(BaseEndpoint + $"teacherclassmappingdetails/{classRoomId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.WebUserMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.UserTypeMaster>> GetAllUserTypes(int userType)
        {
            var request = new RestRequest(BaseEndpoint + $"allusertypes/{userType}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.UserTypeMaster>>(request);
            return response.Data;
        }

        public async Task<bool> ManageUser(WebModel.WebUserMaster userMaster)
        {
            var request = new RestRequest(BaseEndpoint + $"manage", Method.POST, DataFormat.Json).AddJsonBody(userMaster);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> ManageAdmin(WebModel.SchoolAdminMappingMaster schoolAdmin)
        {
            var request = new RestRequest(BaseEndpoint + $"manageadmin", Method.POST, DataFormat.Json).AddJsonBody(schoolAdmin);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> ManageAdminMapping(WebModel.SchoolAdminMapping schoolAdminMapping)
        {
            var request = new RestRequest(BaseEndpoint + $"manageadminmapping", Method.POST, DataFormat.Json).AddJsonBody(schoolAdminMapping);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> ManageClassTeacher(WebModel.TeacherClassMapping teacherClassMapping)
        {
            var request = new RestRequest(BaseEndpoint + $"manageclassteacher", Method.POST, DataFormat.Json).AddJsonBody(teacherClassMapping);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> DeleteSchoolAdminById(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"schooladmin/{id}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> DeleteClassTeacherById(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"classteacher/{id}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }
    }
}