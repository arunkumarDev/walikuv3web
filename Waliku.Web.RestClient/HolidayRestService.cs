﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Web.RestClient
{
    public interface IHolidayRestService
    {
        Task<IEnumerable<YearlyHolidayMaster>> GetHolidayListByMonthForTeacher(HolidayRequestParams holidayRequest);
        Task<IEnumerable<WebModel.AcademicCalenderMaster>> GetAllHoliday(int schoolId, string langId);
        Task<bool> ManageHoliday(WebModel.AcademicCalenderMaster academicCalender);
        Task<bool> DeleteHolidayById(int id);
    }

    public class HolidayRestService : RestClientBase, IHolidayRestService
    {
        private const string BaseEndpoint = "api/holiday/";
        public HolidayRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<IEnumerable<YearlyHolidayMaster>> GetHolidayListByMonthForTeacher(HolidayRequestParams holidayRequest)
        {
            var request = new RestRequest(BaseEndpoint + $"get", Method.POST, DataFormat.Json).AddJsonBody(holidayRequest);
            var response = await RestClient.ExecuteAsync<IEnumerable<YearlyHolidayMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.AcademicCalenderMaster>> GetAllHoliday(int schoolId, string langId)
        {
            var request = new RestRequest(BaseEndpoint + $"{schoolId}/{langId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.AcademicCalenderMaster>>(request);
            return response.Data;
        }

        public async Task<bool> ManageHoliday(WebModel.AcademicCalenderMaster academicCalender)
        {
            var request = new RestRequest(BaseEndpoint + $"manage", Method.POST, DataFormat.Json).AddJsonBody(academicCalender);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> DeleteHolidayById(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"{id}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }
    }
}