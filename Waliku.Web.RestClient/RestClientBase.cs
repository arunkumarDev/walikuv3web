﻿using System.Collections.Generic;
using RestSharp;
using System.Configuration;
namespace Waliku.Web.RestClient
{
    public class RestClientBase
    {
        public RestClientBase(string token, string languageId)
        {
            this.RestClient.AddDefaultHeaders(new Dictionary<string, string>
            {
                {"Authorization", $"Bearer {token}"},
                {"Lang", languageId}
            });
        }
        private IRestClient _restClient;

        public IRestClient RestClient => _restClient ?? (_restClient = new RestSharp.RestClient(ConfigurationManager.AppSettings["ServiceUrl"]));
    }
}
