﻿
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;

namespace Waliku.Web.RestClient
{
    public interface IAttendanceRestService
    {
        Task<VmAttendanceResponse> ManageStudentAttendance(List<AttendanceMaster> attendances);
        Task<VmAbsenteeReason> GetAbsenceDetails(VmAbsenteeReason absentReason);
        Task<VmResponseData> ManageStudentAbsence(VmAbsenteeReason absence);
        Task<VmAbsenceReasonResponse> EditStudentAbsence(VmAbsenteeReason absence);
        Task<AttendanceMaster> GetAttendanceMasterDetails(int id);
        Task<bool> IsAttendanceExists(int childId, int classRoomId, DateTime attendanceDate);
    }

    public class AttendanceRestService : RestClientBase, IAttendanceRestService
    {
        private const string BaseEndpoint = "api/attendance/";
        public AttendanceRestService(string token, string languageId) : base(token, languageId)
        {
        }


        public async Task<VmAttendanceResponse> ManageStudentAttendance(List<AttendanceMaster> attendances)
        {
            var request = new RestRequest(BaseEndpoint + "manage", Method.POST, DataFormat.Json).AddJsonBody(attendances);
            var response = await RestClient.ExecuteAsync<VmAttendanceResponse>(request);
            return response.Data;
        }
        
        public async Task<VmAbsenteeReason> GetAbsenceDetails(VmAbsenteeReason absentReason)
        {
            var request = new RestRequest(BaseEndpoint + "absencedetails", Method.POST, DataFormat.Json).AddJsonBody(absentReason);
            var response = await RestClient.ExecuteAsync<VmAbsenteeReason>(request);
            return response.Data;
        }

        public async Task<VmResponseData> ManageStudentAbsence(VmAbsenteeReason absence)
        {
            var request = new RestRequest(BaseEndpoint + "manageabsence", Method.POST, DataFormat.Json).AddJsonBody(absence);
            var response = await RestClient.ExecuteAsync<VmResponseData>(request);
            return response.Data;
        }

        public async Task<VmAbsenceReasonResponse> EditStudentAbsence(VmAbsenteeReason absence)
        {
            var request = new RestRequest(BaseEndpoint + "editabsence", Method.POST, DataFormat.Json).AddJsonBody(absence);
            var response = await RestClient.ExecuteAsync<VmAbsenceReasonResponse>(request);
            return response.Data;
        }

        public async Task<AttendanceMaster> GetAttendanceMasterDetails(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"details/{id}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<AttendanceMaster>(request);
            return response.Data;
        }

        public async Task<bool> IsAttendanceExists(int childId, int classRoomId, DateTime attendanceDate)
        {
            var request = new RestRequest(BaseEndpoint + $"exists/{childId}/{classRoomId}/{attendanceDate}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<bool>(request);
            return response.Data;
        }
    }
}