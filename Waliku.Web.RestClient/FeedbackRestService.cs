﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
namespace Waliku.Web.RestClient
{
    public interface IFeedbackRestService
    {
        Task<VmFeedBack> ManageFeedBack(FeedBack feedback);
        Task<IEnumerable<VMFeedBack>> GetFeedBackList(int schoolId, string startDate, string endDate);
    }

    public class FeedbackRestService : RestClientBase, IFeedbackRestService
    {
        private const string BaseEndpoint = "api/feedback/";
        public FeedbackRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<VmFeedBack> ManageFeedBack(FeedBack feedback)
        {
            var request = new RestRequest(BaseEndpoint + $"manage", Method.POST, DataFormat.Json).AddJsonBody(feedback);
            var response = await RestClient.ExecuteAsync<VmFeedBack>(request);
            return response.Data;
        }

        public async Task<IEnumerable<VMFeedBack>> GetFeedBackList(int schoolId, string startDate, string endDate)
        {
            var request = new RestRequest(BaseEndpoint + $"getfeedbacklist/{schoolId}/{startDate}/{endDate}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<VMFeedBack>>(request);
            return response.Data;
        }
    }
}