﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Web.RestClient
{
    public interface ISchoolRestService
    {
        Task<IEnumerable<SchoolMaster>> GetAllSchool();
        Task<IEnumerable<SchoolMaster>> GetSchoolListForUser(int userId, string langId);
        Task<int> ManageSchool(WebModel.SchoolMaster schoolMaster);
        Task<bool> DeleteSchoolById(int schoolId);
    }

    public class SchoolRestService : RestClientBase, ISchoolRestService
    {
        private const string BaseEndpoint = "api/school/";
        public SchoolRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<IEnumerable<SchoolMaster>> GetAllSchool()
        {
            var request = new RestRequest(BaseEndpoint + $"allschool", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<SchoolMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<SchoolMaster>> GetSchoolListForUser(int userId, string langId)
        {
            var request = new RestRequest(BaseEndpoint + $"schoollistforuser/{userId}/{langId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<SchoolMaster>>(request);
            return response.Data;
        }

        public async Task<int> ManageSchool(WebModel.SchoolMaster schoolMaster)
        {
            var request = new RestRequest(BaseEndpoint + $"manage", Method.POST, DataFormat.Json).AddJsonBody(schoolMaster);
            var response = await RestClient.ExecuteAsync<int>(request);
            return response.Data;
        }

        public async Task<bool> DeleteSchoolById(int schoolId)
        {
            var request = new RestRequest(BaseEndpoint + $"{schoolId}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<SchoolMaster>>(request);
            return response.IsSuccessful;
        }
    }
}