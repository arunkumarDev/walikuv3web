﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using WebModel = Waliku.Domain.Models.WebModel;
using WebViewModel = Waliku.Domain.Models.WebModel.ViewModel;
namespace Waliku.Web.RestClient
{
    public interface IFirstAidRestService
    {
        Task<bool> ManageFirstAidAnalytics(FirstAidAnlytics entity);
        Task<IEnumerable<WebViewModel.VmFirstAidAnalytics>> GetFirstAidCounterList();
        Task<IEnumerable<WebModel.FirstAidMaster>> GetAllFirstAidList();
    }

    public class FirstAidRestService : RestClientBase, IFirstAidRestService
    {
        private const string BaseEndpoint = "api/firstaid/";
        public FirstAidRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<bool> ManageFirstAidAnalytics(FirstAidAnlytics entity)
        {
            var request = new RestRequest(BaseEndpoint + $"manage", Method.POST, DataFormat.Json).AddJsonBody(entity);
            var response = await RestClient.ExecuteAsync<bool>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebViewModel.VmFirstAidAnalytics>> GetFirstAidCounterList()
        {
            var request = new RestRequest(BaseEndpoint + $"counterlist", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebViewModel.VmFirstAidAnalytics>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.FirstAidMaster>> GetAllFirstAidList()
        {
            var request = new RestRequest(BaseEndpoint + $"list", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.FirstAidMaster>>(request);
            return response.Data;
        }
    }
}