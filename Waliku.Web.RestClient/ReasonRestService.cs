﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Web.RestClient
{
    public interface IReasonRestService
    {
        Task<IEnumerable<ReasonType>> GetAllReasonType();
        Task<IEnumerable<ReasonMaster>> GetAllReasonByReasonType(int reasonId);
        Task<VmReasonData> GetReasonReasonData();
        Task<IEnumerable<WebModel.ReasonMaster>> GetAllReasonMaster(string langId);
        Task<bool> ManageReasonType(WebModel.ReasonMaster reason);
        Task<bool> ManageReason(WebModel.FirstAidMaster reason);
        Task<bool> DeleteReasonTypeById(int reasonTypeId);
        Task<bool> DeleteReasonById(int reasonId);
    }

    public class ReasonRestService : RestClientBase, IReasonRestService
    {
        private const string BaseEndpoint = "api/reason/";
        public ReasonRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<IEnumerable<ReasonType>> GetAllReasonType()
        {
            var request = new RestRequest(BaseEndpoint + $"getallreasontype", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ReasonType>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<ReasonMaster>> GetAllReasonByReasonType(int reasonId)
        {
            var request = new RestRequest(BaseEndpoint + $"getallreasonbytype/{reasonId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ReasonMaster>>(request);
            return response.Data;
        }

        public async Task<VmReasonData> GetReasonReasonData()
        {
            var request = new RestRequest(BaseEndpoint + $"getdata", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<VmReasonData>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ReasonMaster>> GetAllReasonMaster(string langId)
        {
            var request = new RestRequest(BaseEndpoint + $"getdata/{langId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ReasonMaster>>(request);
            return response.Data;
        }

        public async Task<bool> ManageReasonType(WebModel.ReasonMaster reason)
        {
            var request = new RestRequest(BaseEndpoint + $"managereasontype", Method.POST, DataFormat.Json).AddJsonBody(reason);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> ManageReason(WebModel.FirstAidMaster reason)
        {
            var request = new RestRequest(BaseEndpoint + $"managereason", Method.POST, DataFormat.Json).AddJsonBody(reason);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> DeleteReasonTypeById(int reasonTypeId)
        {
            var request = new RestRequest(BaseEndpoint + $"type/{reasonTypeId}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<bool> DeleteReasonById(int reasonId)
        {
            var request = new RestRequest(BaseEndpoint + $"{reasonId}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }
    }
}