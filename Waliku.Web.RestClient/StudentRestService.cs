﻿using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using Waliku.Domain.Models;
using Waliku.Domain.Models.ViewModel;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Web.RestClient
{
    public interface IStudentRestService
    {
        Task<IEnumerable<ClassMaster>> GetClassBySchool(int id);
        Task<IEnumerable<VmChildren>> GetChildrenByClassId(ChildRequestParams childRequest);
        Task<IEnumerable<VmChildren>> GetAbsenceChildrenByClassId(AbsenceChildRequestParams absenceChildRequest);
        Task<VmCount> ClassCountAbsenceCount(ChildRequestParams childRequest);
        Task<IEnumerable<ChildrenMaster>> GetAllChildrenForMe(int userId, int userType, string langId);
        Task<IEnumerable<ChildrenMaster>> GetAllChildrenForMeByFilter(int userId, int userType, int schoolId, int classRoomId, string langId);
        Task<string> GetChildImageById(int childId);
        Task<bool> DeleteChildById(int childId);
        Task<VmChildren> GetChildDetailsById(int childId);
    }

    public class StudentRestService : RestClientBase, IStudentRestService
    {
        private const string BaseEndpoint = "api/children/";
        public StudentRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<IEnumerable<ClassMaster>> GetClassBySchool(int id)
        {
            var request = new RestRequest(BaseEndpoint + $"getclasscyschool/{id}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ClassMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<VmChildren>> GetChildrenByClassId(ChildRequestParams childRequest)
        {
            var request = new RestRequest(BaseEndpoint + $"getchildrenbyclass", Method.POST, DataFormat.Json).AddJsonBody(childRequest);
            var response = await RestClient.ExecuteAsync<IEnumerable<VmChildren>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<VmChildren>> GetAbsenceChildrenByClassId(AbsenceChildRequestParams absenceChildRequest)
        {
            var request = new RestRequest(BaseEndpoint + $"getallabsencechildrenbyclass", Method.POST, DataFormat.Json).AddJsonBody(absenceChildRequest);
            var response = await RestClient.ExecuteAsync<IEnumerable<VmChildren>>(request);
            return response.Data;
        }

        public async Task<VmCount> ClassCountAbsenceCount(ChildRequestParams childRequest)
        {
            var request = new RestRequest(BaseEndpoint + $"getcount", Method.POST, DataFormat.Json).AddJsonBody(childRequest);
            var response = await RestClient.ExecuteAsync<VmCount>(request);
            return response.Data;
        }


        public async Task<IEnumerable<ChildrenMaster>> GetAllChildrenForMe(int userId, int userType, string langId)
        {
            var request = new RestRequest(BaseEndpoint + $"allchildrenforme/{userId}/{userType}/{langId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ChildrenMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<ChildrenMaster>> GetAllChildrenForMeByFilter(int userId, int userType, int schoolId, int classRoomId, string langId)
        {
            var request = new RestRequest(BaseEndpoint + $"allchildrenforme/{userId}/{userType}/{schoolId}/{classRoomId}/{langId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<ChildrenMaster>>(request);
            return response.Data;
        }

        public async Task<string> GetChildImageById(int childId)
        {
            var request = new RestRequest(BaseEndpoint + $"childimage/{childId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<string>(request);
            return response.Data;
        }

        public async Task<bool> DeleteChildById(int childId)
        {
            var request = new RestRequest(BaseEndpoint + $"{childId}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }


        public async Task<VmChildren> GetChildDetailsById(int childId)
        {
            var request = new RestRequest(BaseEndpoint + $"{childId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<VmChildren>(request);
            return response.Data;
        }
    }
}