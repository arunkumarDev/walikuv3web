﻿
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebModel = Waliku.Domain.Models.WebModel;
namespace Waliku.Web.RestClient
{
    public interface IClassRestService
    {
        Task<IEnumerable<WebModel.ClassMaster>> GetAllClasses();
        Task<IEnumerable<WebModel.TeacherClassMapping>> GetAllTeacherClasses(int userId, bool flag);
        Task<IEnumerable<WebModel.ClassMaster>> GetAllTeacherClasses(int teacherId);
        Task<IEnumerable<WebModel.ClassMaster>> GetAllClassesForMe(int schoolId, int userId);
        Task<bool> DeleteClassById(int classId);
        Task<bool> ManageClass(WebModel.ClassMaster classMaster);
    }

    public class ClassRestService : RestClientBase, IClassRestService
    {
        private const string BaseEndpoint = "api/class/";
        public ClassRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<IEnumerable<WebModel.ClassMaster>> GetAllClasses()
        {
            var request = new RestRequest(BaseEndpoint + "allclasses", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ClassMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.TeacherClassMapping>> GetAllTeacherClasses(int userId, bool flag)
        {
            var request = new RestRequest(BaseEndpoint + $"allteacherclasses/{userId}/{flag}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.TeacherClassMapping>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ClassMaster>> GetAllTeacherClasses(int teacherId)
        {
            var request = new RestRequest(BaseEndpoint + $"allteacherclasses/{teacherId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ClassMaster>>(request);
            return response.Data;
        }

        public async Task<IEnumerable<WebModel.ClassMaster>> GetAllClassesForMe(int schoolId, int userId)
        {
            var request = new RestRequest(BaseEndpoint + $"allclassesforme/{schoolId}/{userId}", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<IEnumerable<WebModel.ClassMaster>>(request);
            return response.Data;
        }

        public async Task<bool> DeleteClassById(int classId)
        {
            var request = new RestRequest(BaseEndpoint + $"{classId}", Method.DELETE, DataFormat.Json);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }


        public async Task<bool> ManageClass(WebModel.ClassMaster classMaster)
        {
            var request = new RestRequest(BaseEndpoint + $"manage", Method.POST, DataFormat.Json).AddJsonBody(classMaster);
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }
    }
}