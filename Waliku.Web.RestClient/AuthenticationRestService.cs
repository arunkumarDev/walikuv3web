﻿using RestSharp;
using System.Threading.Tasks;
using RestSharp.Serialization.Json;
using Waliku.Domain.Models.WebModel.RequestModel;

namespace Waliku.Web.RestClient
{
    public interface IAuthenticationRestService
    {
        Task<LoginResponse> Login(LoginRequest credentials);
        Task<bool> Logout();
        Task<UserInfoViewModel> UserInfo();
    }

    public class AuthenticationRestService : RestClientBase, IAuthenticationRestService
    {
        private const string BaseEndpoint = "/api/account/";
        public AuthenticationRestService(string token, string languageId) : base(token, languageId)
        {
        }

        public async Task<LoginResponse> Login(LoginRequest credentials)
        {
            var serializer = new JsonSerializer();
            var request = new RestRequest(BaseEndpoint + $"login", Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("grant_type", "password");
            request.AddParameter("username", credentials.UserName);
            request.AddParameter("password", credentials.Password);
            var response = await RestClient.ExecuteAsync<LoginResponse>(request);
            return response.Data;
        }

        public async Task<bool> Logout()
        {
            var request = new RestRequest(BaseEndpoint + $"logout", Method.POST, DataFormat.Json)
                .AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var response = await RestClient.ExecuteAsync(request);
            return response.IsSuccessful;
        }

        public async Task<UserInfoViewModel> UserInfo()
        {
            var request = new RestRequest(BaseEndpoint + $"userinfo", Method.GET, DataFormat.Json);
            var response = await RestClient.ExecuteAsync<UserInfoViewModel>(request);
            return response.Data;
        }
    }
}